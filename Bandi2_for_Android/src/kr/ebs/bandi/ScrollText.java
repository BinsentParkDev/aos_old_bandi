package kr.ebs.bandi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import kr.ebs.bandi.data.CProgramData;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class ScrollText {
    final String TAG = "ScrollText";
    
    public static final String XML_FILENAME = "scroll_text.xml";
    
    // xml node tags
    public static final String ROWNUM = "rownum";
    public static final String USERID = "userid";
    public static final String USERNAME = "username";
    public static final String CONTENTS = "contents";
    public static final String REGDATE = "regdate";
    
    
    private Context context;
    private File xmlFile ;
    private final long xmlLifeTime = 60 * 1000; // milli sec.
    private ArrayList<HashMap<String, String>> scrollTextList;
    
    private String xmlString = "";
    
    
    public ScrollText(Context context) 
    {
        this.context = context;
        this.xmlFile = new File(context.getCacheDir(), XML_FILENAME);
    }
    
    public ArrayList<HashMap<String, String>> get() 
    {
        parseXml();
        return scrollTextList;
    }
    
    public String toString() 
    {
        StringBuilder sb = new StringBuilder();
        Iterator<HashMap<String, String>> iter = scrollTextList.iterator();
        
        int i = 0;
        String firstString = "";
        while (iter.hasNext()) 
        {
            HashMap<String, String> hmap = (HashMap<String, String>) iter.next();
            sb.append(hmap.get(CONTENTS) + "\n");
            if (i++ == 0) firstString = hmap.get(CONTENTS);
        }
        sb.append(firstString);
        
        //Log.d(TAG, sb.toString());
        return sb.toString();
    }
    
    private String getURL() 
    {
//        TimeTable tt = new TimeTable(this.context);
        String progcd = CProgramData.getInstance().getProgramID();
        return getURL(progcd);
    }
    
    private String getURL(String progcd) 
    {
        Log.d(TAG, "http://home.ebs.co.kr/bandi/Bandi_recent_board_list.jsp?progid="+ progcd +"&rows=5");
        return "http://home.ebs.co.kr/bandi/Bandi_recent_board_list.jsp?progid="+ progcd +"&rows=5";
//        return CProgramData.getInstance().getScrollTextUrl() + CProgramData.getInstance().getProgramID() + "&rows=5";
        //return "http://to302.phps.kr/test/a.xml";
    }
    
    
    public String getXmlString() throws Exception 
    {
        URL url = new URL( getURL());
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        StringBuilder sb = new StringBuilder();
        
        try 
        {
            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "euc-kr"), 8192);
            String inStr;

            while ((inStr = in.readLine()) != null) 
            {
                inStr = inStr.trim();
                // (중요) 파일이 저장될때는 utf-8 로 저장된다.  
                if (inStr.matches("(?i:<\\?xml.*)")) inStr = inStr.replaceFirst("(?i:EUC-KR)", "UTF-8");
                if (!inStr.matches("\\w")) {
                    sb.append(inStr);
                    Log.v(TAG, inStr.trim());
                }
            }
            
            in.close();
            
        } 
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally 
        {
            urlConnection.disconnect();
        }
        
        return sb.toString();
        
    }
    
    private boolean makeXmlFile()
    {
        boolean result = false;
        
        try 
        {
            boolean isExpired = true;
            
            if (xmlFile.exists()) 
            {
                long now = (new Date()).getTime();
                long mod = (new Date(xmlFile.lastModified())).getTime();
                
                long limitLifeTime = xmlLifeTime;
                long lifeTime = now - mod;
                
                if (lifeTime > limitLifeTime) 
                {
                    xmlFile.delete();
                } 
                else
                {
                    isExpired = false;
                }
            }
            
            if (isExpired)
            {
                FileWriter fw = new FileWriter(xmlFile);
                
                Thread thread = new Thread(new Runnable() 
                {
                    public void run()
                    {
                        try 
                        {
                            xmlString = getXmlString();
                        }
                        catch (Exception e) 
                        {
                            // pass
                        }
                    }
                });
                thread.start();
                
                try 
                {
                    thread.join();
                } 
                catch(InterruptedException e)
                {
                    // pass
                } 
                catch(Exception e)
                {
                    // pass
                }
                
                //fw.write(this.getXmlString());
                fw.write(xmlString);
                
                fw.close();
            }
        
            result = true;
        }
        catch (Exception e)
        {
            result = false;
            Log.e(TAG, "makeXmlFile is failure.");
        }
        
        return result;
    }
    
    
    
    private void parseXml() 
    {
        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        
        
        try
        {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setIgnoringComments(true);
            DocumentBuilder builder = factory.newDocumentBuilder();
            
            this.makeXmlFile();
            Document doc = builder.parse(this.xmlFile);
            
            Element root = doc.getDocumentElement();
            Element currChild = (Element) root.getFirstChild();
            if (currChild == null) throw new Exception();
            
            do 
            {
                HashMap<String, String> map = new HashMap<String, String>() ;
                
                NodeList nodes = currChild.getChildNodes();
                for (int i=0; i<nodes.getLength(); i++) 
                {
                    Element node = (Element) nodes.item(i);
                    map.put(node.getTagName(), node.getTextContent());
                    //Log.d(TAG + "." + node.getTagName() , "" + node.getTextContent());
                }
                list.add(map);
            } while((currChild = (Element) currChild.getNextSibling()) != null);
            
                  
        }
        catch (Exception e)
        {
            Toast.makeText(this.context, e.getMessage(), Toast.LENGTH_SHORT).show();
            Log.e(TAG, "Can't parse Xml file." + e.getMessage());
            e.printStackTrace();
            this.xmlFile.delete();
        }
        
        this.scrollTextList = list;
    }

    
}
