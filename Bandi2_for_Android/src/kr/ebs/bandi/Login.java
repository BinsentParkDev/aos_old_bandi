/**
 * EBS 서버를 이용한 로그인 처리. (게시물 작성 시 필요)
 */
package kr.ebs.bandi;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

import kr.ebs.bandi.data.FavoriteData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

/**
 * @since 2012. 3. 8.
 * @deprecated Use LoginApi class. - 2017-04-26 
 */
public class Login {
    private static final String TAG = "Login";
    private Context context;
    
    static final String ID = "id";
    static final String NAME = "name";
    static final String EMAIL = "email";
    
    static final int SUCCESS = 100;
    /**
     * SSO 서버에서 반환되지 않는 코드 (사용하지 않음) - 2015.06.08 확인
     * @deprecated
     */
    static final int SUCCESS_2 = 101;  
    static final int FAILURE_ID = 102;
    static final int FAILURE_PW = 103;
    static final int FAILURE = 104;
    
    private int resultCode = 0;
    private boolean loginSuccess = false;
    
    
    private String userid = "";
    private String passwd = "";
    
    BandiApplication bandiApp;
    Preferences prefs;
    
    
    private String[] userInfo = new String[4];
    Activity activity;
    
    
    /**
     * @deprecated LoginApi 클래스로 대체
     * @param context
     */
    public Login(Context context) 
    {
        this.context = context;
        activity = (Activity)context;
        bandiApp = (BandiApplication)activity.getApplication();
        prefs = new Preferences(context);
    }
    
    
    private String getLoginUrl() 
    {
    	return Url.LOGIN_URL(this.userid, this.passwd);
    }
    
    /**
     * 로그인 URL 반환
     * @param userId
     * @param passwd
     * @return
     */
    private String getLoginUrl(String userId, String passwd)
    {
    	return Url.LOGIN_URL(userId, passwd);
    }
    
    
    public void login(String userid, String passwd, final LoginListener listener) 
    {
        this.userid = userid;
        this.passwd = passwd;
        
        
        String userId = "";
        String userName = "";
        String userEmail = "";
        
        String errorMsg = "";
        try 
        {
            
            Thread thread = new Thread(new Runnable()
            {
                public void run() 
                {
                    try 
                    {
                        userInfo = requestLogin3();
                    }
                    catch(Exception e) 
                    {
                        // pass
                    }
                }
            });
            thread.start();
            
            try 
            {
                thread.join();
            } 
            catch(Exception e) 
            {
                // pass
            } 
            
            resultCode = Integer.parseInt(userInfo[0]);
            userId = userInfo[1];
            userName = userInfo[2];
            userEmail = userInfo[3]; 
        }
        catch(Exception e)
        {
            resultCode = FAILURE;
            errorMsg = e.getMessage();
        }
        
        
        if (resultCode == SUCCESS )
        {  	
        	BandiApplication app = (BandiApplication) context.getApplicationContext();
            app.setUserId(userId);
            app.setUserName(userName);
            app.setUserEmail(userEmail);
            
            // 즐겨찾기 목록을 가져와 저장한다.
            String jsonStr = Url.getServerText(Url.FAVORITE_LIST_JSON(userId));
            bandiApp.setFavoriteData(jsonStr);
               
            
//            bandiApp.favoriteDataRequest(new AjaxCallback<JSONObject>()
//			{
//				
//				@Override
//				public void callback(String url, JSONObject object, AjaxStatus status) 
//				{
//					try
//					{
//						bandiApp.getFavoriteList().clear();
//						JSONObject result = object.getJSONObject("appUserFbmkLists");
//						JSONArray jArray = result.getJSONArray("appUserFbmkLists");
//		
//						for(int i = 0; i < jArray.length(); i++)
//						{
//							FavoriteData data = new FavoriteData();
//							data.setFbmkSno(jArray.getJSONObject(i).getInt("fbmkSno"));
//							data.setProgramID(jArray.getJSONObject(i).getString("courseId"));
//							data.setTitle(jArray.getJSONObject(i).getString("bandiTitle"));
//							bandiApp.getFavoriteList().add(data);
//						}	
//					}
//					catch(JSONException e)
//					{
//						// pass
//					}
//				}
//			});
            
            loginSuccess = true;
            listener.OnLoginSuccess();
        } 
        else if (resultCode == FAILURE_ID || resultCode == FAILURE_PW) 
        {
        	//Toast.makeText(context, "아이디 또는 비밀번호를 잘못 입력하셨습니다.", Toast.LENGTH_SHORT).show();
        	activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					showAlert("아이디 또는 비밀번호를 잘못 입력하셨습니다.");
				}
        	});
        } 
        else
        {
//        	Toast.makeText(context, "로그인 실패(" + resultCode + ") -" + errorMsg, Toast.LENGTH_SHORT).show();
        } 
    }
    
    
    
    public boolean login(String userid, String passwd) 
    {
        this.userid = userid;
        this.passwd = passwd;
        
        
        String userId = "";
        String userName = "";
        String userEmail = "";
        
        String errorMsg = "";
        try 
        {
            
            Thread thread = new Thread(new Runnable() 
            {
                public void run() 
                {
                    try 
                    {
                        userInfo = requestLogin3();
                    }
                    catch(Exception e) 
                    {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
            
            try 
            {
                thread.join();
            } 
            catch(Exception e)
            {
                // pass
            } 
            
            
            resultCode = Integer.parseInt(userInfo[0]);
            Log.d("TEST", "resultCode : "+ resultCode);
            userId = userInfo[1];
            userName = userInfo[2];
            userEmail = userInfo[3]; 
        } 
        catch(Exception e)
        {
            resultCode = FAILURE;
            errorMsg = e.getMessage();
        }
        
        
        if (resultCode == SUCCESS) 
        {
        	BandiApplication app = (BandiApplication) context.getApplicationContext();
            app.setUserId(userId);
            app.setUserName(userName);
            app.setUserEmail(userEmail);
            
            // 즐겨찾기 목록을 가져와 저장한다.
            String jsonStr = Url.getServerText(Url.FAVORITE_LIST_JSON(userId));
            bandiApp.setFavoriteData(jsonStr);
            
//            bandiApp.favoriteDataRequest(new AjaxCallback<JSONObject>()
//			{
//				
//				@Override
//				public void callback(String url, JSONObject object, AjaxStatus status) 
//				{
//					try
//					{
//						Log.d("TEST", "로그인 후 즐겨찾기 셋팅");
//						bandiApp.getFavoriteList().clear();
//						JSONObject result = object.getJSONObject("appUserFbmkLists");
//						JSONArray jArray = result.getJSONArray("appUserFbmkLists");
//		
//						for(int i = 0; i < jArray.length(); i++)
//						{
//							FavoriteData data = new FavoriteData();
//							data.setFbmkSno(jArray.getJSONObject(i).getInt("fbmkSno"));
//							data.setProgramID(jArray.getJSONObject(i).getString("courseId"));
//							data.setTitle(jArray.getJSONObject(i).getString("bandiTitle"));
//							bandiApp.getFavoriteList().add(data);
//						}
//						
//						loginSuccess = true;
//						
//					}
//					catch(JSONException e)
//					{
//						
//					}
//					
//					
//				}
//				
//			});
            
//            loginSuccess = true;
//            listener.OnLoginSuccess();
        } 
        else if (resultCode == FAILURE_ID) 
        {
            showAlert("로그인 실패 - 등록되지 않은 ID 입니다.");
        } 
        else if (resultCode == FAILURE_PW)
        {
            showAlert("로그인 실패 - 비밀번호를 잘못 입력하셨습니다.");
        } 
        else 
        {
            showAlert("로그인 실패 - 오류 : " + resultCode + "-" + errorMsg);
        } 
        
        return loginSuccess;
    }
    
    
    /** 
     * sso 로그인 처리
     * <pre>
     * - 결과코드 -
     * 100 massage(base64encode(id/name/email)) - 성공
     * 102 error-userid - 실패: 아이디 없을 시
     * 103 error-passwd - 실패: 비밀번호가 올바르지 않을 시
     * 104 error - 실패: 사용되지 않는 유저
     * </pre>
     * @since 2012. 9. 13.
     * @return [결과코드, ID, 이름, Email]
     * @throws Exception
     */
    private String[] requestLogin3() throws Exception
    {
        String[] userInfo = new String[4];
        String charset = "utf-8"; 
        
        BufferedReader in = null;
        if (prefs.getSecureLogin()) 
        {
            URL url = new URL( Url.LOGIN_ACTION_URL );
            HttpsURLConnection httpsConn = (HttpsURLConnection) url.openConnection();
            httpsConn.setDoOutput(true);
//            httpsConn.setDoInput(true);
            httpsConn.setReadTimeout(10000);
            httpsConn.setConnectTimeout(15000);
            httpsConn.setRequestMethod("POST");
            
            Uri.Builder builder = new Uri.Builder()
            	.appendQueryParameter("userid", this.userid)
            	.appendQueryParameter("passwd", this.passwd);
            String query = builder.build().getEncodedQuery();
            
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(httpsConn.getOutputStream(), charset));
            writer.write(query);
            writer.flush();
            writer.close();

            Log.d(TAG, "response code : " + httpsConn.getResponseCode());
            
            try {
            	in = new BufferedReader(new InputStreamReader(httpsConn.getInputStream(), charset), 8192);
            } catch(Exception e) {
            	e.printStackTrace();
            }
            Log.d(TAG, "secure login");
        }
        else 
        {
        	URL url = new URL( getLoginUrl() );
        	HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        	in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), charset), 8192);
        	Log.d(TAG, "non-secure login");
        }

                
        String inStr = "";
        String result_cd = "104";
        String message = "";
        while ((inStr = in.readLine()) != null) 
        {
            inStr = inStr.trim();
            Log.d(TAG, ">>line: "+ inStr );
            if (!"".equals(inStr) && inStr != null) 
            {
                final String regex = "([0-9]{3})\\s*(.*)";
                
                if (inStr.matches(regex)) 
                {
                    Pattern p = Pattern.compile(regex);
                    Matcher m = p.matcher(inStr);
                    if (m.find()) 
                    {
                        result_cd = m.group(1);
                        message = new String(Base64.decode(m.group(2), Base64.DEFAULT), charset);
                    }
                }
            }
            Log.d(TAG, ">>cd: "+ result_cd + "/message: " + message );
            
        }
        
        if (SUCCESS == Integer.parseInt(result_cd) )
        {
            try
            {
                String[] tmp = message.split("\\/"); // 0-2
                userInfo[0] = result_cd;
                userInfo[1] = tmp[0];
                userInfo[2] = tmp[1];
                userInfo[3] = tmp[2];
            } 
            catch(Exception e) 
            {
                userInfo[0] = String.format("%d", FAILURE);
            }
        } 
        else 
        {
        	// 로그인 실패 시 자동로그인 해제
        	if (prefs.getAutoLogin())
        	{
        		prefs.putAutoLogin(false); 
                activity.runOnUiThread(new Runnable(){
    				@Override
    				public void run() {
    					Toast.makeText(context, "자동로그인 실패로 자동로그인 설정이 해제되었습니다.", Toast.LENGTH_SHORT).show();
    				}
                });
        	}
            
        	userInfo[0] = result_cd;
        }
        
        return userInfo;
    }
    
    /**
     * BandiIntro 에서 사용될 자동 로그인 기능용. <br>
     * UI Thread 가 아닌 별도 Thread 로 실행 필요 <br>
     * @return
     */
    public boolean doAutoLogin()
    {
    	boolean loginSuccess = false;
    	
    	if ( !"".equals(prefs.getUserId()) && !"".equals(prefs.getPassword()) )
    	{
    		this.userid = prefs.getUserId();
    		this.passwd = prefs.getPassword();
    		try {
				String[] userInfo = requestLogin3();
				if ("100".equals(userInfo[0]))
				{
					bandiApp.setUserId(userInfo[1]);
					bandiApp.setUserName(userInfo[2]);
					bandiApp.setUserEmail(userInfo[3]);
					loginSuccess = true;
				}
			} catch (Exception e) {
				Log.d(TAG, "doAutoLogin error - " + e.getMessage());
				e.printStackTrace();
			}
    	}
    		
    	return loginSuccess;
    }
    
    /**
     * 로그인이 된 상태인지를 확인 <br>
     * 로그인된 상태가 아니지만, 자동로그인 설정이 true 일 경우 <br>
     * 자동로그인 시도하여 결과를 반환
     * @return
     */
    public boolean isNeededToLogin()
    {
    	boolean needToLogin = true;
    	
    	if ("".equals(bandiApp.getUserId()) )
    	{
    		if (prefs.getAutoLogin())
    		{
    			Thread thread = new Thread() 
    			{
	    			@Override
	    			public void run() 
	    			{
	    				loginSuccess = doAutoLogin();
	    			}
	    		};
	    		
	    		thread.start();
	    		
	    		try {
	    			thread.join();
	    		} catch(Exception e) {
	    			
	    		}
	    		if (loginSuccess) needToLogin = false;
    		}
    	}
    	else 
    	{
    		needToLogin = false;
    	}
    	
    	return needToLogin;
    }
       
    /**
     * 로그인 결과 코드 반환. <br> 
     * Login.SUCCESS <br>
     * Login.FAILURE_ID <br>
     * Login.FAILURE_PW <br>
     * @since 2012. 3. 8.
     * @return
     */
    public int getResultCode()
    {
        return resultCode;
    }
    
    /**
     * 로그인 처리 결과를 반환.
     * @since 2012. 3. 9.
     * @return
     */
    public boolean getLoginResult()
    {
        return loginSuccess;
    }
    
    
    
    /**
     * alert 창을 통해 메세지를 보여준다. <br>
     * (확인)버튼만 있음. <br>
     * @since 2012. 3. 8.
     * @param msg
     */
    private void showAlert(String msg) 
    {
        Dialog.showAlert(context, msg);
    }
   
    
    public interface LoginListener
	{
		public void OnLoginSuccess();
		public void OnLoginFailed(String resultCode);
	}
    
}
