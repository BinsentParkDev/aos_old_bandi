package kr.ebs.bandi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import kr.ebs.bandi.data.FavoriteData;
import kr.ebs.bandi.utils.CScrollViewWithProgress.OnScrollLoadingListener;
import kr.ebs.bandi.utils.CScrollViewWithProgress2;
import kr.ebs.bandi.GoogleAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


public class Favorite implements BaseInterface
{

	static final String TAG = "Favorite";

	private Context context ;
	private Activity activity;
	private LayoutInflater inflater;
	LinearLayout scrollLayout;
	CScrollViewWithProgress2 scroll;
	BandiApplication bandiApp;
	MainActivity main;

	DisplayMetrics dm;
	int dpWidth, dpHeight;

	private FrameLayout.LayoutParams fParams;
	private RelativeLayout.LayoutParams rParams;
	
	
	public Favorite(Context context)
	{

		this.context = context;
		this.activity = (Activity) context;
		main = (MainActivity)activity;
		bandiApp = (BandiApplication)activity.getApplication();

		dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		dpWidth = (int)(dm.widthPixels / dm.density);
		dpHeight = (int)(dm.heightPixels / dm.density);

		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		((BandiApplication) activity.getApplication()).setCurrentMenu(TAG);

		/*
		 * 접근 로그 저장
		 */
		BandiLog.event(context, TAG);
		
	}

	@Override
	public void run()
	{
		BaseActivity.swapTitle(context, R.string.favorite);
		BaseActivity.swapLayout(context, R.layout.layout_favorite);

		setListener();

		//** Google Analytics **
		new GoogleAnalytics(this.activity).sendScreenView(TAG);
	}

	private ArrayList<FavoriteData> favoriteData = new ArrayList<FavoriteData>();
	int pageIndex = 0;
	int i = 0;
	@SuppressLint("NewApi")
	private void setListener()
	{
		ImageView icon = (ImageView)activity.findViewById(R.id.imageView_mainIcon);
		icon.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				activity.findViewById(R.id.layout_menu1).performClick();
			}
		});

		main.repeatListViewInit();

		FrameLayout controlBg = (FrameLayout)activity.findViewById(R.id.control_bar_bg);
		controlBg.setBackgroundColor(Color.parseColor("#3f3833"));
		scroll = (CScrollViewWithProgress2)activity.findViewById(R.id.scrollView_favorite);
		scroll.setListener(new OnScrollLoadingListener()
		{

			@Override
			public void onScrollLastPosition() {
				// TODO Auto-generated method stub

			}

			@Override
			public void onLoadingStart() {
				// TODO Auto-generated method stub

			}

			@Override
			public void onLoadingFinish() {
				// TODO Auto-generated method stub

			}
		});
		scroll.noFullToRefresh();

		scrollLayout = (LinearLayout)activity.findViewById(R.id.layout_favorite);

		favoriteListSetting();

	}

	/**
	 * Thread 에서 반환하는 값 처리용
	 */
	private Handler handler = new Handler()
	{
		@Override
		public void handleMessage(Message msg)
		{
			super.handleMessage(msg);

			switch(msg.what)
			{
			case 1: // 즐겨찾기 목록 가져오기.
				makeFavoriteList((String) msg.obj);
				if (favoriteListRetrieverThread.isAlive()) favoriteListRetrieverThread.interrupt();
				break;
			case 2: // 즐겨찾기 아이템 삭제
				remakeFavoriteList((String) msg.obj);
				if (favoriteItemRemoverThread.isAlive()) favoriteItemRemoverThread.interrupt();
				break;
			}
		}
	};

	/**
	 * 즐겨찾기 목록 가져오기 Thread
	 */
	private FavoriteListRetrieverThread favoriteListRetrieverThread;
	class FavoriteListRetrieverThread extends Thread
	{
		@Override
		public void run()
		{
			String jsonStr = Url.getServerText(Url.FAVORITE_LIST_JSON(bandiApp.getUserId()));
			Message msg = handler.obtainMessage(1, jsonStr);
			handler.sendMessage(msg);
			Log.d(TAG, "favoriteListRetrieverThread ended");
		}
	}



	/**
	 * 즐겨찾기 아이템 삭제(1개) Thread 변수
	 */
	private FavoriteItemRemoverThread favoriteItemRemoverThread;
	class FavoriteItemRemoverThread extends Thread
	{
		private String programId = "";
		private int fbmkSno;

		/**
		 * 즐겨찾기 아이템 삭제용 쓰레드 생성자
		 * @param programId - 프로그램 코드
		 * @param fbmkSno - 즐겨찾기 아이템 고유번호
		 */
		public FavoriteItemRemoverThread(String programId, int fbmkSno)
		{
			this.programId = programId;
			this.fbmkSno = fbmkSno;
		}

		@Override
		public void run()
		{
			String jsonStr = Url.getServerText(Url.REMOVE_FAVORITE_ITEM_JSON(bandiApp.getUserId(), this.programId, this.fbmkSno));
			Message msg = handler.obtainMessage(2, jsonStr);
			handler.sendMessage(msg);
		}
	}


	/**
	 * 서버에서 받아온 데이타(json)로 즐겨찾기 목록 구성
	 */
	private void makeFavoriteList(String jsonStr)
	{
		try
		{
			JSONObject jObject = new JSONObject(jsonStr);

			favoriteData.clear();
			scrollLayout.removeAllViews();

			JSONObject result = jObject.getJSONObject("appUserFbmkLists");
			JSONArray jArray = result.getJSONArray("appUserFbmkLists");

			for(int i = 0; i < jArray.length(); i++)
			{
				FavoriteData data = new FavoriteData();
				data.setFbmkSno(jArray.getJSONObject(i).getInt("fbmkSno"));
				data.setProgramID(jArray.getJSONObject(i).getString("courseId"));
				data.setTitle(jArray.getJSONObject(i).getString("bandiTitle"));
				data.setLinkUrl(jArray.getJSONObject(i).getString("bandiMobUrl"));
				favoriteData.add(data);
			}

			bandiApp.setFavoriteList(favoriteData);

			View[] view = new View[jArray.length()];
			RelativeLayout[] main = new RelativeLayout[jArray.length()];
			final TextView[] contents = new TextView[jArray.length()];
			ImageView[] icon = new ImageView[jArray.length()];
			for(int i = 0; i < jArray.length(); i++)
			{
				view[i] = inflater.inflate(R.layout.layout_favorite_list_item, null);

				fParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, getWidthDP(55));
				main[i] = (RelativeLayout)view[i].findViewById(R.id.layout_favoriteItem_main);
				main[i].setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));
				main[i].setLayoutParams(fParams);

				if(i % 2 == 0)
				{
					main[i].setBackgroundColor(Color.parseColor("#fcf8ee"));
				}
				else
				{
					main[i].setBackgroundColor(Color.parseColor("#ffffff"));
				}


				main[i].setTag(i);
				main[i].setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						int index = Integer.parseInt(v.getTag().toString());


						if(favoriteData.get(index).getLinkUrl() == null
								|| favoriteData.get(index).getLinkUrl().equals("null")
								|| "".equals(favoriteData.get(index).getLinkUrl()))
						{
							Dialog.showAlert(context, "해당 프로그램의 방송 홈페이지가 없습니다.");
						}
						else
						{
							Uri uri = Uri.parse(favoriteData.get(index).getLinkUrl());
							Intent it  = new Intent(Intent.ACTION_VIEW, uri);
							activity.startActivity(it);
						}
					}
				});

				contents[i] = (TextView)view[i].findViewById(R.id.textView_favoriteListContents);
				contents[i].setText(favoriteData.get(i).getTitle());
				contents[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));

				rParams = new RelativeLayout.LayoutParams(getHeightDP(34), getHeightDP(34));
				rParams.addRule(RelativeLayout.CENTER_VERTICAL);
				rParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
				icon[i] = (ImageView)view[i].findViewById(R.id.imageView_favoriteOn);
				icon[i].setLayoutParams(rParams);
				icon[i].setTag(i);
				icon[i].setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						final int index = Integer.parseInt(v.getTag().toString());

						/**
						 * 즐겨찾기 지우기 click listener
						 */
						DialogInterface.OnClickListener removeFavorite = new DialogInterface.OnClickListener()
					    {
					    	public void onClick(DialogInterface dialog, int id)
					    	{
					    		dialog.dismiss();

					    		favoriteItemRemoverThread = new FavoriteItemRemoverThread(
					    				favoriteData.get(index).getProgramID(),
					    				favoriteData.get(index).getFbmkSno());
					    		favoriteItemRemoverThread.start();


					    		// http://s-home.ebs.co.kr/bandi/bandiAppUserFbmkModify?userId=아이디&programId=121212&fbmkSno=2&fileType=json

//					    		String url = "http://home.ebs.co.kr/bandi/bandiAppUserFbmkModify";
//								Map<String, Object> params = new HashMap<String, Object>();
//								params.put("userId", bandiApp.getUserId());
//								params.put("programId", favoriteData.get(index).getProgramID());
//								params.put("fbmkSno", favoriteData.get(index).getFbmkSno());
//								params.put("fileType", "json");
//								AQuery aq = new AQuery(context);
//								aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
//								{
//									@Override
//									public void callback(String url, JSONObject object, AjaxStatus status)
//									{
//										try
//										{
//											JSONObject result = object.getJSONObject("resultXml");
//											String resultCode = result.getString("resultCd");
//											if(resultCode.equals("00"))
//											{
//
//												bandiApp.favoriteDataRequest(new AjaxCallback<JSONObject>()
//												{
//													@Override
//													public void callback(String url, JSONObject object, AjaxStatus status)
//													{
//														try
//														{
//															Log.d("TEST", "즐겨찾기 해제 후 재셋팅");
//															bandiApp.getFavoriteList().clear();
//															JSONObject result = object.getJSONObject("appUserFbmkLists");
//															JSONArray jArray = result.getJSONArray("appUserFbmkLists");
//
//															for(int i = 0; i < jArray.length(); i++)
//															{
//																FavoriteData data = new FavoriteData();
//																data.setFbmkSno(jArray.getJSONObject(i).getInt("fbmkSno"));
//																data.setProgramID(jArray.getJSONObject(i).getString("courseId"));
//																data.setTitle(jArray.getJSONObject(i).getString("bandiTitle"));
//																bandiApp.getFavoriteList().add(data);
//															}
//
//															favoriteListSetting();
//														}
//														catch(JSONException e){}
//
//													};
//
//												});
//
//
//											}
//											else
//											{
//												Toast.makeText(context, result.getString("resultMsg"), Toast.LENGTH_LONG).show();
//											}
//										}
//										catch(JSONException e)
//										{
//
//										}
//									};
//
//								});
					    	}
					    };

					    Dialog.confirm(context, "즐겨찾기를 해제합니다.", removeFavorite, "확인", "취소");

//					    AlertDialog.Builder builder = new AlertDialog.Builder(context);
//					    builder.setMessage("즐겨찾기를 해제합니다.")
//					    .setCancelable(false)
//					    .setPositiveButton("확인", removeFavorite);
//						AlertDialog alert = builder.create();
//						alert.show();

					}
				});

				scrollLayout.addView(view[i]);
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * 즐겨찾기 아이템 삭제 후, 목록 재생성.
	 * @param jsonStr
	 */
	private void remakeFavoriteList(String jsonStr)
	{
		JSONObject jObject;
		try {
			jObject = new JSONObject(jsonStr);
			JSONObject result = jObject.getJSONObject("resultXml");
			String resultCode = result.getString("resultCd");
			if(resultCode.equals("00"))
			{
				favoriteListRetrieverThread = new FavoriteListRetrieverThread();
				favoriteListRetrieverThread.start();
			}
			else
			{
				Toast.makeText(context, "즐겨찾기 항목이 삭제되지 않았습니다.\n잠시 후 다시 시도해 보세요.", Toast.LENGTH_SHORT).show();
			}
		} catch (JSONException e) {
			Log.e(TAG, "remakeFavoriteList error - " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void favoriteListSetting()
	{
		favoriteListRetrieverThread = new FavoriteListRetrieverThread();
		favoriteListRetrieverThread.start();
	}

	/**
	 * favoriteListSetting 재작성으로 이름 변경
	 * @deprecated
	 */
	public void favoriteListSetting_old()
	{
		String url = "http://home.ebs.co.kr/bandi/bandiAppUserFbmkList";
    	Map<String, Object> params = new HashMap<String, Object>();
    	params.put("userId", bandiApp.getUserId());
    	params.put("fileType", "json");
    	AQuery aq = new AQuery(context);
    	aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
		{
    		@Override
    		public void callback(String url, JSONObject object, AjaxStatus status)
    		{
    			try
    			{
    				favoriteData.clear();
    				scrollLayout.removeAllViews();

    				JSONObject result = object.getJSONObject("appUserFbmkLists");
    				JSONArray jArray = result.getJSONArray("appUserFbmkLists");

					for(int i = 0; i < jArray.length(); i++)
					{
						FavoriteData data = new FavoriteData();
						data.setFbmkSno(jArray.getJSONObject(i).getInt("fbmkSno"));
						data.setProgramID(jArray.getJSONObject(i).getString("courseId"));
						data.setTitle(jArray.getJSONObject(i).getString("bandiTitle"));
						data.setLinkUrl(jArray.getJSONObject(i).getString("bandiMobUrl"));
						favoriteData.add(data);
					}
//
					bandiApp.setFavoriteList(favoriteData);

					View[] view = new View[jArray.length()];
					RelativeLayout[] main = new RelativeLayout[jArray.length()];
					final TextView[] contents = new TextView[jArray.length()];
					ImageView[] icon = new ImageView[jArray.length()];
					for(int i = 0; i < jArray.length(); i++)
					{
						view[i] = inflater.inflate(R.layout.layout_favorite_list_item, null);

						fParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, getWidthDP(55));
						main[i] = (RelativeLayout)view[i].findViewById(R.id.layout_favoriteItem_main);
						main[i].setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));
						main[i].setLayoutParams(fParams);

						if(i % 2 == 0)
						{
							main[i].setBackgroundColor(Color.parseColor("#fcf8ee"));
						}
						else
						{
							main[i].setBackgroundColor(Color.parseColor("#ffffff"));
						}


						main[i].setTag(i);
						main[i].setOnClickListener(new OnClickListener()
						{

							@Override
							public void onClick(View v)
							{
								int index = Integer.parseInt(v.getTag().toString());

								if(favoriteData.get(index).getLinkUrl() == null || favoriteData.get(index).getLinkUrl().equals("null") || favoriteData.get(index).getLinkUrl().equals(""))
								{
									AlertDialog.Builder builder = new AlertDialog.Builder(context);
								    builder.setMessage("해당 프로그램의 방송 홈페이지가 없습니다.")
								    .setCancelable(false)
								    .setPositiveButton("확인", new DialogInterface.OnClickListener()
								    {
								    	public void onClick(DialogInterface dialog, int id)
								    	{
								    		// http://s-home.ebs.co.kr/bandi/bandiAppUserFbmkModify?userId=아이디&programId=121212&fbmkSno=2&fileType=json
								    		dialog.dismiss();

								    	}
								    });
									AlertDialog alert = builder.create();
									alert.show();
								}
								else
								{
									Uri uri = Uri.parse(favoriteData.get(index).getLinkUrl());
									Intent it  = new Intent(Intent.ACTION_VIEW, uri);
									activity.startActivity(it);
								}
							}
						});

						contents[i] = (TextView)view[i].findViewById(R.id.textView_favoriteListContents);
						contents[i].setText(favoriteData.get(i).getTitle());
						contents[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));

						rParams = new RelativeLayout.LayoutParams(getHeightDP(34), getHeightDP(34));
						rParams.addRule(RelativeLayout.CENTER_VERTICAL);
						rParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
						icon[i] = (ImageView)view[i].findViewById(R.id.imageView_favoriteOn);
						icon[i].setLayoutParams(rParams);
						icon[i].setTag(i);
						icon[i].setOnClickListener(new OnClickListener()
						{

							@Override
							public void onClick(View v)
							{
								final int index = Integer.parseInt(v.getTag().toString());
								AlertDialog.Builder builder = new AlertDialog.Builder(context);
							    builder.setMessage("즐겨찾기를 해제합니다.")
							    .setCancelable(false)
							    .setPositiveButton("확인", new DialogInterface.OnClickListener()
							    {
							    	public void onClick(DialogInterface dialog, int id)
							    	{
							    		// http://s-home.ebs.co.kr/bandi/bandiAppUserFbmkModify?userId=아이디&programId=121212&fbmkSno=2&fileType=json
							    		dialog.dismiss();
							    		String url = "http://home.ebs.co.kr/bandi/bandiAppUserFbmkModify";
										Map<String, Object> params = new HashMap<String, Object>();
										params.put("userId", bandiApp.getUserId());
										params.put("programId", favoriteData.get(index).getProgramID());
										params.put("fbmkSno", favoriteData.get(index).getFbmkSno());
										params.put("fileType", "json");
										AQuery aq = new AQuery(context);
										aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
										{
											@Override
											public void callback(String url, JSONObject object, AjaxStatus status)
											{
												try
												{
													JSONObject result = object.getJSONObject("resultXml");
													String resultCode = result.getString("resultCd");
													if(resultCode.equals("00"))
													{

														bandiApp.favoriteDataRequest(new AjaxCallback<JSONObject>()
														{
															@Override
															public void callback(String url, JSONObject object, AjaxStatus status)
															{
																try
																{
																	Log.d("TEST", "즐겨찾기 해제 후 재셋팅");
																	bandiApp.getFavoriteList().clear();
																	JSONObject result = object.getJSONObject("appUserFbmkLists");
																	JSONArray jArray = result.getJSONArray("appUserFbmkLists");

																	for(int i = 0; i < jArray.length(); i++)
																	{
																		FavoriteData data = new FavoriteData();
																		data.setFbmkSno(jArray.getJSONObject(i).getInt("fbmkSno"));
																		data.setProgramID(jArray.getJSONObject(i).getString("courseId"));
																		data.setTitle(jArray.getJSONObject(i).getString("bandiTitle"));
																		bandiApp.getFavoriteList().add(data);
																	}

																	favoriteListSetting();
																}
																catch(JSONException e){}

															};

														});


													}
													else
													{
														Toast.makeText(context, result.getString("resultMsg"), Toast.LENGTH_LONG).show();
													}
												}
												catch(JSONException e)
												{

												}
											};

										});
							    	}
							    });
								AlertDialog alert = builder.create();
								alert.show();

							}
						});

						scrollLayout.addView(view[i]);
					}
				}
    			catch (JSONException e)
    			{
					e.printStackTrace();
				}

    		}
		});
	}

	public int getWidthDP(int dpValue)
	{

		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpWidth / 360, context.getResources().getDisplayMetrics());
	}

	public int getHeightDP(int dpValue)
	{
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpHeight / 640, context.getResources().getDisplayMetrics());
	}

	public int getCalSP(int spValue)
	{
		return spValue * dpHeight / 640;

//		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, spValue * dpHeight / 640, getResources().getDisplayMetrics());
	}
}
