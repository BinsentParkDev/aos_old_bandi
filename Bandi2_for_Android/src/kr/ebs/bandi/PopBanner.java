package kr.ebs.bandi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.Toast;

public class PopBanner 
{
	static final String TAG = "PopBanner";
	Context context;
	Document doc = null;

	public PopBanner(Context context) 
	{
		this.context = context;
	}

	
	/**
	 * EBS 내부 전면 배너가 존재하는지 확인
	 * @return 존재하면 true 그 외 false (명확히 없음, xml 파싱에러, 네트워크 에러 등 포함)
	 */
	public boolean existPopBanner() 
	{
		boolean exist = false;
		
		Document doc = getXmlDoc(getXmlFile());
		if (doc != null) 
		{
			NodeList nodes = doc.getElementsByTagName("rescd");
			if (nodes.getLength() == 0) return false;

			String rescd = nodes.item(0).getTextContent();
			if ("0000".equals(rescd)) exist = true;
		}
		
		return exist;
	}
	
	
	/**
	 * Xml File 을 Document 형태로 반환
	 * @param localXml
	 * @return
	 */
	private Document getXmlDoc(File localXml) 
	{
		Document doc = null;
		
		try 
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	        factory.setIgnoringComments(true);
	        DocumentBuilder builder = factory.newDocumentBuilder();
	        doc = builder.parse(localXml);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return doc;
	}
	
	/**
	 * 원격지 서버에서 pop banner 관련 xml 파일을 받아와서 내부 캐쉬에 정해진 시간(10분)동안 저장
	 * @return
	 */
	private File getXmlFile() 
	{
		File localXml = new File(context.getCacheDir().toString() + File.separator + "popBanner.xml");
		boolean downloadXml = true;
		
		if (localXml.exists()) {
			Date freshDate = new Date(localXml.lastModified() + 1000*60*10); // milliseconds (+ 10minutes)
			if (freshDate.after(new Date())) {
				Log.d(TAG, "use local xml file - popBanner.xml");
				downloadXml = false;
			} else {
				localXml.delete();
			}
		}

		if (downloadXml) {
			String xmlText = Url.getServerText(Url.POP_BANNER);
			try {
				FileWriter fw = new FileWriter(localXml);
				fw.write(xmlText);
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return localXml;
	}
	
	
	public boolean canParticipate()
	{
		//Log.d(TAG, "pop in");

		try 
		{
			//Log.d(TAG, "pop in2");
			Document doc = Url.getRemoteDoc("http://m.ebs.co.kr/cs/mobile/bandiEvt/evtYn?deviceId=" + BandiLog.getDeviceId(context));
			NodeList nodes = doc.getElementsByTagName("evtYn");
			String can = nodes.item(0).getTextContent(); // Y:이벤트 참여가능 , N:이벤트 참여 불가능
			Log.d(TAG, "can " + can +"/"+ BandiLog.getDeviceId(context));
			if ("Y".equals(can)) return true;
			else return false;
		} catch(Exception e) {
			Log.e(TAG, "error - "+ e.getLocalizedMessage());
			e.printStackTrace();
			return false;
		}

	}


	/**
	 * 팝 배너 정보를 받아온다.
	 * @return String[2] (이미지 경로, 클릭 링크) or null
	 */
	public String[] getInfo() {
		String[] info = new String[2];
		
		Document doc = getXmlDoc(getXmlFile());
		if (doc != null) info = getXmlValue(doc);
		
		return info;
	}


	private String[] getXmlValue(Document doc) {
		String[] xmlValue = new String[2];

		NodeList nodes = doc.getElementsByTagName("rescd");
		if (nodes.getLength() == 0) return null;

		String rescd = nodes.item(0).getTextContent();
		if (! "0000".equals(rescd)) return null;

		nodes = doc.getElementsByTagName("mobUrl");
		if (nodes.getLength() > 0) xmlValue[0] = nodes.item(0).getTextContent();

		nodes = doc.getElementsByTagName("imgeFile");
		if (nodes.getLength() > 0) xmlValue[1] = nodes.item(0).getTextContent();

		return xmlValue;
	}


	public String getBannerFileName() {
		String[] arr = getInfo()[1].split("/");
		return arr[arr.length-1];
	}


	public Bitmap getBannerImage() {
		try {
			File imgFile = new File(context.getCacheDir(), getBannerFileName());

			if (imgFile.exists()) {
	            long now = (new Date()).getTime();
	            long mod = (new Date(imgFile.lastModified())).getTime() + 1000*60*60; // 1 hour

	            if (now > mod) {
	            	 // 현재 파일 반환
	            	return BitmapFactory.decodeFile(imgFile.getCanonicalPath());
	            }
	        }

	        imgFile = downloadBannerImage();

			return BitmapFactory.decodeFile(imgFile.getCanonicalPath());
		} catch(Exception e) {
			e.printStackTrace();
			Log.e(TAG, "Error - " + e.getLocalizedMessage());
			return null;
		}

	}

	/**
	 * 배너 이미지 파일을 캐쉬 경로에 저장한다.
	 * @return downloaded file path
	 */
	public File downloadBannerImage() {
		Thread thread = new Thread(new Runnable() {
            public void run() {
                try {
                    ProgramData.getRemoteFile(getInfo()[1], context.getCacheDir(), getBannerFileName());
                } catch (Exception e) {
                    // pass
                }
            }
        });
        thread.start();

        try {
            thread.join();
        } catch(InterruptedException e) {
            // pass
        } catch(Exception e) {
            // pass
        }

        return new File(context.getCacheDir(), getBannerFileName());
	}
}
