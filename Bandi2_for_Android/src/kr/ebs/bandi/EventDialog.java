package kr.ebs.bandi;

import com.androidquery.AQuery;

import kr.ebs.bandi.data.EventData;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class EventDialog extends Dialog
{

	ImageView icon;
	TextView title;
	TextView message;
	ImageView eventImg;
	LinearLayout closeBtn;
	EventData data = null;
	Context context;
	Activity activity;
	FrameLayout eventPopupDetail;
	ImageButton notiDetailBtn;
	
	private DisplayMetrics dm;
    private int dpWidth, dpHeight;
	
    private FrameLayout.LayoutParams fParams;
    private LinearLayout.LayoutParams lParams;
    private RelativeLayout.LayoutParams rParams;
    
	public EventDialog(Context context)
	{
		super(context);
		this.context = context; 
		activity = (Activity)context;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.event_dialog);
		getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
		layoutParams.dimAmount = 0.5f;
		getWindow().setAttributes(layoutParams);
		getWindow().setBackgroundDrawable(new ColorDrawable(000000));
		
		dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		dpWidth = (int)(dm.widthPixels / dm.density);
		dpHeight = (int)(dm.heightPixels / dm.density);

	}
	
	public void setData(EventData data)
	{
		this.data = data;
		viewSetting();
	}

	public void viewSetting()
	{
		rParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, getHeightDP(60));
		LinearLayout topLayout = (LinearLayout)findViewById(R.id.eventPopupTop);
		topLayout.setLayoutParams(rParams);
		topLayout.setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));
		
		lParams = new LinearLayout.LayoutParams(getWidthDP(48), getWidthDP(20));
		icon = (ImageView)findViewById(R.id.imageView_eventIcon);
		icon.setLayoutParams(lParams);
		
		lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lParams.setMargins(getWidthDP(5), 0, 0, 0);
		title = (TextView)findViewById(R.id.textView_eventTitle);
		title.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
		title.setLayoutParams(lParams);
		
		rParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, getHeightDP(50));
		rParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		FrameLayout bottomLayout = (FrameLayout)findViewById(R.id.eventPopupBottom);
		bottomLayout.setLayoutParams(rParams);
		
		lParams = new LinearLayout.LayoutParams(getWidthDP(28), getWidthDP(28));
		ImageView closeImg = (ImageView)findViewById(R.id.imageView_eventPopupClose);
		closeImg.setLayoutParams(lParams);
		
		lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lParams.setMargins(getWidthDP(5), 0, 0, 0);
		TextView closeTxt = (TextView)findViewById(R.id.textView_eventPopupClose);
		closeTxt.setLayoutParams(lParams);
		closeTxt.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
		
		FrameLayout eventMain = (FrameLayout)findViewById(R.id.eventPopupScroll);
		eventMain.setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));
		
		eventPopupDetail = (FrameLayout)findViewById(R.id.eventPopupDetail);
		eventPopupDetail.setPadding(getWidthDP(5), getWidthDP(5), getWidthDP(5), getWidthDP(5));
		
		
		fParams = new FrameLayout.LayoutParams(getHeightDP(174), getHeightDP(36), Gravity.CENTER);
		notiDetailBtn = (ImageButton)findViewById(R.id.imageButton_notiDetail);
		notiDetailBtn.setLayoutParams(fParams);
		
		Log.d("TEST", "" + data.getLinkUrl());
		
		notiDetailBtn.setOnClickListener(new android.view.View.OnClickListener() 
		{
			
			@Override
			public void onClick(View v)
			{
				Uri uri = Uri.parse("http://" + data.getLinkUrl());
				Intent it  = new Intent(Intent.ACTION_VIEW, uri);
				activity.startActivity(it);
				dismiss();
			}
		});
		
		message = (TextView)findViewById(R.id.textView_eventMsg);
		message.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15));
		
		eventImg = (ImageView)findViewById(R.id.imageView_eventImg);
		
		title.setText(data.getEventTitle());
		if(data.getEventCode().equals("001"))
		{
			//공지
			eventImg.setVisibility(View.GONE);
			message.setVisibility(View.VISIBLE);
			icon.setBackgroundResource(R.drawable.ico_notice);
			message.setText(data.getEventMessage());
//			message.setText(Html.fromHtml(data.getEventMessage()));
			
			if(data.getLinkUrl().equals(""))
			{
				eventPopupDetail.setVisibility(View.GONE);
			}
			else
			{
				eventPopupDetail.setVisibility(View.VISIBLE);
			}
		}
		else if(data.getEventCode().equals("002"))
		{
			//이벤트
			eventPopupDetail.setVisibility(View.GONE);
			icon.setBackgroundResource(R.drawable.ico_event);
			eventImg.setVisibility(View.VISIBLE);
			message.setVisibility(View.GONE);
			AQuery aq = new AQuery(context);
			aq.id(eventImg).image(data.getImageFullUrl());

			eventImg.setOnClickListener(new android.view.View.OnClickListener()
			{
				@Override
				public void onClick(View v) 
				{
					Uri uri = Uri.parse("http://" + data.getEventUrl());
					Intent it  = new Intent(Intent.ACTION_VIEW, uri);
					activity.startActivity(it);
				}
			});
		}
		
		closeBtn = (LinearLayout)findViewById(R.id.layout_eventClose);
		closeBtn.setOnClickListener(new android.view.View.OnClickListener() 
		{
			
			@Override
			public void onClick(View v)
			{
				dismiss();
			}
		});
		
	}
	
	public int getWidthDP(int dpValue)
	{
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpWidth / 360, context.getResources().getDisplayMetrics());
	}
	
	public int getHeightDP(int dpValue)
	{
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpHeight / 640, context.getResources().getDisplayMetrics());
	}
	
	public int getCalSP(int spValue)
	{
		return spValue * dpHeight / 640;
		
//		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, spValue * dpHeight / 640, getResources().getDisplayMetrics());
	}
}
