package kr.ebs.bandi;

import android.app.Activity;
import android.util.Log;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Google Analytics 적용
 * 
 * @since 2017. 4. 7.
 */
public class GoogleAnalytics {
	static final String TAG = "GoogleAnalytics";
	
	private Tracker mTracker = null;
	static enum CHANNEL {FM, IRADIO, UNKNOWN};
	
	/**
	 * 생성자
	 * @param activity
	 */
	public GoogleAnalytics(Activity activity)
	{
		BandiApplication application = (BandiApplication) activity.getApplication();
        this.mTracker =  application.getDefaultTracker();
	}
	
	/**
	 * 화면 조회 로그를 전송 (채널 정보 포함)
	 * @param screen
	 * @param channel
	 */
	public void sendScreenView(String screen, CHANNEL channel)
	{
		try 
		{
			if (channel == null) 
			{
				mTracker.setScreenName("Screen-" + screen);
			}
			else 
			{
				mTracker.setScreenName("Screen-" + screen + "|" + channel);
			}
	    	mTracker.send(new HitBuilders.ScreenViewBuilder().build());
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Log.e(TAG, "Google Analytics - Send Screen View Error.");
		}
	}
	
	/**
	 * 화면 조회 로그 전송 (채널 무관)
	 * @param screen
	 */
	public void sendScreenView(String screen)
	{
		this.sendScreenView(screen, null);
	}
	
}
