package kr.ebs.bandi;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.SeekBar;

public class StreamingMediaPlayer implements OnTouchListener, OnCompletionListener, OnBufferingUpdateListener, OnClickListener
{
	private FrameLayout buttonPlayPause;
	private SeekBar seekBarProgress;
	
	private MediaPlayer mediaPlayer = null;
	private int mediaFileLengthInMilliseconds; // this value contains the song duration in milliseconds. Look at getDuration() method in MediaPlayer class
	
	private final Handler handler = new Handler();
	private String path = "";
	
	public StreamingMediaPlayer(FrameLayout buttonPlayPause, SeekBar seekBar, String path)
	{
		this.buttonPlayPause = buttonPlayPause;
		this.seekBarProgress = seekBar;
		
		this.path = path;
		
		seekBarProgress.setMax(99); // It means 100% .0-99
		seekBarProgress.setOnTouchListener(this);
		
		mediaPlayer = new MediaPlayer();
		mediaPlayer.setOnBufferingUpdateListener(this);
		mediaPlayer.setOnCompletionListener(this);
		
//		buttonPlayPause.setOnClickListener(this);
	}

	private void primarySeekBarProgressUpdater() 
	{
    	seekBarProgress.setProgress((int)(((float)mediaPlayer.getCurrentPosition()/mediaFileLengthInMilliseconds)*100)); // This math construction give a percentage of "was playing"/"song length"
		if (mediaPlayer.isPlaying()) 
		{
			Runnable notification = new Runnable() 
			{
		        public void run() 
		        {
		        	primarySeekBarProgressUpdater();
				}
		    };
		    handler.postDelayed(notification,1000);
    	}
    }
	
	public MediaPlayer getMediaPlayer()
	{
		return mediaPlayer;
	}
	
	public void startPlayer()
	{
		
		
		try 
		{
			mediaPlayer.setDataSource(path); // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
			mediaPlayer.prepare(); // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer. 
		} catch (Exception e) {
			e.printStackTrace();
		}
		mediaFileLengthInMilliseconds = mediaPlayer.getDuration();
		mediaPlayer.start();
		buttonPlayPause.setSelected(true);
		
		primarySeekBarProgressUpdater();
	}
	
	@Override
	public void onClick(View v) {
		 /** ImageButton onClick event handler. Method which start/pause mediaplayer playing */
//		v.setSelected(!v.isSelected());
		try {
			mediaPlayer.setDataSource(path); // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
			mediaPlayer.prepare(); // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer. 
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		mediaFileLengthInMilliseconds = mediaPlayer.getDuration(); // gets the song length in milliseconds from URL
		
		if(!mediaPlayer.isPlaying()){
			mediaPlayer.start();
			buttonPlayPause.setSelected(true);
		}else {
			mediaPlayer.pause();
			buttonPlayPause.setSelected(false);
		}
		
		primarySeekBarProgressUpdater();
	}
	
	public void stopPlayer()
	{
		seekBarProgress.setProgress(0);
		mediaPlayer.seekTo(0);
		mediaPlayer.stop();
//		mediaPlayer.reset();
		
//		mediaPlayer.prepareAsync();
		buttonPlayPause.setSelected(false);
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		// TODO Auto-generated method stub
		if(mediaPlayer.isPlaying()){
	    	SeekBar sb = (SeekBar)v;
			int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * sb.getProgress();
			mediaPlayer.seekTo(playPositionInMillisecconds);
		}
		return false;
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		// TODO Auto-generated method stub
		buttonPlayPause.setSelected(false);
	}

	@Override
	public void onBufferingUpdate(MediaPlayer mp, int percent) {
		// TODO Auto-generated method stub
		
		seekBarProgress.setSecondaryProgress(percent);
		
		
	}
	
}
