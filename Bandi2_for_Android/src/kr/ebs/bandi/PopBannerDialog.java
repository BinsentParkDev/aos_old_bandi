package kr.ebs.bandi;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint("SetJavaScriptEnabled")
public class PopBannerDialog extends Dialog 
{
	
	

	static final String TAG = "PopBannerActivity";
	
	String imgUrl, link;
	
	FrameLayout popBanner ;
	WebView popWebview;
	
	Context context;
	
	public PopBannerDialog(Context context)
	{
		super(context);
		this.context = context;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_pop_banner);
		getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
		layoutParams.dimAmount = 0.5f;
		getWindow().setAttributes(layoutParams);
		getWindow().setBackgroundDrawable(new ColorDrawable(000000));
//		Intent intent = getIntent();
//		this.imgUrl = intent.getStringExtra("imgUrl");
//		this.link = intent.getStringExtra("link");
//		
//		Log.d(TAG, "banner link : " + link);
		
		popBanner = (FrameLayout)findViewById(R.id.pop_banner);
		popWebview = (WebView)findViewById(R.id.pop_webview);
		
		loadImage();
		setListener();
	}
	
	public void setData(String imgUrl, String linkUrl)
	{
		this.imgUrl = imgUrl;
		this.link = linkUrl;
		// link 내의 정의된 문자열 치환
		link = link.replace("__DEVICE_ID__", BandiLog.getDeviceId(context));
		
	}
	
	/*@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
       
	}*/
	
	private void loadImage() {
		try {
			PopBanner popBanner = new PopBanner(context);
			ImageView iv = (ImageView)findViewById(R.id.banner_image);
			iv.setImageBitmap(popBanner.getBannerImage());
		} catch(Exception e) {
			e.printStackTrace();
			Log.e(TAG, "Error - " + e.getLocalizedMessage());
			dismiss();
		}
	}
	
	private void setListener() {
		// 배너 이미지 클릭했을 때.
		ImageView iv = (ImageView)findViewById(R.id.banner_image);
		iv.setOnClickListener(new android.view.View.OnClickListener(){

			@Override
			public void onClick(View v) {
				popWebview.setVisibility(View.VISIBLE);
				popBanner.setVisibility(View.INVISIBLE);		
				popWebview.loadUrl(link);
				popWebview.getSettings().setJavaScriptEnabled(true);
				popWebview.setWebChromeClient(new WebChromeClient());
				popWebview.setWebViewClient(new WebViewClient() {
					@Override
					public boolean shouldOverrideUrlLoading (WebView view, String url) {
						if ("bandi://banner.close".equals(url)) {
//							PopBannerDialog.this.finish();
							dismiss();
						} else {
							view.loadUrl(url);
						}
						return true;
					}
				});
			}
			
		});
		
		// 하루동안 열지 않기 클릭했을 때.
		TextView tv = (TextView)findViewById(R.id.close_oneday);
		tv.setOnClickListener(new android.view.View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.d(TAG, "close oneday.");
				Preferences prefs = new Preferences(context);
				prefs.putBannerClosingTime(24);
				dismiss();
			}		
		});
	}
}
