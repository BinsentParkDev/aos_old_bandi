package kr.ebs.bandi;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;

import kr.ebs.bandi.GoogleAnalytics;
import kr.ebs.bandi.Login.LoginListener;
import kr.ebs.bandi.data.CProgramData;
import kr.ebs.bandi.data.EventData;
import kr.ebs.bandi.sns.LoginManager;
import kr.ebs.bandi.sns.LoginManager.LoginEventCallback;
import kr.ebs.bandi.sns.UserData;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class LoginFormDialog extends android.app.Dialog
{
	static final String TAG = "LoginForm";
	private Context context;
	private Activity activity;
	MainActivity main;
	BandiApplication bandiApp;
	
	private OnLoginSuccessCb loginSuccessCb;
	private OnLoginFailureCb loginFailureCb;
	
	ImageButton facebookLogin, twitterLogin;
	
	private FrameLayout ckSaveId;
    private FrameLayout ckAutoLogin;
    private Preferences prefs;
    
    private EditText userId, password;
    
    private String findIdAndPwUrl = "https://sso.ebs.co.kr/idp/user/find/id.jsp?returnUrl=http://www.ebs.co.kr";
    private String registerUrl = "https://sso.ebs.co.kr/idp/join/intro.jsp?returnUrl=http://ebs.co.kr";
	
    boolean isFavorite;
    private DisplayMetrics dm;
    private int dpWidth, dpHeight;
    
    private FrameLayout.LayoutParams fParams;
    private LinearLayout.LayoutParams lParams;
    private RelativeLayout.LayoutParams rParams;
    
    private volatile static LoginFormDialog single;
    public static LoginFormDialog getInstance(Context context)
    {
        if (single == null) 
        {
            synchronized(LoginFormDialog.class) 
            {
                if (single == null) 
                {
                    single = new LoginFormDialog(context);
                }
            }
        }
        return single;
    }
	private LoginFormDialog(Context context)
	{
		super(context);
		this.context = context; 
		activity = (Activity)context;
		main = (MainActivity)activity;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.login_dialog);
		bandiApp = (BandiApplication)activity.getApplication();
		
		dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		dpWidth = (int)(dm.widthPixels / dm.density);
		dpHeight = (int)(dm.heightPixels / dm.density);
		
		setListener();
	}
	
	public void showDialog(boolean isFavorite)
	{
		if (! prefs.getSaveId()) userId.setText("");
		password.setText("");
		if(isFavorite) // 즐겨찾기 메뉴인가? (즐겨찾기 메뉴에는 sns 로그인이 안보여야 함)
		{
			facebookLogin.setVisibility(View.INVISIBLE);
			twitterLogin.setVisibility(View.INVISIBLE);
		}
		else
		{
			facebookLogin.setVisibility(View.VISIBLE);
			twitterLogin.setVisibility(View.VISIBLE);
		}
		show();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
		layoutParams.softInputMode = WindowManager.LayoutParams.SOFT_INPUT_STATE_UNSPECIFIED;
		layoutParams.dimAmount = 0.5f;
		getWindow().setAttributes(layoutParams);
		getWindow().setBackgroundDrawable(new ColorDrawable(000000));
	}
	
	@Override
	protected void onStart()
	{
		//** Google Analytics **
		new GoogleAnalytics(this.activity).sendScreenView(TAG);
		Log.d(TAG, "LoginFormDialog.onStart()");
	}
	
	private void setListener() 
	{
		
		LinearLayout topArea = (LinearLayout)findViewById(R.id.layout_loginTopArea);
		topArea.setPadding(0, 0, 0, getHeightDP(20));
		
		lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		lParams.setMargins(0, getHeightDP(10), 0, 0);
		FrameLayout iconLayout = (FrameLayout)findViewById(R.id.layout_loginIcon);
		iconLayout.setLayoutParams(lParams);
		
		fParams = new FrameLayout.LayoutParams(getHeightDP(190), getHeightDP(56), Gravity.CENTER_HORIZONTAL);
		ImageView icon = (ImageView)findViewById(R.id.imageView_loginIcon);
		icon.setLayoutParams(fParams);
		
		lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		lParams.setMargins(0, getHeightDP(20), 0, 0);
		FrameLayout loginGroup = (FrameLayout)findViewById(R.id.layout_loginGroup);
		loginGroup.setLayoutParams(lParams);
		
		lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lParams.setMargins(getWidthDP(10), 0, 0, 0);
		LinearLayout inputGroup = (LinearLayout)findViewById(R.id.layout_loginInputGroup);
		inputGroup.setLayoutParams(lParams);
		
		FrameLayout saveGroup = (FrameLayout)findViewById(R.id.layout_loginSaveGroup);
		saveGroup.setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));
		
		lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lParams.setMargins(getWidthDP(25), 0, 0, 0);
		LinearLayout autoLogin = (LinearLayout)findViewById(R.id.layout_loginAutoLogin);
		autoLogin.setLayoutParams(lParams);
		
		lParams = new LinearLayout.LayoutParams(getHeightDP(28), getHeightDP(28));
		ckSaveId = (FrameLayout) findViewById(R.id.btn_save_id);
		ckSaveId.setLayoutParams(lParams);
		
		lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lParams.setMargins(getWidthDP(5), 0, 0, 0);
		TextView saveIdTxt = (TextView)findViewById(R.id.textView_saveId);
		saveIdTxt.setLayoutParams(lParams);
		saveIdTxt.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
		
		lParams = new LinearLayout.LayoutParams(getHeightDP(28), getHeightDP(28));
        ckAutoLogin = (FrameLayout) findViewById(R.id.btn_auto_login);
        ckAutoLogin.setLayoutParams(lParams);
        
        lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lParams.setMargins(getWidthDP(5), 0, 0, 0);
		TextView autoLoginTxt = (TextView)findViewById(R.id.textView_autoLogin);
		autoLoginTxt.setLayoutParams(lParams);
		autoLoginTxt.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
        
        prefs = new Preferences(context);
        
        lParams = new LinearLayout.LayoutParams(getWidthDP(265), getHeightDP(31));
        userId = (EditText)findViewById(R.id.userid);
        userId.setPadding(getWidthDP(10), 0, getWidthDP(10), 0);
        userId.setLayoutParams(lParams);
        userId.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(18));
        
        lParams = new LinearLayout.LayoutParams(getWidthDP(265), getHeightDP(31));
        lParams.setMargins(0, getHeightDP(5), 0, 0);
        password = (EditText)findViewById(R.id.passwd);
        password.setPadding(getWidthDP(10), 0, getWidthDP(10), 0);
        password.setLayoutParams(lParams);
        password.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(18));
        
        TextView findId = (TextView)findViewById(R.id.textView_findId);
        TextView findPw = (TextView)findViewById(R.id.textView_findPw);
        TextView register = (TextView)findViewById(R.id.textView_register);
        
        findId.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
        findPw.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
        register.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
        
        lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        lParams.setMargins(0, getHeightDP(15), 0, 0);
        FrameLayout textGroup = (FrameLayout)findViewById(R.id.layout_loginTextGroup);
        textGroup.setLayoutParams(lParams);
        
        lParams = new LinearLayout.LayoutParams(getWidthDP(265), getHeightDP(36));
        lParams.setMargins(0, getHeightDP(20), 0, 0);
        facebookLogin = (ImageButton)findViewById(R.id.imageButton_facebookLogin);
        facebookLogin.setLayoutParams(lParams);
        
        lParams = new LinearLayout.LayoutParams(getWidthDP(265), getHeightDP(36));
        lParams.setMargins(0, getHeightDP(10), 0, 0);
        twitterLogin = (ImageButton)findViewById(R.id.imageButton_twitterLogin);
        twitterLogin.setLayoutParams(lParams);
        /*
		 * 로그인 기능
		 */
        lParams = new LinearLayout.LayoutParams(getHeightDP(68), getHeightDP(68));
        lParams.setMargins(getWidthDP(5), 0, getWidthDP(10), 0);
		ImageButton loginBtn = (ImageButton) findViewById(R.id.login_btn);
		loginBtn.setLayoutParams(lParams);
		
		loginBtn.setOnClickListener(new android.view.View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				if ("".equals(getUserId())) 
				{
					Dialog.showAlert(context, "아이디를 입력해주세요.");
                    return;
				}
				
				if ("".equals(getPassword())) 
				{
					Dialog.showAlert(context, "비밀번호를 입력해주세요.");
                    return;
				}
				
//				Login login = new Login(context);
//	        	login.login(getUserId(), getPassword(), new LoginListener()
//	        	{
//					
//					@Override
//					public void OnLoginSuccess() 
//					{
//						savePreferences();
//	                    loginSuccessCb.onLoginSuccess();
//	                    dismiss();
//					}
//					
//					@Override
//					public void OnLoginFailed(String resultCode) 
//					{
//						// TODO Auto-generated method stub
//						Dialog.showAlert(context, "아이디 또는 비밀번호를 잘못 입력하셨습니다.");
//					}
//				});
				LoginApi login = new LoginApi(context);
				login.login(getUserId(), getPassword(), new LoginApi.LoginListener() {
					
					@Override
					public void OnLoginSuccess() {
						savePreferences();
	                    loginSuccessCb.onLoginSuccess();
	                    dismiss();
					}
					
					@Override
					public void OnLoginFailure() {
						Log.e(TAG, "LoginApi.LoginListener.OnLoginFailure is called.");
						Toast.makeText(context, "로그인 실패하였습니다.", Toast.LENGTH_SHORT).show();
					}
				});
			}
		});
		
		findViewById(R.id.layout_findIdBtn).setOnClickListener(btnClickListener);
		findViewById(R.id.layout_findPwBtn).setOnClickListener(btnClickListener);
		findViewById(R.id.layout_registerBtn).setOnClickListener(btnClickListener);
		findViewById(R.id.imageButton_facebookLogin).setOnClickListener(btnClickListener);
	    findViewById(R.id.imageButton_twitterLogin).setOnClickListener(btnClickListener);
		/*
		 * 아이디 저장 (리스너 및 기존 설정 셋팅)
		 */
		ckSaveId.setOnClickListener(new android.view.View.OnClickListener() 
		{
			
			@Override
			public void onClick(View v) 
			{
				ckSaveId.setSelected(!ckSaveId.isSelected());
			}
		});
        // 아이디 저장 설정일 때, 기존 아이디 불러오기
		ckSaveId.setSelected(prefs.getSaveId());
        if (prefs.getSaveId())
        {
            //EditText userid = (EditText) findViewById(R.id.userid);
            userId.setText( prefs.getUserId() );
            Log.d(TAG, "get text : " + userId.getText().toString());
            Log.d(TAG, "get text : " + getUserId());
            Log.d(TAG, "userid:" + prefs.getUserId());
        }
        
        /*
		 * 자동 로그인 (리스너 및 기존 설정 셋팅)
		 */
        ckAutoLogin.setOnClickListener(new android.view.View.OnClickListener() 
        {
            public void onClick(View v) 
            {
                ckAutoLogin.setSelected(!ckAutoLogin.isSelected());
            }    
        });
        ckAutoLogin.setSelected(prefs.getAutoLogin());
        
        rParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, getHeightDP(48));
        rParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        FrameLayout loginClose = (FrameLayout)findViewById(R.id.loginCloseLayout);
        loginClose.setLayoutParams(rParams);
        
        /*
         * 로그인 창 닫기
         */
        lParams = new LinearLayout.LayoutParams(getHeightDP(28), getHeightDP(28));
		ImageButton closeBtn = (ImageButton) findViewById(R.id.close_btn);
		closeBtn.setLayoutParams(lParams);
		closeBtn.setOnClickListener(new android.view.View.OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				loginFailureCb.onLoginFailure();
				dismiss();
			}
		});
		
		lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lParams.setMargins(getWidthDP(10), 0, 0, 0);
		TextView closeText = (TextView)findViewById(R.id.textView_loginClose);
		closeText.setLayoutParams(lParams);
		closeText.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
		
	} // end setListener()
	
	private void makeMeRequest(final Session session) 
	{
	    // Make an API call to get user data and define a 
	    // new callback to handle the response.
	    Request request = Request.newMeRequest(session, new Request.GraphUserCallback() 
	    {
	        @Override
	        public void onCompleted(GraphUser user, Response response) 
	        {
	            // If the response is successful
	            if (session == Session.getActiveSession()) 
	            {
	                if (user != null) 
	                {
	                	bandiApp.setFacebookId(user.getId());
	                	bandiApp.setFacebookName(user.getName());
	                	loginSuccessCb.onLoginSuccess();
	                	dismiss();
	                }
	            }
	            if (response.getError() != null)
	            {
	                // Handle errors, will do so later.
	            }
	        }
	    });
	    request.executeAsync();
	} 
	
	@Override
	public void onBackPressed() 
	{
		// TODO Auto-generated method stub
		super.onBackPressed();
		loginFailureCb.onLoginFailure();
		dismiss();
	}

	android.view.View.OnClickListener btnClickListener = new android.view.View.OnClickListener()
	{
		
		@Override
		public void onClick(View v)
		{
			if(v.getId() == R.id.layout_findIdBtn || v.getId() == R.id.layout_findPwBtn)
			{
				Uri uri = Uri.parse(findIdAndPwUrl);
				Intent it  = new Intent(Intent.ACTION_VIEW,uri);
				activity.startActivity(it);
			}
			else if(v.getId() == R.id.layout_registerBtn)
			{
				Uri uri = Uri.parse(registerUrl);
				Intent it  = new Intent(Intent.ACTION_VIEW,uri);
				activity.startActivity(it);
			}
			else if(v.getId() == R.id.imageButton_facebookLogin)
			{
				Session.openActiveSession(activity, true, new Session.StatusCallback() 
				{

					// callback when session changes state
					@Override
					public void call(Session session, SessionState state, Exception exception) 
					{
						if(session.isOpened())
						{
							bandiApp.setIsFacebookLogin(true);
							bandiApp.setTwitterAccessToken("");
							bandiApp.setTwitterAccessTokenSecret("");
							bandiApp.setTwitterName("");
							makeMeRequest(session);
						}
					}
				});
			}
			else if(v.getId() == R.id.imageButton_twitterLogin)
			{
				main.loginToTwitter(true);
			}
		}
	};
	
	
	
	
	/**
     * 사용자 설정 정보 저장.
     * @since 2012. 3. 9.
     */
    private void savePreferences() 
    {
        prefs.putSaveId( this.ckSaveId.isSelected() );
        prefs.putAutoLogin( this.ckAutoLogin.isSelected() );
        
        if (this.ckAutoLogin.isSelected()) 
        {
            prefs.putUserId(getUserId());
            prefs.putPassword(getPassword());
        }
        else
        {
            prefs.putUserId("");
            prefs.putPassword("");
            
            if (this.ckSaveId.isSelected()) prefs.putUserId(getUserId());
        }    
    } // end savePreferences()
	
	private String getUserId() 
	{
        return ( (EditText) findViewById(R.id.userid) ).getText().toString();
    }

    private String getPassword() 
    {
        return ( (EditText) findViewById(R.id.passwd) ).getText().toString();
    } 
	
	
	interface OnLoginSuccessCb 
	{
		void onLoginSuccess();
	}
	
	interface OnLoginFailureCb 
	{
		void onLoginFailure();
	}
	
	public void setOnLoginSuccessCb(OnLoginSuccessCb callback)
	{
		loginSuccessCb = callback;
	}
	
	public void setOnLoginFailureCb(OnLoginFailureCb callback) 
	{
		loginFailureCb = callback;
	}

	public int getWidthDP(int dpValue)
	{
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpWidth / 360, context.getResources().getDisplayMetrics());
	}
	
	public int getHeightDP(int dpValue)
	{
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpHeight / 640, context.getResources().getDisplayMetrics());
	}
	
	public int getCalSP(int spValue)
	{
		return spValue * dpHeight / 640;
		
//		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, spValue * dpHeight / 640, getResources().getDisplayMetrics());
	}
}
