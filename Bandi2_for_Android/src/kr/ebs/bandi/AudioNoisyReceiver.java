package kr.ebs.bandi;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.os.IBinder;
import android.util.Log;

public class AudioNoisyReceiver extends BroadcastReceiver {
	static final String TAG = "AudioNoisyReciever";
	private Context context;
	
	public AudioNoisyReceiver() 
	{
	}

	/** audio streaming service */
    private AudioService audioService;
    
    private ServiceConnection serviceConnection = new ServiceConnection() 
    {
        
    	@Override
        public void onServiceConnected(ComponentName name, IBinder service) 
    	{
            AudioService.ServiceBinder binder = (AudioService.ServiceBinder) service;
            audioService = binder.getService();
            Log.d("TEST", "audionoisy stop player");
            audioService.stopPlayer();
            context.unbindService(serviceConnection);
        }

		@Override
		public void onServiceDisconnected(ComponentName name)
		{
	
		}
    };
    
	@Override
	public void onReceive(Context context, Intent intent) 
	{
		this.context = context;
		
		if (intent.getAction().equals(AudioManager.ACTION_AUDIO_BECOMING_NOISY))
		{
            Intent i = new Intent(context, AudioService.class);
            Log.d(TAG, "becoming noisy");
            context.bindService(i, serviceConnection, Context.BIND_AUTO_CREATE);
        }
		
		/** MainActivity.java 에 헤드셋 상태 관련 filter 있음.
		else if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
        	
        	Bundle bundle = intent.getExtras();
        	Log.d(TAG, "has headset state : " + bundle.containsKey("state"));
        }
        **/
	}
}
