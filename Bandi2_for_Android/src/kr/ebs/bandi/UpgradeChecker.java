package kr.ebs.bandi;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;


public class UpgradeChecker {
	static final String TAG = "UpgradeChecker";
	private int installedVersionCode;
	
	public UpgradeChecker(Context context) {
		PackageInfo pi;
		try {
			pi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			installedVersionCode = pi.versionCode;
			Log.d(TAG, "installedVersionCode : " + installedVersionCode);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public boolean needToUpgrade() {
		boolean doUpgrade = false;
		try {
	        Document doc = Url.getRemoteDoc(Url.UPGRADE_CHECK); 
	        
	        String appVer = doc.getElementsByTagName("appVer").item(0).getTextContent().trim();
	        Log.d(TAG, "appVer:" + appVer);
	        
	        if (Float.parseFloat(appVer) > installedVersionCode) doUpgrade = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
        
		return doUpgrade;
		
	}
}
