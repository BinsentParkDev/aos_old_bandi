package kr.ebs.bandi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

public class DateUtil {
	 /**
     * GMT+9 시간(한국시간)으로 보정된 시간 반환.
     * @since 2012. 3. 3.
     * @return
     */
    public static Calendar getKoreanCalendar() {
//    	Calendar cal = new GregorianCalendar(Locale.KOREAN);
//      return cal;
        
        TimeZone tz = TimeZone.getTimeZone("Asia/Seoul");
        return GregorianCalendar.getInstance(tz, Locale.KOREAN);
    }
    
    /**
     * String 을 Date 형식으로 반환
     * @param dateStr : 날짜 문자열
     * @param format : 날짜 포맷
     * @return
     */
    public static Date parseStringToDate(String dateStr, String format) {
    	Date date = null;
    	SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.KOREAN);
    	try {
			date = sdf.parse(dateStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return date;
    }
    
    /**
     * Calendar 객체를 받아서 8자리로 구성된 yyyyMMdd 형식의 날짜 반환.<br>
     * 예) 20150605
     * @since 2012. 3. 5.
     * @param cal
     * @return
     */
    public static String get8DigitsDateString(Calendar cal) 
    {
        return getDateString(cal, "yyyyMMdd");
    }
    
    
    /**
     * 편성표 페이지에서 사용할 상단 날짜 String 반환
     * @param cal
     * @return
     */
    public static String getDateString(Calendar cal) 
    {
    	return getDateString(cal, "yyyy.MM.dd (E)");
    }
    
    /**
     * 날짜 포맷(SimpleDateFormat)을 받아서 그 형태로 해당 날짜에 대한 String 반환 (로케일 : Locale.KOREA)
     * @param cal
     * @param format
     * @return
     */
    public static String getDateString(Calendar cal, String format) {
    	String dt = "";
    	try {
    		dt = new SimpleDateFormat(format, Locale.KOREA).format(cal.getTime());
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    	return dt;
    }
    
    /**
     * 이주의 시작 날짜를 반환 (일요일~토요일 기준)
     * @since 2012. 3. 5.
     * @param cal
     * @return
     */
    public static Calendar getFirstDateOfWeek(Calendar cal) {
        Calendar tmpCal = (Calendar) cal.clone(); // 이걸 적용안하면 실제 cal 로 넘어온게 변환되어 반환된다. final 도 소용없다.. 이상해.. 버그인가..
        tmpCal.add(Calendar.DATE, (Calendar.SUNDAY - cal.get(Calendar.DAY_OF_WEEK)) );
        return tmpCal;
    }
    
    /**
     * 이주의 마지막 날짜를 반환 (일요일~토요일 기준)
     * @since 2012. 3. 5.
     * @param cal
     * @return
     */
    public static Calendar getLastDateOfWeek(Calendar cal) {
        Calendar tmpCal = (Calendar) cal.clone();
        tmpCal.add(Calendar.DATE, (Calendar.SATURDAY - cal.get(Calendar.DAY_OF_WEEK)) );
        return tmpCal;
    }
    
    /**
     * 어제 날짜 반환 
     * @since 2012. 3. 5.
     * @param cal
     * @return
     */
    public static Calendar getYesterday(Calendar cal) {
        Calendar tmpCal = (Calendar) cal.clone();
        tmpCal.add(Calendar.DATE, -1);
        return tmpCal;
    }
    
    /**
     * 내일 날짜 반환.
     * @since 2012. 3. 5.
     * @param cal
     * @return
     */
    public static Calendar getTomorrow(Calendar cal) {
        Calendar tmpCal = (Calendar) cal.clone();
        tmpCal.add(Calendar.DATE, 1);
        return tmpCal;
    }
    

    /**
     * 라디오 방송 종료시간을 고려해서 2시간 늦은 날짜를 반영. <br />
     * (2월 2일 01시 29분 일 경우 -> 2월 1일 날짜로 반환)
     * @return 8자리 숫자로 된 날짜 ex)20150601
     */
    public static String getAdjustedDateString() 
    {
        // 오늘 날짜를 기준으로..
        Calendar cal = DateUtil.getKoreanCalendar();
        cal.add(Calendar.HOUR_OF_DAY, -2); // 라디오 방송 종료 시간 (익일 새벽 2시)

        return DateUtil.get8DigitsDateString(cal);
    }
    
    /**
	 * 13:14 형태의 시간 문자열을 불러와서 시(int), 분(int) 형태의 배열로 반환한다.
	 * @param timeString
	 * @return
	 */
    public static int[] parseTimeString(String timeString)
	{
		int[] times = {0,0};
		if ("".equals(timeString)) return times;
		
		try
		{
			String[] ts = timeString.split(":");
			times[0] = Integer.parseInt(ts[0]);
			times[1] = Integer.parseInt(ts[1]);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return times;
	}
}
