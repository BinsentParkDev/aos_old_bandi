package kr.ebs.bandi;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import kr.ebs.bandi.data.CProgramData;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.app.Activity;
import android.content.Context;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.util.Log;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

public class BandiLog 
{
	static final String TAG = "BandiLog";
	
	static String getDeviceId(Context context) 
	{
		try 
		{
			return Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
		}
		catch(Exception e) 
		{
			return "";
		}	
	}
	
	
	/** <pre>
	 * 초기 접속 시, 접속 로그 기록
	 * 
	 * url 접속 시, XML 페이지 반환
	 * rescd는 0000저장 성공 , 9999 저장 실패,
     * logSno - 접속 고유번호의 값을 리턴(disconnect, event 에서 사용)
     * </pre>
	 */
	static void connect(final Context context) 
	{
		
//		 http://s-home.ebs.co.kr/bandi/bandiUserLogInsert?mobId=모바일ID&startDtm=앱시작시간&appId=앱아이디&appOsCd=os정보&appOsVer=os버전
		String version = android.os.Build.VERSION.RELEASE; // 2.3.6
		String android_id = Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
		String dateTime16digits = DateUtil.getDateString(DateUtil.getKoreanCalendar(), "yyyyMMddHHmmss");
		
		String url = "http://home.ebs.co.kr/bandi/bandiUserLogInsert";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("mobId", android_id);
		params.put("startDtm", dateTime16digits);
		params.put("appId", "bandi");
		params.put("appOsCd", "Android");
		params.put("appOsVer", version);
		AQuery aq = new AQuery(context);
		aq.ajax(url, params, String.class, new AjaxCallback<String>()
		{
			
			@Override
			public void callback(String url, String object, AjaxStatus status) 
			{
				String sno = "";
				try
				{
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					InputStream istream = new ByteArrayInputStream(object.getBytes("utf-8"));
					Document doc = builder.parse(istream);
					 
//					Element order = doc.getDocumentElement();
//					Log.d("TEST", "" + order.getElementsByTagName("logSno").item(0).getTextContent());
					String resCd = doc.getElementsByTagName("rescd").item(0).getTextContent();
					
					if (resCd.equals("0000")) 
					{
						sno = doc.getElementsByTagName("logSno").item(0).getTextContent();
					}
				}
				catch (Exception e) { e.printStackTrace();Log.d("TEST", "init connect parser exception"); }
				((BandiApplication) context.getApplicationContext()).setSessionId(sno);
			}
			
		});
		
//		new Thread(new Runnable()
//		{
//			@Override
//			public void run() 
//			{
//				String sno = ""; // 접속 고유번호
//				
//				try 
//				{
//					//int sdk_int = android.os.Build.VERSION.SDK_INT; // 10, 11
//					String version = android.os.Build.VERSION.RELEASE; // 2.3.6
//					String android_id = Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
//					String dateTime16digits = DateUtil.getDateString(DateUtil.getKoreanCalendar(), "yyyyMMddHHmmss");
//					
//					//Toast.makeText(context, android_id + "/" + version +"/" + sdk_int+"/"+dateTime16digits, Toast.LENGTH_LONG).show();
//					//9774d56d682e549c/2.3.6/10/20131108142642
//					 
//					String url = Url.BASE_DOMAIN_HOME + "/bandi/bandiUserLogInsert";
//					url += String.format("?mobId=%s&startDtm=%s&endDtm=&appId=bandi&appOsCd=Android&appOsVer=%s", android_id, dateTime16digits, version);
//					Log.d(TAG, ">> connect : " + url);
//					
//					Document doc = Url.getRemoteDoc(url);
//					String resCd = doc.getElementsByTagName("rescd").item(0).getTextContent();
//					if ("0000".equals(resCd)) {
//						sno = doc.getElementsByTagName("logSno").item(0).getTextContent();
//						Log.d(TAG, ">> sno : " + sno);
//					}
//				} 
//				catch(Exception e) 
//				{
//					e.printStackTrace();
//				} 
//				
//				((BandiApplication) context.getApplicationContext()).setSessionId(sno);
//				
//			}
//		}).start();
	}
	
	/**
	 * 앱 종료 시, 종료시간을 기록
	 * rescd는 -1이 아니면 저장 성공, -1 저장 실패
	 */
	static void disconnect(final Context context) 
	{
//		http://s-home.ebs.co.kr/bandi/bandiUserLogUpdate?logSno=로그번호&endDtm=앱종료시간
		String sno = ((BandiApplication) context.getApplicationContext()).getSessionId();
		String dateTime16digits = DateUtil.getDateString(DateUtil.getKoreanCalendar(), "yyyyMMddHHmmss");
		String url = "http://home.ebs.co.kr/bandi/bandiUserLogUpdate";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("logSno", sno);
		params.put("endDtm", dateTime16digits);
		AQuery aq = new AQuery(context);
		aq.ajax(url, params, String.class, new AjaxCallback<String>()
		{
			@Override
			public void callback(String url, String object, AjaxStatus status) 
			{
				try
				{
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					InputStream istream = new ByteArrayInputStream(object.getBytes("utf-8"));
					Document doc = builder.parse(istream);
					String resCd = doc.getElementsByTagName("rescd").item(0).getTextContent();
					if (resCd.equals("-1"))
					{
						Log.d(TAG, ">> Saveing disconnection log fail.");
					}
				}
				catch (Exception e) { Log.d("TEST", "disconnect parser exception"); }
			}
		});
//		new Thread(new Runnable()
//		{
//			@Override
//			public void run() 
//			{
//				String sno = ((BandiApplication) context.getApplicationContext()).getSessionId();
//				
//				if (sno != null && !"".equals(sno)) 
//				{
//					try 
//					{
//						String dateTime16digits = DateUtil.getDateString(DateUtil.getKoreanCalendar(), "yyyyMMddHHmmss");
//						String url = Url.BASE_DOMAIN_HOME + "/bandi/bandiUserLogUpdate";
//						url += String.format("?logSno=%s&endDtm=%s", sno, dateTime16digits);
//						Log.d(TAG, ">> disconnect : " + url);
//						
//						Document doc = Url.getRemoteDoc(url);
//						String resCd = doc.getElementsByTagName("rescd").item(0).getTextContent();
//						if ("-1".equals(resCd)) {
//							Log.d(TAG, ">> Saveing disconnection log fail.");
//						}				
//					} 
//					catch(Exception e)
//					{
//						e.printStackTrace();
//					}	
//				}
//			}
//		}).start();
	}
	
	/**
	 * 사용자의 행위를 기록
	 * @param context
	 * @param action
	 */
	static void event(final Context context, final String action) 
	{
		
//		http://s-home.ebs.co.kr/bandi/bandiVisitLogInsert?modId=모바일ID&pageNm=페이지명&courseId=프로그램ID&userLogSno=앱실행후받은log일련번호
		
		String android_id = Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
		String sno = ((BandiApplication) context.getApplicationContext()).getSessionId();
		String url = "http://home.ebs.co.kr/bandi/bandiVisitLogInsert";
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("modId", android_id);
		params.put("pageNm", action);
		params.put("courseId", CProgramData.getInstance().getProgramID());
		params.put("userLogSno", sno);
		AQuery aq = new AQuery(context);
		aq.ajax(url, params, String.class, new AjaxCallback<String>()
		{
			@Override
			public void callback(String url, String object, AjaxStatus status) 
			{
				try
				{
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					InputStream istream = new ByteArrayInputStream(object.getBytes("utf-8"));
					Document doc = builder.parse(istream);
					String resCd = doc.getElementsByTagName("rescd").item(0).getTextContent();
					if (resCd.equals("-1")) 
					{
						Log.d(TAG, ">> Saving event log fail.");
					}
				}
				catch (Exception e) { Log.d("TEST", "event parser exception"); }
			}
		});
		
//		new Thread(new Runnable()
//		{
//			@Override
//			public void run() 
//			{
//				
//				
//				if (sno != null && !"".equals(sno)) 
//				{
//					try 
//					{
////						TimeTable tt = new TimeTable(context);
////						String progcd = "";
//////						if (tt.get() != null) 
//////						{
//////							progcd = tt.get().get(TimeTable.PROGCD);
//////						}
//						
//						String android_id = Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
//						String url = Url.BASE_DOMAIN_HOME + "/bandi/bandiVisitLogInsert";
//						url += String.format("?mobId=%s&pageNm=%s&courseId=%s&userLogSno=%s", android_id, action, CProgramData.getInstance().getProgramID(), sno);
//						
//						Log.d(TAG, ">> event("+ action +") log : " + url);
//						
//						Document doc = Url.getRemoteDoc(url);
//						String resCd = doc.getElementsByTagName("rescd").item(0).getTextContent();
//						if ("-1".equals(resCd)) 
//						{
//							Log.d(TAG, ">> Saving event log fail.");
//						}
//					} 
//					catch(Exception e)
//					{
//						e.printStackTrace();
//					}
//				}
//			}
//		}).start();
	}
	
	public static void onAirHit(Context context, String id, String logDsCd, String broadType, String onairDate, String onairStartTime)
	{
//		http://s-home.ebs.co.kr/logProce/bandiLogAjax.json?mobId=test&u=test&logDsCd=S&appDsCd=02&broadType=radio&onairDate=20141201&onairStartTime=08
		String url = "http://home.ebs.co.kr/logProce/bandiLogAjax.json";
		String android_id = Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("mobId", android_id);
		params.put("u", id);
		params.put("logDsCd", logDsCd);
		params.put("appDsCd", "02");
		params.put("broadType", broadType);
		params.put("onairDate", onairDate);
		params.put("onairStartTime", onairStartTime);
		
		AQuery aq = new AQuery(context);
		aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
		{
	
			@Override
			public void callback(String url, JSONObject object, AjaxStatus status) 
			{
				
//				Log.d("TEST", "onairhit object === " + object);
			}
	
		});
		
	}
	
	public static void bookInfoHit(Context context, String progId, int bookId)
	{
//		 http://s-home.ebs.co.kr/bandi/bandiBookHit
//		programId(프로그램 ID), appDsCd(앱 구분코드 01:pc, 02:안드로이드, 03:iOS), fileType(파일 형태 json/xml) post로 전달...
		
//		 http://home.ebs.co.kr/bandi/bandiBookHit?programId=00&appDsCd=00&bookId=00&fileType=xml
//			 · programId (프로그램 ID)
//			 · appDsCd (앱 구분코드) : 01 : PC, 02 : 안드로이드, 03 : iOS
//			 · bookId (도서 ID)
//			 · fileType (파일 형태 json/xml)
		
		String url = "http://home.ebs.co.kr/bandi/bandiBookHit";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("programId", progId);
		params.put("appDsCd", "02");
		params.put("bookId", bookId);
		params.put("fileType", "json");
		
		AQuery aq = new AQuery(context);
		aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
		{
	
			@Override
			public void callback(String url, JSONObject object, AjaxStatus status)
			{
//				Log.d("TEST", "bookinfo hit === " + object);
			}
	
		});
	}
}
