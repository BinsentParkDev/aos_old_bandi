package kr.ebs.bandi;

import kr.ebs.bandi.Login.LoginListener;
import kr.ebs.bandi.sns.LoginManager;
import kr.ebs.bandi.sns.LoginManager.LoginEventCallback;
import kr.ebs.bandi.sns.UserData;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

/**
 * @deprecated - 사용하지 않는 클래스인듯..
 * 
 * @since 2016. 7. 29.
 */
public class LoginForm implements BaseInterface
{
	static final String TAG = "LoginForm";
	private Context context;
	private Activity activity;
	private static Activity sactivity;
	BandiApplication bandiApp;

	private OnLoginSuccessCb loginSuccessCb;
	private OnLoginFailureCb loginFailureCb;

	private CheckBox ckSaveId;
    private CheckBox ckAutoLogin;
    private Preferences prefs;

    private EditText userId;

    private String findIdAndPwUrl = "https://sso.ebs.co.kr/idp/user/find/id.jsp?returnUrl=http://www.ebs.co.kr";
    private String registerUrl = "https://sso.ebs.co.kr/idp/join/intro.jsp?returnUrl=http://ebs.co.kr";

	public LoginForm(Context context)
	{
		this.context = context;
		this.activity = (Activity) context;
		this.sactivity = activity;
		bandiApp = (BandiApplication)activity.getApplication();

		((BandiApplication) activity.getApplication()).setCurrentMenu(TAG);

		/*
		 * 접근 로그 저장
		 */
		BandiLog.event(context, TAG);
	}

	@Override
	public void run()
	{
		BaseActivity.swapTitle(context, R.string.login);
		BaseActivity.swapLayout(context, R.layout.login);

        setListener();
	}

	private void setListener()
	{

		this.ckSaveId = (CheckBox) activity.findViewById(R.id.btn_save_id);
        this.ckAutoLogin = (CheckBox) activity.findViewById(R.id.btn_auto_login);
        this.prefs = new Preferences(context);

        userId = (EditText)activity.findViewById(R.id.userid);

        /*
		 * 로그인 기능
		 */
		ImageButton loginBtn = (ImageButton) activity.findViewById(R.id.login_btn);
		loginBtn.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if ("".equals(getUserId()) && "".equals(getPassword()))
				{
					Dialog.showAlert(context, "아이디와 패스워드를 모두 입력하세요.");
                    return;
				}

				Login lg = new Login(context);

				Login login = new Login(context);
	        	login.login(getUserId(), getPassword(), new LoginListener()
	        	{

					@Override
					public void OnLoginSuccess()
					{

						savePreferences();
	                    loginSuccessCb.onLoginSuccess();
	                    new Handler().postDelayed(new Runnable()
	                    {

							@Override
							public void run()
							{
								InputMethodManager imm= (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
								imm.hideSoftInputFromWindow(userId.getWindowToken(), 0);
							}
						}, 100);
					}

					@Override
					public void OnLoginFailed(String resultCode)
					{
						// TODO Auto-generated method stub

					}
				});

//                if (lg.login(getUserId(), getPassword()))
//                {
//
//                }
			}
		});

		activity.findViewById(R.id.layout_findIdBtn).setOnClickListener(btnClickListener);
		activity.findViewById(R.id.layout_findPwBtn).setOnClickListener(btnClickListener);
		activity.findViewById(R.id.layout_registerBtn).setOnClickListener(btnClickListener);
		activity.findViewById(R.id.imageButton_facebookLogin).setOnClickListener(btnClickListener);
	    activity.findViewById(R.id.imageButton_twitterLogin).setOnClickListener(btnClickListener);
		/*
		 * 아이디 저장 (리스너 및 기존 설정 셋팅)
		 */
		final CheckBox btnSaveId = this.ckSaveId;
		btnSaveId.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				// TODO Auto-generated method stub

			}
		});
//        btnSaveId.setOnClickListener(new OnClickListener()
//        {
//            public void onClick(View v)
//            {
//                btnSaveId.setSelected( ! btnSaveId.isSelected() );
//            }
//        });
        // 아이디 저장 설정일 때, 기존 아이디 불러오기
        btnSaveId.setChecked(prefs.getSaveId());
        if (prefs.getSaveId())
        {
            EditText userid = (EditText) activity.findViewById(R.id.userid);
            userid.setText( prefs.getUserId() );
            Log.d(TAG, "userId: " + prefs.getUserId());
        }

        /*
		 * 자동 로그인 (리스너 및 기존 설정 셋팅)
		 */
        final CheckBox btnAutoLogin = this.ckAutoLogin;
//        btnAutoLogin.setOnClickListener(new OnClickListener()
//        {
//            public void onClick(View v)
//            {
//                btnAutoLogin.setSelected( ! btnAutoLogin.isSelected() );
//            }
//        });
        btnAutoLogin.setChecked(prefs.getAutoLogin());

        /*
         * 로그인 창 닫기
         */
		ImageButton closeBtn = (ImageButton) activity.findViewById(R.id.close_btn);
		closeBtn.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				loginFailureCb.onLoginFailure();
			}
		});

	} // end setListener()

	public void backPressed()
	{
		loginFailureCb.onLoginFailure();
	}

	OnClickListener btnClickListener = new OnClickListener()
	{

		@Override
		public void onClick(View v)
		{
			if(v.getId() == R.id.layout_findIdBtn || v.getId() == R.id.layout_findPwBtn)
			{
				Uri uri = Uri.parse(findIdAndPwUrl);
				Intent it  = new Intent(Intent.ACTION_VIEW,uri);
				activity.startActivity(it);
			}
			else if(v.getId() == R.id.layout_registerBtn)
			{
				Uri uri = Uri.parse(registerUrl);
				Intent it  = new Intent(Intent.ACTION_VIEW,uri);
				activity.startActivity(it);
			}
			else if(v.getId() == R.id.imageButton_facebookLogin)
			{
				fbLogin();
			}
			else if(v.getId() == R.id.imageButton_twitterLogin)
			{

			}
		}
	};

	private void fbLogin()
	{
		LoginManager.getInstance().startLogin(activity, UserData.LOGIN_TYPE_FB, new LoginEventCallback()
		{
            public void eventStart()
            {
                // 여기 로딩바 스타트
            }
            @Override
            public void eventSuccess(Object obj)
            {
                // 로딩바 닫기
            	// 페이스북 로그인 후 실행할 것
//                LoginManager.getInstance().getUserData().saveUserData();
            }

            @Override
            public void eventCallActivity(int request)
            {
//                if (request == REQUEST_FB_PHONE_AUTH) {
//                    hideCircleProgress();
//                    Intent intent = new Intent(LoginMainActivity.this, LoginPhoneAuthActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                    startActivityForResult(intent, REQUEST_FB_PHONE_AUTH);
//                }
            }

            @Override
            public void eventFailed() {
//                Toast.makeText(context, getString(R.string.info_login_failure), Toast.LENGTH_SHORT).show(); // laughaway_mod
            }
        });
	}


	/**
     * 사용자 설정 정보 저장.
     * @since 2012. 3. 9.
     */
    private void savePreferences()
    {
        prefs.putSaveId( this.ckSaveId.isChecked() );
        prefs.putAutoLogin( this.ckAutoLogin.isChecked() );

        if (this.ckAutoLogin.isChecked())
        {
            prefs.putUserId(getUserId());
            prefs.putPassword(getPassword());
        }
        else
        {
            prefs.putUserId("");
            prefs.putPassword("");

            if (this.ckSaveId.isChecked()) prefs.putUserId(getUserId());
        }
    } // end savePreferences()

	private String getUserId()
	{
        return ( (EditText) activity.findViewById(R.id.userid) ).getText().toString();
    }

    private String getPassword()
    {
        return ( (EditText) activity.findViewById(R.id.passwd) ).getText().toString();
    }


	interface OnLoginSuccessCb
	{
		void onLoginSuccess();
	}

	interface OnLoginFailureCb
	{
		void onLoginFailure();
	}

	public void setOnLoginSuccessCb(OnLoginSuccessCb callback)
	{
		loginSuccessCb = callback;
	}

	public void setOnLoginFailureCb(OnLoginFailureCb callback)
	{
		loginFailureCb = callback;
	}


}
