package kr.ebs.bandi;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.Toast;

public class WebViewDialog extends Dialog {
	static final String TAG = "WebViewDialog";
	
	private WebView mWebView;
	private Context context;
	private BandiApplication bandiApp;
	private Preferences prefs;
	
	public WebViewDialog(Context context) {
		super(context);
		this.context = context;
		this.bandiApp = (BandiApplication) ((Activity) context).getApplication();
		this.prefs = new Preferences(context);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_webview);
		
		getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		
		
		mWebView = (WebView) this.findViewById(R.id.dlg_webview);
		this.setTitle("EBS 회원 탈퇴");
		if (syncSsoSession()) {
			openWebWithSsoSession(Url.SSO_WITHDRAW_URL);
			bandiApp.clear(); // (중요) 탈퇴 진행하면서 반디 앱 세션은 없앤다.
			prefs.putAutoLogin(false); // 자동로그인도 해제.
			prefs.putPassword("");
			
			Log.d(TAG, "sync sso session");
		} else {
			Toast.makeText(context, "로그인 연동이 되지 않았습니다.", Toast.LENGTH_SHORT).show();
			Log.d(TAG, "fail sync sso session ");
		}
		
    }
	
	
	/**
	 * 로그인할 때 받아온 쿠키를 이용해, SSO 서버 로그인 유지관계를 체크하고, 필요 시 재연결한다. <br>
	 * 
	 * @return
	 */
	private boolean syncSsoSession() {
		boolean result = true;
		LoginApi loginApi = new LoginApi(this.context);
		if (! loginApi.isValidSsoSession()) {
			Log.d(TAG, "invalid session");
			result = loginApi.login(bandiApp.getUserId(), bandiApp.getUserPasswd(), false);
		}
		Log.d(TAG, "syncSsoSession() returns : " + result);
		return result;
	}
	
	/**
	 *  sso 로그인 세션 정보를 이용해서 웹 페이지 호출 <br>
	 *  로그인 된 상태로 웹 페이지가 열린다.
	 */
	public void openWebWithSsoSession(String webPageUri) {
		String requestUri = Uri.parse(Url.LOGIN_OPEN_WEB_HTTPS_GET).buildUpon().appendQueryParameter("returnUrl", webPageUri).build().toString();
		loadWebPage(requestUri);
	}
	
	
	@SuppressLint("SetJavaScriptEnabled")
	public boolean loadWebPage(String uri) {
		Log.d(TAG, "loadWebPage called.");
		
		String ssoCookie = bandiApp.getSsoCookie();
		CookieManager cookieManager = CookieManager.getInstance();
		cookieManager.setAcceptCookie(true);
		cookieManager.setCookie("https://sso.ebs.co.kr", ssoCookie);
		
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.loadUrl(uri);
		mWebView.setWebViewClient(new WebViewClientClass());
		
		return true;
	}
	
	private class WebViewClientClass extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

}
