package kr.ebs.bandi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import kr.ebs.bandi.ProgramData.DataParseListener;
import kr.ebs.bandi.data.CProgramData;
import kr.ebs.bandi.data.ScheduleCompare;
import kr.ebs.bandi.data.ScheduleCompare.OnCompareListener;
import kr.ebs.bandi.utils.CScrollViewWithProgress2;
import kr.ebs.bandi.utils.VerticalRollingText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.LinearInterpolator;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toast;

public class OnViewUpdater
{
	static final String TAG = "OnViewUpdater";
	private Context context;
	private Activity activity;
	private Handler handler = new Handler();
	
	private String progcd;
	private ImageView progImage;
    private ImageView onairViewBtn; // 보이는 라디오
    private PlayerStartCallback startPlayer;
    private PlayerStopCallback stopPlayer;
    private CheckBox favoriteCheck;
    
    BandiApplication bandiApp;
    String nowId, prevId;
    MainActivity main;
    ArrayList<String> boardTxt = new ArrayList<String>();
//    VerticalRollingText rollingTxt, rollingTxt2;
    TextView rollingTxt;
   // TextView title;
    
    
	public OnViewUpdater(final Context context)
	{
		this.context = context;
		this.activity = (Activity) context;
		main = (MainActivity)activity;
		bandiApp = (BandiApplication)activity.getApplication();
		progImage = (ImageView) activity.findViewById(R.id.program_image_zone);
		rollingTxt = (TextView)activity.findViewById(R.id.scroll_text);
//		rollingTxt2 = (VerticalRollingText)activity.findViewById(R.id.scroll_text2);
		//title = (TextView) activity.findViewById(R.id.textView_onAirName);
		
		ScheduleCompare.getInstance().setListener(new OnCompareListener() 
		{
			
			@Override
			public void OnScheduleFinish() 
			{
				Log.d(TAG, "entered OnScheduleFinish");
				activity.runOnUiThread(new Runnable()
				{
					
					@Override
					public void run()
					{
						// TODO Auto-generated method stub
						final ImageButton homeBtn = (ImageButton) activity.findViewById(R.id.imageButton_home);
		                final ImageButton facebookBtn = (ImageButton) activity.findViewById(R.id.imageButton_facebook);
		                final ImageButton twitterBtn = (ImageButton) activity.findViewById(R.id.imageButton_twitter);
		                final ImageButton kakaoBtn = (ImageButton) activity.findViewById(R.id.imageButton_kakao);
		                
		                homeBtn.setVisibility(View.GONE);
		                facebookBtn.setVisibility(View.GONE);
		                twitterBtn.setVisibility(View.GONE);
		                kakaoBtn.setVisibility(View.GONE);
		                
		                CProgramData.getInstance().setAppUseBoardYn("N");
		                CProgramData.getInstance().setViewRadioState("Y");
		                
		                progImage.setBackgroundResource(R.drawable.bandi_main_bg_nodata);
						activity.findViewById(R.id.layout_viewRadio).setVisibility(View.VISIBLE);
						activity.findViewById(R.id.checkBox_favorite).setVisibility(View.INVISIBLE);
						
//						if(main.notification != null)
//						{
//							main.notification.setLatestEventInfo(context, context.getResources().getString(R.string.app_name), "지금은 방송시간이 아닙니다.", main.pi);
//							main.notificationManager.notify(MainActivity.NOTIFICATION_ID, main.notification);
//						}
						
						if(NotificationService._this.notification != null)
						{
							try {
								Log.d(TAG, "notification work (finish)");
								
								NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
												.setContentIntent(NotificationService._this.pi)
								                .setSmallIcon(R.drawable.ic_bandi_launcher_silhouette)
								                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_bandi_launcher))
								                .setContentTitle(context.getResources().getString(R.string.app_name))
								                .setContentText("지금은 방송시간이 아닙니다.")
//								                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
								                .setTicker(context.getResources().getString(R.string.app_name));
								Notification notification = builder.build();
								NotificationService._this.notificationManager.notify(MainActivity.NOTIFICATION_ID, notification);
							} catch(Exception e) {
								Log.e(TAG, "notification work(finish) error - " + e.getMessage());
								e.printStackTrace();
							}
							
				            
//							NotificationService._this.notification.setLatestEventInfo(context, 
//														context.getResources().getString(R.string.app_name), 
//														"지금은 방송시간이 아닙니다.", 
//														NotificationService._this.pi);
//							NotificationService._this.notificationManager.notify(MainActivity.NOTIFICATION_ID, NotificationService._this.notification);
						}
						
					}
				});
				
			}
			
			@SuppressWarnings("deprecation")
			@Override
			public void OnScheduleChange(String id, String title, String onairDate, String onairTime) 
			{
				Log.d(TAG, "entered OnScheduleChange");
				
				if(bandiApp.getIsTabClick())
				{
					BandiLog.onAirHit(context, bandiApp.getUserId(), "S", bandiApp.getIsFMRadio() == true ? "radio" : "iradio", onairDate, onairTime);
					bandiApp.setIsTabClick(false);
				}
				else
				{
					BandiLog.onAirHit(context, bandiApp.getUserId(), "P", bandiApp.getIsFMRadio() == true ? "radio" : "iradio", onairDate, onairTime);
				}
				
				nowId = id;
				bandiApp.setTitleForBoard(title);
				
				if(NotificationService._this.notification != null)
				{
					try {
						NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
											.setContentIntent(NotificationService._this.pi)
							                .setSmallIcon(R.drawable.ic_bandi_launcher_silhouette)
							                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_bandi_launcher))
							                .setContentTitle(context.getResources().getString(R.string.app_name))
							                .setContentText(title)
//							                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
							                .setTicker(context.getResources().getString(R.string.app_name));
						Notification notification = builder.build();
						NotificationService._this.notificationManager.notify(MainActivity.NOTIFICATION_ID, notification);

//						NotificationService._this.notification.setLatestEventInfo(context, context.getResources().getString(R.string.app_name), title, NotificationService._this.pi);
//						NotificationService._this.notificationManager.notify(MainActivity.NOTIFICATION_ID, NotificationService._this.notification);
					} catch(Exception e) {
						Log.e(TAG, "OnScheduleChange() - Error Notification - " + e.getMessage());
						e.printStackTrace();
					}
					
				}
				
//				notification.setLatestEventInfo(NotificationBuilder.this, title, text, intent);
				 //** 보이는 라디오 설정 *****/
                final ImageButton homeBtn = (ImageButton) activity.findViewById(R.id.imageButton_home);
                final ImageButton facebookBtn = (ImageButton) activity.findViewById(R.id.imageButton_facebook);
                final ImageButton twitterBtn = (ImageButton) activity.findViewById(R.id.imageButton_twitter);
                final ImageButton kakaoBtn = (ImageButton) activity.findViewById(R.id.imageButton_kakao);
				final ProgramData pd = new ProgramData(context);
				pd.setProgramData(id, new DataParseListener() 
				{
					
					@Override
					public void OnParseComplete(String resultCode) 
					{
						// TODO Auto-generated method stub
						Log.d(TAG, "OnScheduleChange-setProgramData-OnParseComplete");
						FrameLayout favoriteCheck = (FrameLayout) activity.findViewById(R.id.checkBox_favorite);
		                favoriteCheck.setSelected(false);
		                if(bandiApp.getFavoriteList() != null && bandiApp.getFavoriteList().size() > 0)
		                {
		                	for(int i = 0; i < bandiApp.getFavoriteList().size(); i++)
		                	{
		                		if(bandiApp.getFavoriteList().get(i).getProgramID().equals(CProgramData.getInstance().getProgramID()))
		                		{
		                			String fbmkSno = Integer.toString(bandiApp.getFavoriteList().get(i).getFbmkSno());
		                			CProgramData.getInstance().setNowFavoriteId(fbmkSno);
		                			
		                			favoriteCheck.setSelected(true);
		                		}
		                	}
		                }
						
		                
		                
		                               
						if(resultCode.equals("111"))
						{
							Log.d("11", "resultCode == 111");
							progImage.setBackgroundResource(R.drawable.bandi_main_bg2);
							activity.findViewById(R.id.layout_viewRadio).setVisibility(View.INVISIBLE);
							homeBtn.setVisibility(View.GONE);
			                facebookBtn.setVisibility(View.GONE);
			                twitterBtn.setVisibility(View.GONE);
			                kakaoBtn.setVisibility(View.GONE);
						}
						else
						{
							Log.d("11", "resultCode =else 111");
							pd.setProgramImage(progImage, CProgramData.getInstance().getAppImgUrl());
							ImageView appImg2 = (ImageView)activity.findViewById(R.id.imageView_appImg2);
							pd.setProgramImage(appImg2, CProgramData.getInstance().getAppImg2Url());
			                LinearLayout onairViewBtn = (LinearLayout) activity.findViewById(R.id.layout_viewRadio);
			                if(bandiApp.getIsFMRadio())
			                {
				                //** 보이는 라디오 설정 **
			                	onairViewBtn.setVisibility(ImageView.VISIBLE);
			                }
			                else
			                {
			                	onairViewBtn.setVisibility(View.INVISIBLE);
			                }
			                
			                homeBtn.setVisibility(View.VISIBLE);
			                facebookBtn.setVisibility(View.VISIBLE);
			                twitterBtn.setVisibility(View.VISIBLE);
			                kakaoBtn.setVisibility(View.VISIBLE);
			                
			                homeBtn.setOnClickListener(urlBtnListener);
			                facebookBtn.setOnClickListener(urlBtnListener);
			                twitterBtn.setOnClickListener(urlBtnListener);
			                kakaoBtn.setOnClickListener(urlBtnListener);
			                
			                if(CProgramData.getInstance().getMobilePageUrl() == null ||
			                   CProgramData.getInstance().getMobilePageUrl().equals("") || 
			                   CProgramData.getInstance().getMobilePageUrl().equals("null"))
			                {
			                	homeBtn.setVisibility(View.GONE);
			                }
			                
			                if(CProgramData.getInstance().getFacebookUrl() == null ||
			                   CProgramData.getInstance().getFacebookUrl().equals("") || 
			                   CProgramData.getInstance().getFacebookUrl().equals("null"))
			                {
			                	facebookBtn.setVisibility(View.GONE);
			                }
			                
			                if(CProgramData.getInstance().getTwitterUrl() == null ||
					           CProgramData.getInstance().getTwitterUrl().equals("") ||
					           CProgramData.getInstance().getTwitterUrl().equals("null"))
			                {
			                	twitterBtn.setVisibility(View.GONE);
			                }
			                
			                if(CProgramData.getInstance().getKakaoUrl() == null ||
					           CProgramData.getInstance().getKakaoUrl().equals("") ||
					           CProgramData.getInstance().getKakaoUrl().equals("null"))
			                {
			                	kakaoBtn.setVisibility(View.GONE);
			                }
			                if(bandiApp.getCurrentMenu().equals("BandiBoard")) 
			                {
			                	TextView title = (TextView)activity.findViewById(R.id.textView_onAirName);
			                	title.setText(bandiApp.getTitleForBoard());
			                }
			                if(CProgramData.getInstance().getAppUseBoardYn().equals("Y"))
			                {
			                	bandiApp.setBoardLineContents(new ArrayList<String>());
			                	OnAirScrollTextUpdater.handler.sendEmptyMessage(0);
			                	FrameLayout noBandiBoardNoti = (FrameLayout)activity.findViewById(R.id.layout_noBandiBoardText);
			                	noBandiBoardNoti.setVisibility(View.INVISIBLE);
				                boardTxt.clear();
				                
				                if(bandiApp.getCurrentMenu().equals("BandiBoard")) 
					            {
			        				TextView noBoardTxt = (TextView)activity.findViewById(R.id.textView_noBoard);
			        				CScrollViewWithProgress2 boardScroll = (CScrollViewWithProgress2)activity.findViewById(R.id.scrollView_bandiboard);
			        				noBoardTxt.setVisibility(View.INVISIBLE);
			        				boardScroll.setVisibility(View.VISIBLE);
					            }
				                
				                bandiboardListRetriever = new BandiboardListRetriever();
				                bandiboardListRetriever.start();
				                
//				                String url = "http://home.ebs.co.kr/bandi/bandiBoardList";
//				        		Map<String, Object> params = new HashMap<String, Object>();
//				        		params.put("fileType", "json");
//				        		params.put("programId", CProgramData.getInstance().getProgramID());
//				        		params.put("listType", "L");
//				        		params.put("pageSize", 10);
//				        		AQuery aq = new AQuery(context);
//				        		aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
//				        		{
//				        			
//				        			@Override
//				        			public void callback(String url, JSONObject object, AjaxStatus status)
//				        			{
//				        				JSONArray jArray;
//				        				try
//				        				{
//				        					jArray = object.getJSONArray("bandiPosts");
//				        					for(int i = 0; i < jArray.length(); i++)
//				        					{
//				        						
//				        						if(jArray.getJSONObject(i).getString("contents") == null || jArray.getJSONObject(i).getString("contents").equals("null") || jArray.getJSONObject(i).getString("contents").equals("null"))
//				        						{
//				        							boardTxt.add("");
//				        						}
//				        						else
//				        						{
//				        							boardTxt.add(jArray.getJSONObject(i).getString("contents"));
//				        						}
//				        						
//				        					}
//				        					
//				        					rollingTxt.setVisibility(View.VISIBLE);
////				        					new OnAirScrollTextUpdater(context).setTextList(boardTxt);
//				        					bandiApp.setBoardLineContents(boardTxt);
//				        					OnAirScrollTextUpdater.handler.sendMessage(Message.obtain(OnAirScrollTextUpdater.handler, 1, context));
//				        					
//				        				} 
//				        				catch (JSONException e)
//				        				{
//				        					e.printStackTrace();
//				        				}
//				        			}
//				        			
//				        		});
			                }
			                else
			                {
			                	FrameLayout noBandiBoardNoti = (FrameLayout)activity.findViewById(R.id.layout_noBandiBoardText);
			                	noBandiBoardNoti.setVisibility(View.VISIBLE);
			                	
			                	if(bandiApp.getCurrentMenu().equals("BandiBoard")) 
					            {
			        				TextView noBoardTxt = (TextView)activity.findViewById(R.id.textView_noBoard);
			        				CScrollViewWithProgress2 boardScroll = (CScrollViewWithProgress2)activity.findViewById(R.id.scrollView_bandiboard);
			        				noBoardTxt.setVisibility(View.VISIBLE);
			        				boardScroll.setVisibility(View.INVISIBLE);
					            }
			                	
			                	rollingTxt.setVisibility(View.INVISIBLE);
			                }
						}
					}

					@Override
					public void OnParseError() 
					{
						Log.d("11", "OnParseError");
					}
				});
				
				prevId = nowId;
			}
			
			@Override
			public void OnSchedule(String id, String title, String onairDate, String onairTime) 
			{
				Log.d(TAG, "OnSchedule");
				if(bandiApp.getOnairHitInit() == true)
				{
					if(bandiApp.getIsFMRadio())
					{
						BandiLog.event(context, "OnAir");
					}
					else
					{
						BandiLog.event(context, "OnAirIradio");
					}
					BandiLog.onAirHit(context, bandiApp.getUserId(), "S", bandiApp.getIsFMRadio() == true ? "radio" : "iradio", onairDate, onairTime);
					bandiApp.setOnairHitInit(false);
				}
				
				nowId = id;
				bandiApp.setTitleForBoard(title);

				
				if(NotificationService._this != null && NotificationService._this.notification != null)
				{
					try {
						Log.d(TAG, "notification onschedule.");
						NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
															.setContentIntent(NotificationService._this.pi)
											                .setSmallIcon(R.drawable.ic_bandi_launcher_silhouette)
											                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_bandi_launcher))
											                .setContentTitle(context.getResources().getString(R.string.app_name))
											                .setContentText(title)
//											                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
											                .setTicker(context.getResources().getString(R.string.app_name));
						Notification notification = builder.build();
						NotificationService._this.notificationManager.notify(MainActivity.NOTIFICATION_ID, notification);
	
						//NotificationService._this.notification.setLatestEventInfo(context, context.getResources().getString(R.string.app_name), title, NotificationService._this.pi);
						//NotificationService._this.notificationManager.notify(MainActivity.NOTIFICATION_ID, NotificationService._this.notification);
					} catch (Exception e) {
						Log.e(TAG, "notification error (onschedule) - " + e.getMessage());
						e.printStackTrace();
					}
				}
				
				final ImageButton homeBtn = (ImageButton) activity.findViewById(R.id.imageButton_home);
				final ImageButton facebookBtn = (ImageButton) activity.findViewById(R.id.imageButton_facebook);
				final ImageButton twitterBtn = (ImageButton) activity.findViewById(R.id.imageButton_twitter);
				final ImageButton kakaoBtn = (ImageButton) activity.findViewById(R.id.imageButton_kakao);
				
				final ProgramData pd = new ProgramData(context);
				pd.setProgramData(id, new DataParseListener() 
				{
					@Override
					public void OnParseComplete(String resultCode) 
					{
						Log.d(TAG, "OnSchedule-setProgramData-OnParseComplete");
						FrameLayout favoriteCheck = (FrameLayout) activity.findViewById(R.id.checkBox_favorite);
		                favoriteCheck.setSelected(false);
		                if(bandiApp.getFavoriteList() != null && bandiApp.getFavoriteList().size() > 0)
		                {
		                	for(int i = 0; i < bandiApp.getFavoriteList().size(); i++)
		                	{
		                		if(bandiApp.getFavoriteList().get(i).getProgramID().equals(CProgramData.getInstance().getProgramID()))
		                		{
		                			String fbmkSno = Integer.toString(bandiApp.getFavoriteList().get(i).getFbmkSno());
		                			CProgramData.getInstance().setNowFavoriteId(fbmkSno);
		                			favoriteCheck.setSelected(true);
		                		}
		                	}
		                }
						
						if(resultCode.equals("111"))
						{
							progImage.setBackgroundResource(R.drawable.bandi_main_bg2);
							activity.findViewById(R.id.layout_viewRadio).setVisibility(View.INVISIBLE);
							homeBtn.setVisibility(View.GONE);
			                facebookBtn.setVisibility(View.GONE);
			                twitterBtn.setVisibility(View.GONE);
			                kakaoBtn.setVisibility(View.GONE);
						}
						else
						{
							pd.setProgramImage(progImage, CProgramData.getInstance().getAppImgUrl());
							ImageView appImg2 = (ImageView)activity.findViewById(R.id.imageView_appImg2);
							pd.setProgramImage(appImg2, CProgramData.getInstance().getAppImg2Url());
			                LinearLayout onairViewBtn = (LinearLayout) activity.findViewById(R.id.layout_viewRadio);
			                if(bandiApp.getIsFMRadio())
			                {
				                //** 보이는 라디오 설정 **
			                	onairViewBtn.setVisibility(ImageView.VISIBLE);

			                }
			                else
			                {
			                	onairViewBtn.setVisibility(View.INVISIBLE);
			                }
			               
			                homeBtn.setVisibility(View.VISIBLE);
			                facebookBtn.setVisibility(View.VISIBLE);
			                twitterBtn.setVisibility(View.VISIBLE);
			                kakaoBtn.setVisibility(View.VISIBLE);
			                
			                homeBtn.setOnClickListener(urlBtnListener);
			                facebookBtn.setOnClickListener(urlBtnListener);
			                twitterBtn.setOnClickListener(urlBtnListener);
			                kakaoBtn.setOnClickListener(urlBtnListener);
			                
			                if(CProgramData.getInstance().getMobilePageUrl() == null ||
			                   CProgramData.getInstance().getMobilePageUrl().equals("") || 
			                   CProgramData.getInstance().getMobilePageUrl().equals("null"))
			                {
			                	homeBtn.setVisibility(View.GONE);
			                }
			                
			                if(CProgramData.getInstance().getFacebookUrl() == null ||
			                   CProgramData.getInstance().getFacebookUrl().equals("") || 
			                   CProgramData.getInstance().getFacebookUrl().equals("null"))
			                {
			                	facebookBtn.setVisibility(View.GONE);
			                }
			                
			                if(CProgramData.getInstance().getTwitterUrl() == null ||
					           CProgramData.getInstance().getTwitterUrl().equals("") ||
					           CProgramData.getInstance().getTwitterUrl().equals("null"))
			                {
			                	twitterBtn.setVisibility(View.GONE);
			                }
			                
			                if(CProgramData.getInstance().getKakaoUrl() == null ||
					           CProgramData.getInstance().getKakaoUrl().equals("") ||
					           CProgramData.getInstance().getKakaoUrl().equals("null"))
			                {
			                	kakaoBtn.setVisibility(View.GONE);
			                }
			                
			                if(bandiApp.getCurrentMenu().equals("BandiBoard")) 
			                {
			                	TextView title = (TextView)activity.findViewById(R.id.textView_onAirName);
			                	title.setText(bandiApp.getTitleForBoard());
			                }
			                
			                if(CProgramData.getInstance().getAppUseBoardYn().equals("Y"))
			                {
			                	bandiApp.setBoardLineContents(new ArrayList<String>());
			                	OnAirScrollTextUpdater.handler.sendEmptyMessage(0);
			                	FrameLayout noBandiBoardNoti = (FrameLayout)activity.findViewById(R.id.layout_noBandiBoardText);
			                	noBandiBoardNoti.setVisibility(View.INVISIBLE);
			                	
			                	if(bandiApp.getCurrentMenu().equals("BandiBoard")) 
					            {
			        				TextView noBoardTxt = (TextView)activity.findViewById(R.id.textView_noBoard);
			        				CScrollViewWithProgress2 boardScroll = (CScrollViewWithProgress2)activity.findViewById(R.id.scrollView_bandiboard);
			        				noBoardTxt.setVisibility(View.INVISIBLE);
			        				boardScroll.setVisibility(View.VISIBLE);
					            }
			                	
				                boardTxt.clear();
			                	
				                bandiboardListRetriever = new BandiboardListRetriever();
				                bandiboardListRetriever.start();
				                
//				                String url = "http://home.ebs.co.kr/bandi/bandiBoardList";
//				        		Map<String, Object> params = new HashMap<String, Object>();
//				        		params.put("fileType", "json");
//				        		params.put("programId", CProgramData.getInstance().getProgramID());
//				        		params.put("listType", "L");
//				        		params.put("pageSize", 10);
//				        		AQuery aq = new AQuery(context);
//				        		aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
//				        		{
//				        			
//				        			@Override
//				        			public void callback(String url, JSONObject object, AjaxStatus status)
//				        			{
//				        				JSONArray jArray;
//				        				try
//				        				{
//				        					jArray = object.getJSONArray("bandiPosts");
//				        					for(int i = 0; i < jArray.length(); i++)
//				        					{
//				        						if(jArray.getJSONObject(i).getString("contents") == null || jArray.getJSONObject(i).getString("contents").equals("null") || jArray.getJSONObject(i).getString("contents").equals("null"))
//				        						{
//				        							boardTxt.add("");
//				        						}
//				        						else
//				        						{
//				        							boardTxt.add(jArray.getJSONObject(i).getString("contents"));
//				        						}
////				        						
//				        					}
////				        					
//				        					rollingTxt.setVisibility(View.VISIBLE);
//				        					bandiApp.setBoardLineContents(boardTxt);
//				        					OnAirScrollTextUpdater.handler.sendMessage(Message.obtain(OnAirScrollTextUpdater.handler, 1, context));
//				        					
//				        				} 
//				        				catch (JSONException e)
//				        				{
//				        					// TODO Auto-generated catch block
//				        					e.printStackTrace();
//				        				}
//				        			}
//				        			
//				        		});
			                }
			        		else
			                {
			        			FrameLayout noBandiBoardNoti = (FrameLayout)activity.findViewById(R.id.layout_noBandiBoardText);
			        			noBandiBoardNoti.setVisibility(View.VISIBLE);
			        			
			        			if(bandiApp.getCurrentMenu().equals("BandiBoard")) 
					            {
			        				TextView noBoardTxt = (TextView)activity.findViewById(R.id.textView_noBoard);
			        				CScrollViewWithProgress2 boardScroll = (CScrollViewWithProgress2)activity.findViewById(R.id.scrollView_bandiboard);
			        				noBoardTxt.setVisibility(View.VISIBLE);
			        				boardScroll.setVisibility(View.INVISIBLE);
					            }
			        			
			                	rollingTxt.setVisibility(View.INVISIBLE);
			                }
						}
					}

					@Override
					public void OnParseError() 
					{
						// TODO Auto-generated method stub
//						progImage.setBackgroundResource(R.drawable.bandi_main_bg2);
//						activity.findViewById(R.id.layout_viewRadio).setVisibility(View.INVISIBLE);
//						homeBtn.setVisibility(View.GONE);
//		                facebookBtn.setVisibility(View.GONE);
//		                twitterBtn.setVisibility(View.GONE);
//		                kakaoBtn.setVisibility(View.GONE);
					}
				});
				prevId = nowId;
			}

			@Override
			public void OnScheduleError()
			{
				// TODO Auto-generated method stub
//				progImage.setImageResource(R.drawable.bandi_main_bg2);
				Log.d(TAG, "OnScheduleError");
			}
		});
		
//		handler = new Handler();
//    	handler.removeCallbacks(this);
//    	handler.post(this);
	}
	
	
	OnClickListener urlBtnListener = new OnClickListener() 
	{
		
		@Override
		public void onClick(View v) 
		{
			Uri uri = null;

			if(v.getId() == R.id.imageButton_home)
			{
				uri = Uri.parse(CProgramData.getInstance().getMobilePageUrl());
			}
			else if(v.getId() == R.id.imageButton_facebook)
			{
				uri = Uri.parse(CProgramData.getInstance().getFacebookUrl());
			}
			else if(v.getId() == R.id.imageButton_kakao)
			{
				uri = Uri.parse(CProgramData.getInstance().getKakaoUrl());
			}
			else if(v.getId() == R.id.imageButton_twitter)
			{
				uri = Uri.parse(CProgramData.getInstance().getTwitterUrl()); 
			}
			
			Intent it  = new Intent(Intent.ACTION_VIEW, uri);
			activity.startActivity(it);
		}
	};
	
	private Handler tHandler = new Handler()
	{
		@Override
		public void handleMessage(Message msg)
		{
			super.handleMessage(msg);
			switch(msg.what)
			{
			case 1:
				setRollingText((String)msg.obj);
				if (bandiboardListRetriever != null && bandiboardListRetriever.isAlive()) bandiboardListRetriever.interrupt();
				break;
			}
		}
	};
	
	/**
	 * 반디 게시판 목록을 받아오는 Thread <br>
	 * tHandler를 거쳐 setRollingText() 호출하여 게시글 롤링 기능 구현 <br>
	 * (msg.what = 1)
	 */
	private BandiboardListRetriever bandiboardListRetriever;
	private class BandiboardListRetriever extends Thread
    {
    	@Override
    	public void run()
    	{
    		String urlStr = Url.BANDIBOARD_LIST_JSON(CProgramData.getInstance().getProgramID());
    		String jsonStr = Url.getServerText(urlStr);
    		Message msg = tHandler.obtainMessage(1, jsonStr);
    		tHandler.sendMessage(msg);
    	}
    }
	
	/**
	 * 온에어 메뉴에서 게시물 롤링 텍스트 설정
	 * tHandler 에서 호출됨.
	 * @param jsonStr
	 */
	private void setRollingText(String jsonStr)
	{
		try
		{
			JSONObject jObject = new JSONObject(jsonStr);
			JSONArray jArray = jObject.getJSONArray("bandiPosts");
			for(int i = 0; i < jArray.length(); i++)
			{
				if(jArray.getJSONObject(i).getString("contents") == null || jArray.getJSONObject(i).getString("contents").equals("null") || jArray.getJSONObject(i).getString("contents").equals("null"))
				{
					boardTxt.add("");
				}
				else
				{
					boardTxt.add(TextUtil.HTMLDecode(jArray.getJSONObject(i).getString("contents")));
				}
			}
			
			rollingTxt.setVisibility(View.VISIBLE);
			bandiApp.setBoardLineContents(boardTxt);
			OnAirScrollTextUpdater.handler.sendMessage(Message.obtain(OnAirScrollTextUpdater.handler, 1, context));
		} 
		catch (JSONException e)
		{
			Log.e(TAG, "setRollingText error - " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	/*
	 * 오디오 스트리밍 재생 시작/멈춤 제어 콜백
	 */
	interface PlayerStartCallback 
	{
		void startPlayer();
	}
	interface PlayerStopCallback 
	{
		void stopPlayer();
    }
	
	public void setPlayerStartCallback(PlayerStartCallback callback) 
	{
		this.startPlayer = callback;
	}
	public void setPlayerStopCallback(PlayerStopCallback callback) 
	{
		this.stopPlayer = callback;
	}
}
