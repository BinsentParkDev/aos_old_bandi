package kr.ebs.bandi;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;

import kr.ebs.bandi.GoogleAnalytics;

public class FamilyService implements BaseInterface
{
	static final String TAG = "FamilyService";
	private Context context;
	private Activity activity;
	MainActivity main;
	
	
	public FamilyService(Context context)
	{
		this.context = context;
		this.activity = (Activity) context;
		main = (MainActivity)activity;

		((BandiApplication) activity.getApplication()).setCurrentMenu(TAG);

		/*
		 * 접근 로그 저장
		 */
		BandiLog.event(context, TAG);
	
	}

	@SuppressLint({ "SetJavaScriptEnabled", "NewApi" })
	@Override
	public void run()
	{
		BaseActivity.swapTitle(context, R.string.intro_apps);
		BaseActivity.swapLayout(context, R.layout.layout_intro_apps);

		main.repeatListViewInit();

		ImageView icon = (ImageView)activity.findViewById(R.id.imageView_mainIcon);
		icon.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				activity.findViewById(R.id.layout_menu1).performClick();
			}
		});
		FrameLayout controlBg = (FrameLayout)activity.findViewById(R.id.control_bar_bg);
		controlBg.setBackgroundColor(Color.parseColor("#3f3833"));
		WebView introAppsViewer = (WebView) activity.findViewById(R.id.intro_apps_viewer);
		introAppsViewer.setOnTouchListener(new OnTouchListener()
		{

			@Override
		    public boolean onTouch(View v, MotionEvent event)
		    {
		        if (event.getAction() == MotionEvent.ACTION_UP)
		            main.mainScroll.requestDisallowInterceptTouchEvent(false);
		        else
		        	main.mainScroll.requestDisallowInterceptTouchEvent(true);

		        return false;
		    }
		});
		introAppsViewer.loadUrl(Url.FAMILY_SERVICE_HTML);
		introAppsViewer.getSettings().setJavaScriptEnabled(true);
		//introAppsViewer.setBackgroundColor(0x00000000);
		introAppsViewer.setWebViewClient(new WebViewClient()
		{
			@Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
			{
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                activity.startActivity(intent);
                return true;
            }
		});
		
		//** Google Analytics **
		new GoogleAnalytics(this.activity).sendScreenView(TAG);
    	
	}


}
