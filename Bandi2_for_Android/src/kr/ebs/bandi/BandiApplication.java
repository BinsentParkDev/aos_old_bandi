package kr.ebs.bandi;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import kr.ebs.bandi.data.EventData;
import kr.ebs.bandi.data.FavoriteData;
import kr.ebs.bandi.data.TimeData;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.widget.VideoView;

public class BandiApplication extends Application 
{
	static final String TAG = "BandiApplication";
	
	private String sessionUserId = "";
    private String sessionUserName = "";
    private String sessionUserEmail = "";
    private String ssoCookie = "";
    private String sessionPasswd = "";
	private boolean needToUpgrade = false;
	private String sessionId = ""; // 로그 기록용 접속 아이디
	private boolean isFMRadio = true;
	private String facebookId;
	private String facebookName, twitterName;
	private long twitterId;
	private int imageZoneWidth = 0;
	private int imageZoneHeight = 0;	
	private boolean hasFocus = false; // 화면 상단에 있는지 여부	
	private String currentMenu = ""; // 현재 화면의 메뉴
	private EventData eventData, notiData = null;
	public ArrayList<FavoriteData> favoriteData = new ArrayList<FavoriteData>();
	private FavoriteData fData = new FavoriteData();
	private String twitter_accessToken = "";
	private String twitter_accessTokenSecret = "";
	private boolean isFacebookLogin = false;
	private String titleForBoard;
	
	private ArrayList<String> boardLineContents = new ArrayList<String>();
	
	private JSONArray radioData = null;
	private JSONArray iRadioData = null;
	private ArrayList<TimeData> timeData;
	private boolean onairHitInit = true;
	
	private boolean isInit;
	private boolean isClick = false;
	
	/**
	 * Google Analytics
	 */
	private Tracker mTracker;
	
	/**
	 * return Google Analytics Tracker
	 * @return
	 */
	synchronized public Tracker getDefaultTracker() {
	if (mTracker == null) {
		GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
	  		// To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
	    	mTracker = analytics.newTracker(R.xml.global_tracker);
	  	}
	    return mTracker;
	}
	
	
	/**
	 * URL을 통한 실행 시 선택된 채널 값을 저장
	 * @date : 2016-11-08
	 */
	private AudioService.Channel urlSelectedChannel = null;
	
	public void setUrlSelectedChannel(AudioService.Channel channel) {
		urlSelectedChannel = channel;
	}
	
	public AudioService.Channel getUrlSelectedChannel() {
		return urlSelectedChannel;
	}
	
	/**
	 * 불량회원 정보 저장용
	 */
	private HashMap<String, String> badMember = new HashMap<String, String>();
	
	public void clear() 
	{
        this.sessionUserId = "";
        this.sessionUserName = "";
        this.sessionUserEmail = "";
        this.sessionPasswd = "";
        this.ssoCookie = "";
        this.needToUpgrade = false;
    }
	
	public void setNotiData(EventData notiData)
	{
		this.notiData = notiData;
	}
	
	public EventData getNotiData()
	{
		return notiData;
	}
	
	public void setBoardLineContents(ArrayList<String> contents)
	{
		this.boardLineContents = new ArrayList<String>(contents);
	}
	
	public ArrayList<String> getBoardLineContents()
	{
		return boardLineContents;
	}
	
	public void setIsTabClick(boolean isClick)
	{
		this.isClick = isClick;
	}
	
	public boolean getIsTabClick()
	{
		return isClick;
	}
	
	public void setOnairHitInit(boolean init)
	{
		onairHitInit = init;
	}
	
	public boolean getOnairHitInit()
	{
		return onairHitInit;
	}
	
	public void setTitleForBoard(String title)
	{
		titleForBoard = title;
	}
	
	public String getTitleForBoard()
	{
		return titleForBoard;
	}
	
	public void setIsInit(boolean state)
	{
		isInit = state;
	}
	
	public boolean getIsInit()
	{
		return isInit;
	}
	
	public void setIsFacebookLogin(boolean state)
	{
		isFacebookLogin = state;
	}
	
	public boolean isFacebookLogin()
	{
		return isFacebookLogin;
	}
	
	public boolean isTwitterLogin()
	{
		return twitter_accessToken.equals("") == true ? false : true;
	}
	
	public void setTwitterAccessToken(String token)
	{
		twitter_accessToken = token;
	}
	
	public String getTwitterAccessToken()
	{
		return twitter_accessToken;
	}
	
	public void setTwitterAccessTokenSecret(String secret)
	{
		twitter_accessTokenSecret = secret;
	}
	
	public String getTwitterAccessTokenSecret()
	{
		return twitter_accessTokenSecret;
	}
	
	public void setTwitterId(long id)
	{
		twitterId = id;
	}
	
	public long getTwitterId()
	{
		return twitterId;
	}
	
	/**
	 * FM 방송 선택여부 설정 (true: FM, false: i-radio)
	 * @param state
	 */
	public void setIsFMRadio(boolean state)
	{
		isFMRadio = state;
	}
	
	/**
	 * FM 인지 i-radio 인지 확인 (true: FM, false: i-radio)
	 * @return
	 */
	public boolean getIsFMRadio()
	{
		return isFMRadio;
	}
	
	String viewName;
	public void setLoginPrevView(String viewName)
	{
		this.viewName = viewName;
	}
	
	public String getLoginPrevView()
	{
		return viewName;
	}
	
	public void setEventData(EventData data)
	{
		eventData = data;
	}
	
	public EventData getEventData()
	{
		return eventData;
	}
	
	public boolean isLogin() 
	{
		return ("".equals(this.sessionUserId)? false : true);
	}
	
	public void setFacebookName(String name)
	{
		facebookName = name;
	}
	
	public String getFacebookName()
	{
		return facebookName;
	}
	
	public void setTwitterName(String name)
	{
		twitterName = name;
	}
	
	public String getTwitterName()
	{
		return twitterName;
	}
	
	public void setFacebookId(String id)
	{
		facebookId = id;
	}
	
	public String getFacebookId() 
	{
		return facebookId;
	}
	
	public void setUserId(String userId) { this.sessionUserId = userId; }
	public void setUserName(String userName) { this.sessionUserName = userName; }
	public void setUserEmail(String userEmail) { this.sessionUserEmail = userEmail; }
	public void setSsoCookie(String cookie) { this.ssoCookie = cookie; }
	public void setUserPasswd(String passwd) { this.sessionPasswd = passwd; }
	
	public String getUserId() { return this.sessionUserId; }
	public String getUserName() { return this.sessionUserName; }
	public String getUserEmail() { return this.sessionUserEmail; }
	public String getSsoCookie() { return this.ssoCookie; }
	public String getUserPasswd() { return this.sessionPasswd; }
	
	public void setNeedToUpgrade(boolean flag) { this.needToUpgrade = flag;	}
	public boolean getNeedToUpgrade() { return this.needToUpgrade; }
	
	public void setSessionId(String sno) { this.sessionId = sno; }
	public String getSessionId() {return this.sessionId; }
	
	public void setNowFavoriteData(FavoriteData data)
	{
		fData = data;
	}
	
	public FavoriteData getNowFavoriteData()
	{
		return fData;
	}
	
	public void setProgImageSize(int width, int height) 
	{
		this.imageZoneWidth = width;
		this.imageZoneHeight = height;
	}
	
	public int[] getProgImageSize() 
	{
		int i[] = {imageZoneWidth, imageZoneHeight};
		return i;
	}
	
	/**
	 * 서버 API 를 통해 사용자의 즐겨찾기 프로그램 목록을 받아온다. <br>
	 * json 형식으로 받아온다.
	 * @param callback
	 * @deprecated retrieveFavoriteData(Context, APIWork.APICallback) 으로 대체
	 */
	public void favoriteDataRequest(AjaxCallback<JSONObject> callback)
	{
		//http://s-home.ebs.co.kr/bandi/bandiAppUserFbmkList?userId=아이디&fileType=json
		String url = "http://home.ebs.co.kr/bandi/bandiAppUserFbmkList";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", sessionUserId);
		params.put("fileType", "json");
		AQuery aq = new AQuery(this);
		aq.ajax(url, params, JSONObject.class, callback);
	}
	
	
	/**
	 * favoriteDataRequest(AjaxCallback<JSONObject> callback) 대체용
	 * @param callback
	 */
	public void retrieveFavoriteData(Context context, APIWork.APICallback callback)
	{
		APIWork work = new APIWork(context);
		work.doThreadJob(Url.FAVORITE_LIST_JSON(sessionUserId), null, callback);
	}
	
	
	/**
	 * JSON 형식의 String 을 받아서 Application 레벨에서 저장한다.
	 * @param jsonStr
	 */
	public boolean setFavoriteData(String jsonStr)
	{
		boolean isSet = true; // 저장 결과 
		try
		{
			JSONObject jObject = new JSONObject(jsonStr);
			
			this.getFavoriteList().clear();
			JSONObject result = jObject.getJSONObject("appUserFbmkLists");
			JSONArray jArray = result.getJSONArray("appUserFbmkLists");

			for(int i = 0; i < jArray.length(); i++)
			{
				FavoriteData data = new FavoriteData();
				data.setFbmkSno(jArray.getJSONObject(i).getInt("fbmkSno"));
				data.setProgramID(jArray.getJSONObject(i).getString("courseId"));
				data.setTitle(jArray.getJSONObject(i).getString("bandiTitle"));
				this.getFavoriteList().add(data);
			}
		}
		catch(JSONException e)
		{
			isSet = false;
			Log.d(TAG, "setFavoriteData error - " + e.getMessage());
		}
		
		return isSet;
	}
	
	
	public void setFavoriteList(ArrayList<FavoriteData> data)
	{
		favoriteData = new ArrayList<FavoriteData>(data);
	}
	
	public ArrayList<FavoriteData> getFavoriteList()
	{
		return favoriteData;
	}
	
	public void setFocusState(boolean hasFocus) 
	{
		this.hasFocus = hasFocus;
	}
	
	public boolean getFocusState() 
	{
		return this.hasFocus;
	}
	
	/**
	 * 현재 화면에 보여지는 메뉴의 클래스명 저장
	 * @param className
	 */
	public void setCurrentMenu(String className) 
	{
		Log.d("TEST", "" + className);
		this.currentMenu = className;
	}
	
	/**
	 * 현재 보여지는 메뉴 (좌측 슬라이딩 메뉴 중 하나)
	 * @return
	 */
	public String getCurrentMenu() 
	{
		return currentMenu;
	}
	
	/**
	 * FM 편성 데이타 셋팅
	 * @param data
	 */
	public void setRadioData(JSONArray data)
	{
		radioData = data;
	}
	
	/**
	 * i-Radio 편성 데이타 셋팅
	 * @param data
	 */
	public void setIRadioData(JSONArray data)
	{
		iRadioData = data;
	}
	
	/**
	 * FM 편성 데이타 반환
	 * @return
	 */
	public JSONArray getRadioData()
	{
		return radioData;
	}
	
	/**
	 * i-Radio 편성 데이타 반환
	 * @return
	 */
	public JSONArray getIRadioData()
	{
		return iRadioData;
	}

	
	private String streamUrlAndroid = "";
	private String streamUrlAndroid2 = "";
	private String streamUrlAndroidVod = "";
	private String streamUrlIradioAndroid = "";
	private String streamUrlIradioAndroid2 = "";
	
	/**
	 * FM : iOS용 HLS (audio only) 스트리밍용 URL
	 */
	private String streamUrlFmHLS = "";
	
	/**
	 * i-Radio : iOS용 HLS (audio only) 스트리밍용 URL
	 */
	private String streamUrlIRadioHLS = "";
	
	
	/**
	 * 첫번째 파라메터에 값이 없으면, 두번째 파라메터 값을 반환한다.
	 * @param first
	 * @param second
	 * @return
	 */
	private String getProper(String first, String second)
	{
		return ( (first == null || "".equals(first)) ? second : first );
	}
	
	/**
	 * HLS (iOS용) fm 스트리밍 (audio only) URL 설정
	 * @param url
	 */
	public void setStreamUrlFmHLS(String url)
	{
		this.streamUrlFmHLS = url;
	}
	
	/**
	 * HLS (iOS용) i-radio 스트리밍(audio only) URL 설정
	 * @param url
	 */
	public void setStreamUrlIRadioHLS(String url)
	{
		this.streamUrlIRadioHLS = url;
	}
	
	/**
	 * RTSP 스트리밍 URL 설정 (FM)
	 * @param url
	 */
	public void setStreamUrlAndroid(String url)
	{
		streamUrlAndroid = url;
	}
	
	/**
	 * RTSP 스트리밍 URL 반환 (FM 용)
	 * @return
	 */
	public String getStreamUrlAndroid()
	{
		return getProper(streamUrlAndroid, Url.fmAudioStreamRtsp);
	}
	
	/**
	 * FM HLS 스트리밍 설정 (FM)
	 * @return
	 */
	public void setStreamUrlAndroid2(String url)
	{
		streamUrlAndroid2 = url;
	}
	
	/**
	 * FM HLS 스트리밍 반환 (FM) <br/>
	 * 롤리팝 이상일 때는 iOS 용 HLS 스트리밍을 사용한다. <br/>
	 * 안드로이드용 HLS 는 동영상이 포함된 버전.
	 * 
	 * @return
	 */
	public String getStreamUrlAndroid2()
	{
		String fmHlsUrl = streamUrlAndroid2; // 동영상 스트리밍
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			fmHlsUrl = this.streamUrlFmHLS;
		}
		return getProper(fmHlsUrl, Url.fmAudioStreamHttp);
	}
	
	/**
	 * FM 보이는 라디오 URL 설정	
	 * @param url
	 */
	public void setStreamUrlAndroidVod(String url)
	{
		streamUrlAndroidVod = url;
	}
	
	/**
	 * FM 보이는 라디오 (rtsp)
	 * @return
	 */
	public String getStreamUrlAndroidVod()
	{
		return getProper(streamUrlAndroidVod, Url.fmVideoStreamRtsp);
	}
	
	/**
	 * RTSP 스트리밍 URL 설정 (i-radio)
	 * @param url
	 */
	public void setStreamUrlIradioAndroid(String url)
	{
		streamUrlIradioAndroid = url;
	}
	
	/**
	 * RTSP 스트리밍 URL 반환 (i-radio)
	 * @return
	 */
	public String getStreamUrlIradioAndroid()
	{
		return getProper(streamUrlIradioAndroid, Url.iRadioAudioStreamRtsp);
	}
	
	/**
	 * i-Radio HLS 스트리밍 셋팅 (i-radio)
	 * @param url
	 */
	public void setStreamUrlIradioAndroid2(String url)
	{
		streamUrlIradioAndroid2 = url;
	}
	
	/**
	 * i-Radio HLS 스트리밍 반환 (i-radio)
	 * @param url
	 */
	public String getStreamUrlIradioAndroid2()
	{
		String iRadioHlsUrl = streamUrlIradioAndroid2; // 동영상 스트리밍
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			iRadioHlsUrl = this.streamUrlIRadioHLS;
		}
		return getProper(iRadioHlsUrl, Url.iRadioAudioStreamHttp);
	}
	
	public void setVideoView(VideoView view)
	{
		view.setVideoPath(getStreamUrlAndroid());
		view.requestFocus();
		view.start();
	}
	
	/**
	 * 현재 로그인 한 ID가 불량회원으로 등록되 아이디인지 확인
	 * @return
	 */
	public boolean isBadMember() {
		return isBadMember(null);
	}
		
	/**
	 * 현재 로그인 한 ID의 불량회원 여부를 확인하고,<br> 
	 * 불량회원일 경우 경고 메세지 노출
	 * @param context
	 * @return
	 */
	public boolean isBadMember(final Context context) 
	{
		boolean result = false;
		Log.d(TAG, "update isBadMember Called.");
		HashMap<String, String> map = null;
		if (isLogin()) {
			map = this.getBadMemberInfoCached(getUserId(), "", "");
		} else if (isFacebookLogin()) {
			map = this.getBadMemberInfoCached("", "001", getFacebookId());
		} else if (isTwitterLogin()) {
			map = this.getBadMemberInfoCached("", "002", String.format("%d", getTwitterId()));
		}
		
		if (map.containsKey("result") && "Y".equals(map.get("result"))) {
			result = true;
			if (context != null ) {
				final String msg = map.get("badMmbExp");
				Activity activity = (Activity) context;
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Dialog.showAlert(context, msg);
					}
				});
			}		
		}
		
		return result;
	}
	
	/**
	 * 불량회원 정보를 반환 (캐시가 있으면 직접 access 안하고 캐시를 반환)
	 * @param userId
	 * @param snsDsCd
	 * @param snsUserId
	 * @return
	 */
	private HashMap<String, String> getBadMemberInfoCached(String userId, String snsDsCd, String snsUserId)
	{
		HashMap<String, String> map = new HashMap<String, String>();
		Log.d(TAG, "update getBadMemberInfoCached - " + badMember);
		if (badMember != null && badMember.size() > 0
				&& badMember.containsKey("userId") && badMember.containsKey("snsDsCd") && badMember.containsKey("snsUserId")
				&& badMember.get("userId").equals(userId) 
				&& badMember.get("snsDsCd").equals(snsDsCd) && badMember.get("snsUserId").equals(snsUserId))
		{
			// 1분 내 정보만 사용
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA);
				Date update = sdf.parse(badMember.get("update"));
				
				Calendar cal = DateUtil.getKoreanCalendar();
				cal.add(Calendar.MINUTE, -1); // update 보다 작아야 한다.
				
				Log.d(TAG, "update : " + update + " / now - 1min :" + cal.getTime());
				if (cal.getTime().before(update)) return badMember;
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if (isLogin()) {
			map = Url.getBadMemberInfo(getUserId());
		} else if (isFacebookLogin()) {
			map = Url.getBadMemberInfo("001", getFacebookId());
		} else if (isTwitterLogin()) {
			map = Url.getBadMemberInfo("002", String.format("%d", getTwitterId()));
		}
		
		this.badMember = map;
		
		return map;
	}
	
	/**
	 * 반디의 세션을 종료한다. (EBS.co.kr)
	 */
	public void logout()
	{
		this.clear();
	}
	
}
