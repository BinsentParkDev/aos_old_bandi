package kr.ebs.bandi;

/**
 * 프로그램 데이타를 받아온다.
 * http://home.ebs.co.kr/bandi/info/[PROGCD].txt
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import kr.ebs.bandi.data.CProgramData;
import kr.ebs.bandi.data.EventData;

import org.json.JSONException;
import org.json.JSONObject;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

public class ProgramData 
{
    static final String TAG = "ProgramData"; // for Log
    private final String URL = "http://home.ebs.co.kr/bandi/info/[PROGCD].txt";
    //private final String URL = "http://to302.phps.kr/b/10009931.txt";
    private Context context;
    private Activity activity;
    private String progcd = null;
    private String urlStr;
    private HashMap<String, String> programData;
    private String programDataStr = "";
    private File dataFile;
    
    /**
     * @deprecated 
     * 태크 유효성 체크 사용하지 않음. (적용 시 버전관리 어려움)
     */
    private String[] tags = {"TITLE","MC","LINK_1","LINK_2","IMG_1","IMG_2","VFM","AIR","SCROLL","SCROLL_LINK","CONTENTS","TWITTER","APP_IMG","APP_IMG2","USE_BOARD_YN","END"};
    
    private final long dataLifeTime = (1 * 60 * 60 * 1000); // 1시간 : 로컬 데이타 유지 시간
    private final long programImageLifeTime =  (12 * 60 * 60 * 1000); // 12시간 : 프로그램 이미지 유지시간.
    
    // program data tags.
    public static final String TITLE        = "TITLE";
    public static final String MC           = "MC";
    public static final String LINK_1       = "LINK_1";
    public static final String LINK_2       = "LINK_2";
    public static final String IMG_1        = "IMG_1";
    public static final String IMG_2        = "IMG_2";
    public static final String VFM          = "VFM";
    public static final String AIR          = "AIR";
    public static final String SCROLL       = "SCROLL";
    public static final String SCROLL_LINK  = "SCROLL_LINK";
    public static final String CONTENTS     = "CONTENTS";
    public static final String TWITTER      = "TWITTER";
    public static final String FACEBOOK      = "FACEBOOK";
    public static final String APP_IMG      = "APP_IMG";
    public static final String APP_IMG2     = "APP_IMG2";
    public static final String USE_BOARD_YN = "USE_BOARD_YN";
    public static final String END          = "END";

    /////////////////// 제거 예정..... ////////////
 // 이건 왜 만든거야??? 2015. 6. 19 현재로선 사용하는데도 없는데..
    private volatile static ProgramData single;
    public static ProgramData getInstance() 
    {
        if (single == null) 
        {
            synchronized(ProgramData.class) 
            {
                if (single == null) 
                {
                    single = new ProgramData();
                }
            }
        }
        return single;
    }
	private ProgramData(){}
	///////////// 제거 예정 /////////////////////
	
    public ProgramData(Context context) 
    {
    	this.context = context; // 생성자 호출.
    	this.activity = (Activity) context;
    }
    
    String nowProgramCode;
    String tempProgramCode;
    boolean isSameCode = false;
    
    public ProgramData(Context context, String progcd) 
    {
        new ProgramData(context);
        
        this.progcd = progcd;
        nowProgramCode = progcd;
        
        if(nowProgramCode.equals(tempProgramCode) == false)
        {
        	isSameCode = false;
        	Preferences prefs = new Preferences(context);
        	prefs.removeProgramData();
        }
        else
        {
        	isSameCode = true;
        }
        
        tempProgramCode = nowProgramCode;
        
        this.urlStr = this.URL.replaceAll("\\[PROGCD\\]", progcd);
        this.dataFile = new File(context.getCacheDir(), progcd +".dat");
    }
    
//    public HashMap<String, String> get() 
//    {
//    	
//        this.parseData();
//        return this.programData;
//    }
    
//    public CProgramData get()
//    {
//    	parseProgramData(new DataParseListener() 
//    	{
//			
//			@Override
//			public void OnParseComplete() 
//			{
//				// TODO Auto-generated method stub
//				
//			}
//		});
//    }
    
    
    /**
     * http connection 을 통해서 정보를 받아온다.
     * 문자열 그대로 반환 
     */
    public String getProgramData() throws Exception 
    {
        URL url = new URL(this.urlStr);
        HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
        StringBuilder sb = new StringBuilder();
        
        try 
        {
            BufferedReader in = new BufferedReader(new InputStreamReader(urlConn.getInputStream(), "euc-kr"), 8192);
            String inStr;

            while ((inStr = in.readLine()) != null) 
            {
                inStr = inStr.trim();
                
              //if (!inStr.matches("\\w")) { //왜 했는지 명확치 않아서.. 제거.. 2014-05-28
                    sb.append(inStr + System.getProperty("line.separator"));
                //}
            }
            
            in.close();
        } 
        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        } 
        finally 
        {
            urlConn.disconnect();
        }
        
        return sb.toString();
    }
    

//    String data;
    
    
    
    
    private ProgramInfoRetriever programInfoRetriever;
    class ProgramInfoRetriever extends Thread
    {
    	String programId;
    	DataParseListener listener;
    	
    	public ProgramInfoRetriever(String programId, final DataParseListener listener)
    	{
    		this.programId = programId;
    		this.listener = listener;
    	}
    	
    	@Override
    	public void run()
    	{
    		String jsonStr = Url.getServerText(Url.PROGRAM_INFO_JSON(this.programId));
//    		Log.d(TAG, Url.PROGRAM_INFO_JSON(this.programId));
//    		Log.d(TAG, "jsonStr : " + jsonStr);
    		CProgramData cData = CProgramData.getInstance();
			try 
			{
				Log.d(TAG, "thread run try");
				JSONObject jObject = new JSONObject(jsonStr);
				
				final JSONObject result = jObject.getJSONObject("resultXml");
				JSONObject data = result.getJSONObject("object");
				cData.setProgramID(data.getString("programId"));
				cData.setProgramTitle(data.getString("title"));
				cData.setViewRadioState(data.getString("vfm"));
//				cData.setViewRadioState("Y");
				cData.setScrollText(data.getString("scroll"));
				cData.setScrollTextUrl(data.getString("scrollLink"));
				cData.setTwitterUrl(data.getString("twitter"));
				cData.setAppImgUrl(data.getString("appImg"));
				cData.setAppImg2Url(data.getString("appImg2"));
				cData.setMcInfo(data.getString("mc"));
				cData.setContents(data.getString("contents"));
				cData.setFacebookUrl(data.getString("facebookUrl"));
				cData.setKakaoUrl(data.getString("kakaostoryUrl"));
				cData.setMobilePageUrl(data.getString("mobHmpUrl"));
				cData.setMobileReplayUrl(data.getString("mobReplayUrl"));
				cData.setAppUseBoardYn(data.getString("appUseBoardYn"));
//				cData.setAppUseBoardYn("Y");

				activity.runOnUiThread(new Runnable()
				{
					@Override
					public void run() {
						try {
							listener.OnParseComplete(result.getString("resultCd"));
							Log.d(TAG, "OnParseComplete");
						} catch (JSONException e) {
							Log.e(TAG, "listener.OnParseComplete error - " + e.getMessage());
							e.printStackTrace();
						}
					}
				});
			}
			catch (JSONException e) 
			{
				e.printStackTrace();
				Log.d(TAG, "thread run exception - " + e.getMessage());
				
				activity.runOnUiThread(new Runnable()
				{
					@Override
					public void run()
					{
						listener.OnParseError();
					}
				});
			} // end try.. catch..
    	} // end run()
    }
    
    public void setProgramData(String programId, final DataParseListener listner)
    {
    	this.programInfoRetriever = new ProgramInfoRetriever(programId, listner);
    	this.programInfoRetriever.start();
    	Log.d(TAG, "setProgramData");
    }
    
    /**
     * 호출되는 곳 없음
     * @param id
     * @param listener
     * @deprecated - 2017-05-15 현재 사용되는 곳 없음.
     */
    public void setProgramData2(String id, final DataParseListener listener)
    {
    	String url = "http://home.ebs.co.kr/bandi/bandiProgInfo";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("programId", id);
		params.put("fileType", "json");
		AQuery aq = new AQuery(context);
		
		
		aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
		{
			@Override
			public void callback(String url, JSONObject object, AjaxStatus status) 
			{
				CProgramData cData = CProgramData.getInstance();
				try 
				{
					JSONObject result = object.getJSONObject("resultXml");
					JSONObject data = result.getJSONObject("object");
					cData.setProgramID(data.getString("programId"));
					cData.setProgramTitle(data.getString("title"));
					cData.setViewRadioState(data.getString("vfm"));
//					cData.setViewRadioState("Y");
					cData.setScrollText(data.getString("scroll"));
					cData.setScrollTextUrl(data.getString("scrollLink"));
					cData.setTwitterUrl(data.getString("twitter"));
					cData.setAppImgUrl(data.getString("appImg"));
					cData.setAppImg2Url(data.getString("appImg2"));
					cData.setMcInfo(data.getString("mc"));
					cData.setContents(data.getString("contents"));
					cData.setFacebookUrl(data.getString("facebookUrl"));
					cData.setKakaoUrl(data.getString("kakaostoryUrl"));
					cData.setMobilePageUrl(data.getString("mobHmpUrl"));
					cData.setMobileReplayUrl(data.getString("mobReplayUrl"));
					cData.setAppUseBoardYn(data.getString("appUseBoardYn"));
//					cData.setAppUseBoardYn("Y");
					listener.OnParseComplete(result.getString("resultCd"));
				}
				catch (JSONException e) 
				{
					listener.OnParseError();
				}
				super.callback(url, object, status);
			}
		});
    }
    
//    public void getData(String id, final DataParseListener listener)
//    {
//    	setProgramData(id, new ProgramDataListener() 
//    	{
//			
//			@Override
//			public void OnProgramDataSetComplete() 
//			{
//				CProgramData cData = CProgramData.getInstance();
//				try 
//				{
//					JSONObject data = new JSONObject(prefs.getProgramData());
//					JSONObject result = data.getJSONObject("resultXml");
//					JSONObject object = result.getJSONObject("object");
//					cData.setProgramID(object.getString("programId"));
//					cData.setProgramTitle(object.getString("title"));
//					cData.setViewRadioState(object.getString("vfm"));
//					cData.setScrollText(object.getString("scroll"));
//					cData.setScrollTextUrl(object.getString("scrollLink"));
//					cData.setTwitterUrl(object.getString("twitter"));
//					cData.setAppImgUrl(object.getString("appImg"));
//					cData.setAppImg2Url(object.getString("appImg2"));
//					cData.setMcInfo(object.getString("mc"));
//					cData.setContents(object.getString("contents"));
//					cData.setKakaoUrl(object.getString("kakaostoryUrl"));
//					cData.setMobilePageUrl(object.getString("mobHmpUrl"));
//					cData.setMobileReplayUrl(object.getString("mobReplayUrl"));
//					listener.OnParseComplete();
//				}
//				catch (JSONException e) 
//				{
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		});
//    }

    public interface ProgramDataListener
    {
    	public void OnProgramDataSetComplete();
    }
    
    public interface DataParseListener
    {
    	public void OnParseComplete(String resultCode);
    	public void OnParseError();
    }
    
//    private boolean makeDataFile() 
//    {
//        boolean result = false;
//        
//        try 
//        {
//            boolean isExpired = true;
//            
//            if (dataFile.exists())
//            {
//                long now = (new Date()).getTime();
//                long mod = (new Date(dataFile.lastModified())).getTime();
//                
//                long limitLifeTime = dataLifeTime;
//                long lifeTime = now - mod;
//                Log.d(TAG, "s1: lifeTime : " + lifeTime );
//                
//                if (lifeTime > limitLifeTime) 
//                {
//                    dataFile.delete();
//                } 
//                else
//                {
//                    isExpired = false;
//                }
//                Log.d(TAG, "s1: isExpired = " + isExpired+ " exists: "+ dataFile.exists());
//                
//            }
//            
//            if (isExpired) 
//            {
//                FileWriter fw = new FileWriter(dataFile);
//                Thread thread = new Thread(new Runnable() 
//                {
//                    public void run() 
//                    {
//                        try 
//                        {
//                            programDataStr = getProgramData();
//                        } 
//                        catch (Exception e) 
//                        {
//                            // pass
//                        }
//                    }
//                });
//                thread.start();
//                try 
//                {
//                    thread.join();
//                } 
//                catch (InterruptedException e) 
//                {
//                    // pass
//                } 
//                catch (Exception e) 
//                {
//                    // pass
//                }
//                
//                fw.write(programDataStr);
//                fw.close();
//            }
//        
//            result = true;
//        }
//        catch (Exception e)
//        {
//            result = false;
//            Log.e(TAG, "makeDataFile is failure.");
//        }
//        
//        return result;
//    }
    
//    private void parseData()
//    {
//        HashMap<String, String> hmap = new HashMap<String, String>();
//        
//        String key = "";
//        StringBuilder val = new StringBuilder();
//        try 
//        {
//            this.makeDataFile();
//            
//            //BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(this.dataFile), "utf-8"), 8192);
//            BufferedReader in = new BufferedReader(new FileReader(this.dataFile), 8192);
//            String inStr;
//            
//            while ((inStr = in.readLine()) != null) 
//            {
//                inStr = inStr.trim();
//                Log.d(TAG, "s3 >"+ inStr );
//                //if (inStr.matches("^\\[.*\\]$") && inTags(inStr.replace("[", "").replace("]", "").trim())) {
//                if (inStr.matches("^\\[.*\\]$") ) 
//                {	
//                    if (!"".equals(key)) 
//                    {
//                        hmap.put(key, val.toString());
//                        val = new StringBuilder();
//                    }
//                    key = inStr.replace("[", "").replace("]", "").trim();
//                } 
//                else 
//                {
//                    val.append(inStr + System.getProperty("line.separator"));
//                    //Log.d(TAG, "s3 >"+ key + ":" + val.toString());
//                }
//                
//            }
//            
//            in.close();
//            
//        } 
//        catch(Exception e)
//        {
//            Log.e(TAG, "cat't parse data file." + e.getMessage());
//            e.printStackTrace();
//            this.dataFile.delete();
//        }
//        
//        // 반디 신규 앱에 적용될 이미지 사이즈.
//        if (! hmap.containsKey("APP_IMG2")) 
//        {
//        	Log.d(TAG, "app_img2 does not exists");
//        	hmap.put("APP_IMG2", "http://home.ebs.co.kr/bandi/info/"+ this.progcd +"_4.jpg");
//        }
//        
//        this.programData = hmap;
//    }
    
    /**
     * 프로그램 파일 제공.
     * 캐시 유지 시간 12시간.
     * @param url
     * @return
     */
    public Bitmap getProgramImage(String url)
    {
        Bitmap bm = null;
        boolean isExpired = true;
        
        try 
        {
	        String[] tmp = url.trim().split("\\/");
	        String filename = tmp[tmp.length-1];
	        
	        File imgFile = new File(context.getCacheDir(), filename);
	        if (imgFile.exists()) 
	        {
	            Log.i("TEST", "Make program image file.");
	            long now = (new Date()).getTime();
	            long mod = (new Date(imgFile.lastModified())).getTime();
	            
	            long limitLifeTime = programImageLifeTime; 
	            long lifeTime = now - mod;
	            
	            if (lifeTime > limitLifeTime) 
	            {
	                imgFile.delete();
	            } 
	            else
	            {
	                isExpired = false;
	            }
	        }
	       
	        // 신규 이미지 적용 실패 시 기존 이미지 로딩 로직 적용. 2013-11-13
	        if (isExpired) 
	        {
	            if (! getRemoteFile(url, context.getCacheDir(), filename)) 
	            {
	            	getRemoteFile(programData.get("APP_IMG"), context.getCacheDir(), filename);
	            }
	        }
	        
	        bm = BitmapFactory.decodeFile(context.getCacheDir().toString() + File.separator + filename);
	        
	        //** 이미지 자르기 추가 (2013.11.12)
	        Activity activity = (Activity) context;
	        int size[] = ((BandiApplication) activity.getApplication()).getProgImageSize();
	        Log.d(TAG, "size : " + size[0] + "," + size[1]);
			if (size[0] > 0 && size[1] > 0) 
			{
				int properHeight = (bm.getWidth() * size[1]) / size[0];
				int offsetY = 0;
				if (bm.getHeight() > properHeight ) 
				{
					offsetY = bm.getHeight() - properHeight;
					bm = Bitmap.createBitmap(bm, 0, offsetY, bm.getWidth(), properHeight);
					Log.d(TAG, "image cropped.");
				}
			}
			//** 이미지 자르기 추가 **/
			
        } catch(Exception e) {
        	e.printStackTrace();
        }
        
        return bm;
    }
    
    
    /**
     * 서버 파일 다운로드 
     */
    static boolean getRemoteFile(String url, File saveDir, String filename)
    {
    	File file;
    	InputStream in;
    	FileOutputStream out;
        try {
            file = new File(saveDir, filename);
            
            in = new URL(url).openStream();
            out = new FileOutputStream(file);
                        
            byte[] buf = new byte[10240];
            int length;
            while((length = in.read(buf)) > 0) {
                out.write(buf, 0, length);
                //Log.i("ProgramData", ">>"+length);
            }
            
            in.close();
            out.close();
            
            return true;
            
        } catch(Exception e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
            return false;
        }
    }
    
    /**
     * @deprecated [] 규칙으로 태그 확인하기로 바꿈.
     * @param str
     * @return
     */
    private boolean inTags(String str) {
        for (int i=0; i<this.tags.length; i++) {
            if (str.equals(tags[i])) return true;
        }
        return false;
    }
    
    /**
     * 프로그램 이미지 다운로드 작업 비동기화 위해 추가
     * @since 2012. 4. 9.
     * @param view
     * @param url
     */
    public void setProgramImage(ImageView view, String url)
    {
        if (url != null && !"".equals(url)) 
        {
        	AQuery aq = new AQuery(context);
        	aq.id(view).image(url);
//            new DownloadImageTask(view).execute(url);
        }
    }
    
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        private ImageView imageView;
        
        public DownloadImageTask(ImageView view) {
            this.imageView = view;
        }
        
        @Override
        protected Bitmap doInBackground(String... urls) {
            return getProgramImage(urls[0]);// url
        }
        
        @Override
        protected void onPostExecute(Bitmap result) {
            this.imageView.setImageBitmap(result);
        }
        
    }
    
    
}
