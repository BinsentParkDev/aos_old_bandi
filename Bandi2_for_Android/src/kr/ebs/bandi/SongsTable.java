package kr.ebs.bandi;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

import kr.ebs.bandi.ProgramData.DataParseListener;

import org.w3c.dom.Document;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class SongsTable implements BaseInterface {
	static final String TAG = "SongsTable";
	private Context context;
	private Activity activity;

	public SongsTable(Context context) {
		this.context = context;
		this.activity = (Activity) context;

		((BandiApplication) activity.getApplication()).setCurrentMenu(TAG);

		/*
		 * 접근 로그 저장
		 */
		BandiLog.event(context, TAG);
	}

	@Override
	public void run() {
//		BaseActivity.swapTitle(context, R.string.songs_table);
//		BaseActivity.swapLayout(context, R.layout.layout_songs_table);
//
//		TextView programName = (TextView) activity.findViewById(R.id.program_name);
//		WebView songsTableViewer = (WebView) activity.findViewById(R.id.songs_table_viewer);
//
//		TimeTable tt = new TimeTable(context);
//		HashMap <String, String> tmap = tt.get();
//
//		if (tmap == null) return;
//		Log.d("TEST", "test 5 ");
//		ProgramData pd = new ProgramData(context);
////		HashMap <String, String> pmap = pd.get();
//		pd.getData(new DataParseListener()
//		{
//
//			@Override
//			public void OnParseComplete()
//			{
//				// TODO Auto-generated method stub
//
//			}
//		});
//
//		programName.setText(tmap.get(TimeTable.PROGNAME));
//		songsTableViewer.loadDataWithBaseURL(null, getSongsTableDoc(tmap.get(TimeTable.PROGCD)), "text/html", "utf-8", null);
//		songsTableViewer.setBackgroundColor(0x00000000);
//		songsTableViewer.setWebViewClient(new WebViewClient(){
//			@Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//				view.loadUrl(url);
//	            return true;
//            }
//		});
//
//

	}

	private String getSongsTableDoc(String progcd) {
		String html = "";
		try {
			Document doc = Url.getRemoteDoc(Url.SONGS_TABLE + progcd);
			String rescd = doc.getElementsByTagName("rescd").item(0).getTextContent();
			if (rescd.equals("9999")) {
				html = "<p>선곡 내용이 없습니다.</p>";
			} else {
				html = doc.getElementsByTagName("bandi-contents").item(0).getTextContent();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return html;
	}


}
