package kr.ebs.bandi;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

/**
 * @deprecated - 사용하는 곳이 발견되지 않음.
 * 
 * @since 2016. 7. 13.
 */
public class VideoServiceConnection implements ServiceConnection
{

	private volatile static VideoServiceConnection single;
	private FrameLayout playBtn = null;
	private ProgressBar loadingImage = null;
	
	public static VideoServiceConnection getInstance() 
	{
		if (single == null) 
		{
			synchronized (VideoServiceConnection.class) 
			{
				if (single == null) 
				{
					single = new VideoServiceConnection();
				}
			}
		}
		return single;
	}
	
	private VideoService mService;
	private boolean isBound=false;
	@Override
	public void onServiceConnected(ComponentName name, IBinder service)
	{
		// TODO Auto-generated method stub
		VideoService.ServiceBinder binder = (VideoService.ServiceBinder) service;
		mService = binder.getService();
		isBound = true;
		if (playBtn != null && loadingImage != null) 
		{
			
		}

		mService.play();
	}

	@Override
	public void onServiceDisconnected(ComponentName name)
	{
		// TODO Auto-generated method stub
		mService = null;
		isBound = false;
	}
	
	/**
	 * 서비스 바인딩 상태를 반환한다.
	 * @return isBound
	 */
	public boolean isBound() 
	{
		return isBound;
	}

	public VideoService getVideoService() 
	{
		return mService;
	}

	/**
	 * 재생버튼과 접속 로딩 이미지 제어용.
	 * @param playBtn
	 * @param loadingImage
	 */
	public void setPlayLoadingBtn(FrameLayout playBtn, ProgressBar loadingImage)
	{
		this.playBtn = playBtn;
		this.loadingImage = loadingImage;
	}
	
}
