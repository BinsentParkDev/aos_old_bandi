package kr.ebs.bandi;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlarmManager.AlarmClockInfo;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView.FindListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import kr.ebs.bandi.GoogleAnalytics;

public class Setting implements BaseInterface
{
	static final String TAG = "Setting";
	private Context context ;
	private Activity activity;
	private Handler handler = new Handler();
	private Calendar calendar = Calendar.getInstance();
	private Preferences prefs;
	private final HashMap<Integer, FrameLayout> daysOfWeekBtn = new HashMap<Integer, FrameLayout>();
	//private final SparseArray<ImageView> daysOfWeekBtn = new SparseArray<ImageView>();
	private boolean resetAlarm = false; // 기기 리부팅시 알람 재설정 여부 판단용
	FrameLayout radioCheck, iRadioCheck;
	MainActivity main;

	public DisplayMetrics dm;
	public int dpWidth, dpHeight;

	private LinearLayout.LayoutParams lParams;
	private FrameLayout.LayoutParams fParams;
	private RelativeLayout.LayoutParams rParams;

	FrameLayout autoRun;
	
	/**
	 * 로그인 상태 확인
	 */
	BandiApplication bandiApp;
	

	/**
	 * 자동종료용 오디오 서비스 커넥션
	 */
	boolean isBound = false;
	String finishTime = "";
	
	
	private ServiceConnection timerServiceConnection = new ServiceConnection()
	{
        @Override
        public void onServiceConnected(ComponentName name, IBinder service)
        {
        	TimerService.ServiceBinder binder = (TimerService.ServiceBinder) service;
        	TimerService timerService = binder.getService();
        	finishTime = timerService.getFinishTime();
            isBound = true;
            activity.unbindService(this);

        }
        @Override
        public void onServiceDisconnected(ComponentName name)
        {
        	isBound = false;
        }
    };

	String nowTime;
	class AutoFinishUpdater implements Runnable
	{

		public AutoFinishUpdater()
		{

		}

		@Override
		public void run()
		{
			Calendar calendar = Calendar.getInstance();
			Date date = calendar.getTime();
			Log.d(TAG, "getNowDateTime() end");
			nowTime = new SimpleDateFormat("HHmm").format(date);
			handler.postAtTime(this, SystemClock.uptimeMillis() + (1 * 1000));
		}
	}

	/**
	 * AutoRunBootReceiver 에서 호출. 기기 부팅시 알람 재설정용.
	 * @param context
	 * @param prefs
	 */
    public Setting(Context context, Preferences prefs)
    {
    	this.context = context;    	
    	this.prefs = prefs;
    }

    /**
     * UI 상에서 호출
     * @param context
     */
	public Setting(Context context)
	{
		this.context = context;
		this.activity = (Activity) context;
		main = (MainActivity)activity;
		this.prefs = new Preferences(context);
		dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		dpWidth = (int)(dm.widthPixels / dm.density);
		dpHeight = (int)(dm.heightPixels / dm.density);

		((BandiApplication) activity.getApplication()).setCurrentMenu(TAG);
		
		bandiApp = (BandiApplication) activity.getApplication();
		
		/*
		 * 접근 로그 저장
		 */
		BandiLog.event(context, TAG);
		
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public void run()
	{
		BaseActivity.swapTitle(context, R.string.setting);
		BaseActivity.swapLayout(context, R.layout.layout_setting);
		
		setListener();

		// 최신 버전 확인
		try
		{
			TextView appVersion = (TextView) activity.findViewById(R.id.app_version_text);
			appVersion.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(20));
			PackageInfo pi= context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			appVersion.setText("Ver. " + pi.versionName);

			TextView latestVersion = (TextView) activity.findViewById(R.id.latest_version_text);
			latestVersion.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15));
			boolean needToUpgrade = ((BandiApplication) activity.getApplicationContext()).getNeedToUpgrade();

			if (needToUpgrade) latestVersion.setText( "업그레이드 버전이 있습니다." );
			else latestVersion.setText( "최신 버전입니다." );
		}
		catch(Exception e)
		{
			// pass
		}
		
		//** Google Analytics **
		new GoogleAnalytics(this.activity).sendScreenView(TAG);

	}


	@SuppressLint({ "ClickableViewAccessibility", "NewApi" })
	private void setListener()
	{
		lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, getWidthDP(42));
		FrameLayout title0 = (FrameLayout)activity.findViewById(R.id.layout_settingTitle0);
		title0.setPadding(getWidthDP(10), 0, 0, 0);
		title0.setLayoutParams(lParams);

		FrameLayout title1 = (FrameLayout)activity.findViewById(R.id.layout_settingTitle1);
		title1.setPadding(getWidthDP(10), getWidthDP(8), getWidthDP(10), getWidthDP(8));

		lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lParams.setMargins(getWidthDP(10), getWidthDP(10), getWidthDP(10), 0);
		TextView networkNoti = (TextView)activity.findViewById(R.id.textView_networkNoti);
		networkNoti.setLayoutParams(lParams);
		networkNoti.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(13));

		lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, getWidthDP(42));
		lParams.setMargins(0, getWidthDP(10), 0, 0);
		FrameLayout title2 = (FrameLayout)activity.findViewById(R.id.layout_settingTitle2);
		title2.setPadding(getWidthDP(10), 0, 0, 0);
		title2.setLayoutParams(lParams);

		RelativeLayout title3 = (RelativeLayout)activity.findViewById(R.id.layout_settingTitle3);
		title3.setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));

		lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, getWidthDP(42));
		lParams.setMargins(0, getWidthDP(10), 0, 0);
		FrameLayout title4 = (FrameLayout)activity.findViewById(R.id.layout_settingTitle4);
		title4.setPadding(getWidthDP(10), 0, 0, 0);
		title4.setLayoutParams(lParams);

		lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, getWidthDP(42));
		FrameLayout title5 = (FrameLayout)activity.findViewById(R.id.layout_settingTitle5);
		title5.setPadding(getWidthDP(10), 0, 0, 0);
		title5.setLayoutParams(lParams);

		lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, getWidthDP(42));
		FrameLayout title6 = (FrameLayout)activity.findViewById(R.id.layout_settingTitle6);
		title6.setPadding(getWidthDP(10), 0, 0, 0);
		title6.setLayoutParams(lParams);

		lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, getWidthDP(42));
		FrameLayout title7 = (FrameLayout)activity.findViewById(R.id.layout_settingTitle7);
		title7.setPadding(getWidthDP(10), 0, 0, 0);
		title7.setLayoutParams(lParams);

		lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, getWidthDP(42));
		FrameLayout title8 = (FrameLayout)activity.findViewById(R.id.layout_settingTitle8);
		title8.setPadding(getWidthDP(10), 0, 0, 0);
		title8.setLayoutParams(lParams);

		TextView setting0 = (TextView)activity.findViewById(R.id.textView_setting0);
		TextView setting1 = (TextView)activity.findViewById(R.id.textView_setting1);
		TextView setting2 = (TextView)activity.findViewById(R.id.textView_setting2);
		TextView setting3 = (TextView)activity.findViewById(R.id.textView_setting3);
		TextView setting4 = (TextView)activity.findViewById(R.id.textView_setting4);
		TextView setting5 = (TextView)activity.findViewById(R.id.textView_setting5);
		TextView setting6 = (TextView)activity.findViewById(R.id.textView_setting6);
		TextView setting7 = (TextView)activity.findViewById(R.id.textView_setting7);
		TextView setting8 = (TextView)activity.findViewById(R.id.textView_setting8);
		TextView setting9 = (TextView)activity.findViewById(R.id.textView_setting9);
		TextView setting10 = (TextView)activity.findViewById(R.id.textView_setting10);
		TextView setting11 = (TextView)activity.findViewById(R.id.textView_setting11);
		TextView use_auto_login_text = (TextView)activity.findViewById(R.id.use_auto_login_text);
		TextView login_id_text = (TextView)activity.findViewById(R.id.login_id_text);
		TextView use_secure_login_text = (TextView)activity.findViewById(R.id.use_secure_login_text);
		TextView personal_information_handling_policies_text = (TextView)activity.findViewById(R.id.personal_information_handling_policies_text);
		TextView setting_hls = (TextView)activity.findViewById(R.id.textView_setting_hls);
		TextView tv_explain_hls = (TextView)activity.findViewById(R.id.tv_explain_hls);

		setting0.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(18));
		setting1.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
		setting2.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(18));
		setting3.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(18));
		setting4.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
		setting5.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(18));
		setting6.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(13));
		setting7.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(18));
		setting8.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15));
		setting9.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(13));
		setting10.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(18));
		setting11.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(18));
		use_auto_login_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15));
		login_id_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15));
		use_secure_login_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15));
		personal_information_handling_policies_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15)); 
		setting_hls.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15));
		tv_explain_hls.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(13));

		// 3G/LTE/Wibro 사용 여부
		fParams = new FrameLayout.LayoutParams(getHeightDP(63), getHeightDP(31), Gravity.RIGHT | Gravity.CENTER_VERTICAL);
		final FrameLayout usePaidNetwork = (FrameLayout) activity.findViewById(R.id.use_paid_network_check);
		usePaidNetwork.setLayoutParams(fParams);
		usePaidNetwork.setSelected(prefs.getUsePaidNetwork());
		usePaidNetwork.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				if(v.isSelected() == false)
				{
					AlertDialog.Builder builder = new AlertDialog.Builder(context);
				    builder.setMessage(R.string.use_paid_network_on_msg)
				    .setCancelable(false)
				    .setPositiveButton("확인", new DialogInterface.OnClickListener()
				    {
				    	public void onClick(DialogInterface dialog, int id)
				    	{
				    		dialog.dismiss();
				    		v.setSelected(true);
				    		prefs.putUsePaidNetwork(v.isSelected());
				    	}
				    }).show();
				}
				else
				{
					v.setSelected(!v.isSelected());
					prefs.putUsePaidNetwork(v.isSelected());
				}

				main.audioCheckCount = 0;
            	main.checkAudioState();
			}
		});

		ScrollView scroll = (ScrollView)activity.findViewById(R.id.scroll_setting);
		scroll.setOnTouchListener(new OnTouchListener()
		{
		    @Override
		    public boolean onTouch(View v, MotionEvent event)
		    {
		        if (event.getAction() == MotionEvent.ACTION_UP)
		            main.mainScroll.requestDisallowInterceptTouchEvent(false);
		        else
		        	main.mainScroll.requestDisallowInterceptTouchEvent(true);

		        return false;
		    }
		});

		main.repeatListViewInit();

		lParams = new LinearLayout.LayoutParams(getHeightDP(97), getHeightDP(31));
		FrameLayout autoRunDate = (FrameLayout)activity.findViewById(R.id.layout_settingAutoRunDate);
		autoRunDate.setLayoutParams(lParams);
		/*
		 * 기능명 : 자동 실행
		 * 예약된 시간에 자동으로 앱이 실행되게 하는 기능
		 */

		// 자동 실행 여부 체크
		rParams = new RelativeLayout.LayoutParams(getHeightDP(63), getHeightDP(31));
		rParams.addRule(RelativeLayout.CENTER_VERTICAL);
		rParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		autoRun = (FrameLayout) activity.findViewById(R.id.auto_run_check);
		autoRun.setLayoutParams(rParams);
		autoRun.setSelected(prefs.getAutoRun());
		Log.d(TAG, "getAutoRun() : " + prefs.getAutoRun());
		autoRun.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				v.setSelected(!v.isSelected());
				prefs.putAutoRun(v.isSelected());
//				reserveAutoRun();

				Iterator<Integer> iter = daysOfWeekBtn.keySet().iterator();
				boolean selectMode = true; // 기본적으로 전부 선택하는 것으로 동작
				while(iter.hasNext())
				{
					FrameLayout btn = daysOfWeekBtn.get(iter.next());
					if (btn.isSelected())
					{
						// 하나라도 선택되어 있으면 선택 해지로 동작
						selectMode = false;
					}
				}
				
				
				iter = daysOfWeekBtn.keySet().iterator();
				while(iter.hasNext())
				{
					FrameLayout btn = daysOfWeekBtn.get(iter.next());
					if (selectMode)
					{
						if (!btn.isSelected()) btn.performClick(); // btn.callOnClick(); //<-- api 15 이상.
					}
					else
					{
						if (btn.isSelected()) btn.performClick();
					}
				}
			}
		});

		// 자동 실행 시간 설정 팝업 띄우기
		final TextView autoRunTime = (TextView) activity.findViewById(R.id.auto_run_time_text);
		autoRunTime.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
		final int[] wakeTime = parseTimeString(prefs.getWakeTime());
		autoRunTime.setText(formatTime(wakeTime[0], wakeTime[1]));
		autoRunTime.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				int initHour = 0;
				int initMin = 0;
				if ("".equals(prefs.getWakeTime()))
				{
					initHour = calendar.get(Calendar.HOUR_OF_DAY);
					initMin = calendar.get(Calendar.MINUTE);
				}
				else
				{
					int[] wakeTime1 = parseTimeString(prefs.getWakeTime());
					initHour = wakeTime1[0];
					initMin = wakeTime1[1];
				}
				new TimePickerDialog(context ,new TimePickerDialog.OnTimeSetListener()
				{
						@Override
				        public void onTimeSet(TimePicker view, int hourOfDay, int minute)
						{
							autoRunTime.setText(formatTime(hourOfDay, minute)); // 화면에 시간 노출
							prefs.putWakeTime(hourOfDay+":"+minute); // 설정 저장. (24시간단위로 저장; 예: 13:4, 1:15)
							reserveAutoRun();
				        }
				}, initHour, initMin, false).show();
			}
		});

		lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, getWidthDP(65));
		LinearLayout autoRunDay = (LinearLayout)activity.findViewById(R.id.layout_settingAutoRunDay);
		autoRunDay.setLayoutParams(lParams);

		lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lParams.setMargins(getWidthDP(10), getWidthDP(10), 0, 0);
		TextView autoRunNoti = (TextView)activity.findViewById(R.id.textView_autoRunNoti);
		autoRunNoti.setLayoutParams(lParams);
		autoRunNoti.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));

		// 자동 실행 요일 설정 버튼 (일 ~ 토)
		daysOfWeekBtn.put(Calendar.SUNDAY, (FrameLayout) activity.findViewById(R.id.sunday_btn));
		daysOfWeekBtn.put(Calendar.MONDAY, (FrameLayout) activity.findViewById(R.id.monday_btn));
		daysOfWeekBtn.put(Calendar.TUESDAY, (FrameLayout) activity.findViewById(R.id.tuesday_btn));
		daysOfWeekBtn.put(Calendar.WEDNESDAY, (FrameLayout) activity.findViewById(R.id.wednesday_btn));
		daysOfWeekBtn.put(Calendar.THURSDAY, (FrameLayout) activity.findViewById(R.id.thursday_btn));
		daysOfWeekBtn.put(Calendar.FRIDAY, (FrameLayout) activity.findViewById(R.id.friday_btn));
		daysOfWeekBtn.put(Calendar.SATURDAY, (FrameLayout) activity.findViewById(R.id.saturday_btn));
		final String[] btnColorValue = {"#898077", "#9e9188", "#ac9e93", "#9e9188", "#7d746b", "#898077", "#9e9188"};

		TextView[] dayText = new TextView[7];
		int[] dayTextRes = {R.id.textView_day0, R.id.textView_day1, R.id.textView_day2, R.id.textView_day3, R.id.textView_day4, R.id.textView_day5, R.id.textView_day6};
		for(int i = 0; i < dayTextRes.length; i++)
		{
			dayText[i] = (TextView)activity.findViewById(dayTextRes[i]);
			dayText[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
		}

		class OnDayOfWeekClickListener implements View.OnClickListener
		{
			private Integer dayOfWeek;

			public OnDayOfWeekClickListener(Integer dayOfWeek)
			{
				this.dayOfWeek = dayOfWeek;
			}

			@Override
			public void onClick(View v)
			{
				v.setSelected(!v.isSelected());
				if(v.isSelected() == true)
				{
					v.setBackgroundColor(Color.parseColor("#3f3833"));
					autoRun.setSelected(true);
					prefs.putAutoRun(true);
				}
				else
				{
					v.setBackgroundColor(Color.parseColor(btnColorValue[dayOfWeek - 1]));
				}
				prefs.putDayOfWeek(dayOfWeek, v.isSelected());

				boolean isSelected = false;
				Iterator<Integer> iter = daysOfWeekBtn.keySet().iterator();

				while(iter.hasNext())
				{
					Integer dayOfWeek = iter.next();
					FrameLayout btn = daysOfWeekBtn.get(dayOfWeek);
					if(btn.isSelected()) isSelected = true;
				}

				if(isSelected == false)
				{
					autoRun.setSelected(false);
					prefs.putAutoRun(false);
				}
				reserveAutoRun();
			}
		}

		Iterator<Integer> iter = daysOfWeekBtn.keySet().iterator();

		while(iter.hasNext())
		{
			Integer dayOfWeek = iter.next();
			FrameLayout btn = daysOfWeekBtn.get(dayOfWeek);
			btn.setOnClickListener(new OnDayOfWeekClickListener(dayOfWeek));
			btn.setOnTouchListener(new OnTouchListener()
			{

				@Override
				public boolean onTouch(View v, MotionEvent event)
				{
					if (event.getAction() == MotionEvent.ACTION_UP)
					{
			            main.mainScroll.requestDisallowInterceptTouchEvent(false);
					}
					else
					{
			        	main.mainScroll.requestDisallowInterceptTouchEvent(true);
					}
					return false;
				}
			});
			btn.setSelected(prefs.getDayOfWeek(dayOfWeek)); // 지난 설정 반영.
			dayText[dayOfWeek - 1].setSelected(prefs.getDayOfWeek(dayOfWeek));
			if(prefs.getDayOfWeek(dayOfWeek) == true)
			{
				btn.setBackgroundColor(Color.parseColor("#3f3833"));
			}
			else
			{
				btn.setBackgroundColor(Color.parseColor(btnColorValue[dayOfWeek - 1]));
			}

		}


		/*
		 * 기능명 : 자동 종료
		 * 정해진 시간이 지나면 자동으로 반디 앱이 종료된다.
		 */
		RelativeLayout title9 = (RelativeLayout)activity.findViewById(R.id.layout_settingTitle9);
		title9.setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));

		final TextView countdownText = (TextView) activity.findViewById(R.id.countdown_text);
		countdownText.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
		final FrameLayout autoCloseCheck = (FrameLayout)activity.findViewById(R.id.checkBox_autoClose);

		int finishTimeHour = Integer.parseInt(prefs.getFinishTime().split(":")[0]);
		int finishTimeMinute = Integer.parseInt(prefs.getFinishTime().split(":")[1]);
		countdownText.setText(formatTime(finishTimeHour, finishTimeMinute));

		rParams = new RelativeLayout.LayoutParams(getHeightDP(63), getHeightDP(31));
		rParams.addRule(RelativeLayout.CENTER_VERTICAL);
		rParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		autoCloseCheck.setLayoutParams(rParams);
		autoCloseCheck.setSelected(prefs.getAutoFinishState());
		autoCloseCheck.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				v.setSelected(!v.isSelected());
				prefs.putAutoFinishState(v.isSelected());
				if(v.isSelected())
				{
					Intent intent = new Intent(context, TimerService.class);
					intent.putExtra("finishTime", prefs.getFinishTime());

					activity.startService(intent);
				}
				else
				{
					Intent intent = new Intent(context, TimerService.class);
					activity.stopService(intent);
				}
			}
		});

		FrameLayout title10 = (FrameLayout)activity.findViewById(R.id.layout_settingTitle10);
		title10.setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));

		FrameLayout title11 = (FrameLayout)activity.findViewById(R.id.layout_settingTitle11);
		title11.setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));
		
		FrameLayout title11_secure = (FrameLayout)activity.findViewById(R.id.layout_settingTitle11_secure);
		title11_secure.setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));
		
		FrameLayout layout_settingTitle_privacy = (FrameLayout)activity.findViewById(R.id.layout_settingTitle_privacy);
		layout_settingTitle_privacy.setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));

		FrameLayout title12 = (FrameLayout)activity.findViewById(R.id.layout_settingTitle12);
		title12.setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));
		
		FrameLayout title13 = (FrameLayout)activity.findViewById(R.id.layout_settingTitle13);
		title13.setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));
		
		FrameLayout titleHLS = (FrameLayout)activity.findViewById(R.id.layout_settingHLS);
		titleHLS.setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));

		FrameLayout title14 = (FrameLayout)activity.findViewById(R.id.layout_settingTitle14);
		title14.setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));

		LinearLayout title15 = (LinearLayout)activity.findViewById(R.id.layout_settingTitle15);
		title15.setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));

		RelativeLayout title16 = (RelativeLayout)activity.findViewById(R.id.layout_settingTitle16);
		title16.setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));

		rParams = new RelativeLayout.LayoutParams(getHeightDP(97), getHeightDP(31));
		rParams.addRule(RelativeLayout.CENTER_VERTICAL);
		FrameLayout countdown = (FrameLayout)activity.findViewById(R.id.countdown);
		countdown.setLayoutParams(rParams);
		countdown.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				int initHour = 0;
				int initMin = 0;
				if ("".equals(prefs.getFinishTime()))
				{
					initHour = calendar.get(Calendar.HOUR_OF_DAY);
					initMin = calendar.get(Calendar.MINUTE);
				}
				else
				{
					int[] wakeTime1 = parseTimeString(prefs.getFinishTime());
					initHour = wakeTime1[0];
					initMin = wakeTime1[1];
				}
				new TimePickerDialog(context ,new TimePickerDialog.OnTimeSetListener()
				{
						@Override
				        public void onTimeSet(TimePicker view, int hourOfDay, int minute)
						{
							countdownText.setText(formatTime(hourOfDay, minute)); // 화면에 시간 노출
//							if(hour)
							prefs.putFinishTime(String.format("%02d", hourOfDay)+":"+String.format("%02d", minute)); // 설정 저장. (24시간단위로 저장; 예: 13:4, 1:15)
							if(prefs.getAutoFinishState())
							{
								Intent intent = new Intent(context, TimerService.class);
								intent.putExtra("finishTime", String.format("%02d", hourOfDay)+":"+String.format("%02d", minute));
								activity.startService(intent);
							}

				        }
				}, initHour, initMin, false).show();
			}
		});

//		// 자동종료 : 메뉴 재진입시 TimerService에서 시간을 받아와서 현재 상태 설정
//		Intent i = new Intent(context, TimerService.class);
//		activity.bindService(i, timerServiceConnection, Context.BIND_AUTO_CREATE);


		/*
		 * 자동 로그인 설정 관련
		 */
		fParams = new FrameLayout.LayoutParams(getHeightDP(63), getHeightDP(31), Gravity.RIGHT);
		final FrameLayout useAutoLogin = (FrameLayout) activity.findViewById(R.id.use_auto_login_check);
		useAutoLogin.setLayoutParams(fParams);
		useAutoLogin.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (prefs.getAutoLogin())
				{
					prefs.putAutoLogin(false);
					v.setSelected(false);
					TextView useAutoLoginText = (TextView) activity.findViewById(R.id.use_auto_login_text);
					useAutoLoginText.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15));
					useAutoLoginText.setText(context.getResources().getText(R.string.use_auto_login));
					Toast.makeText(context, "자동로그인이 해제되었습니다.", Toast.LENGTH_LONG).show();
				}
				else
				{
					v.setSelected(false);
					Toast.makeText(context, "해제만 할 수 있습니다.\n설정은 로그인 시 가능합니다.", Toast.LENGTH_LONG).show();
				}
			}
		});
		useAutoLogin.setSelected(prefs.getAutoLogin()); // 저장된 설정 적용.
		if (prefs.getAutoLogin())
		{
			TextView useAutoLoginText = (TextView) activity.findViewById(R.id.use_auto_login_text);
			useAutoLoginText.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15));
			useAutoLoginText.setText( useAutoLoginText.getText() + " ( ID : " + prefs.getUserId() + " )" );
		}
		
		/*
		 * 보안로그인 설정
		 */
		fParams = new FrameLayout.LayoutParams(getHeightDP(63), getHeightDP(31), Gravity.RIGHT);
		final FrameLayout useSecureLogin = (FrameLayout) activity.findViewById(R.id.use_secure_login_check);
		useSecureLogin.setLayoutParams(fParams);
		useSecureLogin.setSelected(prefs.getSecureLogin()); // 저장된 설정 적용.
		useSecureLogin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				v.setSelected(! v.isSelected());
				prefs.putSecureLogin(v.isSelected());
				Log.d(TAG, "secure login : " + prefs.getSecureLogin());
			}
		});
		
		// 로그아웃 과 회원탈퇴가 포함된 영역 노출 여부 결정
		FrameLayout layoutSettingLogoutUpperLine = (FrameLayout) activity.findViewById(R.id.layout_setting_logout_upper_line);
		RelativeLayout layoutSettingLogout = (RelativeLayout) activity.findViewById(R.id.layout_setting_logout);
		if (! bandiApp.isLogin()) {
			layoutSettingLogoutUpperLine.setVisibility(View.GONE);
			layoutSettingLogout.setVisibility(View.GONE);
		} else {
			// 로그인 되어 있다면,
			TextView tv = (TextView) activity.findViewById(R.id.login_id_text);
			tv.append(" : " + bandiApp.getUserId() );
		}
		
		/*
		 * 로그아웃 
		 */
		Button logoutBtn = (Button) activity.findViewById(R.id.logout_button);
		rParams = (android.widget.RelativeLayout.LayoutParams) logoutBtn.getLayoutParams();
		rParams.height = getHeightDP(31);
		rParams.width = getHeightDP(65);
		logoutBtn.setLayoutParams(rParams);
		logoutBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Dialog.confirm(context, "알림", "자동로그인이 설정되어 있다면, 자동로그인이 해지됩니다.\n로그아웃 하시겠습니까?", 
						new DialogInterface.OnClickListener(){

							@Override
							public void onClick(DialogInterface dialog, int which) {
								bandiApp.logout();
								prefs.putAutoLogin(false);
								Dialog.showAlert(context, "로그아웃 되었습니다.");
								Setting.this.run();
							}
						});
			} // end onClick
		});
		
		/*
		 * 회원탈퇴 
		 */
		Button withdrawBtn = (Button) activity.findViewById(R.id.withdraw_button);
		rParams = (android.widget.RelativeLayout.LayoutParams) withdrawBtn.getLayoutParams();
		rParams.height = getHeightDP(31);
		rParams.width = getHeightDP(65);
		withdrawBtn.setLayoutParams(rParams);
		withdrawBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO "로그아웃 확인 창을 먼저 띄우기.."
				Dialog.confirm(context, "안내", "회원탈퇴를 위해 반디앱에서 로그아웃 처리됩니다.", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						WebViewDialog wvDlg = new WebViewDialog(Setting.this.context); 
						wvDlg.show(); // <-- 여기서 웹페이지 로딩하면서 반디 세션 지움.
						Setting.this.run(); //  화면 갱신 위해.
					}
				});				
				
			}
		});
		
		/*
		 * 개인정보처리방침
		 */
		Button personalInfoHandlingPoliciesBtn = (Button) activity.findViewById(R.id.personal_information_handling_policies_button);
		fParams = (android.widget.FrameLayout.LayoutParams) personalInfoHandlingPoliciesBtn.getLayoutParams();
		fParams.height = getHeightDP(31);
		fParams.width = getHeightDP(31);
		personalInfoHandlingPoliciesBtn.setLayoutParams(fParams);
		personalInfoHandlingPoliciesBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://m.ebs.co.kr/cs/privacy"));
		    	activity.startActivity(intent);				
			}
		});
		
		
		/*
		 * HLS 스트리밍 사용 설정
		 */
		fParams = new FrameLayout.LayoutParams(getHeightDP(63), getHeightDP(31), Gravity.RIGHT);
		final FrameLayout useHLS = (FrameLayout) activity.findViewById(R.id.checkBox_useHLS);
		useHLS.setLayoutParams(fParams);
		useHLS.setSelected(prefs.getHLS()); // 저장된 설정 적용.
		useHLS.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				v.setSelected(! v.isSelected());
				prefs.putHLS(v.isSelected());
				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP
						&& v.isSelected()) 
				{
					Dialog.showAlert(context, context.getResources().getString(R.string.alert_hls));
				}
			}
		});
		

		lParams = new LinearLayout.LayoutParams(getHeightDP(28), getHeightDP(28));
		radioCheck = (FrameLayout)activity.findViewById(R.id.checkBox_settingRadio);
		radioCheck.setLayoutParams(lParams);
		iRadioCheck = (FrameLayout)activity.findViewById(R.id.checkBox_settingIRadio);
		iRadioCheck.setLayoutParams(lParams);
		radioCheck.setOnClickListener(startChannelSettingListener);
		iRadioCheck.setOnClickListener(startChannelSettingListener);
		if(prefs.getIsStartFMRadio())
		{
			radioCheck.setSelected(true);
			iRadioCheck.setSelected(false);
		}
		else
		{
			radioCheck.setSelected(false);
			iRadioCheck.setSelected(true);
		}

		lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lParams.setMargins(getWidthDP(5), 0, 0, 0);
		TextView settingRadioTxt = (TextView)activity.findViewById(R.id.textView_settingRadio);
		TextView settingIRadioTxt = (TextView)activity.findViewById(R.id.textView_settingIRadio);
		settingRadioTxt.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15));
		settingIRadioTxt.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15));
		settingRadioTxt.setLayoutParams(lParams);
		settingIRadioTxt.setLayoutParams(lParams);

		ImageView icon = (ImageView)activity.findViewById(R.id.imageView_mainIcon);
		icon.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
//				new OnAir(context).run();
				activity.findViewById(R.id.layout_menu1).performClick(); // 초기 선택
			}
		});
		FrameLayout controlBg = (FrameLayout)activity.findViewById(R.id.control_bar_bg);
		controlBg.setBackgroundColor(Color.parseColor("#3f3833"));

		fParams = new FrameLayout.LayoutParams(getHeightDP(63), getHeightDP(31), Gravity.RIGHT);
		FrameLayout bgRun = (FrameLayout)activity.findViewById(R.id.checkBox_backgroundRun);
		bgRun.setLayoutParams(fParams);
		bgRun.setSelected(prefs.getBackgroundRun());
		bgRun.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				v.setSelected(!v.isSelected());
				if(v.isSelected())
				{
					prefs.putBackgroundRun(true);
				}
				else
				{
					prefs.putBackgroundRun(false);
				}
			}
		});
	} // end setListener();

	OnClickListener startChannelSettingListener = new OnClickListener()
	{

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(v.getId() == R.id.checkBox_settingRadio)
			{
				if(v.isSelected()) return;
				else
				{
					v.setSelected(true);
					iRadioCheck.setSelected(false);
					prefs.putIsStartFMRadio(true);
				}
			}
			else if(v.getId() == R.id.checkBox_settingIRadio)
			{

				if(v.isSelected()) return;
				else
				{
					v.setSelected(true);
					radioCheck.setSelected(false);
					prefs.putIsStartFMRadio(false);
				}

			}
		}
	};



	/**
	 * 24시 단위의 시간 정보(시-int, 분-int)를 오전,오후 로 변경한 문자열을 반환한다.<br/>
	 * {@code formatTime(13,3)} returns "오후 01:03" <br/>
	 *
	 * @param hourOfDay
	 * @param minute
	 * @return
	 */
	private String formatTime(int hourOfDay, int minute)
	{
		String timeString ="";
		timeString += (hourOfDay < 12 ? "오전 " : "오후 ");
		timeString += (hourOfDay > 13 ? String.format("%02d", (hourOfDay-12)) : String.format("%02d", hourOfDay));
		timeString += ":" + String.format("%02d", minute);

		return timeString;
	}

	/**
	 * 13:14 형태의 시간 문자열을 불러와서 시(int), 분(int) 형태의 배열로 반환한다.
	 * @param timeString
	 * @return
	 */
	private int[] parseTimeString(String timeString)
	{
		return DateUtil.parseTimeString(timeString);
	}

	/**
	 * 안드로이드 버전 6.0 이상일 때, 자동실행과 관련된 이벤트 발생시 호출됨.
	 * reserveAutoRun() 대체 코드 재작성 (2017-04-19)
	 */
	public void reserveAutoRun2() {
		Log.d(TAG, "reserveAutoRun2 is called.");
		
		AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Calendar krNow = DateUtil.getKoreanCalendar();
		int[] wakeTime = parseTimeString(prefs.getWakeTime());
		Intent intentMain = new Intent(context, MainActivity.class);
		Intent intentReceiver = new Intent(context, AutoRunReceiver2.class);
		
		int[] daysOfWeek = {Calendar.SUNDAY, Calendar.MONDAY, Calendar.TUESDAY, 
				Calendar.WEDNESDAY, Calendar.THURSDAY, Calendar.FRIDAY, Calendar.SATURDAY};
		
		if (prefs.getAutoRun()) {
			// 우선 일주일 치를 예약하고 다음 예약이 실행될 때마다 예약 스케쥴 체크 (AutoRunReceiver2 에서.)
			for (int iDayOfWeek : daysOfWeek) {
				Log.d(TAG, "day of week " + iDayOfWeek + " is Selected : " + prefs.getDayOfWeek(iDayOfWeek));
			
				PendingIntent pi = PendingIntent.getActivity(context, iDayOfWeek, intentMain, 0);
				int diff = iDayOfWeek - krNow.get(Calendar.DAY_OF_WEEK);
				
				Calendar setCal = DateUtil.getKoreanCalendar(); // 예약된 날짜시간
        		setCal.set(Calendar.HOUR_OF_DAY, wakeTime[0]);
        		setCal.set(Calendar.MINUTE, wakeTime[1]);
        		setCal.set(Calendar.SECOND, 0);
        		setCal.add(Calendar.DAY_OF_MONTH, diff);
        		
        		if (setCal.before(krNow)) setCal.add(Calendar.DAY_OF_MONTH, 7);
        		
        		if (prefs.getDayOfWeek(iDayOfWeek)) {
					PendingIntent pi2=PendingIntent.getActivity(context, setCal.get(Calendar.DAY_OF_WEEK), intentMain, 0);
					AlarmClockInfo acInfo = new AlarmClockInfo(setCal.getTimeInMillis(), pi2);
					
					PendingIntent pi3=PendingIntent.getBroadcast(context, setCal.get(Calendar.DAY_OF_WEEK), intentReceiver, 0);
					am.setAlarmClock(acInfo, pi3);
				} else {
					PendingIntent pi3=PendingIntent.getBroadcast(context, setCal.get(Calendar.DAY_OF_WEEK), intentReceiver, 0);
					am.cancel(pi3);
				}
			}
		} else {
			// 자동 실행 체크되지 않았을 때.. 기존 모든 알람 설정 삭제
			for (int iDayOfWeek : daysOfWeek) {
				Intent i3=new Intent(context, AutoRunReceiver2.class);
				PendingIntent pi3=PendingIntent.getBroadcast(context, iDayOfWeek, i3, 0);
				am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
				am.cancel(pi3);
			}
		}
		
		//** 알람 설정 검정용 확인 로그 **
		AlarmClockInfo ac1 = am.getNextAlarmClock();
		if (ac1 != null) {
			Calendar krCal = DateUtil.getKoreanCalendar();
			krCal.setTimeInMillis(ac1.getTriggerTime());
			Log.d(TAG, "NextAlarmClock : " + DateUtil.getDateString(krCal, "yyyy-MM-dd HH:mm"));
		}
		//** 알람 설정 검정용 확인 로그 **/
	}
	
	
	/**
	 * 자동실행과 관련된 이벤트가 발생할 시 호출해 예약을 설정한다.
	 */
	public void reserveAutoRun()
	{
		// 안드로이드 6.0 이상일 경우 코드 분기
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			reserveAutoRun2();
			return;
		}
		
		if (prefs.getAutoRun())
		{
			// 자동 실행 체크 되었을 때
			Intent intent = new Intent(context, MainActivity.class); // <----- (중요) 나중에 광고 추가시.. 그것으로 변경하기..
			Intent intent0 = new Intent(context, AutoRunReceiver.class);

			PendingIntent[] pi =
			{
				// 요일별 시행.. (요일 미지정(0), 일(1)~토(7))
	    		PendingIntent.getBroadcast(context, 0, intent0, 0) // 요일 미지정
	    		,PendingIntent.getActivity(context, 1, intent, 0) // 일
	    		,PendingIntent.getActivity(context, 2, intent, 0)
	    		,PendingIntent.getActivity(context, 3, intent, 0)
	    		,PendingIntent.getActivity(context, 4, intent, 0)
	    		,PendingIntent.getActivity(context, 5, intent, 0)
	    		,PendingIntent.getActivity(context, 6, intent, 0) // 금
	    		,PendingIntent.getActivity(context, 7, intent, 0) // 토
	        };

	        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
	        Calendar cal = DateUtil.getKoreanCalendar();

	        if (getSelectedDayOfWeek().isEmpty())
	        {
	        	/*
	        	 *  요일 선택 되지 않았을 때, 최근 해당 시간에 예약 (일회성)
	        	 */
//	        	int[] wakeTime = parseTimeString(prefs.getWakeTime());
//	        	String setStr = String.format(Locale.KOREA, "%02d:%02d", wakeTime[0], wakeTime[1] );
//	        	String nowStr = String.format(Locale.KOREA, "%02d:%02d", cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE) );
//	        	if (nowStr.compareTo(setStr) >= 0)
//	        	{
//	        		// 설정시간이 현재 시간 이전이면 내일 날짜 실행.
//	                cal = DateUtil.getTomorrow(cal);
//	                Log.d(TAG, ">> now:" + nowStr +" / set:"+ setStr);
//	            }
//	        	cal.set(Calendar.HOUR_OF_DAY, wakeTime[0]);
//	            cal.set(Calendar.MINUTE, wakeTime[1]);
//	            cal.set(Calendar.SECOND, 0);
//
//	            am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pi[0]);
//
//	            String rt = cal.get(Calendar.YEAR) +"-"+ (cal.get(Calendar.MONTH)+1) +"-"+ cal.get(Calendar.DAY_OF_MONTH) + " ";
//	            rt += String.format("%02d",cal.get(Calendar.HOUR_OF_DAY)) +":"+ String.format("%02d",cal.get(Calendar.MINUTE));
	            //if (!resetAlarm) Toast.makeText(context, rt +" 예약되었습니다.", Toast.LENGTH_LONG).show();
	        }
	        else
	        {
	        	/*
	        	 *  요일 선택 되었을 경우.(반복)
	        	 */
	        	Iterator<Integer> iter = getSelectedDayOfWeek().iterator();
	        	int dayOfWeek, diff;
	        	int[] wakeTime = parseTimeString(prefs.getWakeTime());
	        	
	        	while (iter.hasNext())
	        	{

	        		dayOfWeek = iter.next().intValue();
	        		diff = dayOfWeek - cal.get(Calendar.DAY_OF_WEEK);

	        		Calendar setCal = DateUtil.getKoreanCalendar(); // 예약된 날짜시간
	        		setCal.set(Calendar.HOUR_OF_DAY, wakeTime[0]);
	        		setCal.set(Calendar.MINUTE, wakeTime[1]);
	        		setCal.set(Calendar.SECOND, 0);
	        		setCal.add(Calendar.DAY_OF_MONTH, diff);

	        		Log.d(TAG, "wakeTime == " + wakeTime[0] + " , " + wakeTime[1] + "/ day of week " + dayOfWeek);

	        		if (setCal.before(cal)) setCal.add(Calendar.DAY_OF_MONTH, 7);
	        		
	        		
        			// android 6.0 아래면 기존 방식대로 7일 주기 등록
	        		am.setRepeating(AlarmManager.RTC_WAKEUP, setCal.getTimeInMillis(), AlarmManager.INTERVAL_DAY * 7, pi[setCal.get(Calendar.DAY_OF_WEEK)]);
	        	}
	        }
		} 
	}


	/**
	 * 자동 실행 관련, 선택된 요일 집합을 반환한다.
	 * @return
	 */
	private HashSet<Integer> getSelectedDayOfWeek()
	{
		Iterator<Integer> iter = daysOfWeekBtn.keySet().iterator();
		HashSet <Integer> selectedDayOfWeek = new HashSet<Integer> ();
		while(iter.hasNext())
		{
			Integer dayOfWeek = iter.next();
			if (daysOfWeekBtn.get(dayOfWeek).isSelected()) selectedDayOfWeek.add(dayOfWeek);
		}
		return selectedDayOfWeek;
	}

	public int getWidthDP(int dpValue)
	{
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpWidth / 360, context.getResources().getDisplayMetrics());
	}

	public int getHeightDP(int dpValue)
	{
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpHeight / 640, context.getResources().getDisplayMetrics());
	}

	public int getCalSP(int spValue)
	{
		return spValue * dpHeight / 640;
	}
}
