package kr.ebs.bandi.data;

import android.util.MonthDisplayHelper;

//evtId: 1354589275013,
//evtTitle: "테스트 공지_06",
//evtClsCd: "001",
//linkUrl: null,
//evtStartDt: null,
//evtEndDt: null,
//thmnlFilePathNm: null,
//thmnlFileLogcNm: null,
//thmnlFilePhscNm: null,
//evtCntn: "공지사항 6 내용이 출력됩니다.",
//crtnDtm: "20141221164513",
//mobLinkUrl: null,
//shwSiteDsCd: "BANDI_ALL",
//mobThmnlFilePathNm: null,
//mobThmnlFileLogcNm: null,
//mobThmnlFilePhscNm: null

public class EventData 
{
//	private volatile static EventData single;
//    public static EventData getInstance()
//    {
//        if (single == null) 
//        {
//            synchronized(EventData.class) 
//            {
//                if (single == null) 
//                {
//                    single = new EventData();
//                }
//            }
//        }
//        return single;
//    }
	public EventData(){}
	
	long evtId; //1354589275013,
	String evtTitle;//: "테스트 공지_06",
	String evtClsCd;//: "001",
	String evtStartDt;//: null,
	String evtEndDt;//: null,
	String linkUrl;
	String evtCntn;//: "공지사항 6 내용이 출력됩니다.",
	String mobLinkUrl;//: null,
	String shwSiteDsCd;//: "BANDI_ALL",
	String mobThmnlFilePathNm;
	String mobThmnlFilePhscNm;//: null
	
	public void setEventID(long id) { evtId = id; }
	public void setLinkUrl(String url) { linkUrl = url; }
	public void setEventTitle(String title) { evtTitle = title; }
	public void setEventCode(String code) { evtClsCd = code; }
	public void setEventStartDay(String day) { evtStartDt = day; }
	public void setEventEndDay(String day) { evtEndDt = day; }
	public void setEventMessage(String message) { evtCntn = message; }
	public void setEventUrl(String url) { mobLinkUrl = url; }
	public void setEventDeviceCode(String code) { shwSiteDsCd = code; }
	public void setImagePathUrl(String url) { mobThmnlFilePathNm = url; }
	public void setImagePhsUrl(String url) { mobThmnlFilePhscNm = url; }
	
	public long getEventID() { return evtId; }
	public String getLinkUrl() { return linkUrl; }
	public String getEventTitle() { return evtTitle; }
	public String getEventCode() { return evtClsCd; }
	public String getEventStartDay() { return evtStartDt; }
	public String getEventMessage()	{ return evtCntn; }
	public String getEventEndDay() { return evtEndDt; }
	public String getEventUrl()	{ return mobLinkUrl; }
	public String getEventDeviceCode() { return shwSiteDsCd; }
	public String getImageFullUrl() { return "http://static.ebs.co.kr/images/" + mobThmnlFilePathNm + mobThmnlFilePhscNm; }
	
	
}
