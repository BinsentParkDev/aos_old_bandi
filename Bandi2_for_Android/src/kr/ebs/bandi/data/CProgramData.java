package kr.ebs.bandi.data;

public class CProgramData
{
	String resultMsg;//(결과 메시지)
	String object;// : 프로그램 정보
	String programId;// : 프로그램 ID
	String title;// : 프로그램 명
	String link1;// : 프로그램 방송 홈페이지 URL
	String link2;// : 반디 게시판 URL
	String vfm;// : 보이는 라디오 여부 (Yes/No)
	String scroll;// : 반디 스크롤 텍스트
	String scrollLink;// : 반디 스크롤 LINK URL
	String twitter;// : 트위터 URL
	String appImg;// : 앱이미지
	String appImg2;// : 앱이미지2
	String facebookUrl;// : 페이스북 URL
	String mc;// : 진행자 정보
	String contents;// : 프로그램 상세 정보
	String kakaostoryUrl;// : 카카오스토리 URL
	String mobHmpUrl;// : 모바일 홈페이지 URL
	String mobReplayUrl;// : 모바일 웹 다시듣기 URL
	String appUseBoardYn;
	
	String nowFavoriteId;
	
	private volatile static CProgramData single;
    public static CProgramData getInstance()
    {
        if (single == null) 
        {
            synchronized(CProgramData.class) 
            {
                if (single == null) 
                {
                    single = new CProgramData();
                }
            }
        }
        return single;
    }
	private CProgramData(){}
	
	public void setProgramID(String id) { programId = id; }
	public void setProgramTitle(String title) { this.title = title; }
	public void setHomePageUrl(String url) { link1 = url; }
	public void setListUrl(String url) { link2 = url; }
	public void setViewRadioState(String state) { vfm = state; }
	public void setScrollText(String text) { scroll = text; }
	public void setScrollTextUrl(String url) { scrollLink = url; }
	public void setTwitterUrl(String url) { twitter = url; }
	public void setAppImgUrl(String url) { appImg = url; }
	public void setAppImg2Url(String url) { appImg2 = url; }
	public void setFacebookUrl(String url) { facebookUrl = url; }
	public void setMcInfo(String info) { mc = info; }
	public void setContents(String contents) { this.contents = contents; }
	public void setKakaoUrl(String url) { this.kakaostoryUrl = url; }
	public void setMobilePageUrl(String url) { mobHmpUrl = url; }
	public void setMobileReplayUrl(String url) { mobReplayUrl = url; }
	public void setNowFavoriteId(String id) { nowFavoriteId = id; }
	public void setAppUseBoardYn(String appUseBoardYn) { this.appUseBoardYn = appUseBoardYn; }
	
	public String getProgramID() { return programId; }
	public String getProgramTitle() { return title; }
	public String getHomePageUrl() { return link1; }
	public String getListUrl() { return link2; }
	public String getViewRadioState() { return vfm; }
	public String getScrollText() { return scroll; }
	public String getScrollTextUrl() { return scrollLink; }
	public String getTwitterUrl() { return twitter; }
	public String getAppImgUrl() { return appImg; }
	public String getAppImg2Url() { return appImg2; }
	public String getFacebookUrl() { return facebookUrl; }
	public String getMcInfo() { return mc; }
	public String getContents() { return contents; }
	public String getKakaoUrl() { return kakaostoryUrl; }
	public String getMobilePageUrl() { return mobHmpUrl; }
	public String getMobileReplayUrl() { return mobReplayUrl; }
	public String getNowFavoriteId() { return nowFavoriteId; }
	public String getAppUseBoardYn() { return appUseBoardYn; }
	
}
