package kr.ebs.bandi.data;

public class RadioData 
{
	String programId;
	String onairDate;
	String startTime;
	String endTime;
	
	
	public void setProgramId(String id)
	{
		programId = id;
	}
	
	public void setOnairDate(String date)
	{
		onairDate = date;
	}
	
	public void setStartTime(String time)
	{
		startTime = time;
	}
	
	public void setEndTime(String time)
	{
		endTime = time;
	}
	
	public String getProgramId()
	{
		return programId;
	}
	
	public String getOnairDate()
	{
		return onairDate;
	}
	
	public String getStartTime()
	{
		return startTime;
	}
	
	public String getEndTime()
	{
		return endTime;
	}
}
