package kr.ebs.bandi.data;

import android.annotation.SuppressLint;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import kr.ebs.bandi.utils.StreamingMediaPlayer;

public class RepeatData 
{
	
	String lectId;//: "10275991",
	String lectNm;//: "한줄 헤드라인 뉴스/ 오늘의 문장/ 영작 노래방",
	String lectSuplExpCntn;//: "뉴스를 통해 시사감각을 익히고 오늘의 문장을 통해 말하기 연습, 그리고 다양한 영어표현들을 익혀본다.",
	long repeatDate;
	boolean vodChrgClsCd;
	StreamingMediaPlayer player;
	
	public RepeatData() {};
	
	public void setRepeatDate(long date)
	{
		repeatDate = date;
	}
	
	@SuppressLint("SimpleDateFormat")
	public String getRepeatDate()
	{
		SimpleDateFormat sdfCurrent = new SimpleDateFormat ("yyyy-MM-dd"); 
		Timestamp currentTime = new Timestamp(repeatDate); 
		Date date = new Date(currentTime.getTime());
		String day = sdfCurrent.format(date);
		return day;
	}
	
	public void setLectId(String id)
	{
		lectId = id;
	}
	
	public void setLectNm(String lectNm)
	{
		this.lectNm = lectNm;  
	}
	
	public void setIsCharged(boolean code)
	{
		vodChrgClsCd = code;
	}
	
	public void setPlayer(StreamingMediaPlayer player)
	{
		this.player = player;
	}
	
	public StreamingMediaPlayer getPlayer()
	{
		return player;
	}
	
	public boolean getIsCharged()
	{
		return vodChrgClsCd;
	}
	
	public String getLectId()
	{
		return lectId;
	}
	
	public String getLectNm()
	{
		return lectNm;
	}
}
