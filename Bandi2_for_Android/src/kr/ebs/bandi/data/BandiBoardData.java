package kr.ebs.bandi.data;

public class BandiBoardData 
{
//	board list == {"bandiPosts":[
//	{"gubun":0,
//	"listSeqNo":null,
//	"snsDsCd":"001",
//	"contents":"우오",
//	"maskingUserId":null,
//	"broadType":null,
//	"progcd":"10009935",
//	"snsUseYn":"Y",
//	"snsUserNm":"조기성",
//	"regtime":"033157",
//	"programId":"10009935",
//	"rid":0,
//	"sequence":null,
//	"appDsCd":"02",
//	"regdate":"2015-01-04 15:15:27",
//	"userId":null,
//	"adminYn":"N",
//	"seq":10001458221,
//	"maskingWriter":null,
//	"writer":null,
//	"snsUserId":"619236724869531"
//	}

	//1. bandiPosts : 반디 게시글
	String programId;// : 프로그램 ID
	long seq;// : 게시글 일련번호
	String userId;// : 글쓴이 ID
	String writer;// : 글쓴이명
	String contents;// : 게시글 내용
	String regdate;// : 작성 날짜
	String regtime;// : 작성 시간
	String adminYn;// : 관리자 여부
	String snsUserNm;
	String appDsCd;// : 앱 관련 코드 (00 : PC, 01 : 안드로이드, 02 : iOS)
	String snsUseYn;
	
	public BandiBoardData(){}
	
	public void setProgramID(String id) 			{ this.programId = id; }
	public void setSEQ(long seq) 					{ this.seq = seq; }
	public void setUserID(String id) 				{ this.userId = id; }
	public void setWriter(String writer)			{ this.writer = writer; }
	public void setContents(String contens) 		{ this.contents = contens; }
	public void setRegDate(String regDate) 			{ this.regdate = regDate; }
	public void setRegTime(String regTime) 			{ this.regtime = regTime; }
	public void setAdminYN(String adminYn) 			{ this.adminYn = adminYn; }
	public void setAppDsCd(String appDsCd) 			{ this.appDsCd = appDsCd; }
	public void setSNSUserName(String snsUserNm) 	{ this.snsUserNm = snsUserNm; }
	public void setSNSUseYN(String snsUseYn)		{ this.snsUseYn = snsUseYn; }
	
	public String getProgramID() 	{ return programId; }
	public long getSEQ() 			{ return seq; }
	public String getUserID() 		{ return userId; }
	public String getWriter()		{ return writer; }
	public String getContents() 	{ return contents; }
	public String getRegDate() 		{ return regdate; }
	public String getRegTime() 		{ return regtime; }
	public String getAdminYN() 		{ return adminYn; }
	public String getAppDsCd() 		{ return appDsCd; }
	public String getSNSUserName()	{ return snsUserNm; }
	public String getSNSUseYn()		{ return snsUseYn; }
	
}
