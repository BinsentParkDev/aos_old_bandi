package kr.ebs.bandi.data;

public class TimeData 
{
	String startTime, endTime, title;
	
	public TimeData(){}
	
	public void setStartTime(String time)
	{
		startTime = time;
	};
	
	public void setEndTime(String time)
	{
		endTime = time;
	}
	
	public String getStartTime()
	{
		return startTime;
	}
	
	public String getEndTime()
	{
		return endTime;
	}
	
	public void setTitle(String title)
	{
		this.title = title;
	}
	
	public String getTitle()
	{
		return title;
	}
}
