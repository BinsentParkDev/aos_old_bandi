package kr.ebs.bandi.sns;

import kr.ebs.bandi.data.CProgramData;
import kr.ebs.bandi.data.EventData;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;


public class LoginManager {

    private UserData mUserData;
    private Facebook mFacebook;
    private String mFacebookToken;
    private final String TAG = "LoginManager";

    private volatile static LoginManager single;
    public static LoginManager getInstance()
    {
        if (single == null) 
        {
            synchronized(LoginManager.class) 
            {
                if (single == null) 
                {
                    single = new LoginManager();
                }
            }
        }
        return single;
    }
    
    private LoginManager()
    {}
    
    public LoginManager(UserData userData) {
        super();
        this.mUserData = userData;
    }

    public UserData getUserData() {
        return mUserData;
    }

    public void clearUserData() {
        mUserData = new UserData();
    }

    public Facebook getFacebook() {
        return mFacebook;
    }

    public void setFacebook(Facebook mFacebook) {
        this.mFacebook = mFacebook;
    }

    
    
    
    

    @SuppressWarnings("deprecation")
    public void startLogin(final Activity activity, int loginType, final LoginEventCallback callback) {
        switch (loginType) {
            case UserData.LOGIN_TYPE_FB: {

                if (mFacebook == null)
                    mFacebook = new Facebook("826581740742444");

                if (mFacebook == null) {
                    Toast.makeText(activity, "페이스북에서 문제가 발생하였습니다.", Toast.LENGTH_SHORT).show();
                    return;
                }
                
                Communicator.CommUnit facebookAuthCommUnit = null;
                if (!mFacebook.isSessionValid() || mFacebookToken == null) {
                    // 페이스북 세션이 없는 경우 새로운 세션 생성
                    facebookAuthCommUnit = Communicator.CreateCommUnit();
                    
                    Log.d("TEST", "asdfjsadkljfaslkjflsajflaksjdflk" + facebookAuthCommUnit);
                    
                    // 페이스북 인증
                    facebookAuthCommUnit.setCommModule(new Communicator.CommModule() 
                    {
                        @Override
                        public void communication(Communicator.CommUnit commUnit, final Communicator.CommCallback commCallback) {
                        	
                        	
                        	
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    String[] PERMS = new String[]{"read_stream", "email", "user_likes", "user_photos", "publish_actions"};
                                    mFacebook.authorize(activity, PERMS, new DialogListener() {
                                        @Override
                                        public void onComplete(Bundle values) {
                                        	Log.e(TAG,"faceBookLogin ### onComplete");
                                            Communicator.CommResult result = Communicator.CommResult.SUCCEED_RESULT;
                                            commCallback.callback(Communicator.CommResult.SUCCEED_RESULT);
                                        }

                                        @Override
                                        public void onFacebookError(FacebookError error) {
                                            error.printStackTrace();
                                            commCallback.callback(Communicator.CommResult.FAILED_RESULT);
                                            Log.e(TAG,"faceBookLogin ### onFacebookError");
                                        }

                                        @Override
                                        public void onError(DialogError e) {
                                            e.printStackTrace();
                                            commCallback.callback(Communicator.CommResult.FAILED_RESULT);
                                            Log.e(TAG,"faceBookLogin ### onError");
                                        }

                                        @Override
                                        public void onCancel() {
                                        	Log.e(TAG,"faceBookLogin ### onCancel");
                                            commCallback.callback(Communicator.CommResult.FAILED_RESULT);
                                        }
                                    });
                                }
                            });
                        }
                    })
                            .setCommListener(new Communicator.CommListener() {
                                @Override
                                public boolean succeed(Communicator.CommUnit nextUnit, Object resultObj) throws Exception {
                                    // 인증 후 페이스 북 엑세스 토큰 저장
                                    mFacebookToken = mFacebook.getAccessToken();
                                    return true;
                                }

                                @Override
                                public void failed(Object resultObj) throws Exception {
                                    callback.eventFailed();
                                }
                            });
                } 
                Communicator.CommUnit facebookProfileGetUnit = null == facebookAuthCommUnit ? Communicator.CreateCommUnit(Communicator.Method.GET) : facebookAuthCommUnit.addCommUnit(Communicator.Method.GET);
                // 인증 코드를 가져온 후 페이스북 프로필 정보 습득
                facebookProfileGetUnit.setUri("CommUnit get Facebook Profile");
                facebookProfileGetUnit.setCommModule(new Communicator.CommFBModule(mFacebook))
                        .setCommListener(new Communicator.CommListener() {
                            @Override
                            public boolean succeed(Communicator.CommUnit nextUnit, Object resultObj) throws Exception {
                                callback.eventStart();
                                // 페이스북 프로필 정보 습득
                                JSONObject jsonObj = (JSONObject) resultObj;
                                if (parseFBProfile(jsonObj)) {
                                    if (checkFbLogined()) {
                                    	Log.e(TAG, "CommUnit add login param for facebook YES");
                                        nextUnit.addParam("email", mUserData.getFb_email())
                                                .addParam("password", mUserData.getFbId())
                                                .addParam("fb_id", mUserData.getFbId())
                                                .addParam("fb_name", mUserData.getFb_name())
                                                .addParam("fb_image", mUserData.getFb_image())
                                                .addParam("fb_first_name", mUserData.getFb_first_name())
                                                .addParam("fb_last_name", mUserData.getFb_last_name())
                                                .addParam("fb_access_token", mUserData.getFb_access_token())
                                                .addParam("phone", mUserData.getPhone());
                                        return true;
                                    }
                                }
                                return false;
                            }

                            @Override
                            public void failed(Object resultObj) throws Exception {
                                callback.eventFailed();
                            }
                        });
               
            }
            break;
            
        }
    }

    private boolean parseFBProfile(JSONObject jsonObj) {
        // laughaway_log
    	Log.e(TAG, "###" + jsonObj.toString());

        boolean res = true;
        try {

            if (jsonObj.has("id")) {
                mUserData.setFbId(jsonObj.getString("id"));
                mUserData.setFb_image("http://graph.facebook.com/" + mUserData.getFbId() + "/picture?type=large");
                mUserData.setFb_access_token(this.mFacebookToken);

                if (jsonObj.has("name"))
                    mUserData.setFb_name(jsonObj.getString("name"));

                if (jsonObj.has("email"))
                    mUserData.setFb_email(jsonObj.getString("email"));
                else
                    mUserData.setFb_email(mUserData.getFbId() + "@nomail.com"); // v1.0.3
                // 전화번호
                // 가입자의
                // 경우
                // dummy
                // email값으로
                // 넘김

                if (jsonObj.has("first_name"))
                    mUserData.setFb_first_name(jsonObj.getString("first_name"));

                if (jsonObj.has("last_name"))
                    mUserData.setFb_last_name(jsonObj.getString("last_name"));
                if (jsonObj.has("gender"))
                    mUserData.setFb_gender(jsonObj.getString("last_name"));
            } else {
                res = false;
            }
        } catch (JSONException e) {
            res = false;
            e.printStackTrace();
        }

        return res;
    }

    private boolean checkFbLogined() {
        mUserData.setPhone(Random());
        if (mUserData.getPhone() == null)
            return false;
        else
            return true;
    }

    private String Random() {

        // 랜덤 숫자 구하기
        int random = (int) (Math.random() * 1000000000);
        String result_user_no = String.format("99%09d", random);
        Log.e(TAG,"random number random: " + result_user_no);

        return result_user_no;
    }

    public void clearFacebook() {
        this.mFacebook = null;
    }

    public static interface LoginCallback {
        public void callback();
    }

    public static abstract class LoginEventCallback extends Handler {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    eventSuccess(msg.obj);
                    break;
                case -1:
                    eventFailed();
                    break;
            }
        }

        public abstract void eventSuccess(Object obj);

        public void eventStart(){

        }

        public void eventCallActivity(int request) {

        }

        public abstract void eventFailed();
    }

    
    

//    private static class Item {
//        public final int textId;
//        public final int icon;
//        public final AuthType authType;
//
//        public Item(final int textId, final Integer icon, final AuthType authType) {
//            this.textId = textId;
//            this.icon = icon;
//            this.authType = authType;
//        }
//    }

    

}
