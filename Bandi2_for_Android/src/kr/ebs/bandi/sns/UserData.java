package kr.ebs.bandi.sns;

import java.io.Serializable;
import java.util.ArrayList;

public class UserData implements Serializable {

	public static final int LOGIN_TYPE_FB = 0;
	public static final int LOGIN_TYPE_SIGNUP = 1;
	public static final int LOGIN_TYPE_KAKAOTALK = 2;

	private String m_fb_id = null;
	private String m_fb_name = null;
	private String m_fb_email = null;
	private String m_fb_image = null;
	private String m_fb_first_name = null;
	private String m_fb_last_name = null;
	private String m_fb_access_token = null;
	private String m_fb_gender = null;


	public String getFbId() {
		return m_fb_id;
	}

	public void setFbId(String fb_id) {
		m_fb_id = fb_id;
	}

	public String getFb_email() {
		return m_fb_email;
	}

	public void setFb_email(String fb_email) {
		m_fb_email = fb_email;
	}

	public String getFb_name() {
		return m_fb_name;
	}

	public void setFb_name(String fb_name) {
		m_fb_name = fb_name;
	}

	public String getFb_image() {
		return m_fb_image;
	}

	public void setFb_image(String fb_image) {
		m_fb_image = fb_image;
	}

	public String getFb_first_name() {
		return m_fb_first_name;
	}

	public void setFb_first_name(String fb_first_name) {
		m_fb_first_name = fb_first_name;
	}

	public String getFb_last_name() {
		return m_fb_last_name;
	}

	public void setFb_last_name(String fb_last_name) {
		m_fb_last_name = fb_last_name;
	}

	public String getFb_access_token() {
		return m_fb_access_token;
	}

	public void setFb_access_token(String fb_access_token) {
		m_fb_access_token = fb_access_token;
	}

	public String getFb_gender() {
		return m_fb_gender;
	}

	public void setFb_gender(String m_fb_gender) {
		this.m_fb_gender = m_fb_gender;
	}

	public Object getPhone() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setPhone(String random) {
		// TODO Auto-generated method stub
		
	}

	
}
