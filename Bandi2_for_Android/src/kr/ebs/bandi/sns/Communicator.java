package kr.ebs.bandi.sns;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * 통신 단위 클래스
 * CommModule 클래스의 만들기에 따라 단순한 스레드 모듈로 사용도 가능
 */
public class Communicator {

	public static class ParamBean {
		private final ArrayList<NameValuePair> params;

		ParamBean() {
			super();
			this.params = new ArrayList<NameValuePair>();
		}

		ParamBean addParam(String key, String value) {
            for(NameValuePair pair : params) {
                if(key.equals(pair.getName())) {
                    params.set(params.indexOf(pair), new BasicNameValuePair(key, value));
                    break;
                }
            }
			params.add(new BasicNameValuePair(key, value));
			return this;
		}

		ParamBean addParam(String key, Integer value) {
			this.addParam(key, value.toString());
			return this;
		}

		ParamBean addParam(String key, Long value) {
            this.addParam(key, value.toString());
			return this;
		}

		ArrayList<NameValuePair> params() {
			return params;
		}

        public String toUri(String uriPrefix) {
            for(NameValuePair pair : params) {
                uriPrefix += ( 0 == params.indexOf(pair) ? "?" : "&" ) + pair.getName() + "=" + pair.getValue();
            }
            return uriPrefix;
        }

        public String toJson() {
            HashMap<String, Object> map = new HashMap<String, Object>();
            for(NameValuePair pair : params) {
                map.put(pair.getName(), pair.getValue());
            }
            return new JSONObject(map).toString();
        }
    }

	public static ParamBean CreateParam() {
		return new ParamBean();
	}
	
	
    public static enum Method {
        GET,
        POST,
        NONE,
    }

    public static enum ResultCode {
        SUCCEED,
        FAILED, // 기타 실패
        FAILED_TIMEOUT, // 소켓 응답 실패
        FAILED_JSON, // json 변환 실패
        FAILED_CONNECTION, // 응답값 없음
        FAILED_SERVER // 요청 타임아웃
    }

    public final static Communicator COMMUNICATOR = new Communicator();

    public static CommUnit CreateCommUnit(Method method) {
        return new CommUnit(COMMUNICATOR, method);
    }

    public static CommUnit CreateCommUnit() {
        return CreateCommUnit(Method.NONE);
    }

    public static class CommThread extends Thread {

        private Handler mHandler;
        private final CommUnit mCommUnit;

        private final Integer threadId;

        // 컨텍스트내에서 사용하는 통신 스레드 풀
        private final static HashMap<Integer, CommThread> ThreadPool = new HashMap<Integer, CommThread>();

        public CommThread(CommUnit commUnit) {
            super(null == commUnit.mContext ? commUnit.toString() : commUnit.mContext.get().toString());
            this.mCommUnit = commUnit;
            this.threadId = this.hashCode();

            if(false == ThreadPool.containsKey(this.threadId)) {
                ThreadPool.put(threadId, this);
            }
        }

        public void close() {
            if(this.getState() == State.RUNNABLE) {
                this.interrupt();
            }
            ThreadPool.remove(threadId);
        }

        public Integer getThreadId() {
            return threadId;
        }

        @Override
        public void run() {
            runNext(mCommUnit);
        }

        public void runNext(final CommUnit nextUnit) {
            if(null == nextUnit) {
                this.close();
            } else {
                if(null == mHandler) {
                    Looper.prepare();
                    mHandler = new Handler();
                    runCommUnit(nextUnit);
                    Looper.loop();
                }
                else {
                    runCommUnit(nextUnit);
                }
            }
        }

        private void runCommUnit(final CommUnit nextUnit) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if( null != nextUnit) {
                        nextUnit.requestCommunication(CommThread.this);
                    }
                }
            });
        }

        public void sendPost(Runnable runnable) {
            new Handler(Looper.getMainLooper()).post(runnable);
        }

        protected static void start(CommUnit commUnit) {
            CommThread thread = new CommThread(commUnit);
            thread.start();
        }
    }

    public static class CommUnit {

        private final Communicator mCommunicator;
        private final Method mMethod;
        private volatile WeakReference<Context> mContext;

        private volatile String mUri = null;
        private volatile ParamBean mBean = null;

        private CommModule mCommModule = null;

        private CommUnit mNextUnit = null;
        private CommUnit mPrevUnit = null;

        private CommListener mCommListener = null;

        public CommUnit(Communicator communicator, Method method) {
            super();
            this.mCommunicator = communicator;
            this.mMethod = method;
        }

        public CommUnit setUri(String uri) {
            this.mUri = uri;
            return this;
        }

        public CommUnit getTopUnit() {
            if(null == mPrevUnit) {
                return this;
            }
            else {
                return mPrevUnit.getTopUnit();
            }
        }

        public CommUnit setParamBeans(ParamBean bean) {
            mBean = bean;
            return this;
        }

        public CommModule getCommModule() {
            return mCommModule;
        }

        public CommUnit setCommModule(CommModule commModule) {
            this.mCommModule = commModule;
            return this;
        }

        public CommUnit addCommUnit(Method method) {
            CommUnit unit = new CommUnit(COMMUNICATOR, method);
            this.mNextUnit = unit;
            unit.mPrevUnit = this;
            return unit;
        }

        public CommUnit addCommUnit() {
            return addCommUnit(Method.NONE);
        }

        public CommListener getCommListener() {
            return mCommListener;
        }

        public CommUnit setCommListener(CommListener mCommListener) {
            this.mCommListener = mCommListener;
            return this;
        }

        public CommUnit request(Context context) {
            CommUnit topUnit = getTopUnit();
            topUnit.setContext(context);
            CommThread.start(topUnit);
            return this;
        }

        /**
         * 통신 로직을 실행
         * @return
         */
        private CommUnit requestCommunication(final CommThread thread) {

            if(null == this.mCommModule)
            {
                mCommModule = new CongKongCommModule();
            }
            Log.e("CommUnit" ,"request uri1111 : "+ mCommModule.toString() );
            Log.e("CommUnit request parameters",  null != mBean ? mBean.toJson() : "None");
            final CommUnit nextUnit = mNextUnit;
            this.mCommModule.communication(this, new CommCallback() {
                @Override
                public void callback(final CommResult result) {
                    if(checkContext()) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                sendResultToListener(thread, nextUnit, result.resultCode, result.message);
                                thread.runNext(mNextUnit);
                            }
                        });
                    } else {
                        thread.runNext(null);
                    }
                }
            });
            return this;
        }

        private boolean checkContext() {
            if(null == mContext) {
                return true;
            } else {
                if(null != mContext.get()) {
                    return true;
                }
            }
            return false;
        }

        protected void sendResultToListener(CommThread thread, CommUnit nextUnit, ResultCode resultCode, Object resultObj){
            try {
                if (null == mContext) { // 컨텍스트에 종속하지 않는 경우
                    switch (resultCode) {
                        case SUCCEED:
                            if (null != mCommListener && false == mCommListener.succeed(nextUnit, resultObj)) {
                                thread.close();
                            }
                            break;
                        default:
                            mCommListener.failed(resultObj);
                            thread.close();
                            break;
                    }
                } else {
                    final Context context = mContext.get();
                    if (null != context) {
                        switch (resultCode) {
                            case SUCCEED:
                                if (null != mCommListener && false == mCommListener.succeed(nextUnit, resultObj)) {
                                    thread.close();
                                }
                                break;
                            default:
                                mCommListener.failed(resultObj);
                                thread.close();
                                break;
                        }
                    }
                }
            } catch (Exception e) {
                thread.close();
            }
        }

        public void setContext(Context context) {
            this.mContext = null == context ? null : new WeakReference<Context>(context);
            if(null != mNextUnit) {
                mNextUnit.setContext(context);
            }
        }

        public CommUnit addParam(String key, Object value) {
            if(null == mBean) {
                mBean = new ParamBean();
            }
            mBean.addParam(key, value.toString());
            return this;
        }
    }

    public static interface CommListener{
        public boolean succeed(CommUnit nextUnit, Object resultObj) throws Exception;

        public void failed(Object resultObj) throws Exception;
    }


    public interface CommModule {
        public void communication(CommUnit commUnit, CommCallback commCallback);
    }

    public interface CommCallback {
        public void callback(CommResult result);
    }

    public interface CommUnitCallback {
        public void callback(CommUnit nextCommUnit);
    }

    public static class CommResult {
        public volatile ResultCode resultCode = ResultCode.FAILED;
        public volatile Object message = null;

        public CommResult() {
            super();
        }

        public CommResult(ResultCode code, String message) {
            super();
            this.resultCode = code;
            this.message = message;
        }

        public static final CommResult FAILED_RESULT = new CommResult(ResultCode.FAILED, null);
        public static final CommResult SUCCEED_RESULT = new CommResult(ResultCode.SUCCEED, null);
    }

    public static class CongKongCommModule implements CommModule{

        /*
        * Error Code
        */
        private final int ERROR_SERVER_SUCCESS							= 200;
        private final int ERROR_SERVER_400								= 400;
        private final int ERROR_SERVER_403								= 403;
        private final int ERROR_SERVER_404								= 404;
        private final int ERROR_SERVER_500								= 500;

        /*
        * HTTP Parameter
        */
        private final int	 	CONN_TIMEOUT				= 60;
        private final int		SOCKET_TIMEOUT				= 60;

        public void communication(CommUnit commUnit, CommCallback commCallback) {
            CommResult commResult = new CommResult();
            commResult.message = null;
            commResult.resultCode = ResultCode.FAILED;

            String jsonStr = null;
            int code = 0;
            try {
                DefaultHttpClient httpclient = new DefaultHttpClient();
                HttpParams httpParams = httpclient.getParams();
                HttpConnectionParams.setConnectionTimeout(httpParams, CONN_TIMEOUT * 1000);
                HttpConnectionParams.setSoTimeout(httpParams, SOCKET_TIMEOUT * 1000);
                httpclient.setParams(httpParams);

                HttpResponse response = null;

                switch(commUnit.mMethod) {
                    case GET:
                        HttpGet httpGet = new HttpGet(null == commUnit.mBean ? commUnit.mUri : commUnit.mBean.toUri(commUnit.mUri));
                        Log.e("CommUnit request(GET)", httpGet.getURI().toString());
                        response = httpclient.execute(httpGet);
                        break;
                    case POST:
                        HttpPost httpPost = new HttpPost(commUnit.mUri);
                        if(null != commUnit.mBean) {
                            httpPost.setEntity(new UrlEncodedFormEntity(commUnit.mBean.params(), HTTP.UTF_8));
                        }
                        Log.e("CommUnit request(POST)", commUnit.mUri + "\n" +( commUnit.mBean != null ? commUnit.mBean.toJson() : "null"));
                        response = httpclient.execute(httpPost);
                        break;
                }

                HttpEntity resEntity = response.getEntity();
                if (resEntity == null) {
                    commResult.resultCode = ResultCode.FAILED_CONNECTION;
                    commResult.message = "인터넷 연결상태를 확인해 주세요.";
                    commCallback.callback(commResult);
                }

                jsonStr = EntityUtils.toString(resEntity);
                Log.e("CommUnit response text", jsonStr);
                if(jsonStr == null) {
                    commResult.message = "정보확인에 실패하였습니다.";
                    commResult.resultCode = ResultCode.FAILED_JSON;
                    commCallback.callback(commResult);
                }
                Log.e("CommUnit response", jsonStr);
                JSONObject jsonObj = new JSONObject(jsonStr);

                code = jsonObj.optInt("code", -1);

                commResult.message = jsonStr;

            }
            catch(MalformedURLException e) {e.printStackTrace();
                commResult.resultCode = ResultCode.FAILED_CONNECTION;
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                commResult.resultCode = ResultCode.FAILED_TIMEOUT;
            } catch(IOException e) {e.printStackTrace();
            Log.e("CommUnit" , "========================IOException");
                commResult.resultCode = ResultCode.FAILED_CONNECTION;
            }
            catch (JSONException e) {e.printStackTrace();
                commResult.resultCode = ResultCode.FAILED_JSON;
            }
            catch (Exception e) {
            	Log.e("CommUnit" , "========================Exception");
                e.printStackTrace();
                commResult = CommResult.FAILED_RESULT;
            }
            finally {
                switch(code) {
                    case ERROR_SERVER_500 :
                    case ERROR_SERVER_404 :
                    case ERROR_SERVER_403 :
                    case ERROR_SERVER_400 : {
                        commResult.resultCode = ResultCode.FAILED_SERVER;
                        commResult.message = jsonStr;
                    }break;

                    default: {
                        commResult.resultCode = ResultCode.SUCCEED;
                        commResult.message = jsonStr;
                    } break;
                }

            }
            commCallback.callback(commResult);
            return;
        }
    }

    public static class CommFBModule implements CommModule {

        private final Facebook mFacebook;
        /*
         * Error Code
         */
        public static final int ERROR_APP_NONE = 1201;
        public static final int ERROR_APP_CONNECTION = 1401;
        public static final int ERROR_APP_JSON = 1402;
        public static final int ERROR_APP_FACEBOOK = 1403;

        public CommFBModule(Facebook facebook) {
            super();
            this.mFacebook = facebook;
        }

        @Override
        public void communication(CommUnit commUnit, final CommCallback commCallback) {

            new Thread(new Runnable() {
                @Override
                public void run() {
                    CommResult result = new CommResult();
                    int errorCode = ERROR_APP_NONE;
                    JSONObject jsonObj = null;
                    try {
                        jsonObj = Util.parseJson(mFacebook.request("me"));

                    } catch (FacebookError e) {
                        errorCode = ERROR_APP_FACEBOOK;
                        e.printStackTrace();
                    } catch (MalformedURLException e) {
                        errorCode = ERROR_APP_CONNECTION;
                        e.printStackTrace();
                    } catch (JSONException e) {
                        errorCode = ERROR_APP_JSON;
                        e.printStackTrace();
                    } catch (IOException e) {
                        errorCode = ERROR_APP_CONNECTION;
                        e.printStackTrace();
                    } catch( Exception e ) {
                        errorCode = ERROR_APP_FACEBOOK;
                        e.printStackTrace();
                    } finally {
                        Message msg = new Message();

                        switch (errorCode) {
                            case ERROR_APP_FACEBOOK: {
                                result.resultCode = ResultCode.FAILED;
                                result.message = "페이스북에서 문제가 발생하였습니다.";
                            }
                            break;

                            case ERROR_APP_JSON: {
                                result.resultCode = ResultCode.FAILED_JSON;
                                result.message = "정보확인에 실패하였습니다.";
                            }
                            break;

                            case ERROR_APP_CONNECTION: {
                                result.resultCode = ResultCode.FAILED_CONNECTION;
                                result.message = "인터넷 연결상태를 확인해 주세요.";
                            }
                            break;

                            default: {
                                result.resultCode = ResultCode.SUCCEED;
                                result.message = jsonObj;
                            }
                            break;
                        }

                        commCallback.callback(result);
                    }
                }
            }).start();
        }
    }
}