package kr.ebs.bandi;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

/**
 * @deprecated - 사용 확인되는 곳이 없음 (VideoServiceConnection.jav 빼고)
 * 
 * @since 2016. 7. 13.
 */
public class VideoService extends Service
{

	VideoView videoView;
	FrameLayout playBtn;
	ProgressBar loading;
	BandiApplication bandiApp;
	
	@Override
	public void onCreate()
	{
		Log.d("TEST", "asjdflkasjflkjsfld");
		
		super.onCreate();
		videoView = new VideoView(getApplicationContext());
		bandiApp = (BandiApplication)getApplication();
		play();
	}
	
	public void setControllView(FrameLayout playBtn, ProgressBar loading)
	{
		this.playBtn = playBtn;
		this.loading = loading;
	}
	
	public void play(String path)
	{
		videoView.setVideoPath(path);
		videoView.requestFocus();
		videoView.setOnPreparedListener(new OnPreparedListener()
		{
			@Override
			public void onPrepared(MediaPlayer mp)
			{
				
				mp.setOnBufferingUpdateListener(new OnBufferingUpdateListener()
				{
	                @Override
	                public void onBufferingUpdate(MediaPlayer mp, int percent) 
	                {
	                	if(percent == 100)
	                	{
	                		loading.setVisibility(View.INVISIBLE);
	    					playBtn.setSelected(false);
	    					mp.start();
	                	}
	                }
	            });
			}
		});
	}
	
	public VideoView getVideo()
	{
		return videoView;
	}
	
	public void play()
	{
		
		Log.d("TEST", "ajskldsfjlfsajldfk");
		
		if(bandiApp.getIsFMRadio())
		{
			videoView.setVideoPath(bandiApp.getStreamUrlAndroid());
		}
		else
		{
			videoView.setVideoPath(bandiApp.getStreamUrlIradioAndroid());
		}
		
		videoView.setOnPreparedListener(new OnPreparedListener()
		{
			@Override
			public void onPrepared(MediaPlayer mp)
			{
				
				mp.setOnBufferingUpdateListener(new OnBufferingUpdateListener()
				{
	                @Override
	                public void onBufferingUpdate(MediaPlayer mp, int percent) 
	                {
	                	if(percent == 100)
	                	{
	                		Log.d("TEST", "ajskldsfjafsdfsadfsadlfsajldfk");
	                		loading.setVisibility(View.INVISIBLE);
	    					playBtn.setSelected(false);
	    					mp.start();
	                	}
	                }
	            });
			}
		});
	}
	
	public void stopPlay()
	{
		videoView.stopPlayback();
	}
	
	@Override
	public IBinder onBind(Intent intent)
	{
		// TODO Auto-generated method stub
		return serviceBinder;
	}

	private final IBinder serviceBinder = new ServiceBinder();
	public class ServiceBinder extends Binder 
    {
		VideoService getService() 
        {
            return VideoService.this;
        }
    }
}
