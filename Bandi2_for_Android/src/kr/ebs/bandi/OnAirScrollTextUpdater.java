package kr.ebs.bandi;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toast;

public class OnAirScrollTextUpdater implements Runnable {
	final static String TAG = "OnAirScrollTextUpdater";
	private Context context;
	private Activity activity;
    BandiApplication bandiApp;
	
	/**<pre>
	 * 스크롤러 업데이트 제어용 클래스 변수 (handler 에서 변경)
	 * 1 또는 0 의 값을 가지며, 
	 * 1 일 경우 스크롤 텍스트가 업데이트 된다.</pre>
	 */
	private static int runUpdater = 1;
	
	/**
	 * 스크롤러 업데이트 제어 기능 추가된 핸들러
	 */
	public static Handler handler = new Handler() 
	{
		public void handleMessage(Message msg) 
		{
			
			switch(msg.what) 
			{
			case 0:
				runUpdater = 0;
				Log.d(TAG, "runUpdater : 0");
				break;
			case 1:
				runUpdater = 1;
				new OnAirScrollTextUpdater((Context) msg.obj);
				Log.d(TAG, "runUpdater : 1");
				break;
			default: 
				Log.d(TAG, "defalut");
			}
        }
	};
	
	private TextView scrollText;
	private Scroller scroller;
	private int idx;
//	ArrayList<String> txtList;
    public OnAirScrollTextUpdater(Context context) 
    {
    	this.context = context;
    	this.activity = (Activity) context;
    	bandiApp = (BandiApplication)activity.getApplication();
    	try 
    	{
    		scrollText = (TextView) activity.findViewById(R.id.scroll_text);
    		
    		if (scrollText != null) 
    		{
    			scrollText.setMovementMethod(new ScrollingMovementMethod());
            	scroller = new Scroller(context, new LinearInterpolator());
            	scrollText.setScroller(scroller);
            	    	
            	this.idx = 0;
            	
            	handler.removeCallbacks(this);
            	handler.post(this);
    		}
    	}
    	catch(Exception e) 
    	{
    		e.printStackTrace();
    	}
    	
    }
//    
//    public void setTextList(ArrayList<String> list)
//    {
//    	idx = 0;
//    	txtList = new ArrayList<String>(list);
//    }
//    
    
    @Override
    public void run() 
    {
    	
        try 
        {
        	if (activity.getWindow().findViewById(R.id.scroll_text) == null) 
        	{
        		Log.d(TAG, "return run()");
        		return;
        	}

            ArrayList<String> txtList = bandiApp.getBoardLineContents();
            int lineCount = txtList.size();
           
            //* 화면 사이즈에 맞게 스크롤 글자 자르기..***************
            WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
            Display d = wm.getDefaultDisplay();
            float maxWidth = (float) (d.getWidth() * 0.75);
            
            String scrollMsg = "";
            TextView scrollText = (TextView) activity.getWindow().findViewById(R.id.scroll_text);
            Paint paint = scrollText.getPaint();
            
            String sTxt = "";
            for(int i = 0; i < txtList.size(); i++)
            {
            	txtList.set(i, txtList.get(i).replace("\n", ""));
            	sTxt += txtList.get(i);
            	if(i < txtList.size())
            	{
            		sTxt += "@j";
            	}
            }
            
            String line[] = sTxt.split("@j");
            for (int i = 0; i < line.length; i++)
            {
            	
            	String leftReplace = line[i].replace("&lt;", "<");
				String rightReplce = leftReplace.replace("&gt;", ">");
				
            	int endPos = paint.breakText(rightReplce, true, maxWidth, null);
            	
            	if (! scrollMsg.equals("")) scrollMsg += "\n";
            	scrollMsg += rightReplce.substring(0, endPos);
            	if (rightReplce.length() > endPos) scrollMsg += "...";
            }
            scrollText.setText(scrollMsg);
            //************************/
    		
    		//scrollText.setText(pd.get().get(ProgramData.SCROLL).trim() + "\n" + st.toString().trim() );
            //scrollText.setText(st.toString().trim() );
            
            int height= scrollText.getLineHeight();
            //Log.d(TAG, ">> scroll : " + scrollText.getText());
            
            scroller.startScroll(0, idx * height, 0, height, 1* 1000);
            if(lineCount == 0) lineCount = 1;
            if (++idx % lineCount == 0) idx = 0;
            if (runUpdater == 1) handler.postAtTime(this, SystemClock.uptimeMillis() + (5 * 1000));
                        
        } 
        catch(Exception e) 
        {
            e.printStackTrace();
        }
    }
    
    
    /**
     * 프로세스 확인 (사용안함)
     * Log.d(TAG, Boolean.toString(isRunningProcess(context, "kr.ebs.bandi")));
     * @param context
     * @param packageName
     * @return
     */
    /*
    public static boolean isRunningProcess(Context context, String packageName) {
    	 
        boolean isRunning = false;
 
        ActivityManager actMng = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);                      
 
        List<RunningAppProcessInfo> list = actMng.getRunningAppProcesses();     
 
        for(RunningAppProcessInfo rap : list)                           
        {                                
            Log.d(TAG, ">>>"+rap.processName);
        	if(rap.processName.equals(packageName))                              
            {                                   
                isRunning = true;     
                break;
            }                         
        }
 
        return isRunning;
    }
	*********************/
}
