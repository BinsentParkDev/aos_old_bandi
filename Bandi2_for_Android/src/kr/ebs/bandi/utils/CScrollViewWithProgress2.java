package kr.ebs.bandi.utils;



import kr.ebs.bandi.R;
import kr.ebs.bandi.utils.CScrollViewWithProgress.OnScrollLoadingListener;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;


public class CScrollViewWithProgress2 extends ScrollView
{

	private int _yDelta;
	
	int p, bottomYDelta = 0;
	int target = 0;
	private int p1 = 0;
	private int target1 = 0;
	int m = 20;
	int k = 1;
	int b = 2;
	
	Handler handler;
	Thread thread;
	
	boolean isEnable = false;
	boolean isRunning = false;
	RelativeLayout layout;
	
	boolean isZero = false;
	boolean isRefresh = false;
	
	boolean isScroll = false;
	boolean isScrollStart = false;
	private boolean isScrollEnd = false;
	private boolean isFullToRefresh = true;
	
	int X;
	int Y;
	
	float tempY = 0;
	
	ImageView img;
	
	ImageView TweenRotate;
	Animation animation;
	
	boolean isOver = false;
	int screenHeight;
	ImageView imageView;
	Bitmap bmp;
	Context context;
	int marginValue = 30;
	
//	int[] startProgressRes = {
//								R.drawable.global_loading_start_01, R.drawable.global_loading_start_02,
//								R.drawable.global_loading_start_03, R.drawable.global_loading_start_04,
//								R.drawable.global_loading_start_05, R.drawable.global_loading_start_06,
//								R.drawable.global_loading_start_07, R.drawable.global_loading_start_08
//							 };
						
	ProgressBar pBar;
	int scrollState;
	boolean isLastPage = false;
	OnScrollLoadingListener listener;
	enum FLICK_MODE
	{
		
	}
	
	public CScrollViewWithProgress2(Context context)
	{
		super(context);
		setOverScrollMode(OVER_SCROLL_NEVER);
	}
	
	public CScrollViewWithProgress2(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		setOverScrollMode(OVER_SCROLL_NEVER);
	}

	public void isLastPage(boolean isLastPage)
	{
		this.isLastPage = isLastPage;
	}
	
	public void setContext(Context context)
	{
		this.context = context;
	}
	public void setProgressImageView(ImageView imageView)
	{
		this.imageView = imageView;  
		this.imageView.setVisibility(View.INVISIBLE);
	}
	
	public void setParent(RelativeLayout layout)
	{
		this.layout = layout;
		
	}
	
	public void setListener(OnScrollLoadingListener listener)
	{
		this.listener = listener;
	}
	
	public void setProgressBar(ProgressBar pBar)
	{
		this.pBar = pBar;
		this.pBar.setVisibility(View.INVISIBLE);
	}
	
	public void setLoading(ImageView img, Animation ani)
	{
		TweenRotate = img;
		animation = ani;
	}

	
	boolean isLoading = false;
	public void setHeight(int screenHeight)
	{
		this.screenHeight = screenHeight;
	}
	
	public void setScroll(boolean isScrolling)
	{
		this.isScrolling = isScrolling;
	}
	
	int frameIndex = 0;
	int topPadding = 0;
	boolean isScrolling = true;
	@Override
	public boolean onTouchEvent(MotionEvent ev)
	{
//		int pointerIndex = (ev.getAction() & MotionEvent.ACTION_POINTER_ID_MASK) >> MotionEvent.ACTION_POINTER_ID_SHIFT;
//		int pointerId = ev.getPointerId(pointerIndex);
		
		if(isLoading)
		{
			return false;
		}
		
		if(isScrolling == false)
		{
			return false;
		}
		
		X = (int) ev.getRawX();
	    Y = (int) ev.getRawY();
	    
	    switch (ev.getAction()) 
	    {
	    case MotionEvent.ACTION_DOWN:
	    	tempY = getScrollY();
	    	break;
	    case MotionEvent.ACTION_MOVE:
	    	isScroll = true;
	    	if(isFullToRefresh)
	    	{
		    	if(isScrollStart)
		    	{
		    		scrollState = 0;
		    		if(isRefresh == false)
					{
						RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) this.getLayoutParams();
				        _yDelta = Y - lParams.topMargin;
						isRefresh = true;
						isScroll = false;
					}
					
					RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.getLayoutParams();
					
	//	            layoutParams.leftMargin = 0;
		            if(Y - _yDelta < 0) setPadding(0, 0, 0, 0);// = 0;
		            else 
		            {
		            	setVerticalScrollBarEnabled(false);
		            	ev.setAction(MotionEvent.ACTION_MASK);
		            	topPadding = Y - _yDelta;
		            	topPadding *= 0.3f;
		            	setPadding(0, topPadding, 0, 0);
	//	            	layoutParams.topMargin = Y - _yDelta;
	//	            	layoutParams.topMargin *= 0.3;
	//	            	if(layoutParams.topMargin != 0 && getScrollY() != 0) scrollTo(0, 0);
		            	if(topPadding != 0 && getScrollY() != 0) scrollTo(0, 0);
		            }
		            
		            int topMargin = (int)(screenHeight * 0.3) / 10;
		            if(getPaddingTop() > topMargin)
		            {
		            	//imageView.setVisibility(View.VISIBLE);
		            	//pBar.setVisibility(View.INVISIBLE);
		            	frameIndex = (getPaddingTop()) / topMargin;
		            	if(frameIndex > 6)
		            	{
		            		frameIndex = 6;
		            	}
		            	new Handler().post(new Runnable()
						{
							
							@Override
							public void run()
							{
	//							imageView.setBackgroundResource(startProgressRes[frameIndex - 1]);
							}
						});
		            	
		            }
		            else
		            {
		            	//imageView.setVisibility(View.INVISIBLE);
		            	//pBar.setVisibility(View.INVISIBLE);
		            	//imageView.setBackgroundResource(startProgressRes[0]);
		            }
		            
		            this.setLayoutParams(layoutParams);
		    	}
	    	}
	    	
	    	if(isScrollEnd)
	    	{
//	    		scrollState = 1;
//	    		if(isRefresh == false)
//	    		{
//	    			RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) this.getLayoutParams();
//	    			bottomYDelta = Y - lParams.bottomMargin;
//	    			isRefresh = true;
//	    			isScroll = false;
//	    		}
//	    		
//	    		int bottomPadding = 0;
//
//	    		bottomPadding = bottomYDelta - Y;
//	    		bottomPadding *= 0.3;
//	    		
//	    		this.setPadding(0, 0, 0, bottomPadding);
	    	}
	    	
	    	break;
	    case MotionEvent.ACTION_UP:
	    	if(isFullToRefresh)
	    	{
				isRefresh = false;
				isScrollStart = false;
	        	isEnable = true;
	        	isRunning = true;
	        	
	        	if(isEnable)
	        	{
	        		setVerticalScrollBarEnabled(true);
	        		if(frameIndex >= 6)
	        		{
	//        			imageView.setVisibility(View.INVISIBLE);
	//        			pBar.setVisibility(View.VISIBLE);
			    		listPositionSetting((int)(screenHeight * 0.3) / 2, 0.5f);
			    		isLoading = true;
			    		listener.onLoadingStart();
	        		}
	        		else
	        		{
	        			//frameIndex = 1;
	//        			imageView.setVisibility(View.INVISIBLE);
	//        			pBar.setVisibility(View.INVISIBLE);
	        			isLoading = false;
	        			listPositionSetting(0, 0.5f);
	        		}
	        	}
	    	}
	    	break;
	    }
	    
		return super.onTouchEvent(ev);
	}
	
	public void noFullToRefresh()
	{
		isFullToRefresh = false;
	}
	
	public void loadingFinish()
	{
		frameIndex = 1;
		//pBar.setVisibility(View.INVISIBLE);
		listPositionSetting(0, 0.1f);
		
	}
	
	private void listPositionSetting(int _target, float accel)
	{
		isRunning = true;
		this.accel = accel;
		if(scrollState == 0)
		{
	    	p = getPaddingTop();
	    	target = _target;
		}
		else
		{
			p1 = getPaddingBottom();
			if(p1 < 0) p1 = 0;
			target1 = _target;
		}
    	handler = new Handler(callback);
    	thread = new Thread(new Runnable() 
    	{
			
			@Override
			public void run()
			{
				while(isRunning) 
				{
					try 
					{
						Message msg = new Message();
						msg.what = scrollState;
						
						handler.sendMessage(msg);
						Thread.sleep(30);
					} catch (InterruptedException e) { e.printStackTrace(); }

				}
			}
		});
		thread.start();
	}
	
	int scrollCount = 0;
	boolean isTrace = false;
	int scrollLastPosition = 0;
	@Override
	public void computeScroll()
	{
		super.computeScroll();
		if(isScroll)
		{
			if(getScrollY() <= 0)
			{
				scrollLastPosition = 0;
				isScrollStart = true;
				isScrollEnd = false;
			}
			else if((getScrollY() + getHeight()) == computeVerticalScrollRange())
			{
				if(isLastPage == true)
				{
					isScrollStart = false;
					isScrollEnd = true;
				}
				else
				{
					if(scrollLastPosition == 0)
					{
						listener.onScrollLastPosition();
					}
					scrollLastPosition++;
				}
				
			}
			else 
			{
				scrollLastPosition = 0;
			}
		}
	}
	
	float accel;
	private Callback callback = new Callback()
	{
		@Override
		public boolean handleMessage(Message msg)
		{
			if(msg.what == 0) 
			{
				if(isRunning == true)
				{
					
					p += accel * (target - p);
					
	//				lParams.topMargin = p;
					if(p <= 0)
					{
						isLoading = false;
						isRunning = false;
						thread = null;
					}
	//				setLayoutParams(lParams);
					setPadding(0, p, 0, 0);
				}
			}
			else if(msg.what == 1)
			{
				if(isRunning == true)
				{
					p1 += accel * (target1 - p1);
					if(p1 <= 0)
					{
						isLoading = false;
						isRunning = false;
						thread = null;
					}
					
					setPadding(0, 0, 0, p1);
				}
			}
			return false;
		}
	};
	
//	public interface OnScrollLoadingListener
//	{
//		public void onLoadingStart();
//		public void onLoadingFinish();
//		public void onScrollLastPosition();
//	}
	
	
}
