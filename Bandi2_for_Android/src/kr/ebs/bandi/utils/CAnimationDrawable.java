package kr.ebs.bandi.utils;

import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;

public class CAnimationDrawable extends AnimationDrawable
{
	Handler mAnimationHandler;
	OnAnimationListener listener;
	
	public CAnimationDrawable(AnimationDrawable drawable)
	{
		for (int i = 0; i < drawable.getNumberOfFrames(); i++) 
		{
	        this.addFrame(drawable.getFrame(i), drawable.getDuration(i));
	    }
	}
	
	@Override
	public void start()
	{
		// TODO Auto-generated method stub
		super.start();
		mAnimationHandler = new Handler();
		mAnimationHandler.postDelayed(new Runnable() 
		{
		     public void run() 
		     {
		    	 if(listener != null) listener.onAnimationFinish(CAnimationDrawable.this);
		     }
		}, getTotalDuration());
	}
	
	
	public void setListener(OnAnimationListener listener)
	{
		this.listener = listener;
	}
	
	/**
	 * Gets the total duration of all frames.
	 * 
	 * @return The total duration.
	 */
	public int getTotalDuration() 
	{

	    int iDuration = 0;

	    for (int i = 0; i < this.getNumberOfFrames(); i++) 
	    {
	        iDuration += this.getDuration(i);
	    }

	    return iDuration;
	}
	
	public interface OnAnimationListener
	{
		public void onAnimationFinish(AnimationDrawable ani);
	}
}
