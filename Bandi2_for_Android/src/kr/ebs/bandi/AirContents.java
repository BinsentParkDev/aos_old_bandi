package kr.ebs.bandi;

import java.util.HashMap;

import kr.ebs.bandi.ProgramData.DataParseListener;
import kr.ebs.bandi.data.CProgramData;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.util.Log;
import android.widget.TextView;


public class AirContents implements BaseInterface {
	static final String TAG = "AirContents";

	private Context context;
	private Activity activity;

	public AirContents(Context context) {
		this.context = context;
		this.activity = (Activity) context;

		((BandiApplication) activity.getApplication()).setCurrentMenu(TAG);


		/*
		 * 접근 로그 저장
		 */
		BandiLog.event(context, TAG);
	}

	String contentsText = "";
	TextView programName;

	@Override
	public void run() {
//		BaseActivity.swapTitle(context, R.string.air_contents);
//		BaseActivity.swapLayout(context, R.layout.layout_air_contents);
//
//		programName = (TextView) activity.findViewById(R.id.program_name);
//		TextView airContentsText = (TextView) activity.findViewById(R.id.air_contents_text);
//
//		TimeTable tt = new TimeTable(context);
//		HashMap <String, String> tmap = tt.get();
//
//		ProgramData pd = null;
//		HashMap <String, String> pmap = null;
//		ProgramData data = new ProgramData(context);
//		data.getData(new DataParseListener()
//		{
//
//			@Override
//			public void OnParseComplete()
//			{
//				// TODO Auto-generated method stub
//				CProgramData pData = CProgramData.getInstance();
//				programName.setText(pData.getProgramTitle());
//
//				if (pData.getContents() == null || pData.getContents().equals("null") || "".equals(pData.getContents().trim())) {
//		        	contentsText = "등록된 방송 내용이 없습니다.";
//		        } else {
//		        	contentsText = pData.getContents();
//		        }
//
//			}
//		});

//		try {
//			pd = new ProgramData(context);
//			pmap = pd.get();
//
//			programName.setText(tmap.get(TimeTable.PROGNAME));
//
//			if (pmap.get(ProgramData.CONTENTS) == null || "".equals(pmap.get(ProgramData.CONTENTS).trim())) {
//	        	contentsText = "등록된 방송 내용이 없습니다.";
//	        } else {
//	        	contentsText = pmap.get(ProgramData.CONTENTS);
//	        }
//		} catch(Exception e) {
//			e.printStackTrace();
//			contentsText = "방송 내용을 불러 올 수 없습니다.";
//		}

		/* 줄바꿈 test ************
		ScrollText st = new ScrollText(context);
		st.get();
		contentsText = st.toString();
		Paint p = airContentsText.getPaint();
		int endIndex = p.breakText(contentsText, true, 400, null);
		contentsText += "---------------\n" + contentsText.substring(0, endIndex);
		//* ***********/

//		airContentsText.setText(contentsText);

	}

}
