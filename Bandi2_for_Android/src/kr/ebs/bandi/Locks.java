package kr.ebs.bandi;

import java.util.Calendar;

import android.content.Context;
import android.content.ContextWrapper;
import android.os.PowerManager;

/**
 * Working draft
 * 
 * @since 2017. 5. 24.
 */
public class Locks {
	static final String TAG = "Locks";
	
	private volatile static Locks locks = null;
	private Context context;
	final String wakeLockTag = "bandi_wake_lock";
	final String wifiLockTag = "bandi_wifi_lock";
	
	private PowerManager.WakeLock wakeLock = null;
	
	private Locks(Context context) {
		this.context = context;
	}
	
	public static  Locks getInstance(Context context) {
		if (locks == null) {
			synchronized (Locks.class) {
				if (locks == null) {
					locks = new Locks(context);
				}
			}	
		}
		return locks;
	}
	
	public void acquirePartialWakeLock() {
		PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
	    wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, wakeLockTag);
	    wakeLock.acquire();
	}
	
	public void releaseWakeLock() {
		
	}
	
	public void acquireWifiLock() {
		
	}
	
	public void releaseWifiLock() {
		
	}
}