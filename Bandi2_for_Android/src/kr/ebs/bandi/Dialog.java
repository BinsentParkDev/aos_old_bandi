package kr.ebs.bandi;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class Dialog {
	
	/**
	 * alert 창을 통해 메세지를 보여준다. <br>
     * (확인)버튼만 있음. <br>
	 * @param context
	 * @param msg
	 */
    static void showAlert(Context context, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(msg)
               .setCancelable(false)
               .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                   }
               });
        AlertDialog alert = builder.create();
        alert.show();
    }
    
    
    
    /**<pre>
     * 제목과 메세지를 동시에 보여줌
     * 확인 버튼만 노출
     * </pre>
     * @param context
     * @param title
     * @param msg
     */
    static void alert(Context context, String title, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(msg)
               .setTitle(title)
               .setCancelable(false)
               .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                   }
               });
        AlertDialog alert = builder.create();
        alert.show();
    }
    
    
    /**
     * 타이틀, 메세지와 {확인, 취소} 중 '확인'을 눌렀을 때 액션 정의
     * @param context
     * @param title
     * @param msg
     * @param positiveClickListener
     */
    static void confirm(Context context, String title, String msg, DialogInterface.OnClickListener positiveClickListener) {
        confirm(context,title, msg, "확인", "취소", positiveClickListener);
    }
    
    
    /**
     * 타이틀, 메세지와 positive, negative 문구 및 positive button 눌렀을 때 액션 정의
     * @param context
     * @param title
     * @param msg
     * @param positiveMsg
     * @param negativeMsg
     * @param positiveClickListener
     */
    static void confirm(Context context, String title, String msg, 
    		String positiveMsg, String negativeMsg, 
    		DialogInterface.OnClickListener positiveClickListener) 
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(msg)
               .setTitle(title)
               .setCancelable(false)
               .setNegativeButton(negativeMsg, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int which) {
                       dialog.dismiss();
                   }
               })
               .setPositiveButton(positiveMsg, positiveClickListener);
               
        AlertDialog alert = builder.create();
        alert.show();
    }
    
    /**
     * 타이틀, 메세지와 positive, negative 문구 및 각 버튼의 클릭에 대한 눌렀을 때 액션 정의
     * @param context
     * @param title
     * @param msg
     * @param positiveMsg
     * @param negativeMsg
     * @param positiveClickListener
     * @param negativeClickListener
     */
    static void confirm(Context context, String title, String msg, 
    		String positiveMsg, String negativeMsg, 
    		DialogInterface.OnClickListener positiveClickListener,
    		DialogInterface.OnClickListener negativeClickListener) 
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(msg)
               .setTitle(title)
               .setCancelable(false)
               .setNegativeButton(negativeMsg, negativeClickListener)
               .setPositiveButton(positiveMsg, positiveClickListener);
               
        AlertDialog alert = builder.create();
        alert.show();
    }
    
    /**
     * 제목이 없는 형태의 confirm 창. (긍정 액션에 대한 글자를 입력해야 함.)
     * @param context
     * @param msg
     * @param positiveClickListener
     */
    static void confirm(Context context, String msg, 
    		DialogInterface.OnClickListener positiveClickListener, 
    		String positiveButtonText) 
    {
    	confirm(context, msg, positiveClickListener, positiveButtonText, "닫기");
    }
    
    
    
    /**
     * 제목 없이 메세지만 노출 <br>
     * positive action 은 정의해야함 <br>
     * negative action 은 dialog.dismiss() <br>
     * 
     * @param context
     * @param msg
     * @param positiveClickListener
     * @param positiveButtonText
     * @param negativeButtonText
     */
    static void confirm(Context context, String msg, 
    		DialogInterface.OnClickListener positiveClickListener, 
    		String positiveButtonText,
    		String negativeButtonText) 
    {
    	DialogInterface.OnClickListener negativeClickListener = new DialogInterface.OnClickListener() 
    	{
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
    	};
    	
    	confirm(context, msg, positiveClickListener, negativeClickListener, positiveButtonText, negativeButtonText);
    }
    
    /**
     * 제목없이 메세지만 노출 <br>
     * positive, negative action 및 button 명 정의해야함 <br>
     * 
     * @param context
     * @param msg
     * @param positiveClickListener
     * @param negativeClickListener
     * @param positiveButtonText
     * @param negativeButtonText
     */
    static void confirm(Context context, String msg, 
    		DialogInterface.OnClickListener positiveClickListener, 
    		DialogInterface.OnClickListener negativeClickListener,
    		String positiveButtonText,
    		String negativeButtonText) 
    {
    	AlertDialog.Builder builder = new AlertDialog.Builder(context);
    	builder.setMessage(msg)
        .setCancelable(false)
        .setNegativeButton(negativeButtonText, negativeClickListener)
        .setPositiveButton(positiveButtonText, positiveClickListener);
		        
		 AlertDialog alert = builder.create();
		 alert.show();
    }
    
    /**
     * 타이틀이 있는 사용자 확인(confirm)창
     * @param context
     * @param title
     * @param msg
     * @param positiveClickListener
     * @param positiveButtonText
     * @param negativeButtonText
     */
    static void confirmWithTitle(Context context, String title, String msg, 
    		DialogInterface.OnClickListener positiveClickListener, 
    		String positiveButtonText,
    		String negativeButtonText) 
    {
    	AlertDialog.Builder builder = new AlertDialog.Builder(context);
    	builder.setTitle(title)
    	.setMessage(msg)
    	.setCancelable(false)
    	.setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener() 
    		{
             public void onClick(DialogInterface dialog, int which) {
                 dialog.dismiss();
             }
        })
        .setPositiveButton(positiveButtonText, positiveClickListener);
		        
		 AlertDialog alert = builder.create();
		 alert.show();
    }
    
    /**
     * 종료 여부 확인 용.
     * @param context
     * @param msg
     * @param positiveClickListener
     */
    static void confirmQuit(Context context, String msg, DialogInterface.OnClickListener positiveClickListener) {
    	confirm(context, msg, positiveClickListener, "종료");
    }
    
}
