package kr.ebs.bandi;



import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ShareDialog extends Dialog 
{
	
	private Context context;
	private DisplayMetrics dm;
	private int dpWidth, dpHeight;
	private SharePopupListener listener;
	private RelativeLayout.LayoutParams rParams;
	private FrameLayout.LayoutParams fParams;
	private LinearLayout.LayoutParams lParams;
	private ImageButton close, twitter, facebook, kakao;
	
	public ShareDialog(Context context) 
	{
		super(context);
		this.context = context;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_share);
//		getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
//		layoutParams.dimAmount = 0.9f;
		getWindow().setAttributes(layoutParams);
		getWindow().setBackgroundDrawable(new ColorDrawable(000000));
		
		dm = new DisplayMetrics();
		((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(dm);
		
		dpWidth = (int)(dm.widthPixels / dm.density);
		dpHeight = (int)(dm.heightPixels / dm.density);
		
		LinearLayout main = (LinearLayout)findViewById(R.id.sharePopup_main);
		rParams = new RelativeLayout.LayoutParams(getWidthDP(230), getWidthDP(131));
		main.setLayoutParams(rParams);
		
		FrameLayout topArea = (FrameLayout)findViewById(R.id.sharePopupTopArea);
		lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, getWidthDP(42));
		topArea.setLayoutParams(lParams);
		
		TextView title = (TextView)findViewById(R.id.sharePopup_title);
		title.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15));
		
		close = (ImageButton)findViewById(R.id.sharePopup_close);
		twitter = (ImageButton)findViewById(R.id.sharePopup_twitter);
		facebook = (ImageButton)findViewById(R.id.sharePopup_facebook);
		kakao = (ImageButton)findViewById(R.id.sharePopup_kakao);
		
		fParams = new FrameLayout.LayoutParams(getWidthDP(28), getWidthDP(28), Gravity.CENTER_VERTICAL | Gravity.RIGHT);
		fParams.setMargins(0, 0, getWidthDP(7), 0);
		close.setLayoutParams(fParams);
		
		lParams = new LinearLayout.LayoutParams(getWidthDP(43), getWidthDP(43));
		twitter.setLayoutParams(lParams);
		
		lParams = new LinearLayout.LayoutParams(getWidthDP(43), getWidthDP(43));
		lParams.setMargins(getWidthDP(30), 0, 0, 0);
		facebook.setLayoutParams(lParams);
		
		lParams = new LinearLayout.LayoutParams(getWidthDP(43), getWidthDP(43));
		lParams.setMargins(getWidthDP(30), 0, 0, 0);
		kakao.setLayoutParams(lParams);
		
		close.setOnClickListener(clickListener);
		twitter.setOnClickListener(clickListener);
		facebook.setOnClickListener(clickListener);
		kakao.setOnClickListener(clickListener);
	}
	
	android.view.View.OnClickListener clickListener = new android.view.View.OnClickListener()
	{
		
		@Override
		public void onClick(View v) 
		{
			if(v.getId() == R.id.sharePopup_close)
			{
				dismiss();
			}
			else if(v.getId() == R.id.sharePopup_twitter)
			{
				if(listener != null) listener.twitterClick();
				dismiss();
			}
			else if(v.getId() == R.id.sharePopup_facebook)
			{
				if(listener != null) listener.facebookClick();
				dismiss();
			}
			else if(v.getId() == R.id.sharePopup_kakao)
			{
				if(listener != null) listener.kakaoClick();
				dismiss();
			}
		}
	};
	
	public void setListener(SharePopupListener listener)
	{
		this.listener = listener;
	}

	public int getWidthDP(int dpValue)
    {
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpWidth / 360, context.getResources().getDisplayMetrics());
    }
	
	public int getHeightDP(int dpValue)
	{
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpHeight / 640, context.getResources().getDisplayMetrics());
	}
	
	public int getCalSP(int spValue)
	{
		return (int)(spValue * dpHeight / 640);
	}
	
	public interface SharePopupListener
	{
		public void twitterClick();
		public void facebookClick();
		public void kakaoClick();
	}
	
}
