package kr.ebs.bandi;

public class TextUtil {
	final static String TAG = "TextUtil";
	
	public static String HTMLDecode(String str)	{
		str = str.replace("&lt;", "<");
		str = str.replace("&gt;", ">");
		str = str.replace("&#34;", "\"");
		str = str.replace("&#35;", "#");
		str = str.replace("&#39;", "'");
		str = str.replace("&#40;", "(");
		str = str.replace("&#41;", ")");
		str = str.replace("&#42;", "*");
		str = str.replace("&#43;", "+");
		str = str.replace("&#44;", ",");
		str = str.replace("&#45;", "-");
		str = str.replace("&#46;", ".");
		str = str.replace("&#47;", "/");

		str = str.replace("&#38;", "&");
		str = str.replace("&amp;", "&");
		
		return str;
	}
}
