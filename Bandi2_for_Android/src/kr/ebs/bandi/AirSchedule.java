package kr.ebs.bandi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import kr.ebs.bandi.data.ScheduleCompare;
import kr.ebs.bandi.GoogleAnalytics;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("SimpleDateFormat")
public class AirSchedule implements BaseInterface
{
	static final String TAG = "AirSchedule";

	BandiApplication bandiApp;
	public AudioServiceConnection serviceConnection;

	MainActivity main;
//	String radio = Url.FM_TIMETABLE_HTML;
//	String iRadio = Url.IRADIO_TIMETABLE_HTML;
	String url;

	private Context context;
	private Activity activity;

	private int[] dayOfWeekIDs = {0, R.id.sunday_btn, R.id.monday_btn, R.id.tuesday_btn, R.id.wednesday_btn, R.id.thursday_btn, R.id.friday_btn, R.id.saturday_btn};
	final String[] btnColorValue = {"", "#898077", "#9e9188", "#ac9e93", "#9e9188", "#7d746b", "#898077", "#9e9188"};
	private SparseArray<FrameLayout> dayOfWeekBtn = new SparseArray<FrameLayout>();
	WebView webView;
	ChannelChangeInterface channerChangeListener;

	public DisplayMetrics dm;
	public int dpWidth, dpHeight;

	private FrameLayout.LayoutParams fParams;
	private LinearLayout.LayoutParams lParams;

	Preferences prefs;
	

	public AirSchedule(Context context)
	{
		this.context = context;
		this.activity = (Activity) context;
		main = (MainActivity)activity;
		prefs = new Preferences(context);

		bandiApp = (BandiApplication)activity.getApplication();

        serviceConnection = AudioServiceConnection.getInstance();

        dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		dpWidth = (int)(dm.widthPixels / dm.density);
		dpHeight = (int)(dm.heightPixels / dm.density);

        if(bandiApp.getIsFMRadio()) url = Url.FM_TIMETABLE_HTML;
        else url = Url.IRADIO_TIMETABLE_HTML;

		/*
		 * 현재화면(메뉴) 위치 기록
		 */
		((BandiApplication) activity.getApplication()).setCurrentMenu(TAG);


		/*
		 * 접근 로그 저장
		 */
		if(bandiApp.getIsFMRadio())
		{
			BandiLog.event(context, "AirSchedule");
		}
		else
		{
			BandiLog.event(context, "AirScheduleIradio");
		}
		
	}


	@Override
	public void run()
	{
		BaseActivity.swapTitle(context, R.string.air_schedule);
		BaseActivity.swapLayout(context, R.layout.layout_air_schedule);

		setListener();

		setAirScheduleWeb();
		
		//** Google Analytics **
		if(bandiApp.getIsFMRadio())
		{
			new GoogleAnalytics(this.activity).sendScreenView(TAG, GoogleAnalytics.CHANNEL.FM);
		}
		else
		{
			new GoogleAnalytics(this.activity).sendScreenView(TAG, GoogleAnalytics.CHANNEL.IRADIO);
		}
		
	}

	OnClickListener topBtnListener = new OnClickListener()
	{

		@Override
		public void onClick(View v)
		{
			if(v.getId() == R.id.topMenuBtn0)
			{
				if(v.isSelected()) return;
				BandiLog.event(context, "AirSchedule");
				ScheduleCompare.getInstance().setData(bandiApp.getRadioData());
				if(v.isSelected() == false)
				{
					v.setSelected(true);
					activity.findViewById(R.id.topMenuBtn1).setSelected(false);
					bandiApp.setIsFMRadio(true);
					url = Url.FM_TIMETABLE_HTML;
					if(calendar == null)
					{
						setAirScheduleWeb();
					}
					else
					{
						setDayOfWeek(calendar);
						setDayOfWeekClickAction(calendar);
					}

					if(MainActivity.isWifi(context) == false && prefs.getUsePaidNetwork() == false)
					{
						String confirmMsg = context.getResources().getString(R.string.use_paid_network_alert_msg);
						Dialog.confirm(context, confirmMsg, new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialog, int which)
							{
//								boardTextLayout.setVisibility(View.GONE);
								new Handler().postDelayed(new Runnable() {

									@Override
									public void run() {
										// TODO Auto-generated method stub
										LinearLayout setting = (LinearLayout) activity.findViewById(R.id.layout_menu7);
										setting.performClick();
									}
								}, 300);
							}
						}, "설정으로 이동");
						return;
					}

					if(serviceConnection.getAudioService() != null)
					{
						serviceConnection.getAudioService().stopPlayer(main.playBtn, main.loading);
//						serviceConnection.getAudioService().switchUrl(bandiApp.getStreamUrlAndroid(), bandiApp.getStreamUrlAndroid2());
						serviceConnection.getAudioService().switchUrl(AudioService.Channel.FM, 
								bandiApp.getStreamUrlAndroid(), bandiApp.getStreamUrlAndroid2());
						serviceConnection.getAudioService().initPlayer(main.playBtn, main.loading);
					}
					else
					{
						try { main.bindAudioService(); } catch (Exception e) { Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show(); }
					}

				}
			}
			else if(v.getId() == R.id.topMenuBtn1)
			{
				if(v.isSelected()) return;
				BandiLog.event(context, "AirScheduleIradio");
				ScheduleCompare.getInstance().setData(bandiApp.getIRadioData());
				if(v.isSelected() == false)
				{
					v.setSelected(true);
					activity.findViewById(R.id.topMenuBtn0).setSelected(false);
					bandiApp.setIsFMRadio(false);
					activity.findViewById(R.id.layout_viewRadio).setVisibility(View.INVISIBLE);
					url = Url.IRADIO_TIMETABLE_HTML;
					if(calendar == null)
					{
						setAirScheduleWeb();
					}
					else
					{
						setDayOfWeek(calendar);
						setDayOfWeekClickAction(calendar);
					}

					if(MainActivity.isWifi(context) == false && prefs.getUsePaidNetwork() == false)
					{
						String confirmMsg = context.getResources().getString(R.string.use_paid_network_alert_msg);
						Dialog.confirm(context, confirmMsg, new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialog, int which)
							{
//								boardTextLayout.setVisibility(View.GONE);
								new Handler().postDelayed(new Runnable() {

									@Override
									public void run() {
										// TODO Auto-generated method stub
										LinearLayout setting = (LinearLayout) activity.findViewById(R.id.layout_menu7);
										setting.performClick();
									}
								}, 300);
							}
						}, "설정으로 이동");
						return;
					}

					if(serviceConnection.getAudioService() != null)
					{
						serviceConnection.getAudioService().stopPlayer(main.playBtn, main.loading);
//						serviceConnection.getAudioService().switchUrl(bandiApp.getStreamUrlIradioAndroid(), bandiApp.getStreamUrlIradioAndroid2());
						serviceConnection.getAudioService().switchUrl(AudioService.Channel.IRADIO,
								bandiApp.getStreamUrlIradioAndroid(), bandiApp.getStreamUrlIradioAndroid2());
						serviceConnection.getAudioService().initPlayer(main.playBtn, main.loading);
					}
					else
					{
						try { main.bindAudioService(); } catch (Exception e) { Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show(); }
					}

				}
			}
		}
	};


	String day;
	Calendar calendar;
	@SuppressLint("NewApi")
	private void setListener()
	{

		FrameLayout scheduleTop = (FrameLayout)activity.findViewById(R.id.layout_scheduleTop);
		scheduleTop.setPadding(0, getWidthDP(5), 0, getWidthDP(5));

		ImageView icon = (ImageView)activity.findViewById(R.id.imageView_mainIcon);
		icon.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				activity.findViewById(R.id.layout_menu1).performClick();
			}
		});

		main.repeatListViewInit();

		TextView day0 = (TextView)activity.findViewById(R.id.textView_day0);
		TextView day1 = (TextView)activity.findViewById(R.id.textView_day1);
		TextView day2 = (TextView)activity.findViewById(R.id.textView_day2);
		TextView day3 = (TextView)activity.findViewById(R.id.textView_day3);
		TextView day4 = (TextView)activity.findViewById(R.id.textView_day4);
		TextView day5 = (TextView)activity.findViewById(R.id.textView_day5);
		TextView day6 = (TextView)activity.findViewById(R.id.textView_day6);

		day0.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
		day1.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
		day2.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
		day3.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
		day4.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
		day5.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
		day6.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));


		FrameLayout controlBg = (FrameLayout)activity.findViewById(R.id.control_bar_bg);
		controlBg.setBackgroundColor(Color.parseColor("#3f3833"));
		activity.findViewById(R.id.topMenuBtn0).setOnClickListener(topBtnListener);
		activity.findViewById(R.id.topMenuBtn1).setOnClickListener(topBtnListener);
		/*
		 * 날짜 변경 관련
		 */
		// 오늘 날짜 셋팅

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		try { cal.setTime(sdf.parse(ScheduleCompare.getInstance().getDateForXmlRetrieving())); } catch (ParseException e) {}
		final Calendar _today = cal;

		lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lParams.setMargins(getWidthDP(10), 0, getWidthDP(10), 0);
		final TextView airScheduleDate = (TextView) activity.findViewById(R.id.today_text);
		airScheduleDate.setLayoutParams(lParams);
		airScheduleDate.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(20));
		airScheduleDate.setText( DateUtil.getDateString(_today) );
		setDayOfWeek(_today.get(Calendar.DAY_OF_WEEK));
		setDayOfWeekClickAction(_today);

		// 전날로 이동.
		lParams = new LinearLayout.LayoutParams(getHeightDP(30), getHeightDP(30));
		ImageButton prevBtn = (ImageButton) activity.findViewById(R.id.prev_btn);
		prevBtn.setLayoutParams(lParams);
		prevBtn.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				_today.add(Calendar.DATE, -1);
				calendar = _today;
				airScheduleDate.setText( DateUtil.getDateString(_today) );
				setDayOfWeek(_today);
				setDayOfWeekClickAction(_today);
			}
		});

		// 다음날로 이동.
		lParams = new LinearLayout.LayoutParams(getHeightDP(30), getHeightDP(30));
		ImageButton nextBtn = (ImageButton) activity.findViewById(R.id.next_btn);
		nextBtn.setLayoutParams(lParams);
		nextBtn.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				_today.add(Calendar.DATE, 1);
				calendar = _today;
				airScheduleDate.setText( DateUtil.getDateString(_today) );
				setDayOfWeek(_today);
				setDayOfWeekClickAction(_today);
			}
		});

		// 오늘 날짜로 이동
		fParams = new FrameLayout.LayoutParams(getHeightDP(63), getHeightDP(31), Gravity.CENTER_VERTICAL | Gravity.RIGHT);
		fParams.setMargins(0, 0, getWidthDP(5), 0);
		ImageButton todayBtn = (ImageButton) activity.findViewById(R.id.today_btn);
		todayBtn.setLayoutParams(fParams);
		todayBtn.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				setListener();
				setAirScheduleWeb();
			}
		});

		lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, getWidthDP(65));
		LinearLayout scheduleDay = (LinearLayout)activity.findViewById(R.id.layout_scheduleDay);
		scheduleDay.setLayoutParams(lParams);

	} // end setListener()


	/**
	 * 현재 선택일을 기준으로 요일 버튼을 클릭했을 경우 액션 정의
	 */
	private void setDayOfWeekClickAction(Calendar today)
	{
		/*
		 * 요일 버튼 매핑 (일~토:1~7)
		 */
		for (int i=1; i<dayOfWeekIDs.length; i++)
		{
			dayOfWeekBtn.append(i, (FrameLayout) activity.findViewById(dayOfWeekIDs[i]));
		}

		for (int i=1; i<=dayOfWeekBtn.size(); i++)
		{
			dayOfWeekBtn.get(i).setOnClickListener(new OnDayOfWeekClickListener(today, i));
		}
	}

	class OnDayOfWeekClickListener implements View.OnClickListener
	{
		private Calendar cal;
		private int dayOfWeek;

		public OnDayOfWeekClickListener(Calendar cal, int dayOfWeek)
		{
			this.cal = cal;
			this.dayOfWeek = dayOfWeek;
		}

		@Override
		public void onClick(View v)
		{
			final TextView airScheduleDate = (TextView) activity.findViewById(R.id.today_text);
			cal.add(Calendar.DAY_OF_MONTH, -(cal.get(Calendar.DAY_OF_WEEK)-dayOfWeek) );
			calendar = cal;
			airScheduleDate.setText( DateUtil.getDateString(cal) );
			setDayOfWeek(cal);
			setDayOfWeekClickAction(cal);
		}
	}

	/**
	 * 모든 요일을 선택하지 않은 상태로 돌린다.
	 */
	private void unsetDayOfWeek()
	{
		for(int i = 1; i < 8; i++)
		{
//			if (dayOfWeek != 0)
//			{
				activity.findViewById(dayOfWeekIDs[i]).setSelected(false);
				activity.findViewById(dayOfWeekIDs[i]).setBackgroundColor(Color.parseColor(btnColorValue[i]));
//			}
		}
	}

	/**
	 * calendar.get(Calendar.DAY_OF_WEEK) 의 값을 변수로 받아 요일을 설정한다. (1-7)
	 * @param dayOfWeek
	 * @param unsetAll
	 */
	private void setDayOfWeek(int dayOfWeek, boolean unsetAll)
	{
		if (unsetAll) unsetDayOfWeek();
		if (dayOfWeek < 1 || dayOfWeek > 7) return;
		activity.findViewById(dayOfWeekIDs[dayOfWeek]).setSelected(true);
		activity.findViewById(dayOfWeekIDs[dayOfWeek]).setBackgroundColor(Color.parseColor("#3f3833"));
	}

	/**
	 * calendar.get(Calendar.DAY_OF_WEEK) 의 값을 변수로 받아 요일을 설정한다. (1-7)
	 * @param dayOfWeek
	 */
	private void setDayOfWeek(int dayOfWeek)
	{

		setDayOfWeek(dayOfWeek, true);
	}

	/**
	 * 사용자가 날짜나 요일을 변경시켰을 때, 날짜를 변경하고 해당일의 편성표 웹페이지를 호출한다.
	 * @param cal
	 */
	private void setDayOfWeek(Calendar cal)
	{
		setDayOfWeek(cal.get(Calendar.DAY_OF_WEEK), true);
		setAirScheduleWeb(new SimpleDateFormat("yyyyMMdd", Locale.KOREA).format(cal.getTime()));
	}

	/**
	 * 편성 웹 페이지 불러오기
	 */
	@SuppressLint({ "SetJavaScriptEnabled", "ClickableViewAccessibility" })
	private void setAirScheduleWeb(String date8digits)
	{
		WebView airScheduleWeb = (WebView) activity.findViewById(R.id.webview_air_schedule);
		airScheduleWeb.setOnTouchListener(new OnTouchListener()
		{

			@Override
		    public boolean onTouch(View v, MotionEvent event)
		    {
		        if (event.getAction() == MotionEvent.ACTION_UP)
		            main.mainScroll.requestDisallowInterceptTouchEvent(false);
		        else
		        	main.mainScroll.requestDisallowInterceptTouchEvent(true);

		        return false;
		    }
		});
		airScheduleWeb.getSettings().setJavaScriptEnabled(true);

		//if (date8digits.equals("") || date8digits.equals(DateUtil.get8DigitsDateString(DateUtil.getKoreanCalendar()))) {
		if (date8digits.equals("") || isTodaySchedule(date8digits))
		{
			airScheduleWeb.loadUrl(url + "#current");
		}
		else
		{
			airScheduleWeb.loadUrl(url + "&onairDate=" + date8digits + "#current");
		}

        airScheduleWeb.setWebViewClient(new WebViewClient()
        {
        	@Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
        	{
        		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                activity.startActivity(intent);
                return true;
            }
        });
	}

	private void setAirScheduleWeb()
	{
		setAirScheduleWeb("");
	}

	/**
	 * 두시간 마이너스 보정된 시간으로 편성표 호출 날짜 지정 (새벽 2시 까지 방송, 2일 새벽 1시 방송은 1일 편성표에 보여지므로)
	 * @param date8digits
	 * @return
	 */
	private boolean isTodaySchedule(String date8digits)
	{
		Calendar cal = (Calendar) DateUtil.getKoreanCalendar().clone();
		cal.add(Calendar.HOUR_OF_DAY, TimeTable.HOUR_ADJUSTMENT);

		if (date8digits.equals(DateUtil.get8DigitsDateString(cal)))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public int getWidthDP(int dpValue)
	{
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpWidth / 360, context.getResources().getDisplayMetrics());
	}

	public int getHeightDP(int dpValue)
	{
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpHeight / 640, context.getResources().getDisplayMetrics());
	}

	public int getCalSP(int spValue)
	{
		return spValue * dpHeight / 640;

//		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, spValue * dpHeight / 640, getResources().getDisplayMetrics());
	}
}
