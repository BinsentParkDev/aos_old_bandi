package kr.ebs.bandi;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import kr.co.uplusad.dmpcontrol.LGUDMPAdView;
import kr.ebs.bandi.BandiBoard.NoAscCompare;
import kr.ebs.bandi.ShareDialog.SharePopupListener;
import kr.ebs.bandi.TwitterDialog.OnTwitterDialogListener;
import kr.ebs.bandi.data.BandiBoardData;
import kr.ebs.bandi.data.BookInfoData;
import kr.ebs.bandi.data.CProgramData;
import kr.ebs.bandi.data.EventData;
import kr.ebs.bandi.data.FavoriteData;
import kr.ebs.bandi.data.RepeatData;
import kr.ebs.bandi.data.ScheduleCompare;
import kr.ebs.bandi.data.TimeData;
import kr.ebs.bandi.utils.CScrollViewWithProgress;
import kr.ebs.bandi.utils.CScrollViewWithProgress.OnScrollLoadingListener;
import kr.ebs.bandi.utils.CScrollViewWithProgress2;
import kr.ebs.bandi.StreamingMediaPlayer;
import kr.ebs.bandi.utils.VideoViewCustom;
import kr.ebs.bandi.GoogleAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.pm.ActivityInfo;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.util.Xml;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.facebook.FacebookException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.kakao.KakaoLink;
import com.kakao.KakaoParameterException;
import com.kakao.KakaoTalkLinkMessageBuilder;

public class MainActivity extends BaseActivity
{
	private static final String TAG = "MainActivity";
	
	AudioManager audio;
	SeekBar vol;
	private InputMethodManager imm;
	private AudioServiceConnection serviceConnection;
	private Intent audioServiceIntent = null;
	private LayoutInflater inflater;
	private BandiApplication bandiApp;
	private AudioNoisyReceiver audioNoisyReceiver = null;
	
	private Thread mThread;
	public Handler mHandler, viewRadioHandler;
	private Preferences prefs;
	private Twitter twitter = null;
	private RequestToken requestToken = null;
	
	// LG U+ 전면 광고 시행 시 추가
	private boolean isFirst = true;
	private boolean isTabSelected;
	private boolean[] isTabLoading = {false, false, false, false};
	private boolean isRunning = false;
	private boolean isFirstLoading;
	private boolean isFirstInit;
	private boolean isFromRadioBtn;
	
	// 하단 메뉴 올리고 내릴때의 마진값의 기준이 될 레이아웃
	public int targetScrollHeight, nowScrollHeight, initSwapZoneHeight, nowBottomMenuMargin, targetBottomMenuMargin, originalMarginStandard;
	public int tabIndex = -1;
	
	// tabBtn0 ~ 3 : 다시듣기, 도서정보, 방송내용, 선곡표
	private int[] tabMenuBtnRes = {R.id.tabBtn0, R.id.tabBtn1, R.id.tabBtn2, R.id.tabBtn3};
	private int[] notiContentRes = {R.id.content0, R.id.content1, R.id.content2, R.id.content3};
	public int state = 0;
	
	private int loginCode = 0;
	private final int LGUP_REQUEST_CODE = 101;
	private final int EBS_REQUEST_CODE = 1010;
	private int boardPageIndex = 0;
	static final int NOTIFICATION_ID = 101;
	private float accelValue = 0.12f;

	private LGUDMPAdView adZone;
	private LinearLayout[] notiContent = new LinearLayout[4];
	public FrameLayout[] tabMenuBtn = new FrameLayout[4];
	private EditText boardInput;
	private FrameLayout marginStandard;
	public CScrollViewWithProgress2 mainScroll;
	private TextView noBoardTxt;
	private ImageView snsIcon;
	public FrameLayout appImg2;
	private FrameLayout replayImg;
	public FrameLayout controlLayout;
	public FrameLayout videoPlayBtn;
	public LinearLayout subScrollMain;
	private TextView subScrollNoti;
	/**
	 * 보이는 라디오 전체 화면 보기 버튼
	 */
	public ImageButton videoFullBtn;
	public VideoView video;
	public TextView titleText;
	public FrameLayout favorite;
	LayoutInflater footerInflater;
	View footer;
	
	public FrameLayout shareBtn, playBtn, videoLayout;
	public ProgressBar loading, subScrollProgress;
	private LinearLayout bandiBoardLayout, shareGroup, bottomMenu;

	public FrameLayout boardTextLayout;
	public LinearLayout bottomBtnGroup;
	public FrameLayout notiLayout;
	private CScrollViewWithProgress subScroll;
	public RelativeLayout textZone, writeZone;
	/**
	 * 보이는 라디오 버튼
	 */
	public FrameLayout viewRadioCheck;
	private FrameLayout swapZone;
	
	private OnAirScrollTextUpdater scrollUpdater;
	
	private LinearLayout.LayoutParams lParams;
	private FrameLayout.LayoutParams fParams;
	private RelativeLayout.LayoutParams rParams;
	
	private NetworkChangeReceiver networkChangeReceiver;
	private String boardContent;
//	private KakaoLink kakaoLink;
//    private KakaoTalkLinkMessageBuilder kakaoTalkLinkMessageBuilder;
    /**
     * 카카오톡 공유 메세지.
     */
	final String stringText = "신개념 EBS 인터넷 라디오 [EBS 반디]\n안드로이드 http://me2.do/5tUcd9cW\niOS http://me2.do/5mA1uzWc";
//    final String stringUrl = "http://www.ebs.co.kr";
	
    int videoTempWidth, videoTempHeight;
//    Notification notification;
//    PendingIntent pi;
//    NotificationManager notificationManager;
    
    Intent notiIntent;
    
    AlertDialog.Builder networkPopupBuilder;
    AlertDialog networkPopup;
    
    WifiManager.WifiLock wifiLock = null;
    PowerManager.WakeLock wakeLock = null;
  
    RelativeLayout mainLayout;
    private ArrayList<FavoriteData> favoriteData = new ArrayList<FavoriteData>();
    boolean isFavorite = false;
    
    
	@SuppressLint("NewApi") 
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		prefs = new Preferences(this);
		bandiApp = (BandiApplication) getApplicationContext();
		bandiApp.setIsInit(true);
		
		isFirstInit = true;
		isFirstLoading = true;
		isTabSelected = false;
		
		imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		setListenerToRootView();
		
//		try
//		{
//			kakaoLink = KakaoLink.getKakaoLink(bandiApp);
//            kakaoTalkLinkMessageBuilder = kakaoLink.createKakaoTalkLinkMessageBuilder();
//        }
//		catch (KakaoParameterException e){
//			Log.e(TAG, e.getMessage());
//		}
		
		// 로그 저장
		BandiLog.connect(this);
		setListener();
		
		viewRadioHandler = new Handler();
		
		networkPopupBuilder = new AlertDialog.Builder(MainActivity.this);
    	networkPopupBuilder.setMessage(R.string.use_paid_network_alert_msg)
	    .setCancelable(false)
	    .setPositiveButton("설정으로 이동", new DialogInterface.OnClickListener() 
	    {
	    	public void onClick(DialogInterface dialog, int id) 
	    	{
	    		if(TutorialDialog.getInstance(MainActivity.this).isShowing())
	    		{
	    			TutorialDialog.getInstance(MainActivity.this).dismiss();
	    		}
	    		dialog.dismiss();
	    		new Handler().postDelayed(new Runnable() 
	    		{
					@Override
					public void run() 
					{
						LinearLayout setting = (LinearLayout) findViewById(R.id.layout_menu7);
						setting.performClick();
					}
				}, 300);
	    	}
	    })
	    .setNegativeButton("닫기", new DialogInterface.OnClickListener() 
	    {
			
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				dialog.dismiss();
//				receiveCount = 0;
			}
		});
    	
    	networkPopup = networkPopupBuilder.create();
    	
		/** 헤드셋 상태 관련 필터 **
		IntentFilter receiverFilter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
		AudioNoisyReceiver receiver = new AudioNoisyReceiver();
	    registerReceiver( receiver, receiverFilter );
		****/

		//등록
//		if (wakeLock == null) 
//		{
//			PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
//		    wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "wakelock");
//		    wakeLock.acquire();
//		}
		
		//등록
		if (wifiLock == null) 
		{
			WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		    wifiLock = wifiManager.createWifiLock("wifi_lock_bandi_audio");
		    wifiLock.setReferenceCounted(true);
		    wifiLock.acquire();
		}
		
		
		
		/*
		 * 오디오 서비스 바인딩
		 */
		try 
		{
			audioServiceIntent = new Intent(this, AudioService.class);
			serviceConnection = AudioServiceConnection.getInstance();
			serviceConnection.setPlayLoadingBtn(playBtn, loading);
			
			bindAudioService(); 
		} 
		catch (Exception e) 
		{ 
			Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show(); 
		}
		
		
		
		try { adZone.execute(); } catch(Exception e){}
		
		networkChangeReceiver = new NetworkChangeReceiver();
		registerReceiver(quitBandi, new IntentFilter("quit_bandi"));
		registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

		try
		{
			SimpleDateFormat dateFormat = new  SimpleDateFormat("yyyyMMdd", Locale.KOREA);

	        if(bandiApp.getEventData() != null && bandiApp.getEventData().getEventCode().equals("002"))
	        {
				Date endDay = dateFormat.parse(bandiApp.getEventData().getEventEndDay());
				Date now = dateFormat.parse(DateUtil.get8DigitsDateString(DateUtil.getKoreanCalendar()));
				
				if(now.after(endDay) == false)
				{
					EventDialog d = new EventDialog(this);
					d.setData(bandiApp.getEventData());
					d.show();
				}
	        }
		}
		catch (ParseException e) {}
		
		// 광고
		new AsyncAction().execute();
		// 게시물 스크롤러 시작
		scrollUpdater = new OnAirScrollTextUpdater(this);
		
		// 자동종료 셋팅
		if(prefs.getAutoFinishState())
		{
			Intent intent = new Intent(this, TimerService.class);
			intent.putExtra("finishTime", prefs.getFinishTime());
			startService(intent);
		}
		
		if(prefs.getIsStartFMRadio())
		{
			findViewById(R.id.topMenuBtn0).setSelected(true); // 상단 fm, i-radio 탭 선택
			findViewById(R.id.topMenuBtn1).setSelected(false);
			bandiApp.setIsFMRadio(true);
		}
		else
		{
			findViewById(R.id.topMenuBtn0).setSelected(false);
			findViewById(R.id.topMenuBtn1).setSelected(true);
			bandiApp.setIsFMRadio(false);
		}
		
		notiIntent = new Intent(MainActivity.this, NotificationService.class);
		startService(notiIntent);
		
		
		if(ScheduleCompare.getInstance().getData() == null)
		{
			loadingCount = 0;
			radioDataSetting();
			iRadioDataSetting();
		}
		else
		{
			if(prefs.getIsStartFMRadio()) ScheduleCompare.getInstance().setData(bandiApp.getRadioData());
			else ScheduleCompare.getInstance().setData(bandiApp.getIRadioData());
			ScheduleCompare.getInstance().compareStart();
		}
		snsIconCheck();
		
		// 이어폰 사용하다 빼는 동작 탐지 (2014-05 추가)
		IntentFilter filter = new IntentFilter();
		filter.addAction("android.media.AUDIO_BECOMING_NOISY");
		audioNoisyReceiver = new AudioNoisyReceiver();
		registerReceiver(audioNoisyReceiver, filter); // 일단 스트리밍 부터 바꾸고 나중에 하자..
	
	}

	/**
	 * current value : 2
	 */
	int maxLoadingCount = 2;
	int loadingCount = 0;
	
	public void radioDataSetting()
	{
		String jsonStr = Url.getServerText(Url.FM_TIMETABLE_JSON);
		JSONObject jObject;
		
		try {
			jObject = new JSONObject(jsonStr);
			Log.d(TAG, "jObject - " + jObject.toString());
			
			if(jObject != null)
			{
				try 
				{
					ArrayList<TimeData> tData = new ArrayList<TimeData>();
					JSONArray jArray = jObject.getJSONObject("channels").getJSONArray("programs");
					bandiApp.setRadioData(jArray);
					
					loadingCount++;
					Log.d(TAG, "loadingCount 1" + loadingCount);
    				if(loadingCount == maxLoadingCount)
    				{
    					if(prefs.getIsStartFMRadio()) ScheduleCompare.getInstance().setData(bandiApp.getRadioData());
    					else ScheduleCompare.getInstance().setData(bandiApp.getIRadioData());
    					ScheduleCompare.getInstance().compareStart();
    					Log.d(TAG, "radioDataSetting callback compareStarted");
    				}
				}
				catch (JSONException e) 
				{
//					Log.e(TAG,"11 - callback error");
					e.printStackTrace();
				}
			}
		} catch (JSONException e) {
			Toast.makeText(this, "'책읽어주는 라디오'의 편성표 정보를 가져오지 못했습니다.", Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}
	

	
	public void iRadioDataSetting() {
		String jsonStr = Url.getServerText(Url.IRADIO_TIMETABLE_JSON);
//		Log.d(TAG, "jsonStr (iradio) - " + jsonStr);
		JSONObject jObject;
		
		try {
			jObject = new JSONObject(jsonStr);
//			Log.d(TAG, "jObject (iradio) - " + jObject.toString());
			
			if(jObject != null)
			{
				try 
				{
					bandiApp.setIRadioData(jObject.getJSONObject("channels").getJSONArray("programs"));
				}
				catch (JSONException e) 
				{
					e.printStackTrace();
				}
				
				loadingCount++;
				
				if(loadingCount == maxLoadingCount)
				{
					Log.d(TAG, "loadingCount i-radio - " + loadingCount);
					if(prefs.getIsStartFMRadio()) ScheduleCompare.getInstance().setData(bandiApp.getRadioData());
					else ScheduleCompare.getInstance().setData(bandiApp.getIRadioData());
					ScheduleCompare.getInstance().compareStart();
					Log.d(TAG, "iRadioDataSetting callback compareStarted");
				}
			}
		} catch (JSONException e) {
			Toast.makeText(this, "'외국어 라디오'의 편성표 정보를 가져오지 못했습니다.", Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}
	

	private void publishFeedDialog() 
	{
	    Bundle params = new Bundle();
	    params.putString("name", "EBS 반디");
	    params.putString("caption", "신개념 EBS 인터넷 라디오");
	    params.putString("description", "PC에서도, 모바일에서도 EBS라디오를 언제나 반디와 함께 즐기세요!");
	    params.putString("link", "http://www.ebs.co.kr/radio/bandi");
	    params.putString("picture", "http://static.ebs.co.kr/images/bhp/public/images/2015/01/15/22/41/13/3edd9692-fb81-4d3d-a41a-cc326f662cf0.png");
	    WebDialog feedDialog = (new WebDialog.FeedDialogBuilder(MainActivity.this, Session.getActiveSession(), params)).setOnCompleteListener(new OnCompleteListener() 
        {
			@Override
			public void onComplete(Bundle values, FacebookException error) 
			{
				if (error == null) 
				{
                    final String postId = values.getString("post_id");
                    if (postId != null) 
                    {
                    	AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
    				    builder.setMessage("페이스북에 등록되었습니다.")
    				    .setCancelable(false)
    				    .setPositiveButton("확인", new DialogInterface.OnClickListener() 
    				    {
    				    	public void onClick(DialogInterface dialog, int id) 
    				    	{
    				    		dialog.dismiss();
    				    	}
    				    }).show();
//                    	Toast.makeText(MainActivity.this, "정상적으로 등록되었습니다.", Toast.LENGTH_SHORT).show(); 
                    }
                    else {}
                } 
			}
        }).build();
	    feedDialog.show();
	}
	
	
	private void startVideo()
    {
		Log.d(TAG, "startVideo() is called.");
        Uri uri = Uri.parse(bandiApp.getStreamUrlAndroidVod()); 
        video.setVideoURI(uri);
        video.start();
    }

	
	OnClickListener tabBtnListener = new OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			tabIndex = Integer.parseInt(v.getTag().toString());
			replayImg.setVisibility(View.INVISIBLE);
			if(tabMenuBtn[tabIndex].isSelected() == false)
			{
				tabMenuBtn[tabIndex].setSelected(true);
				isTabSelected = true;
			}
			else
			{
				tabMenuBtn[tabIndex].setSelected(false);
				isTabSelected = false;
			}
			
			for(int i = 0; i < 4; i++)
			{
				if(i != tabIndex)
				{
					tabMenuBtn[i].setSelected(false);
					notiContent[i].setVisibility(View.GONE);
				}
				else
				{
					notiContent[i].setVisibility(View.VISIBLE);
					
					//** Google Analytics **
					GoogleAnalytics.CHANNEL channel = (bandiApp.getIsFMRadio() ? GoogleAnalytics.CHANNEL.FM : GoogleAnalytics.CHANNEL.IRADIO);
					if (isTabSelected) 
					{
						String[] menuTitle = {"Aod","BookInfo","AirContents","SongTable"};
						new GoogleAnalytics(MainActivity.this).sendScreenView(menuTitle[i], channel);
						Log.d(TAG, menuTitle[i] + " tab is loaded. channel is " + channel.toString());
					}
					else 
					{
						new GoogleAnalytics(MainActivity.this).sendScreenView(OnAir.TAG, channel);
						Log.d(TAG, " tab is closed. channel is " + channel.toString());
					}
				}
			}
			
			if(isTabSelected && notiLayout.getHeight() == 0)
			{
				if(notiLayout.getHeight() == 0 && subScrollMain.getHeight() == 0)
				{
					// 아무것도 펼쳐지지 않았을 때

					nowBottomMenuMargin = 0;
					targetBottomMenuMargin = getHeightDP(180);
					
					nowScrollHeight = subScrollMain.getHeight();
					targetScrollHeight = 0;
					
					state = 1;
					writeZone.setVisibility(View.INVISIBLE);
					textZone.setVisibility(View.VISIBLE);
					
				}
				else if(notiLayout.getHeight() != 0 && subScrollMain.getHeight() == 0)
				{
					if(viewRadioCheck.isSelected())
					{
						targetScrollHeight = getHeightDP(180);
						nowScrollHeight = 0;
						writeZone.setVisibility(View.INVISIBLE);
						textZone.setVisibility(View.VISIBLE);
						isFromRadioBtn = false;
						state = 2;
					}
					else
					{
						appImg2.setVisibility(View.INVISIBLE);
						new Handler().post(new Runnable()
						{
							
							@Override
							public void run()
							{
								mainScroll.scrollTo(0, 0);
							}
						});
						
						RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams)bottomMenu.getLayoutParams();
						nowBottomMenuMargin = params1.topMargin;
						targetBottomMenuMargin = originalMarginStandard + 5;
						state = 0;
						writeZone.setVisibility(View.INVISIBLE);
						textZone.setVisibility(View.VISIBLE);
					}
				}
				else if(notiLayout.getHeight() == 0 && subScrollMain.getHeight() != 0)
				{
					nowBottomMenuMargin = 0;
					targetBottomMenuMargin = getHeightDP(180);
					
					nowScrollHeight = subScrollMain.getHeight();
					targetScrollHeight = 0;
					
					state = 1;
					writeZone.setVisibility(View.INVISIBLE);
					textZone.setVisibility(View.VISIBLE);
				}
				moveLayout();
			}
			else if(isTabSelected == false)
			{
				if(viewRadioCheck.isSelected())
				{
					targetScrollHeight = getHeightDP(180);
					nowScrollHeight = 0;
					isFromRadioBtn = false;
					state = 2;
					writeZone.setVisibility(View.VISIBLE);
					textZone.setVisibility(View.INVISIBLE);
				}
				else
				{
					appImg2.setVisibility(View.INVISIBLE);
					new Handler().post(new Runnable()
					{
						
						@Override
						public void run()
						{
							mainScroll.scrollTo(0, 0);
						}
					});
					
					RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams)bottomMenu.getLayoutParams();
					nowBottomMenuMargin = params1.topMargin;
					targetBottomMenuMargin = originalMarginStandard + 5;
					state = 0;
					writeZone.setVisibility(View.INVISIBLE);
					textZone.setVisibility(View.VISIBLE);
				}
				moveLayout();
			}
			else if(isTabSelected == true && notiLayout.getHeight() != 0)
			{
				new Handler().post(new Runnable()
				{
					
					@Override
					public void run()
					{
						mainScroll.scrollTo(0, 0);
					}
				});
				LinearLayout.LayoutParams notiContentParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, nowBottomMenuMargin);
				notiLayout.setLayoutParams(notiContentParams);
				
				if(tabIndex == 0)
				{
					replayImg.setVisibility(View.INVISIBLE);
					repeatListViewInit();
					repeatPageIndex = 1;
					repeatRequest();
				}
				else if(tabIndex == 1)
				{
					replayImg.setVisibility(View.INVISIBLE);
					repeatListViewInit();
					bookInfoRequest();
				}
				else if(tabIndex == 2)
				{
					replayImg.setVisibility(View.INVISIBLE);
					repeatListViewInit();
					onAirContents();
				}
				else if(tabIndex == 3)
				{
					replayImg.setVisibility(View.INVISIBLE);
					repeatListViewInit();
					songListRequest();
				}
			}
		}
	};
	
	
	
	@Override
	protected void onDestroy() 
	{
		
		super.onDestroy();
		if(quitBandi != null) unregisterReceiver(quitBandi);
		if (audioNoisyReceiver != null) unregisterReceiver(audioNoisyReceiver);
		if(networkChangeReceiver != null) unregisterReceiver(networkChangeReceiver);
		BandiLog.disconnect(this); // BandiApplication.clear() 전에 등장해야함.
		
		//해제
		if (wifiLock != null) 
		{
			wifiLock.release();
		    wifiLock = null;
		}
		
		//해제
//		if (wakeLock != null) 
//		{
//		    wakeLock.release();
//		    wakeLock = null;
//		}
		
		((BandiApplication) getApplicationContext()).clear();
		if(audioServiceIntent != null) stopService(audioServiceIntent);
		if(notiIntent != null) {
			stopService(notiIntent);
		}

		
		// 광고 멈춤.
		try { if (isFinishing() && adZone != null) adZone.destroy(); } catch(Exception e) {}
		
	}
	
	@Override
	protected void onPause() 
	{
		
		if (adZone != null) adZone.destroy();
		if(isFirstInit == false)
		{
			if(prefs.getBackgroundRun() == false)
			{
				if(serviceConnection != null && serviceConnection.getAudioService() != null) 
				{
					serviceConnection.getAudioService().stopPlayer(playBtn, loading);
				}
				video.stopPlayback();
				repeatListViewInit();
			}
			else
			{
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
				if(viewRadioCheck.isSelected() && tabIndex != -1)
				{
					serviceConnection.getAudioService().initPlayer(playBtn, loading);
					state = 3;
					nowScrollHeight = subScrollMain.getHeight();
					targetScrollHeight = 0;
					
					viewRadioHandler.removeCallbacks(viewRadioRunnable);
					FrameLayout onAirTools = (FrameLayout)findViewById(R.id.onAirTools);
					onAirTools.setVisibility(View.VISIBLE);
					RelativeLayout controlBar = (RelativeLayout)findViewById(R.id.control_bar);
					controlBar.setVisibility(View.VISIBLE);
					
					videoFullBtn.setVisibility(View.INVISIBLE);
					video.stopPlayback();
					videoLayout.setVisibility(View.INVISIBLE);
					videoPlayBtn.setVisibility(View.INVISIBLE);
					controlLayout.setVisibility(View.VISIBLE);
					appImg2.setVisibility(View.INVISIBLE);
					writeZone.setVisibility(View.INVISIBLE);
					textZone.setVisibility(View.VISIBLE);
					viewRadioCheck.setSelected(false);
					videoFullBtn.setVisibility(View.INVISIBLE);
					moveLayout();
					
					new Handler().postDelayed(new Runnable() 
					{
						@Override
						public void run() 
						{
							if(tabIndex == 0)
							{
								tabMenuBtn[0].performClick();
							}
							if(tabIndex == 1)
							{
								tabMenuBtn[1].performClick();
							}
							else if(tabIndex == 2)
							{
								tabMenuBtn[2].performClick();
							}
							else if(tabIndex == 3)
							{
								tabMenuBtn[3].performClick();
							}
						}
					}, 200);
				}
				else if(viewRadioCheck.isSelected())
				{
//					main.viewRadioCheck.performClick();
					serviceConnection.getAudioService().initPlayer(playBtn, loading);
					state = 3;
					nowScrollHeight = subScrollMain.getHeight();
					targetScrollHeight = 0;
					
					viewRadioHandler.removeCallbacks(viewRadioRunnable);
					FrameLayout onAirTools = (FrameLayout)findViewById(R.id.onAirTools);
					onAirTools.setVisibility(View.VISIBLE);
					RelativeLayout controlBar = (RelativeLayout)findViewById(R.id.control_bar);
					controlBar.setVisibility(View.VISIBLE);
					
					videoFullBtn.setVisibility(View.INVISIBLE);
					video.stopPlayback();
					videoLayout.setVisibility(View.INVISIBLE);
					videoPlayBtn.setVisibility(View.INVISIBLE);
					controlLayout.setVisibility(View.VISIBLE);
					appImg2.setVisibility(View.INVISIBLE);
					writeZone.setVisibility(View.INVISIBLE);
					textZone.setVisibility(View.VISIBLE);
					viewRadioCheck.setSelected(false);
					videoFullBtn.setVisibility(View.INVISIBLE);
					moveLayout();
				}
//				else
//				{
//					if(videoPlayBtn.getVisibility() == View.VISIBLE)
//					{
//						serviceConnection.getAudioService().initPlayer(playBtn, loading);
//						videoFullBtn.setVisibility(View.INVISIBLE);
//						video.stopPlayback();
//						videoLayout.setVisibility(View.INVISIBLE);
//						videoPlayBtn.setVisibility(View.INVISIBLE);
//						controlLayout.setVisibility(View.VISIBLE);
//					}
//				}
				
			}
			
		}
		if(isFirstInit)
		{
			isFirstInit = false;
		}

		//스크롤텍스트 업데이트 중지 - 화면이 보이지 않을 때, 텍스트 스크롤러 중지 기능
		OnAirScrollTextUpdater.handler.sendEmptyMessage(0);
		super.onPause();
	}
	
	@Override
	protected void onResume() 
	{
		super.onResume();
		setVolumnBarPosition();
		try { if (adZone != null && adZone.isDestroyed()) adZone.execute(); } catch(Exception e) {}
		//스크롤텍스트 업데이트 재게 - 화면 복귀시, 텍스트 스크롤러 수행.
		OnAirScrollTextUpdater.handler.sendMessage(Message.obtain(OnAirScrollTextUpdater.handler, 1, (Context) this));
			
	}
	
	
	/**
	 * 프로그램 이미지 영역 가로*세로 크기 측정
	 */
	@Override
	public void onWindowFocusChanged(boolean hasFocus) 
	{
		if(hasFocus)
    	{
			if(isFirstLoading == true)
			{
	    		originalMarginStandard = marginStandard.getHeight();
	    		swapZone = (FrameLayout)findViewById(R.id.menu_swap_zone);
	    		initSwapZoneHeight = swapZone.getHeight();
	    		RelativeLayout.LayoutParams swapZoneParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, initSwapZoneHeight);
	    		swapZoneParams.addRule(RelativeLayout.ABOVE, R.id.bottom_standard);
	    		swapZoneParams.addRule(RelativeLayout.BELOW, R.id.top);
	    		swapZone.setLayoutParams(swapZoneParams);
	    		isFirstLoading = false;
			}
    	}
	}

	
	private void setListener() 
	{
		inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		footerInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		footer = footerInflater.inflate(R.layout.board_list_footer, null);
		
		mainLayout = (RelativeLayout)findViewById(R.id.main_layout);
		
		lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lParams.setMargins(getWidthDP(5), 0, 0, 0);
		titleText = (TextView)findViewById(R.id.title_text);
		titleText.setLayoutParams(lParams);
		titleText.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(22));
		
		TextView viewRadioText = (TextView)findViewById(R.id.textView_viewRadio);
		viewRadioText.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
		
		TextView scrollText = (TextView)findViewById(R.id.scroll_text);
		scrollText.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15));
		
		rParams = new RelativeLayout.LayoutParams(getWidthDP(40), LayoutParams.MATCH_PARENT);
		FrameLayout menuBtn = (FrameLayout) findViewById(R.id.menu_btn);
		menuBtn.setLayoutParams(rParams);
		menuBtn.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				baseMenu.toggle();
//				if(shareBtn.isSelected())
//				{
//					shareBtn.performClick();
//				}
			}
		});
		
		fParams = new FrameLayout.LayoutParams(getWidthDP(18), getWidthDP(19), Gravity.CENTER);
		ImageView menuIcon = (ImageView)findViewById(R.id.imageView_menuBtn);
		menuIcon.setLayoutParams(fParams);
		
		FrameLayout titleBar = (FrameLayout)findViewById(R.id.title_bar);
		rParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, getHeightDP(45));
		titleBar.setLayoutParams(rParams);

		adZone = (LGUDMPAdView) findViewById(R.id.ad);
		video = (VideoView) findViewById(R.id.videoView);
		
		
		lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, getHeightDP(45));
		boardTextLayout = (FrameLayout)findViewById(R.id.layout_board_text);
		boardTextLayout.setLayoutParams(lParams);
		
		bottomBtnGroup = (LinearLayout)findViewById(R.id.bottomMenu);
		subScrollMain = (LinearLayout)findViewById(R.id.layout_subScroll);
		shareGroup = (LinearLayout)findViewById(R.id.layout_shareGroup);
		noBoardTxt = (TextView)findViewById(R.id.textView_noBandiboard_subScroll);
		noBoardTxt.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
		
		TextView noBoardTxt2 = (TextView)findViewById(R.id.textView_noBoardNoti);
		noBoardTxt2.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(13));
		
		appImg2 = (FrameLayout)findViewById(R.id.layout_appImg2);
		mainScroll = (CScrollViewWithProgress2)findViewById(R.id.scrollView_main);
		
		rParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, getHeightDP(204));
		rParams.addRule(RelativeLayout.BELOW, R.id.top);
		replayImg = (FrameLayout)findViewById(R.id.layout_replayImg);
		replayImg.setLayoutParams(rParams);
		
		rParams = new RelativeLayout.LayoutParams(getHeightDP(34), getHeightDP(33));
		rParams.addRule(RelativeLayout.CENTER_VERTICAL);
		rParams.setMargins(getWidthDP(5), 0, 0, 0);
		snsIcon = (ImageView)findViewById(R.id.imageView_snsIcon);
		snsIcon.setLayoutParams(rParams);
		
		rParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		rParams.setMargins(getWidthDP(10), 0, 0, 0);
		rParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		rParams.addRule(RelativeLayout.CENTER_VERTICAL);
		controlLayout = (FrameLayout)findViewById(R.id.play_control_wrapper);
		controlLayout.setLayoutParams(rParams);
		
		rParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		rParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		rParams.addRule(RelativeLayout.CENTER_VERTICAL);
		LinearLayout layoutSnsBtnGroup = (LinearLayout)findViewById(R.id.layout_snsBtnGroup);
		layoutSnsBtnGroup.setPadding(0, 0, getWidthDP(10), 0);
		layoutSnsBtnGroup.setLayoutParams(rParams);
		
		rParams = new RelativeLayout.LayoutParams(getHeightDP(34), getHeightDP(34));
		rParams.addRule(RelativeLayout.CENTER_VERTICAL);
		rParams.setMargins(getWidthDP(10), 0, 0, 0);
		videoPlayBtn = (FrameLayout)findViewById(R.id.layout_videoPlayBtn);
		videoPlayBtn.setLayoutParams(rParams);
		
		rParams = new RelativeLayout.LayoutParams(getWidthDP(40), LayoutParams.MATCH_PARENT);
		rParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		shareBtn = (FrameLayout)findViewById(R.id.share_btn);
		shareBtn.setLayoutParams(rParams);
		
		fParams = new FrameLayout.LayoutParams(getWidthDP(26), getWidthDP(20), Gravity.CENTER);
		ImageView shareIcon = (ImageView)findViewById(R.id.imageView_shareBtn);
		shareIcon.setLayoutParams(fParams);
		
		lParams = new LinearLayout.LayoutParams(getWidthDP(28), getWidthDP(34));
		ImageView mainIcon = (ImageView)findViewById(R.id.imageView_mainIcon);
		mainIcon.setLayoutParams(lParams);
		
		lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, getHeightDP(40));
		LinearLayout topBtnGroup = (LinearLayout)findViewById(R.id.topBtnGroup);
		topBtnGroup.setLayoutParams(lParams);
		
		rParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, getHeightDP(102));
		rParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		FrameLayout bottomStandard = (FrameLayout)findViewById(R.id.bottom_standard);
		bottomStandard.setLayoutParams(rParams);

		rParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, getHeightDP(50));
		rParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		FrameLayout bottomStandard1 = (FrameLayout)findViewById(R.id.bottom_standard1);
		bottomStandard1.setLayoutParams(rParams);
		
		FrameLayout onAirTools = (FrameLayout)findViewById(R.id.onAirTools);
		onAirTools.setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));
		
		lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, getHeightDP(50));
		RelativeLayout controlBar = (RelativeLayout)findViewById(R.id.control_bar);
		controlBar.setLayoutParams(lParams);
		
		lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, getHeightDP(57));
		LinearLayout layoutBottomBtnGroup = (LinearLayout)findViewById(R.id.layout_bottomBtnGroup);
		layoutBottomBtnGroup.setLayoutParams(lParams);
				
		 // play/pause
		
		fParams = new FrameLayout.LayoutParams(getHeightDP(34), getHeightDP(34), Gravity.CENTER);
        playBtn = (FrameLayout) findViewById(R.id.play_btn);
        playBtn.setLayoutParams(fParams);
        
        fParams = new FrameLayout.LayoutParams(getHeightDP(24), getHeightDP(24), Gravity.CENTER);
        loading = (ProgressBar) findViewById(R.id.audio_loading);
        loading.setLayoutParams(fParams);
		
        bandiBoardLayout = (LinearLayout)findViewById(R.id.layout_bandiboard);
		subScrollNoti = (TextView)findViewById(R.id.textView_bandiBoardNoti);
		subScrollNoti.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
	
		subScrollProgress = (ProgressBar)findViewById(R.id.progressBar1);
		
		lParams = new LinearLayout.LayoutParams(getHeightDP(34), getHeightDP(34));
		lParams.setMargins(getWidthDP(5), 0, 0, 0);
		viewRadioCheck = (FrameLayout)findViewById(R.id.checkBox_viewRadio); // 보이는 라디오 on/off 버튼
		viewRadioCheck.setLayoutParams(lParams);
		
		fParams = new FrameLayout.LayoutParams(getHeightDP(34), getHeightDP(34));
		videoFullBtn = (ImageButton)findViewById(R.id.imageButton_videoFull);
		videoFullBtn.setLayoutParams(fParams);
		
        lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0);
		subScrollMain.setLayoutParams(lParams);
		
		lParams = new LinearLayout.LayoutParams(getWidthDP(46), getWidthDP(46));
		ImageButton shareFacebook = (ImageButton)findViewById(R.id.imageButton_shareFacebook);
		ImageButton shareTwitter = (ImageButton)findViewById(R.id.imageButton_shareTwitter);
		ImageButton shareKakao = (ImageButton)findViewById(R.id.imageButton_shareKakao);
		
		shareFacebook.setLayoutParams(lParams);
		shareTwitter.setLayoutParams(lParams);
		shareKakao.setLayoutParams(lParams);
		
		shareFacebook.setOnClickListener(shareBtnClickListener);
		shareTwitter.setOnClickListener(shareBtnClickListener);
		shareKakao.setOnClickListener(shareBtnClickListener);
		
		videoFullBtn.setVisibility(View.INVISIBLE);
		replayImg.setVisibility(View.INVISIBLE);
		noBoardTxt.setVisibility(View.GONE);
		appImg2.setVisibility(View.INVISIBLE);
		videoPlayBtn.setVisibility(View.INVISIBLE);
		subScrollProgress.setVisibility(View.GONE);
		shareGroup.setVisibility(View.GONE);
		
		mainScroll.noFullToRefresh();
		mainScroll.setListener(new OnScrollLoadingListener() 
		{
			@Override
			public void onScrollLastPosition() 
			{
				if(tabIndex == 0)
				{
					if(isRepeatLoadingFinish == true)
					{
						repeatPageIndex++;
						isRepeatLoadingFinish = false;
						repeatRequest();
					}
				}
			}
			@Override
			public void onLoadingStart() {}
			@Override
			public void onLoadingFinish() {}
		});
				
		videoFullBtn.setOnClickListener(videoFullClickListener);
		shareBtn.setOnClickListener(shareClickListener);
		videoPlayBtn.setOnClickListener(videoPlayCheckListener);
		viewRadioCheck.setOnClickListener(viewRaidoCheckListener);
		playBtn.setOnClickListener(playBtnClickListener);
		 
		for(int i = 0; i < 4; i++)
		{
			tabMenuBtn[i] = (FrameLayout)findViewById(tabMenuBtnRes[i]);
			tabMenuBtn[i].setTag(i);
			tabMenuBtn[i].setSelected(false);
			tabMenuBtn[i].setOnClickListener(tabBtnListener);
			notiContent[i] = (LinearLayout)findViewById(notiContentRes[i]);
		}
		
		bottomMenu = (LinearLayout)findViewById(R.id.bottomMenu);

		marginStandard = (FrameLayout)findViewById(R.id.marginStandard);
		notiLayout = (FrameLayout)findViewById(R.id.content);

        nowBottomMenuMargin = 0;
        targetBottomMenuMargin = 0;

		textZone = (RelativeLayout)findViewById(R.id.text_zone);
		FrameLayout textZoneBtn = (FrameLayout)findViewById(R.id.layout_textZoneBtn);
        textZoneBtn.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				
				unselectAllMenus(MainActivity.this);
				TextView bandi_board = (TextView) findViewById(R.id.bandi_board_menu);
		        ImageView icon_bandi_board = (ImageView)findViewById(R.id.imageView_bandi_board_menu);
	        	bandi_board.setSelected(true);
	        	icon_bandi_board.setSelected(true);
	        	findViewById(R.id.layout_menu3).setBackgroundColor(Color.parseColor("#3f3833"));

				new BandiBoard(MainActivity.this).run();
			}
		});
        writeZone = (RelativeLayout)findViewById(R.id.write_zone);
        
        rParams = new RelativeLayout.LayoutParams(getWidthDP(305), getHeightDP(31));
        rParams.addRule(RelativeLayout.CENTER_VERTICAL);
        rParams.setMargins(getWidthDP(5), 0, getWidthDP(5), 0);
        rParams.addRule(RelativeLayout.LEFT_OF, R.id.imageButton_write);
        rParams.addRule(RelativeLayout.RIGHT_OF, R.id.imageView_snsIcon);
        boardInput = (EditText)findViewById(R.id.editText_bandiInput);
        boardInput.setPadding(getHeightDP(15), 0, getHeightDP(15), 0);
        boardInput.setLayoutParams(rParams);
        boardInput.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
        
        boardInput.setOnClickListener(new OnClickListener() 
        {
			
			@Override
			public void onClick(View v) 
			{
				if(CProgramData.getInstance().getAppUseBoardYn().equals("Y"))
				{
					if(isNeededToLogin() && bandiApp.isFacebookLogin() == false && bandiApp.isTwitterLogin() == false)
					{
						new Handler().postDelayed(new Runnable()
					    {
					    	
							@Override
							public void run()
							{
								imm.hideSoftInputFromWindow(boardInput.getWindowToken(), 0);
							}
							
					    }, 0);
						
						callLogin("OnAir", false);
					}
				}
				else
				{
					new Handler().postDelayed(new Runnable()
				    {
				    	
						@Override
						public void run()
						{
							imm.hideSoftInputFromWindow(boardInput.getWindowToken(), 0);
						}
						
				    }, 0);
					
					AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
				    builder.setMessage("해당 프로그램은 반디게시판이 운영되지 않습니다.")
				    .setCancelable(false)
				    .setPositiveButton("확인", new DialogInterface.OnClickListener() 
				    {
				    	public void onClick(DialogInterface dialog, int id) 
				    	{
				    		dialog.dismiss();
				    	}
				    }).show();
				}
			}
		});
        
        subScroll = (CScrollViewWithProgress)findViewById(R.id.scrollView_bandiboard);
		subScroll.setHeight(getHeightDP(180));
		subScroll.setOnTouchListener(new OnTouchListener() 
		{
			
			@Override
			public boolean onTouch(View v, MotionEvent event) 
			{
				if (event.getAction() == MotionEvent.ACTION_UP)
		            mainScroll.requestDisallowInterceptTouchEvent(false);
		        else 
		        	mainScroll.requestDisallowInterceptTouchEvent(true);
		 
		        return false;
			}
		});
		
		subScroll.setListener(new OnScrollLoadingListener() 
		{
			
			@Override
			public void onScrollLastPosition() 
			{
				if(isLastPage == false)
				{
					bandiBoardLayout.removeView(footer);
					bandiBoardLayout.addView(footer);
					new Handler().postDelayed(new Runnable()
					{
						
						@Override
						public void run()
						{
							subScroll.fullScroll(ScrollView.FOCUS_DOWN);
							boardListAdd();
						}
					}, 50);
				}
			}
			
			@Override
			public void onLoadingStart() 
			{
				boardPageIndex = 0;
				subScrollProgress.setVisibility(View.VISIBLE);
				String url = "http://home.ebs.co.kr/bandi/function/bandiBoardList.json";

				Map<String, Object> params = new HashMap<String, Object>();
		        params.put("programId", CProgramData.getInstance().getProgramID());
		        params.put("listType", "L");
		        params.put("pageSize", "5");
		        
		        APIWork work = new APIWork(MainActivity.this);
		        work.doThreadJob(url, params, new APIWork.APICallback()
//				AQuery aq = new AQuery(MainActivity.this);
//				aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
				{
					@Override
//					public void callback(String url, JSONObject object, AjaxStatus status)
					public void callback(String responseStr)
					{
						bandiBoardListData.clear();
						bandiBoardLayout.removeAllViews();
						
						try 
						{
							JSONObject object = new JSONObject(responseStr);
							
							JSONArray jArray = object.getJSONArray("bandiPosts");
							for(int i = 0; i < jArray.length(); i++)
							{
								BandiBoardData data = new BandiBoardData();
								data.setWriter(jArray.getJSONObject(i).getString("writer"));
								data.setRegDate(jArray.getJSONObject(i).getString("regdate"));
								data.setContents(jArray.getJSONObject(i).getString("contents"));
								data.setAdminYN(jArray.getJSONObject(i).getString("adminYn"));
								data.setSEQ(jArray.getJSONObject(i).getLong("seq"));
								data.setSNSUserName(jArray.getJSONObject(i).getString("snsUserNm"));
								data.setSNSUseYN(jArray.getJSONObject(i).getString("snsUseYn"));
								bandiBoardListData.add(data);
							}
							
							bandiList = new LinearLayout[jArray.length()];
							bandiListWriter = new TextView[jArray.length()];
							bandiListDate = new TextView[jArray.length()];
							bandiListContents = new TextView[jArray.length()];
							
							for(int i = 0; i < jArray.length(); i++)
							{
								
								View view = null;
								if(bandiBoardListData.get(i).getAdminYN().equals("Y"))
								{
									view = inflater.inflate(R.layout.bandi_board_manager_list_item, null);
									RelativeLayout main = (RelativeLayout)view.findViewById(R.id.layout_managerMain);
									main.setPadding(getHeightDP(5), getHeightDP(5), getHeightDP(5), getHeightDP(5));
									bandiListContents[i] = (TextView)view.findViewById(R.id.textView_managerMsg);
									String leftReplace = bandiBoardListData.get(i).getContents().replace("&lt;", "<");
									String rightReplce = leftReplace.replace("&gt;", ">");
									bandiListContents[i].setText(rightReplce);
									bandiListContents[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
									rParams = new RelativeLayout.LayoutParams(getHeightDP(51), getHeightDP(51));
									rParams.addRule(RelativeLayout.CENTER_VERTICAL);
									rParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
									ImageView icon = (ImageView)view.findViewById(R.id.imageView_managerIcon);
									icon.setLayoutParams(rParams);
								}
								else
								{
								
									view = inflater.inflate(R.layout.bandi_board_list_item, null);
									
									bandiList[i] = (LinearLayout)view.findViewById(R.id.boardListMain);
									bandiList[i].setPadding(getHeightDP(5), getHeightDP(5), getHeightDP(5), getHeightDP(5));
									if(i % 2 == 0)
									{
										bandiList[i].setBackgroundColor(Color.parseColor("#fcf8ee"));
									}
									else
									{
										bandiList[i].setBackgroundColor(Color.parseColor("#ffffff"));
									}
									
									bandiListWriter[i] = (TextView)view.findViewById(R.id.boardListName);
									bandiListWriter[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
									
									lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
									lParams.setMargins(getWidthDP(5), 0, 0, 0);
									bandiListDate[i] = (TextView)view.findViewById(R.id.boardListDate);
									bandiListDate[i].setLayoutParams(lParams);
									bandiListDate[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
									
									lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
									lParams.setMargins(0, getWidthDP(5), 0, 0);
									bandiListContents[i] = (TextView)view.findViewById(R.id.boardListMsg);
									bandiListContents[i].setLayoutParams(lParams);
									bandiListContents[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
									
									if(bandiBoardListData.get(i).getSNSUseYn().equals("N"))
									{
										bandiListWriter[i].setText(bandiBoardListData.get(i).getWriter());
									}
									else if(bandiBoardListData.get(i).getSNSUseYn().equals("Y"))
									{
										bandiListWriter[i].setText(bandiBoardListData.get(i).getSNSUserName());
									}
									bandiListDate[i].setText(bandiBoardListData.get(i).getRegDate());
									if(bandiBoardListData.get(i).getContents().equals("null") == false)
									{
										String leftReplace = bandiBoardListData.get(i).getContents().replace("&lt;", "<");
										String rightReplce = leftReplace.replace("&gt;", ">");
										bandiListContents[i].setText(rightReplce);
//										bandiListContents[i].setText(bandiBoardListData.get(i).getContents());
									}
								
								}
								bandiBoardLayout.addView(view);
							}
							
							if(jArray.length() == 5)
							{
								isLastPage = false;
//								listLayout.addView(footer);
							}
							else
							{
								isLastPage = true;
							}
							
							subScroll.loadingFinish();
							subScrollProgress.setVisibility(View.GONE);
						}
						catch (JSONException e) { e.printStackTrace(); }
						
					};
					
				});
			}
			
			@Override
			public void onLoadingFinish() 
			{
				
			}
		});
        
		rParams = new RelativeLayout.LayoutParams(getHeightDP(34), getHeightDP(34));
		rParams.setMargins(0, 0, getWidthDP(5), 0);
		rParams.addRule(RelativeLayout.CENTER_VERTICAL);
		rParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		ImageButton writeBtn = (ImageButton)findViewById(R.id.imageButton_write);
		writeBtn.setLayoutParams(rParams);
		writeBtn.setOnClickListener(new OnClickListener() 
        {
			
			@Override
			public void onClick(View v) 
			{
				if(CProgramData.getInstance().getAppUseBoardYn() == null)
				{
					return;
				}
				if(CProgramData.getInstance().getAppUseBoardYn().equals("Y"))
				{
					if(isNeededToLogin() && bandiApp.isFacebookLogin() == false && bandiApp.isTwitterLogin() == false)
					{
						LoginFormDialog loginForm = LoginFormDialog.getInstance(MainActivity.this);
						bandiApp.setLoginPrevView("OnAir");
						loginForm.setOnLoginSuccessCb(new LoginFormDialog.OnLoginSuccessCb() 
						{
							@Override
							public void onLoginSuccess() 
							{
								snsIconCheck();
								
//								// -*- 로그인 과정에서 '즐겨찾기' 가져오기를 하므로 여기서는 뺀다.
//								if(bandiApp.getFavoriteList() == null || bandiApp.getFavoriteList().size() <= 0)
//				                {
//									String url = "http://home.ebs.co.kr/bandi/bandiAppUserFbmkList";
//							    	Map<String, Object> params = new HashMap<String, Object>();
//							    	params.put("userId", bandiApp.getUserId());
//							    	params.put("fileType", "json");
//							    	AQuery aq = new AQuery(MainActivity.this);
//							    	aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
//									{
//							    		@Override
//							    		public void callback(String url, JSONObject object, AjaxStatus status) 
//							    		{
//							    			try 
//							    			{
//							    				Log.d("TEST", "로그인 성공, 즐겨찾기 리스트가 없어서 다시 받아온 결과 == " + object);
//							    				favoriteData.clear();
//							    				JSONObject result = object.getJSONObject("appUserFbmkLists");
//							    				JSONArray jArray = result.getJSONArray("appUserFbmkLists");
//
//												for(int i = 0; i < jArray.length(); i++)
//												{
//													FavoriteData data = new FavoriteData();
//													data.setFbmkSno(jArray.getJSONObject(i).getInt("fbmkSno"));
//													data.setProgramID(jArray.getJSONObject(i).getString("courseId"));
//													data.setTitle(jArray.getJSONObject(i).getString("bandiTitle"));
//													data.setLinkUrl(jArray.getJSONObject(i).getString("bandiMobUrl"));
//													favoriteData.add(data);
//												}
////												
//												bandiApp.setFavoriteList(favoriteData);
//												
//												for(int i = 0; i < bandiApp.getFavoriteList().size(); i++)
//							                	{
//							                		if(bandiApp.getFavoriteList().get(i).getProgramID().equals(CProgramData.getInstance().getProgramID()))
//							                		{
//							                			String fbmkSno = Integer.toString(bandiApp.getFavoriteList().get(i).getFbmkSno());
//							                			CProgramData.getInstance().setNowFavoriteId(fbmkSno);
//							                			favorite.setSelected(true);
//							                		}
//							                	}
//												
//												
//							    			}
//							    			catch(Exception e) {}
//							    		}
//									});
//				                }
//								else
								{
									// 로그인 후 받아온 즐겨찾기 목록에서 방송중인 프로그램이 있으면, 즐겨찾기 선택 표시를 한다.
									for(int i = 0; i < bandiApp.getFavoriteList().size(); i++)
				                	{
				                		if(bandiApp.getFavoriteList().get(i).getProgramID().equals(CProgramData.getInstance().getProgramID()))
				                		{
				                			String fbmkSno = Integer.toString(bandiApp.getFavoriteList().get(i).getFbmkSno());
				                			CProgramData.getInstance().setNowFavoriteId(fbmkSno);
				                			favorite.setSelected(true);
				                		}
				                	}
				                	
				                	
								}
							}
						});
						loginForm.setOnLoginFailureCb(new LoginFormDialog.OnLoginFailureCb() 
						{
							@Override
							public void onLoginFailure() 
							{
								snsIconCheck();
							}
						});
						
						loginForm.showDialog(false);
					}
					else
					{
						boardPageIndex = 0;
						boardContent = boardInput.getText().toString();
						boardInput.setText("");
						String url = "http://home.ebs.co.kr/bandi/bandiAppPost2";
						Map<String, Object> params = new HashMap<String, Object>();
						params.put("progcd", ScheduleCompare.getInstance().getNowOnAirId());
						params.put("contents", boardContent);
						params.put("appDsCd", "02");
						
						if(bandiApp.getIsFMRadio())	params.put("broadType", "radio");
						else params.put("broadType", "iradio");
						
						if(loginCode == 0)
						{
							params.put("userid", bandiApp.getUserId());
							params.put("writer", bandiApp.getUserName());
						}
						else if(loginCode == 1)
						{
							params.put("snsDsCd", "001");
							params.put("userid", "");
							params.put("writer", "");
							params.put("snsUserId", bandiApp.getFacebookId());
							params.put("snsUserNm", bandiApp.getFacebookName());
							params.put("snsUseYn", "Y");
						}
						else if(loginCode == 2)
						{
							params.put("snsDsCd", "002");
							params.put("userid", "");
							params.put("writer", "");
							params.put("snsUserId", bandiApp.getTwitterId());
							params.put("snsUserNm", bandiApp.getTwitterName());
							params.put("snsUseYn", "Y");
						}
	
						AQuery aq = new AQuery(MainActivity.this);
						aq.ajax(url, params, String.class, new AjaxCallback<String>()
						{
							@Override
							public void callback(String url, String object, AjaxStatus status) 
							{
								if(object.equals("0"))
								{
									String murl = "http://home.ebs.co.kr/bandi/function/bandiBoardList.json";

									Map<String, Object> params = new HashMap<String, Object>();
							        params.put("programId", CProgramData.getInstance().getProgramID());
							        params.put("listType", "L");
							        params.put("pageSize", "5");
									AQuery aq = new AQuery(MainActivity.this);
									aq.ajax(murl, params, JSONObject.class, new AjaxCallback<JSONObject>()
									{
										@Override
										public void callback(String url, JSONObject object, AjaxStatus status) 
										{
											bandiBoardListData.clear();
											bandiBoardLayout.removeAllViews();
											try 
											{
												JSONArray jArray = object.getJSONArray("bandiPosts");
												for(int i = 0; i < jArray.length(); i++)
												{
													BandiBoardData data = new BandiBoardData();
													data.setWriter(jArray.getJSONObject(i).getString("writer"));
													data.setRegDate(jArray.getJSONObject(i).getString("regdate"));
													data.setContents(jArray.getJSONObject(i).getString("contents"));
													data.setAdminYN(jArray.getJSONObject(i).getString("adminYn"));
													data.setSEQ(jArray.getJSONObject(i).getLong("seq"));
													data.setSNSUserName(jArray.getJSONObject(i).getString("snsUserNm"));
													data.setSNSUseYN(jArray.getJSONObject(i).getString("snsUseYn"));
													bandiBoardListData.add(data);
												}
												
												bandiList = new LinearLayout[jArray.length()];
												bandiListWriter = new TextView[jArray.length()];
												bandiListDate = new TextView[jArray.length()];
												bandiListContents = new TextView[jArray.length()];
												
												for(int i = 0; i < jArray.length(); i++)
												{
													View view = null;
													if(bandiBoardListData.get(i).getAdminYN().equals("Y"))
													{
														view = inflater.inflate(R.layout.bandi_board_manager_list_item, null);
														
														RelativeLayout main = (RelativeLayout)view.findViewById(R.id.layout_managerMain);
														main.setPadding(getHeightDP(5), getHeightDP(5), getHeightDP(5), getHeightDP(5));
														bandiListContents[i] = (TextView)view.findViewById(R.id.textView_managerMsg);
														String leftReplace = bandiBoardListData.get(i).getContents().replace("&lt;", "<");
														String rightReplce = leftReplace.replace("&gt;", ">");
														bandiListContents[i].setText(rightReplce);
//														bandiListContents[i].setText(bandiBoardListData.get(i).getContents());
														bandiListContents[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
														rParams = new RelativeLayout.LayoutParams(getHeightDP(51), getHeightDP(51));
														rParams.addRule(RelativeLayout.CENTER_VERTICAL);
														rParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
														ImageView icon = (ImageView)view.findViewById(R.id.imageView_managerIcon);
														icon.setLayoutParams(rParams);
													}
													else
													{
														view = inflater.inflate(R.layout.bandi_board_list_item, null);
														
														bandiList[i] = (LinearLayout)view.findViewById(R.id.boardListMain);
														bandiList[i].setPadding(getHeightDP(5), getHeightDP(5), getHeightDP(5), getHeightDP(5));
														if(i % 2 == 0)
														{
															bandiList[i].setBackgroundColor(Color.parseColor("#fcf8ee"));
														}
														else
														{
															bandiList[i].setBackgroundColor(Color.parseColor("#ffffff"));
														}
														bandiListWriter[i] = (TextView)view.findViewById(R.id.boardListName);
														bandiListWriter[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
														
														lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
														lParams.setMargins(getWidthDP(5), 0, 0, 0);
														bandiListDate[i] = (TextView)view.findViewById(R.id.boardListDate);
														bandiListDate[i].setLayoutParams(lParams);
														bandiListDate[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
														
														lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
														lParams.setMargins(0, getWidthDP(5), 0, 0);
														bandiListContents[i] = (TextView)view.findViewById(R.id.boardListMsg);
														bandiListContents[i].setLayoutParams(lParams);
														bandiListContents[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
														
														if(bandiBoardListData.get(i).getSNSUseYn().equals("N"))
														{
															bandiListWriter[i].setText(bandiBoardListData.get(i).getWriter());
														}
														else if(bandiBoardListData.get(i).getSNSUseYn().equals("Y"))
														{
															bandiListWriter[i].setText(bandiBoardListData.get(i).getSNSUserName());
														}
														
														bandiListDate[i].setText(bandiBoardListData.get(i).getRegDate());
														if(bandiBoardListData.get(i).getContents().equals("null") == false)
														{
															String leftReplace = bandiBoardListData.get(i).getContents().replace("&lt;", "<");
															String rightReplce = leftReplace.replace("&gt;", ">");
															bandiListContents[i].setText(rightReplce);
//															bandiListContents[i].setText(bandiBoardListData.get(i).getContents());
														}
													
													}
													bandiBoardLayout.addView(view);
												}
												
												if(jArray.length() == 5)
												{
													isLastPage = false;
//													listLayout.addView(footer);
												}
												else
												{
													isLastPage = true;
												}
												
												subScroll.loadingFinish();
												subScrollProgress.setVisibility(View.GONE);
											}
											catch (JSONException e) { e.printStackTrace(); }
											
										};
										
									});
								}
								else
								{
									Toast.makeText(MainActivity.this, "글을 올리는 중 에러가 발생하였습니다.", Toast.LENGTH_LONG).show();
								}
							}
						});
					}
					
				}
				else
				{
					Dialog.showAlert(MainActivity.this, "해당 프로그램은 반디게시판이 운영되지 않습니다.");
					
//					AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
//				    builder.setMessage("해당 프로그램은 반디게시판이 운영되지 않습니다.")
//				    .setCancelable(false)
//				    .setPositiveButton("확인", new DialogInterface.OnClickListener() 
//				    {
//				    	public void onClick(DialogInterface dialog, int id) 
//				    	{
//				    		dialog.dismiss();
//				    	}
//				    }).show();
				}
			}
		});
        
		
        videoLayout = (FrameLayout)findViewById(R.id.layout_mainVideoView);
        videoLayout.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				if(isConfigChange)
				{
					return;
				}
				
				FrameLayout onAirTools = (FrameLayout)findViewById(R.id.onAirTools);
				onAirTools.setVisibility(View.VISIBLE);
				RelativeLayout controlBar = (RelativeLayout)findViewById(R.id.control_bar);
				controlBar.setVisibility(View.VISIBLE);
				viewRadioHandler.removeCallbacks(viewRadioRunnable);
				viewRadioHandler.postDelayed(viewRadioRunnable, 5000);
			}
		});
        videoLayout.setVisibility(View.INVISIBLE);
		
		lParams = new LinearLayout.LayoutParams(getHeightDP(34), getHeightDP(34));
		lParams.setMargins(getWidthDP(5), 0, 0, 0);
		favorite = (FrameLayout)findViewById(R.id.checkBox_favorite);
		favorite.setLayoutParams(lParams);
		favorite.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v) 
			{
				
				if(isNeededToLogin())
				{
					favorite.setSelected(false);
					LoginFormDialog loginForm = LoginFormDialog.getInstance(MainActivity.this);
					bandiApp.setLoginPrevView("OnAir");
					loginForm.setOnLoginSuccessCb(new LoginFormDialog.OnLoginSuccessCb() 
					{
						@Override
						public void onLoginSuccess() 
						{
							snsIconCheck();
							new Handler().postDelayed(new Runnable()
						    {
								@Override
								public void run()
								{
									imm.hideSoftInputFromWindow(boardInput.getWindowToken(), 0);
								}
								
						    }, 200);
							
							isFavorite = false;
							if(bandiApp.getFavoriteList() == null || bandiApp.getFavoriteList().size() <= 0)
			                {
								String url = "http://home.ebs.co.kr/bandi/bandiAppUserFbmkList";
						    	Map<String, Object> params = new HashMap<String, Object>();
						    	params.put("userId", bandiApp.getUserId());
						    	params.put("fileType", "json");
						    	AQuery aq = new AQuery(MainActivity.this);
						    	aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
								{
						    		@Override
						    		public void callback(String url, JSONObject object, AjaxStatus status) 
						    		{
						    			try 
						    			{
						    				Log.d(TAG, "로그인 성공, 즐겨찾기 리스트가 없어서 다시 받아온 결과 == " + object);
						    				favoriteData.clear();
						    				JSONObject result = object.getJSONObject("appUserFbmkLists");
						    				JSONArray jArray = result.getJSONArray("appUserFbmkLists");

											for(int i = 0; i < jArray.length(); i++)
											{
												FavoriteData data = new FavoriteData();
												data.setFbmkSno(jArray.getJSONObject(i).getInt("fbmkSno"));
												data.setProgramID(jArray.getJSONObject(i).getString("courseId"));
												data.setTitle(jArray.getJSONObject(i).getString("bandiTitle"));
												data.setLinkUrl(jArray.getJSONObject(i).getString("bandiMobUrl"));
												favoriteData.add(data);
											}
//											
											bandiApp.setFavoriteList(favoriteData);
											
											for(int i = 0; i < bandiApp.getFavoriteList().size(); i++)
						                	{
						                		if(bandiApp.getFavoriteList().get(i).getProgramID().equals(CProgramData.getInstance().getProgramID()))
						                		{
						                			String fbmkSno = Integer.toString(bandiApp.getFavoriteList().get(i).getFbmkSno());
						                			CProgramData.getInstance().setNowFavoriteId(fbmkSno);
						                			isFavorite = true;
						                			favorite.setSelected(true);
						                		}
						                	}
											
											if(isFavorite == false)
											{
												url = "http://home.ebs.co.kr/bandi/bandiAppUserFbmkAdd";
												Map<String, Object> params = new HashMap<String, Object>();
												params.put("userId", bandiApp.getUserId());
												params.put("programId", CProgramData.getInstance().getProgramID());
												params.put("addDsCd", "02");
												params.put("fileType", "json");
												AQuery aq = new AQuery(MainActivity.this);
												aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
												{
													@Override
													public void callback(String url, JSONObject object, AjaxStatus status) 
													{
														try 
														{
															JSONObject result = object.getJSONObject("resultXml");
															if(result.getString("resultCd").equals("00"))
															{
																CProgramData.getInstance().setNowFavoriteId(result.getString("fbmkSno"));
																bandiApp.favoriteDataRequest(new AjaxCallback<JSONObject>()
																{
																	
																	@Override
																	public void callback(String url, JSONObject object, AjaxStatus status) 
																	{
																		try
																		{
																			bandiApp.getFavoriteList().clear();
																			JSONObject result = object.getJSONObject("appUserFbmkLists");
																			JSONArray jArray = result.getJSONArray("appUserFbmkLists");
															
																			for(int i = 0; i < jArray.length(); i++)
																			{
																				FavoriteData data = new FavoriteData();
																				data.setFbmkSno(jArray.getJSONObject(i).getInt("fbmkSno"));
																				data.setProgramID(jArray.getJSONObject(i).getString("courseId"));
																				data.setTitle(jArray.getJSONObject(i).getString("bandiTitle"));
																				bandiApp.getFavoriteList().add(data);
																			}

																			AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
																		    builder.setMessage("즐겨찾기에 등록하였습니다. 즐겨찾기를 확인하시겠습니까?")
																		    .setCancelable(false)
																		    .setPositiveButton("확인", new DialogInterface.OnClickListener() 
																		    {
																		    	public void onClick(DialogInterface dialog, int id) 
																		    	{
																		    		dialog.dismiss();
																		    		unselectAllMenus(MainActivity.this);
																		    		TextView favorite = (TextView) findViewById(R.id.favorite);
																		            ImageView icon_favorite = (ImageView)findViewById(R.id.imageView_favorite);
																		        	favorite.setSelected(true);
																		        	icon_favorite.setSelected(true);
																		        	findViewById(R.id.layout_menu4).setBackgroundColor(Color.parseColor("#3f3833"));
																		        	new Favorite(MainActivity.this).run();
																		    	}
																		    })
																		    .setNegativeButton("취소", new DialogInterface.OnClickListener() 
																		    {
																				@Override
																				public void onClick(DialogInterface dialog, int which) 
																				{
																					dialog.dismiss();
																				}
																			});
																	        AlertDialog alert = builder.create();
																	        alert.show();
																		}
																		catch(JSONException e) {}
																	}
																});
															}
															else
															{
																favorite.setSelected(false);
																Toast.makeText(MainActivity.this, "즐겨찾기 등록 중 오류가 발생하였습니다.", Toast.LENGTH_LONG).show();
															}
														} catch (JSONException e) {}
													};
												});
											}
											else
											{
												favorite.setSelected(true);
											}
						    			}
						    			catch(Exception e) {}
						    		}
								});
			                }
							else
							{
								isFavorite = false;
								for(int i = 0; i < bandiApp.getFavoriteList().size(); i++)
			                	{
			                		if(bandiApp.getFavoriteList().get(i).getProgramID().equals(CProgramData.getInstance().getProgramID()))
			                		{
			                			String fbmkSno = Integer.toString(bandiApp.getFavoriteList().get(i).getFbmkSno());
			                			CProgramData.getInstance().setNowFavoriteId(fbmkSno);
			                			isFavorite = true;
			                		}
			                	}
			                	
			                	if(isFavorite == false)
								{
			                		String url = "http://home.ebs.co.kr/bandi/bandiAppUserFbmkAdd";
									Map<String, Object> params = new HashMap<String, Object>();
									params.put("userId", bandiApp.getUserId());
									params.put("programId", CProgramData.getInstance().getProgramID());
									params.put("addDsCd", "02");
									params.put("fileType", "json");
									APIWork work = new APIWork(MainActivity.this);
									work.doThreadJob(url, params, new APIWork.APICallback()
//									AQuery aq = new AQuery(MainActivity.this);
//									aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
									{
										@Override
//										public void callback(String url, JSONObject object, AjaxStatus status)
										public void callback(String responseStr)
										{
											try 
											{
												JSONObject object = new JSONObject(responseStr);
												
												JSONObject result = object.getJSONObject("resultXml");
												if(result.getString("resultCd").equals("00"))
												{
													CProgramData.getInstance().setNowFavoriteId(result.getString("fbmkSno"));
													bandiApp.favoriteDataRequest(new AjaxCallback<JSONObject>()
													{
														@Override
														public void callback(String url, JSONObject object, AjaxStatus status) 
														{
															try
															{
																bandiApp.getFavoriteList().clear();
																JSONObject result = object.getJSONObject("appUserFbmkLists");
																JSONArray jArray = result.getJSONArray("appUserFbmkLists");
												
																for(int i = 0; i < jArray.length(); i++)
																{
																	FavoriteData data = new FavoriteData();
																	data.setFbmkSno(jArray.getJSONObject(i).getInt("fbmkSno"));
																	data.setProgramID(jArray.getJSONObject(i).getString("courseId"));
																	data.setTitle(jArray.getJSONObject(i).getString("bandiTitle"));
																	bandiApp.getFavoriteList().add(data);
																}
																AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
															    builder.setMessage("즐겨찾기에 등록하였습니다. 즐겨찾기를 확인하시겠습니까?")
															    .setCancelable(false)
															    .setPositiveButton("확인", new DialogInterface.OnClickListener() 
															    {
															    	public void onClick(DialogInterface dialog, int id) 
															    	{
															    		dialog.dismiss();
															    		unselectAllMenus(MainActivity.this);
															    		TextView favorite = (TextView) findViewById(R.id.favorite);
															            ImageView icon_favorite = (ImageView)findViewById(R.id.imageView_favorite);
															        	favorite.setSelected(true);
															        	icon_favorite.setSelected(true);
															        	findViewById(R.id.layout_menu4).setBackgroundColor(Color.parseColor("#3f3833"));
															        	new Favorite(MainActivity.this).run();
															    	}
															    })
															    .setNegativeButton("취소", new DialogInterface.OnClickListener() 
															    {
																	@Override
																	public void onClick(DialogInterface dialog, int which) 
																	{
																		dialog.dismiss();
																		favorite.setSelected(true);
																	}
																});
														        AlertDialog alert = builder.create();
														        alert.show();
															}
															catch(JSONException e) {}
														}
													});
												}
												else
												{
													favorite.setSelected(false);
													Toast.makeText(MainActivity.this, "즐겨찾기 등록 중 오류가 발생하였습니다.", Toast.LENGTH_LONG).show();
												}
											} catch (JSONException e) {}
										};
									});
								}
								else
								{
									favorite.setSelected(true);
								}
							}
						}
					});
					loginForm.setOnLoginFailureCb(new LoginFormDialog.OnLoginFailureCb() 
					{
						@Override
						public void onLoginFailure() 
						{
							snsIconCheck();
						}
					});
					loginForm.showDialog(true);
				}
				else
				{
					if(favorite.isSelected() == false)
					{
						String url = "http://home.ebs.co.kr/bandi/bandiAppUserFbmkAdd";
						Map<String, Object> params = new HashMap<String, Object>();
						params.put("userId", bandiApp.getUserId());
						params.put("programId", CProgramData.getInstance().getProgramID());
						params.put("addDsCd", "02");
						params.put("fileType", "json");
						
						APIWork work = new APIWork(MainActivity.this);
						work.doThreadJob(url, params, new APIWork.APICallback()
//						AQuery aq = new AQuery(MainActivity.this);
//						aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
						{
							@Override
//							public void callback(String url, JSONObject object, AjaxStatus status)
							public void callback(String responseStr)
							{
								try 
								{
									JSONObject object = new JSONObject(responseStr);
									
									JSONObject result = object.getJSONObject("resultXml");
									if(result.getString("resultCd").equals("00"))
									{
										CProgramData.getInstance().setNowFavoriteId(result.getString("fbmkSno"));
										bandiApp.retrieveFavoriteData(MainActivity.this, new APIWork.APICallback()
//										bandiApp.favoriteDataRequest(new AjaxCallback<JSONObject>()
										{
											
											@Override
//											public void callback(String url, JSONObject object, AjaxStatus status)
											public void callback(String responseStr)
											{
												try
												{
													JSONObject object = new JSONObject(responseStr);
													
													bandiApp.getFavoriteList().clear();
													JSONObject result = object.getJSONObject("appUserFbmkLists");
													JSONArray jArray = result.getJSONArray("appUserFbmkLists");
									
													for(int i = 0; i < jArray.length(); i++)
													{
														FavoriteData data = new FavoriteData();
														data.setFbmkSno(jArray.getJSONObject(i).getInt("fbmkSno"));
														data.setProgramID(jArray.getJSONObject(i).getString("courseId"));
														data.setTitle(jArray.getJSONObject(i).getString("bandiTitle"));
														bandiApp.getFavoriteList().add(data);
													}

													
													AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
												    builder.setMessage("즐겨찾기에 등록하였습니다. 즐겨찾기를 확인하시겠습니까?")
												    .setCancelable(false)
												    .setPositiveButton("확인", new DialogInterface.OnClickListener() 
												    {
												    	public void onClick(DialogInterface dialog, int id) 
												    	{
												    		dialog.dismiss();
												    		unselectAllMenus(MainActivity.this);
												    		TextView favorite = (TextView) findViewById(R.id.favorite);
												            ImageView icon_favorite = (ImageView)findViewById(R.id.imageView_favorite);
												        	favorite.setSelected(true);
												        	icon_favorite.setSelected(true);
												        	findViewById(R.id.layout_menu4).setBackgroundColor(Color.parseColor("#3f3833"));
												        	new Favorite(MainActivity.this).run();
												    	}
												    })
												    .setNegativeButton("취소", new DialogInterface.OnClickListener() 
												    {
														
														@Override
														public void onClick(DialogInterface dialog, int which) 
														{
															dialog.dismiss();
															favorite.setSelected(true);
//															new OnAir(MainActivity.this).run();
														}
													});
											        AlertDialog alert = builder.create();
											        alert.show();
												}
												catch(JSONException e)
												{
													
												}
											}
										});
									}
									else
									{
										favorite.setSelected(false);
										Toast.makeText(MainActivity.this, "즐겨찾기 등록 중 오류가 발생하였습니다.", Toast.LENGTH_LONG).show();
									}
								}
								catch (JSONException e) 
								{
									e.printStackTrace();
								}
							};
						});
					}
					else
					{
						AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
					    builder.setMessage("즐겨찾기에서 삭제되었습니다.")
					    .setCancelable(false)
					    .setPositiveButton("확인", new DialogInterface.OnClickListener() 
					    {
					    	public void onClick(DialogInterface dialog, int id) 
					    	{
					    		// http://s-home.ebs.co.kr/bandi/bandiAppUserFbmkModify?userId=아이디&programId=121212&fbmkSno=2&fileType=json
					    		dialog.dismiss();
					    		String url = "http://home.ebs.co.kr/bandi/bandiAppUserFbmkModify";
								Map<String, Object> params = new HashMap<String, Object>();
								params.put("userId", bandiApp.getUserId());
								params.put("programId", CProgramData.getInstance().getProgramID());
								params.put("fbmkSno", CProgramData.getInstance().getNowFavoriteId());
								params.put("fileType", "json");
								
								APIWork work = new APIWork(MainActivity.this);
								work.doThreadJob(url, params, new APIWork.APICallback()
//								AQuery aq = new AQuery(MainActivity.this);
//								aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
								{
									@Override
//									public void callback(String url, JSONObject object, AjaxStatus status)
									public void callback(String responseStr)
									{
										try
										{
											JSONObject object = new JSONObject(responseStr);
											
											JSONObject result = object.getJSONObject("resultXml");
											String resultCode = result.getString("resultCd");
											if(resultCode.equals("00"))
											{
												favorite.setSelected(false);
												bandiApp.retrieveFavoriteData(MainActivity.this, new APIWork.APICallback()
//												bandiApp.favoriteDataRequest(new AjaxCallback<JSONObject>()
												{
													
													@Override
//													public void callback(String url, JSONObject object, AjaxStatus status)
													public void callback(String responseStr)
													{
														try
														{
															JSONObject object = new JSONObject(responseStr);
															
															bandiApp.getFavoriteList().clear();
															JSONObject result = object.getJSONObject("appUserFbmkLists");
															JSONArray jArray = result.getJSONArray("appUserFbmkLists");
											
															for(int i = 0; i < jArray.length(); i++)
															{
																FavoriteData data = new FavoriteData();
																data.setFbmkSno(jArray.getJSONObject(i).getInt("fbmkSno"));
																data.setProgramID(jArray.getJSONObject(i).getString("courseId"));
																data.setTitle(jArray.getJSONObject(i).getString("bandiTitle"));
																bandiApp.getFavoriteList().add(data);
															}
														}
														catch(JSONException e)
														{
															
														}
													}
												});
											}
											else
											{
												Toast.makeText(MainActivity.this, result.getString("resultMsg"), Toast.LENGTH_LONG).show();
												favorite.setSelected(true);
											}
										}
										catch(JSONException e)
										{
											
										}
									};
									
								});
					    	}
					    });
						AlertDialog alert = builder.create();
						alert.show();
					}
				}
			}
		});

		LinearLayout layoutSubScrollNoti = (LinearLayout)findViewById(R.id.layout_subScrollNoti);
		layoutSubScrollNoti.setPadding(getHeightDP(5), getHeightDP(5), getHeightDP(5), getHeightDP(5));
		layoutSubScrollNoti.setOnClickListener(new OnClickListener() 
		{
			
			@Override
			public void onClick(View v) 
			{
				if(bandiApp.getNotiData() == null)
				{
//					Dialog.showAlert(MainActivity.this, "공지사항이 없습니다.");
					AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
				    builder.setMessage("공지사항이 없습니다.")
				    .setCancelable(true)
				    .setPositiveButton("확인", new DialogInterface.OnClickListener() 
				    {
				    	public void onClick(DialogInterface dialog, int id) 
				    	{
				    		dialog.dismiss();
				    	}
				    }).show();
				}
				else
				{
					EventDialog d = new EventDialog(MainActivity.this);
					d.setData(bandiApp.getNotiData());
					d.show();
				}
			}
		});
		
		lParams = new LinearLayout.LayoutParams(getHeightDP(48), getHeightDP(20));
		ImageView notiIcon = (ImageView)findViewById(R.id.imageView_bandiBoardNotiIcon);
		notiIcon.setLayoutParams(lParams);
		
        writeZone.setVisibility(View.INVISIBLE);
        
		/*
		 * 음원 재생과 관련된 기능
		 */
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		
		// volume control 
		vol = (SeekBar) findViewById(R.id.volume_bar);
        vol.setMax(audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        vol.setProgress(audio.getStreamVolume(AudioManager.STREAM_MUSIC));
        vol.setOnSeekBarChangeListener(new OnSeekBarChangeListener()
        {
        	@Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) 
        	{
                audio.setStreamVolume(AudioManager.STREAM_MUSIC, vol.getProgress(), AudioManager.MODE_NORMAL);
            }
        	
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) 
            {
                audio.setStreamVolume(AudioManager.STREAM_MUSIC, vol.getProgress(), AudioManager.MODE_NORMAL);
            }

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) { /* pass */	}
        });

        lParams = new LinearLayout.LayoutParams(getHeightDP(28), getHeightDP(28));
        lParams.setMargins(0, 0, getWidthDP(3), 0);
        ImageButton twitterBtn = (ImageButton)findViewById(R.id.imageButton_twitter);
        twitterBtn.setLayoutParams(lParams);
        ImageButton facebookBtn = (ImageButton)findViewById(R.id.imageButton_facebook);
        facebookBtn.setLayoutParams(lParams);
        ImageButton kakaoBtn = (ImageButton)findViewById(R.id.imageButton_kakao);
        kakaoBtn.setLayoutParams(lParams);
        
        lParams = new LinearLayout.LayoutParams(getHeightDP(34), getHeightDP(34));
        ImageButton homeBtn = (ImageButton)findViewById(R.id.imageButton_home);
        homeBtn.setLayoutParams(lParams);
        
        rParams = new RelativeLayout.LayoutParams(getHeightDP(50), LayoutParams.MATCH_PARENT);
        rParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        FrameLayout showBandiboardBtn = (FrameLayout)findViewById(R.id.show_bandiboard_btn);
        showBandiboardBtn.setLayoutParams(rParams);
        
        fParams = new FrameLayout.LayoutParams(getHeightDP(25), getHeightDP(25), Gravity.CENTER);
        ImageView ivShowBandiboardBtn = (ImageView)findViewById(R.id.imageView_show_bandiboard_btn);
        ivShowBandiboardBtn.setLayoutParams(fParams);
        
        // mute on/off
        rParams = new RelativeLayout.LayoutParams(getWidthDP(22), getHeightDP(16));
        rParams.addRule(RelativeLayout.CENTER_VERTICAL);
        rParams.setMargins(getWidthDP(10), 0, 0, 0);
        rParams.addRule(RelativeLayout.RIGHT_OF, R.id.play_control_wrapper);
        final ImageView muteBtn = (ImageView) findViewById(R.id.mute_btn);
        muteBtn.setLayoutParams(rParams);
        muteBtn.setSelected(false);
        muteBtn.setOnClickListener(muteBtnClickListener);
	}
	
	public void boardListAdd()
	{
		String url = "http://home.ebs.co.kr/bandi/bandiBoardList";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("fileType", "json");
        params.put("programId", CProgramData.getInstance().getProgramID());
        params.put("listType", "N");
        params.put("pageSize", "5");

        
        ArrayList<BandiBoardData> copy = new ArrayList<BandiBoardData>(bandiBoardListData);
		Collections.sort(copy, new NoAscCompare());
		params.put("seq", copy.get(0).getSEQ());
		AQuery aq = new AQuery(MainActivity.this);
		aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
		{
			@Override
			public void callback(String url, JSONObject object, AjaxStatus status) 
			{
				try 
				{
					boardPageIndex++;
					JSONArray jArray = object.getJSONArray("bandiPosts");
					for(int i = 0; i < jArray.length(); i++)
					{
						BandiBoardData data = new BandiBoardData();
						data.setWriter(jArray.getJSONObject(i).getString("writer"));
						data.setRegDate(jArray.getJSONObject(i).getString("regdate"));
						data.setContents(jArray.getJSONObject(i).getString("contents"));
						data.setAdminYN(jArray.getJSONObject(i).getString("adminYn"));
						data.setSEQ(jArray.getJSONObject(i).getLong("seq"));
						data.setSNSUserName(jArray.getJSONObject(i).getString("snsUserNm"));
						data.setSNSUseYN(jArray.getJSONObject(i).getString("snsUseYn"));
						bandiBoardListData.add(data);
					}
					
					bandiList = new LinearLayout[jArray.length()];
					bandiListWriter = new TextView[jArray.length()];
					bandiListDate = new TextView[jArray.length()];
					bandiListContents = new TextView[jArray.length()];
					
					for(int i = 0; i < jArray.length(); i++)
					{
						View view = null;
						if(bandiBoardListData.get(i + (boardPageIndex * 5)).getAdminYN().equals("Y"))
						{
							view = inflater.inflate(R.layout.bandi_board_manager_list_item, null);
							RelativeLayout main = (RelativeLayout)view.findViewById(R.id.layout_managerMain);
							main.setPadding(getHeightDP(5), getHeightDP(5), getHeightDP(5), getHeightDP(5));
							bandiListContents[i] = (TextView)view.findViewById(R.id.textView_managerMsg);
							String leftReplace = bandiBoardListData.get(i + (boardPageIndex * 5)).getContents().replace("&lt;", "<");
							String rightReplce = leftReplace.replace("&gt;", ">");
							bandiListContents[i].setText(rightReplce);
//							bandiListContents[i].setText(bandiBoardListData.get(i + (boardPageIndex * 5)).getContents());
							bandiListContents[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
							rParams = new RelativeLayout.LayoutParams(getHeightDP(51), getHeightDP(51));
							rParams.addRule(RelativeLayout.CENTER_VERTICAL);
							rParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
							ImageView icon = (ImageView)view.findViewById(R.id.imageView_managerIcon);
							icon.setLayoutParams(rParams);
						}
						else
						{
						
							view = inflater.inflate(R.layout.bandi_board_list_item, null);
							
							bandiList[i] = (LinearLayout)view.findViewById(R.id.boardListMain);
							bandiList[i].setPadding(getHeightDP(5), getHeightDP(5), getHeightDP(5), getHeightDP(5));
							if(i % 2 == 0)
							{
								bandiList[i].setBackgroundColor(Color.parseColor("#fcf8ee"));
							}
							else
							{
								bandiList[i].setBackgroundColor(Color.parseColor("#ffffff"));
							}
							
							bandiListWriter[i] = (TextView)view.findViewById(R.id.boardListName);
							bandiListWriter[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
							
							lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
							lParams.setMargins(getWidthDP(5), 0, 0, 0);
							bandiListDate[i] = (TextView)view.findViewById(R.id.boardListDate);
							bandiListDate[i].setLayoutParams(lParams);
							bandiListDate[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
							
							lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
							lParams.setMargins(0, getWidthDP(5), 0, 0);
							bandiListContents[i] = (TextView)view.findViewById(R.id.boardListMsg);
							bandiListContents[i].setLayoutParams(lParams);
							bandiListContents[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
							
							if(bandiBoardListData.get(i + (boardPageIndex * 5)).getSNSUseYn().equals("N"))
							{
								bandiListWriter[i].setText(bandiBoardListData.get(i + (boardPageIndex * 5)).getWriter());
							}
							else if(bandiBoardListData.get(i + (boardPageIndex * 5)).getSNSUseYn().equals("Y"))
							{
								bandiListWriter[i].setText(bandiBoardListData.get(i + (boardPageIndex * 5)).getSNSUserName());
							}
							
							bandiListDate[i].setText(bandiBoardListData.get(i + (boardPageIndex * 5)).getRegDate());
							
							if(bandiBoardListData.get(i + (boardPageIndex * 5)).getContents().equals("null") == false)
							{
								String leftReplace = bandiBoardListData.get(i + (boardPageIndex * 5)).getContents().replace("&lt;", "<");
								String rightReplce = leftReplace.replace("&gt;", ">");
								bandiListContents[i].setText(rightReplce);
//								bandiListContents[i].setText(bandiBoardListData.get(i + (boardPageIndex * 5)).getContents());
							}
							
						}
						bandiBoardLayout.addView(view);
					}
					
					if(jArray.length() == 5)
					{
						isLastPage = false;
//						listLayout.addView(footer);
					}
					else
					{
						isLastPage = true;
					}
					
					bandiBoardLayout.removeView(footer);
				}
				catch (JSONException e) { e.printStackTrace(); }
				
			};
			
		});
	}
	
	OnClickListener shareBtnClickListener = new OnClickListener()
	{
		
		@Override
		public void onClick(View v)
		{
			if(v.getId() == R.id.imageButton_shareFacebook)
			{
				facebookLogin();
			}
			else if(v.getId() == R.id.imageButton_shareTwitter)
			{
				if(bandiApp.isTwitterLogin() == false)
				{
					loginToTwitter(false);
				}
				else
				{
					TwitterDialog dialog = new TwitterDialog(MainActivity.this);
					dialog.setListener(new OnTwitterDialogListener()
					{
						@Override
						public void OnConfirmClick(String message) 
						{
							updateTwitterStatus update = new updateTwitterStatus();
							update.execute(message);
						}
						
						@Override
						public void OnCancelClick()	{}
					});
					dialog.show();
				}
			}
			else if(v.getId() == R.id.imageButton_shareKakao)
			{
				try 
				{
					KakaoLink kakaoLink = KakaoLink.getKakaoLink(MainActivity.this);
					KakaoTalkLinkMessageBuilder kakaoTalkLinkMessageBuilder = kakaoLink.createKakaoTalkLinkMessageBuilder();
					
					kakaoTalkLinkMessageBuilder.addText(stringText);
					
					final String linkContents = kakaoTalkLinkMessageBuilder.build();
					kakaoLink.sendMessage(linkContents, MainActivity.this);
					
				}
				catch (KakaoParameterException e1) {}
			}
		}
	};
	OnClickListener videoPlayCheckListener = new OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			v.setSelected(!v.isSelected());
			
			if(v.isSelected() == true)
			{
				repeatListViewInit();
				startVideo();
				replayImg.setVisibility(View.INVISIBLE);
			}
			else
			{
				video.stopPlayback();
			}
		}
	};
	OnClickListener videoFullClickListener = new OnClickListener()
	{
		
		@Override
		public void onClick(View v)
		{
			if(isWifi(MainActivity.this) == false && prefs.getUsePaidNetwork() == false)
			{
				String confirmMsg = getResources().getString(R.string.use_paid_network_alert_msg);
				Dialog.confirm(MainActivity.this, confirmMsg, new DialogInterface.OnClickListener() 
				{
					@Override
					public void onClick(DialogInterface dialog, int which) 
					{
//						boardTextLayout.setVisibility(View.GONE);
						new Handler().postDelayed(new Runnable()
						{
							
							@Override
							public void run() 
							{
								LinearLayout setting = (LinearLayout) findViewById(R.id.layout_menu7);
								setting.performClick();
							}
						}, 300);
					}
				}, "설정으로 이동");
				return;
			}
			
			if (CProgramData.getInstance().getViewRadioState().startsWith("Y") || CProgramData.getInstance().getViewRadioState().startsWith("y")) 
            {
				repeatListViewInit();
//				video.stopPlayback();
				
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
				
				
//				Intent i = new Intent(MainActivity.this, VideoActivity.class);
//				startActivityForResult(i, 1001);
            }
			else
			{
				Dialog.showAlert(MainActivity.this, "해당 프로그램은 보이는 라디오 서비스를 제공하지 않습니다.");
			}
		}
	};
	OnClickListener shareClickListener = new OnClickListener()
	{
		
		@Override
		public void onClick(final View v)
		{
			if(v.isSelected() == false)
			{
				v.setSelected(true);
				ShareDialog dialog = new ShareDialog(MainActivity.this);
				dialog.setListener(shareListener);
				dialog.setOnDismissListener(new OnDismissListener() 
				{
					@Override
					public void onDismiss(DialogInterface dialog) 
					{
						v.setSelected(false);
					}
				});
				dialog.show();
			}
		}
	};
	
	SharePopupListener shareListener = new SharePopupListener() 
	{
		
		@Override
		public void twitterClick()
		{
			if(bandiApp.isTwitterLogin() == false)
			{
				loginToTwitter(false);
			}
			else
			{
				TwitterDialog dialog = new TwitterDialog(MainActivity.this);
				dialog.setListener(new OnTwitterDialogListener()
				{
					@Override
					public void OnConfirmClick(String message) 
					{
						updateTwitterStatus update = new updateTwitterStatus();
						update.execute(message);
					}
					
					@Override
					public void OnCancelClick()	{}
				});
				dialog.show();
			}
		}
		
		@Override
		public void kakaoClick()
		{
			try 
			{
				KakaoLink kakaoLink = KakaoLink.getKakaoLink(MainActivity.this);
				KakaoTalkLinkMessageBuilder kakaoTalkLinkMessageBuilder = kakaoLink.createKakaoTalkLinkMessageBuilder();
				
				kakaoTalkLinkMessageBuilder.addText(stringText);
				
				final String linkContents = kakaoTalkLinkMessageBuilder.build();
				kakaoLink.sendMessage(linkContents, MainActivity.this);
				
			}
			catch (KakaoParameterException e1) {}
		}
		
		@Override
		public void facebookClick() 
		{
			facebookLogin();
		}
	};
	
	OnClickListener muteBtnClickListener = new OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			if (v.isSelected()) 
            {
            	v.setBackgroundResource(R.drawable.selector_ico_volume);
            	v.setSelected(false); // turn sound on
                audio.setStreamMute(AudioManager.STREAM_MUSIC, false);  
            }
            else
            {
            	v.setBackgroundResource(R.drawable.selector_ico_volume2);
                v.setSelected(true); // turn sound off
                audio.setStreamMute(AudioManager.STREAM_MUSIC, true);
            }
		}
	};
	
	/**
	 * 보이는 라디오 버튼 클릭 리스너
	 */
	OnClickListener viewRaidoCheckListener = new OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			if(isWifi(MainActivity.this) == false && prefs.getUsePaidNetwork() == false)
			{
				String confirmMsg = getResources().getString(R.string.use_paid_network_alert_msg);
				Dialog.confirm(MainActivity.this, confirmMsg, new DialogInterface.OnClickListener() 
				{
					@Override
					public void onClick(DialogInterface dialog, int which) 
					{
						new Handler().postDelayed(new Runnable() 
						{
							
							@Override
							public void run() 
							{
								LinearLayout setting = (LinearLayout) findViewById(R.id.layout_menu7);
								setting.performClick();
							}
						}, 300);
					}
				}, "설정으로 이동");
				return;
			}
			
			//if (CProgramData.getInstance() == null) return;
			if (CProgramData.getInstance() == null || CProgramData.getInstance().getViewRadioState() == null) return;
			if (CProgramData.getInstance().getViewRadioState().startsWith("Y") || CProgramData.getInstance().getViewRadioState().startsWith("y")) 
            {
				if(v.isSelected()) // 비디오 -> 오디오 전환 시
				{
					state = 3;
					viewRadioHandler.removeCallbacks(viewRadioRunnable);
					FrameLayout onAirTools = (FrameLayout)findViewById(R.id.onAirTools);
					onAirTools.setVisibility(View.VISIBLE);
					RelativeLayout controlBar = (RelativeLayout)findViewById(R.id.control_bar);
					controlBar.setVisibility(View.VISIBLE);
					nowScrollHeight = subScrollMain.getHeight();
					targetScrollHeight = 0;
					
					videoFullBtn.setVisibility(View.INVISIBLE);
					video.stopPlayback();
					videoLayout.setVisibility(View.INVISIBLE);
					videoPlayBtn.setVisibility(View.INVISIBLE);
					controlLayout.setVisibility(View.VISIBLE);
					appImg2.setVisibility(View.INVISIBLE);

					if (isFreeNetwork()) 
					{
						replayImg.setVisibility(View.INVISIBLE);
						serviceConnection.getAudioService().initPlayer(playBtn, loading);
						repeatListViewInit();
					}
					else 
					{
						alertNetworkState();
						switchToPauseState();
					}
					
					writeZone.setVisibility(View.INVISIBLE);
					textZone.setVisibility(View.VISIBLE);
					
					v.setSelected(false);
					
					Log.d(TAG, "비디오 -> 오디오 전환");
					//** Google Analytics **
					new GoogleAnalytics(MainActivity.this).sendScreenView("VisibleRadio", GoogleAnalytics.CHANNEL.FM);
					
				}
				else // 오디오 -> 비디오 전환 시
				{
					
					isFromRadioBtn = true;
					state = 2;
					viewRadioHandler.postDelayed(viewRadioRunnable, 5000);
					targetScrollHeight = getHeightDP(180);
					nowScrollHeight = 0;
					writeZone.setVisibility(View.VISIBLE);
					textZone.setVisibility(View.INVISIBLE);

					if(serviceConnection != null && serviceConnection.getAudioService() != null) 
					{
						serviceConnection.getAudioService().stopPlayer(playBtn, loading);
					}
					
					controlLayout.setVisibility(View.INVISIBLE);
					videoPlayBtn.setVisibility(View.VISIBLE);
					videoPlayBtn.setSelected(true);
	                
					replayImg.setVisibility(View.INVISIBLE);
					repeatListViewInit();
					
					v.setSelected(true);
					
					Log.d(TAG, "오디오 -> 비디오 전환"); 
					//** Google Analytics **
					GoogleAnalytics.CHANNEL channel = (bandiApp.getIsFMRadio() ? GoogleAnalytics.CHANNEL.FM : GoogleAnalytics.CHANNEL.IRADIO);
					new GoogleAnalytics(MainActivity.this).sendScreenView(OnAir.TAG, channel);
				}
				moveLayout();
            }
			else
			{
				v.setSelected(false);
				AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
			    builder.setMessage("해당 프로그램은 보이는 라디오 서비스를 제공하지 않습니다")
			    .setCancelable(false)
			    .setPositiveButton("확인", new DialogInterface.OnClickListener() 
			    {
			    	public void onClick(DialogInterface dialog, int id) 
			    	{
			    		dialog.dismiss();
			    	}
			    }).show();
			}
		}
	};
	OnClickListener playBtnClickListener = new OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			replayImg.setVisibility(View.INVISIBLE);
			if (playBtn.isSelected()) 
			{
				if(state == 2)
				{
					viewRadioCheck.setSelected(false);
					state = 3;
					nowScrollHeight = subScrollMain.getHeight();
					targetScrollHeight = 0;
					moveLayout();
					videoFullBtn.setVisibility(View.INVISIBLE);
					video.stopPlayback();
					videoLayout.setVisibility(View.INVISIBLE);
				}
				
				repeatListViewInit();
//				controllerPlay();
				if (!serviceConnection.isBound()) 
				{
					if (isFreeNetwork()) 
					{
						getApplicationContext().bindService(audioServiceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
					}
					else 
					{
						alertNetworkState();
						switchToPauseState();
					}
				}
				else 
				{
					if (isFreeNetwork()) 
					{
						serviceConnection.getAudioService().initPlayer(playBtn, loading);
					}
					else 
					{
						alertNetworkState();
						switchToPauseState();
					}
				}
			} 
			else
			{
				if(serviceConnection != null && serviceConnection.getAudioService() != null) 
				{
					serviceConnection.getAudioService().stopPlayer(playBtn, loading);
				}
			}
		}
	};
	
	@Override
	protected void finishActivity() 
	{
		
		if(quitBandi != null) unregisterReceiver(quitBandi);
		if (audioNoisyReceiver != null) unregisterReceiver(audioNoisyReceiver);
		
		if(networkChangeReceiver != null) unregisterReceiver(networkChangeReceiver);
		BandiLog.disconnect(this); // BandiApplication.clear() 전에 등장해야함.
		
		if (wifiLock != null) 
		{
            wifiLock.release();
            wifiLock = null;
		}
		
		//해제
//		if (wakeLock != null)
//		{
//		    wakeLock.release();
//		    wakeLock = null;
//		}
		
		((BandiApplication) getApplicationContext()).clear();
		if(audioServiceIntent != null) stopService(audioServiceIntent);
		if(notiIntent != null) stopService(notiIntent);
		// AudioService 에 있는 걸 여기에서도 중복 처리 (아이콘이 사라지지 않는 경우가 생겨서..)
//		try 
//		{
//			NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//	    	notificationManager.cancel(NOTIFICATION_ID);
//		} 
//		catch(Exception e) {}
		
		// 광고 멈춤.
		try { if (isFinishing() && adZone != null) adZone.destroy(); } catch(Exception e) {}
	};
	
	/**
	 * 기기의 볼륨값을 받아와서 volume bar 의 위치를 설정
	 */
	private void setVolumnBarPosition() 
	{
        final SeekBar vol = (SeekBar) findViewById(R.id.volume_bar);
        final AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        vol.setProgress(audio.getStreamVolume(AudioManager.STREAM_MUSIC));
    }
	
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		// 기기 볼륨버튼과 화면 볼륨바 연동
        AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        final SeekBar vol = (SeekBar) findViewById(R.id.volume_bar); 
        
        switch (keyCode) 
        {
            case KeyEvent.KEYCODE_VOLUME_UP:
                audio.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, AudioManager.MODE_NORMAL);
                vol.setProgress(audio.getStreamVolume(AudioManager.STREAM_MUSIC));
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                audio.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, AudioManager.MODE_NORMAL);
                vol.setProgress(audio.getStreamVolume(AudioManager.STREAM_MUSIC));
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }
	
	
	private class AsyncAction extends AsyncTask<Void, Void, Void> 
	{
		private String link="", imgUrl="";
		@Override
		protected Void doInBackground(Void... arg0) 
		{
			// 팝업배너 혹은 LG U+전면 광고
			PopBanner popBanner = new PopBanner(MainActivity.this);
			if (popBanner.existPopBanner()) 
			{
				//** EBS 전면 배너
				Preferences prefs = new Preferences(MainActivity.this);
				if (! prefs.inBannerClosedTime()) 
				{
					if (popBanner.canParticipate()) 
					{
						String[] info = popBanner.getInfo();
						if (info != null) 
						{
							link = info[0];
							imgUrl = info[1];
							
							popBanner.downloadBannerImage();
						}
					}
				}
			} 
			return null;
		}
		
		@Override
        protected void onPostExecute(Void arg0) 
		{
			// 팝업 배너 혹은 LG U+전면 광고
			PopBanner popBanner = new PopBanner(MainActivity.this);
			if (popBanner.existPopBanner()) 
			{
				//** 팝업 배너
				if (! "".equals(imgUrl)) 
				{
					Log.d(TAG, "popBanner-imgUrl : " + imgUrl + " and link : " + link);
					Intent intent = new Intent(MainActivity.this, PopBannerActivity.class);
					intent.putExtra("link", link);
					intent.putExtra("imgUrl", imgUrl);
					startActivityForResult(intent, EBS_REQUEST_CODE);
				}
			} 
			else 
			{
				//** LG U+ 광고
				if(isFirst)
		    	{
					LGUDMPAdView.fullscreenImage(MainActivity.this, LGUP_REQUEST_CODE, "0958764861", true, null);//(test) 0773420419 //0958764861
		        	isFirst = false;
		    	}
			}
        }
	}
	
	/**
	 * ebs 로그인인지, sns 로그인인지에 따라 반디 아이콘 설정
	 */
	public void snsIconCheck()
	{
		try
		{
			if(isNeededToLogin() == false)
			{
				snsIcon.setBackgroundResource(R.drawable.bandi_sns_ico_1);
				snsIcon.setVisibility(View.VISIBLE);
				loginCode = 0;
			}
			else if(bandiApp.isFacebookLogin())
			{
				snsIcon.setBackgroundResource(R.drawable.bandi_sns_ico_3);
				snsIcon.setVisibility(View.VISIBLE);
				loginCode = 1;
			}
			else if(bandiApp.isTwitterLogin())
			{
				snsIcon.setBackgroundResource(R.drawable.bandi_sns_ico_2);
				snsIcon.setVisibility(View.VISIBLE);
				loginCode = 2;
			}
			else
			{
				snsIcon.setVisibility(View.GONE);
				loginCode = 0;
			}
		}
		catch(Exception e) {}
	}
	
	
	private boolean isWifi() 
	{
		return isWifi(this);
    }
	
	static boolean isWifi(Context context) 
	{
		Activity activity = (Activity) context;
		
		ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        boolean isWifiAvail = ni.isAvailable();
        boolean isWifiConn = ni.isConnected();
        
        if (isWifiAvail && isWifiConn) return true;
        else return false;
	}
	
	public void alertNetworkState() 
	{
		String confirmMsg = getResources().getString(R.string.use_paid_network_alert_msg);
		Dialog.confirm(this, confirmMsg, new DialogInterface.OnClickListener() 
		{
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
//				boardTextLayout.setVisibility(View.GONE);
				new Handler().postDelayed(new Runnable() 
				{
					@Override
					public void run() 
					{
						LinearLayout setting = (LinearLayout) findViewById(R.id.layout_menu7);
						setting.performClick();
					}
				}, 300);
			}
		}, "설정으로 이동");
		
	}
	
//	static void alertNetworkState(Context context) 
//	{
//		final Activity activity = (Activity) context;
//		
//		String confirmMsg = context.getResources().getString(R.string.use_paid_network_alert_msg);
//		Dialog.confirm(context, confirmMsg, new DialogInterface.OnClickListener() 
//		{
//			@Override
//			public void onClick(DialogInterface dialog, int which) 
//			{
//				TextView setting = (TextView) activity.findViewById(R.id.setting);
//				setting.performClick();
//			}
//		}, "설정으로 이동");
//	}
	
	public void switchToPauseState() 
	{
		try
		{
			findViewById(R.id.audio_loading).setVisibility(View.INVISIBLE);
			findViewById(R.id.play_btn).setSelected(true);
		} 
		catch(Exception e) 
		{
			// pass
		}
	}
	
	/**
	 * Wi-fi 이거나 유료네트워크 사용 체크 되어 있다면 true 반환, 그렇지 않으면 false
	 * @return
	 */
	public boolean isFreeNetwork() 
	{
		return isFreeNetwork(this);
//		Preferences prefs = new Preferences(this);
//		if (prefs.getUsePaidNetwork() || isWifi()) return true;
//		else return false;
	}
	
	static boolean isFreeNetwork(Context context)
	{
		Preferences prefs = new Preferences(context);
		if (prefs.getUsePaidNetwork() || isWifi(context)) return true;
		else return false;
	}
	
	
	public void moveLayout()
	{
		isRunning = true;
		mHandler = new Handler(callback);
		mThread = new Thread(new Runnable() 
		{
			@Override
			public void run() 
			{
				while(isRunning)
				{
					try 
					{
						Message msg = new Message();
						msg.what = state;
						mHandler.sendMessage(msg);
						Thread.sleep(10);
					}
					catch (InterruptedException e) 
					{
						e.printStackTrace();
					}
				}
			}
		});
		mThread.start();
	}
	
	ArrayList<BandiBoardData> bandiBoardListData = new ArrayList<BandiBoardData>();
	
	LinearLayout[] bandiList;
	TextView[] bandiListWriter;
	TextView[] bandiListDate;
	TextView[] bandiListContents;
	
	private Callback callback = new Callback() 
	{
		@Override
		public boolean handleMessage(Message msg) 
		{
			
			if(msg.what == 0)
			{
				if(isRunning)
				{
					// 접어라~
					nowScrollHeight += accelValue * (targetScrollHeight - nowScrollHeight);
					LinearLayout.LayoutParams scrollParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, nowScrollHeight);
					subScrollMain.setLayoutParams(scrollParams);
					
					nowBottomMenuMargin += accelValue * (targetBottomMenuMargin - nowBottomMenuMargin);
					RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
					params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
					params.topMargin = nowBottomMenuMargin;
					bottomMenu.setLayoutParams(params);
									
					if(Math.abs(accelValue * (targetBottomMenuMargin - nowBottomMenuMargin)) <= 1.0f)
					{
						isRunning = false;
						
						repeatPageIndex = 1;
						tabIndex = -1;
						repeatListViewInit();

						for(int i = 0; i < 4; i++)
						{
							isTabLoading[i] = false;
							notiContent[i].removeAllViews();
						}
						
						RelativeLayout.LayoutParams swapZoneParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, initSwapZoneHeight);
			    		swapZoneParams.addRule(RelativeLayout.ABOVE, R.id.bottom_standard);
			    		swapZoneParams.addRule(RelativeLayout.BELOW, R.id.top);
			    		swapZone.setLayoutParams(swapZoneParams);
						
						LinearLayout.LayoutParams notiContentParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0);
						notiLayout.setLayoutParams(notiContentParams);

						RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
						params1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
						params1.setMargins(0, 0, 0, 0);
						bottomMenu.setLayoutParams(params1);
					}
				}
			}
			else if(msg.what == 1)
			{
				// 하단메뉴 펼쳐라
				// 만일 공지가 펼쳐져 있으면 접어라
				
				if(isRunning)
				{
				
					nowScrollHeight += accelValue * (targetScrollHeight - nowScrollHeight);
					LinearLayout.LayoutParams scrollParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, nowScrollHeight);
					subScrollMain.setLayoutParams(scrollParams);
					
					nowBottomMenuMargin += accelValue * (targetBottomMenuMargin - nowBottomMenuMargin);
					RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
					params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//					params.setMargins(0, 0, 0, nowBottomMenuMargin);
					bottomMenu.setLayoutParams(params);
					
					LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, nowBottomMenuMargin);
					notiLayout.setLayoutParams(lParams);

					if(Math.abs(accelValue * (targetBottomMenuMargin - nowBottomMenuMargin)) <= 1.0f)
					{
						isRunning = false;
						appImg2.setVisibility(View.VISIBLE);
						
						FrameLayout testLayout = (FrameLayout)findViewById(R.id.test);
						int testHeight = testLayout.getHeight();
						videoTempHeight = testLayout.getHeight();
						
						RelativeLayout.LayoutParams swapZoneParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, initSwapZoneHeight);
			    		swapZoneParams.addRule(RelativeLayout.BELOW, R.id.top);
			    		swapZone.setLayoutParams(swapZoneParams);
						
						RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
						params1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
						params1.topMargin = marginStandard.getHeight();
						params1.bottomMargin = 0;
						bottomMenu.setLayoutParams(params1);
						
						rParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, testHeight + getHeightDP(50));
						rParams.addRule(RelativeLayout.BELOW, R.id.top);
						replayImg.setLayoutParams(rParams);
						tempParams = rParams;
						
						rParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, testHeight + getHeightDP(50));
						rParams.addRule(RelativeLayout.BELOW, R.id.top);
						appImg2.setLayoutParams(rParams);
						
						LinearLayout.LayoutParams scrollParams1 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0);
						subScrollMain.setLayoutParams(scrollParams1);
	
						LinearLayout.LayoutParams notiContentParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, nowBottomMenuMargin);
						notiLayout.setLayoutParams(notiContentParams);

						if(tabIndex == 0)
						{
							repeatPageIndex = 1;
							repeatRequest();
						}
						if(tabIndex == 1)
						{
							
							bookInfoRequest();
						}
						else if(tabIndex == 2)
						{
							
							onAirContents();
						}
						else if(tabIndex == 3)
						{
							
							songListRequest();
						}

						isTabLoading[tabIndex] = true;
					}
				
				}
			}
			else if(msg.what == 2)
			{
				if(isRunning)
				{
					nowScrollHeight += accelValue * (targetScrollHeight - nowScrollHeight);
					LinearLayout.LayoutParams scrollParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, nowScrollHeight);
					subScrollMain.setLayoutParams(scrollParams);
	
					if(Math.abs(accelValue * (targetScrollHeight - nowScrollHeight)) <= 1.0f)
					{
						isRunning = false;
						
						if(isFromRadioBtn)
						{
							videoFullBtn.setVisibility(View.VISIBLE);
							videoLayout.setVisibility(View.VISIBLE);
							startVideo();
							
						}
					
						LinearLayout.LayoutParams notiContentParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0);
						notiLayout.setLayoutParams(notiContentParams);
						
						for(int i = 0; i < 4; i++)
						{
							tabMenuBtn[i].setSelected(false);
							isTabLoading[i] = false;
							notiContent[i].removeAllViews();
						}
						
						tabIndex = -1;
						isTabSelected = false;
						boardPageIndex = 0;

						RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
						params1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
						bottomMenu.setLayoutParams(params1);

						FrameLayout testLayout = (FrameLayout)findViewById(R.id.test);
						int testHeight = testLayout.getHeight();
						if(testHeight < 0) testHeight = videoTempHeight;
						rParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, testHeight + getHeightDP(50));
						rParams.addRule(RelativeLayout.BELOW, R.id.top);
						videoLayout.setLayoutParams(rParams);
						tempParams = rParams;
						
						rParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, testHeight + getHeightDP(50));
						rParams.addRule(RelativeLayout.BELOW, R.id.top);
						replayImg.setLayoutParams(rParams);
						
//						videoTempWidth = videoLayout.getWidth();
						if(testLayout.getHeight() > 0) videoTempHeight = testLayout.getHeight();

						params1 = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
						subScroll.setLayoutParams(params1);
						
						String notiUrl = "http://home.ebs.co.kr/bandi/bandiEventNoticeList";
						
						Map<String, Object> notiParams = new HashMap<String, Object>();
						notiParams.put("appDsCd", "02");
						notiParams.put("fileType", "json");
						
						APIWork work = new APIWork(MainActivity.this);
						work.doThreadJob(notiUrl, notiParams, new APIWork.APICallback()
//						AQuery notiAq = new AQuery(MainActivity.this);
//						notiAq.ajax(notiUrl, notiParams, JSONObject.class, new AjaxCallback<JSONObject>()
						{
							@Override
//							public void callback(String url, JSONObject object, AjaxStatus status) 
							public void callback(String responseStr)
							{
								try
								{
									JSONObject object = new JSONObject(responseStr);
									
									
									JSONArray result = object.getJSONArray("bandiEventLists");
									
									for(int i = 0; i < result.length(); i++)
									{
										if(result.getJSONObject(i).getString("evtClsCd").equals("001"))
										{
											EventData data = new EventData();
											data.setEventID(result.getJSONObject(i).getLong("evtId"));
											data.setLinkUrl(result.getJSONObject(i).getString("linkUrl"));
											data.setEventTitle(result.getJSONObject(i).getString("evtTitle"));
											data.setEventCode(result.getJSONObject(i).getString("evtClsCd"));
											data.setEventStartDay(result.getJSONObject(i).getString("evtStartDt"));
											data.setEventEndDay(result.getJSONObject(i).getString("evtEndDt"));
											data.setEventMessage(result.getJSONObject(i).getString("evtCntn"));
											data.setEventUrl(result.getJSONObject(i).getString("mobLinkUrl"));
											data.setEventDeviceCode(result.getJSONObject(i).getString("shwSiteDsCd"));
											data.setImagePathUrl(result.getJSONObject(i).getString("mobThmnlFilePathNm"));
											data.setImagePhsUrl(result.getJSONObject(i).getString("mobThmnlFilePhscNm"));
											bandiApp.setNotiData(data);
											subScrollNoti.setText(result.getJSONObject(i).getString("evtTitle"));
											break;
										}
									}
									
									if(subScrollNoti.getText().equals(""))
									{
										subScrollNoti.setText("공지사항이 없습니다.");
									}
								}
								catch(JSONException e)
								{
									
								}
							}
						});
						
						bandiboardInit();
							
					}
				}
			}
			else if(msg.what == 3)
			{
				if(isRunning)
				{
					nowScrollHeight += accelValue * (targetScrollHeight - nowScrollHeight);
					LinearLayout.LayoutParams scrollParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, nowScrollHeight);
					subScrollMain.setLayoutParams(scrollParams);
	
					if(Math.abs(accelValue * (targetScrollHeight - nowScrollHeight)) <= 1.0f)
					{ 
						isRunning = false;
						boolean isSelected = false;
						for(int i = 0; i < 4; i++)
						{
							isSelected = tabMenuBtn[i].isSelected();
							if(isSelected == true) break;
						}
						
						if(isSelected)
						{
							appImg2.setVisibility(View.VISIBLE);
						}
						else
						{
							tabIndex = -1;
						}
						
						bandiBoardLayout.removeAllViews();
						bandiBoardListData.clear();
						LinearLayout.LayoutParams scroll1Params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0);
						subScrollMain.setLayoutParams(scroll1Params);
						
						RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
						subScroll.setLayoutParams(params1);
					}
				}
			}
			return false;
		}
	};
	
	/**
	 * 다시듣기 초기화 인가?? 뭐지??
	 */
	public void repeatListViewInit()
	{
		for(int i = 0; i < repeatDataList.size(); i++)
		{
			if(playerList.get(i) != null)
			{
				playerList.get(i).getMediaPlayer().pause();
				playerList.get(i).getMediaPlayer().seekTo(0);
				mPlayBtn.get(i).setSelected(false);
				mStremingBar.get(i).setVisibility(View.INVISIBLE);
	    		mRepeatDate.get(i).setVisibility(View.VISIBLE);
			}
		}

//		repeatDataList.clear();
//		playerList.clear();
	}
	
	boolean isLastPage = false;
	public void bandiboardInit()
	{
		if(CProgramData.getInstance().getAppUseBoardYn() == null)
		{
			return;
		}
		
		if(CProgramData.getInstance().getAppUseBoardYn().equals("Y"))
		{
			noBoardTxt.setVisibility(View.INVISIBLE);
			subScroll.setVisibility(View.VISIBLE);
			String url = "http://home.ebs.co.kr/bandi/function/bandiBoardList.json";
			Map<String, Object> params = new HashMap<String, Object>();
	        params.put("programId", CProgramData.getInstance().getProgramID());
	        params.put("listType", "L");
	        params.put("pageSize", "5");
			AQuery aq = new AQuery(MainActivity.this);
			aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
			{
				@Override
				public void callback(String url, JSONObject object, AjaxStatus status) 
				{
					bandiBoardListData.clear();
					bandiBoardLayout.removeAllViews();
					try 
					{
						JSONArray jArray = object.getJSONArray("bandiPosts");
						for(int i = 0; i < jArray.length(); i++)
						{
							BandiBoardData data = new BandiBoardData();
							data.setWriter(jArray.getJSONObject(i).getString("writer"));
							data.setRegDate(jArray.getJSONObject(i).getString("regdate"));
							data.setContents(jArray.getJSONObject(i).getString("contents"));
							data.setAdminYN(jArray.getJSONObject(i).getString("adminYn"));
							data.setSEQ(jArray.getJSONObject(i).getLong("seq"));
							data.setSNSUserName(jArray.getJSONObject(i).getString("snsUserNm"));
							data.setSNSUseYN(jArray.getJSONObject(i).getString("snsUseYn"));
							bandiBoardListData.add(data);
						}
						
						bandiList = new LinearLayout[jArray.length()];
						bandiListWriter = new TextView[jArray.length()];
						bandiListDate = new TextView[jArray.length()];
						bandiListContents = new TextView[jArray.length()];
						
						for(int i = 0; i < jArray.length(); i++)
						{
							View view = null;
							if(bandiBoardListData.get(i).getAdminYN().equals("Y"))
							{
								view = inflater.inflate(R.layout.bandi_board_manager_list_item, null);
								RelativeLayout main = (RelativeLayout)view.findViewById(R.id.layout_managerMain);
								main.setPadding(getHeightDP(5), getHeightDP(5), getHeightDP(5), getHeightDP(5));
								bandiListContents[i] = (TextView)view.findViewById(R.id.textView_managerMsg);
								String leftReplace = bandiBoardListData.get(i).getContents().replace("&lt;", "<");
								String rightReplce = leftReplace.replace("&gt;", ">");
								bandiListContents[i].setText(rightReplce);
//								bandiListContents[i].setText(bandiBoardListData.get(i).getContents());
								bandiListContents[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
								rParams = new RelativeLayout.LayoutParams(getHeightDP(51), getHeightDP(51));
								rParams.addRule(RelativeLayout.CENTER_VERTICAL);
								rParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
								ImageView icon = (ImageView)view.findViewById(R.id.imageView_managerIcon);
								icon.setLayoutParams(rParams);
							}
							else
							{
								view = inflater.inflate(R.layout.bandi_board_list_item, null);
								
								bandiList[i] = (LinearLayout)view.findViewById(R.id.boardListMain);
								bandiList[i].setPadding(getHeightDP(5), getHeightDP(5), getHeightDP(5), getHeightDP(5));
								if(i % 2 == 0)
								{
									bandiList[i].setBackgroundColor(Color.parseColor("#fcf8ee"));
								}
								else
								{
									bandiList[i].setBackgroundColor(Color.parseColor("#ffffff"));
								}
								bandiListWriter[i] = (TextView)view.findViewById(R.id.boardListName);
								bandiListWriter[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
								bandiListDate[i] = (TextView)view.findViewById(R.id.boardListDate);
								bandiListContents[i] = (TextView)view.findViewById(R.id.boardListMsg);
								
								if(bandiBoardListData.get(i).getSNSUseYn().equals("N"))
								{
									bandiListWriter[i].setText(bandiBoardListData.get(i).getWriter());
								}
								else if(bandiBoardListData.get(i).getSNSUseYn().equals("Y"))
								{
									bandiListWriter[i].setText(bandiBoardListData.get(i).getSNSUserName());
								}
								
								lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
								lParams.setMargins(getWidthDP(5), 0, 0, 0);
								bandiListDate[i].setText(bandiBoardListData.get(i).getRegDate());
								bandiListDate[i].setLayoutParams(lParams);
								bandiListDate[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
								
								
								lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
								lParams.setMargins(0, getWidthDP(5), 0, 0);
								bandiListContents[i].setLayoutParams(lParams);
								bandiListContents[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
								if(bandiBoardListData.get(i).getContents().equals("null") == false)
								{
									String leftReplace = bandiBoardListData.get(i).getContents().replace("&lt;", "<");
									String rightReplce = leftReplace.replace("&gt;", ">");
									bandiListContents[i].setText(rightReplce);
//									bandiListContents[i].setText(bandiBoardListData.get(i).getContents());	
								}
							}
							bandiBoardLayout.addView(view);
							
						}
						
						if(jArray.length() == 5)
						{
							isLastPage = false;
						}
						else
						{
							isLastPage = true;
						}
					}
					catch (JSONException e) { e.printStackTrace(); }
					
				};
				
			});
		}
		else if(CProgramData.getInstance().getAppUseBoardYn().equals("N"))
		{
			RelativeLayout.LayoutParams noBoardTxtParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			noBoardTxtParams.addRule(RelativeLayout.CENTER_IN_PARENT);
			noBoardTxt.setVisibility(View.VISIBLE);
			noBoardTxt.setLayoutParams(noBoardTxtParams);
			subScroll.setVisibility(View.INVISIBLE);
		}
	}
	
	int repeatPageIndex = 1;
	boolean isRepeatLoadingFinish = false;
	ArrayList<RepeatData> repeatDataList = new ArrayList<RepeatData>();
	ArrayList<StreamingMediaPlayer> playerList = new ArrayList<StreamingMediaPlayer>();
	ArrayList<View> mView = new ArrayList<View>();
	ArrayList<TextView> mTitle = new ArrayList<TextView>();
	ArrayList<FrameLayout> mPlayBtn = new ArrayList<FrameLayout>();
	ArrayList<SeekBar> mStremingBar = new ArrayList<SeekBar>();
	ArrayList<TextView> mRepeatDate = new ArrayList<TextView>();
	String prevId;
	
	public void repeatRequest()
	{
		
		if(bandiApp.getIsFMRadio())
		{
			BandiLog.event(MainActivity.this, "Aod");
		}
		else
		{
			BandiLog.event(MainActivity.this, "AodIradio");
		}
		
//		String url = "http://home.ebs.co.kr/bandi/replayList";
//		Map<String, Object> params = new HashMap<String, Object>();
//		params.put("programId", CProgramData.getInstance().getProgramID());
//		params.put("pageVodFig", 10);
//		params.put("page", repeatPageIndex);
//		params.put("fileType", "json");
//		AQuery aq = new AQuery(this);
		
		prevId = CProgramData.getInstance().getProgramID();
		APIWork work = new APIWork(this);
		work.doThreadJob(Url.AOD_LIST_JSON(CProgramData.getInstance().getProgramID(), repeatPageIndex), null, new APIWork.APICallback() 
		
//		aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
		{
			@Override
//			public void callback(String url, JSONObject object, AjaxStatus status)
			public void callback(String responseStr)
			{
				try
				{
					JSONObject object = new JSONObject(responseStr);
					
					JSONArray jArray = object.getJSONObject("vods").getJSONArray("content");
					if(jArray.length() > 0)
					{
						isRepeatLoadingFinish = true;
						LinearLayout.LayoutParams notiContentParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
						notiLayout.setLayoutParams(notiContentParams);
						if(repeatPageIndex == 1)
						{
							notiContent[0].removeAllViews();
							repeatDataList.clear();
							playerList.clear();
						}
						
						for(int i = 0; i < jArray.length(); i++)
						{
							RepeatData data = new RepeatData();
							data.setLectId(jArray.getJSONObject(i).getString("lectId"));
							data.setLectNm(jArray.getJSONObject(i).getString("lectNm"));
							data.setIsCharged(jArray.getJSONObject(i).getString("vodChrgClsCd").equals("A") == true ? false : true);
							data.setRepeatDate(jArray.getJSONObject(i).getLong("brdcDt"));
							repeatDataList.add(data);
							StreamingMediaPlayer player = null;
							playerList.add(player);
							
							View view = null;
							TextView title = null;
							FrameLayout playBtn = null;
							SeekBar streamingBar = null;
							TextView date = null;
							
							mView.add(view);
							mTitle.add(title);
							mPlayBtn.add(playBtn);
							mStremingBar.add(streamingBar);
							mRepeatDate.add(date);
						}

						for(int i = ((repeatPageIndex - 1) * jArray.length()); i < repeatDataList.size(); i++)
						{
							mView.set(i, inflater.inflate(R.layout.layout_repeat_list_item, null));
							fParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, getWidthDP(55));
							RelativeLayout main = (RelativeLayout)mView.get(i).findViewById(R.id.layout_repeatList_main);
							main.setLayoutParams(fParams);
							
							rParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
							rParams.addRule(RelativeLayout.CENTER_VERTICAL);
							rParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
							rParams.setMargins(0, 0, getWidthDP(10), 0);
							FrameLayout _controlLayout = (FrameLayout)mView.get(i).findViewById(R.id.layout_repeatList_control);
							_controlLayout.setLayoutParams(rParams);
							
							FrameLayout barLayout = (FrameLayout)mView.get(i).findViewById(R.id.layout_repeatListBar);
							barLayout.setPadding(getWidthDP(10), 0, 0, 0);
							
							lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
							lParams.setMargins(getWidthDP(10), 0, getWidthDP(10), getWidthDP(5));
							mTitle.set(i, (TextView)mView.get(i).findViewById(R.id.textView_repeatList_title));
							mTitle.get(i).setLayoutParams(lParams);
							mTitle.get(i).setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
							mTitle.get(i).setText(repeatDataList.get(i).getLectNm());
							mRepeatDate.set(i, (TextView)mView.get(i).findViewById(R.id.textView_repeatDate));
							mRepeatDate.get(i).setText(repeatDataList.get(i).getRepeatDate());
							mRepeatDate.get(i).setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
							mStremingBar.set(i, (SeekBar)mView.get(i).findViewById(R.id.repeat_streaming_bar));
							mStremingBar.get(i).setVisibility(View.INVISIBLE);
							
							fParams = new FrameLayout.LayoutParams(getHeightDP(34), getHeightDP(34));
							mPlayBtn.set(i, (FrameLayout)mView.get(i).findViewById(R.id.checkBox_repeatList_playBtn));
							mPlayBtn.get(i).setLayoutParams(fParams);
							mPlayBtn.get(i).setTag(i);
							mPlayBtn.get(i).setOnClickListener(new OnClickListener() 
							{
								@Override
								public void onClick(View v) 
								{
									final int index = Integer.parseInt(v.getTag().toString());
									if(prevId.equals(CProgramData.getInstance().getProgramID()) == false)
									{
										return;
									}
									
									if(mPlayBtn.get(index).isSelected())
									{
										playerList.get(index).getMediaPlayer().pause();
										mPlayBtn.get(index).setSelected(false);
										mStremingBar.get(index).setVisibility(View.INVISIBLE);
							    		mRepeatDate.get(index).setVisibility(View.VISIBLE);
									}
									else
									{
										if(repeatDataList.get(index).getIsCharged())
										{
											if(isNeededToLogin())
											{
												mPlayBtn.get(index).setSelected(false);
												LoginFormDialog loginForm = LoginFormDialog.getInstance(MainActivity.this);
												loginForm.setOnLoginSuccessCb(new LoginFormDialog.OnLoginSuccessCb() 
												{
													@Override
													public void onLoginSuccess() 
													{
														snsIconCheck();
														
														// 로그인 과정중에 처리하므로 제거.
//														if(bandiApp.getFavoriteList() == null || bandiApp.getFavoriteList().size() <= 0)
//										                {
//															String url = "http://home.ebs.co.kr/bandi/bandiAppUserFbmkList";
//													    	Map<String, Object> params = new HashMap<String, Object>();
//													    	params.put("userId", bandiApp.getUserId());
//													    	params.put("fileType", "json");
//													    	AQuery aq = new AQuery(MainActivity.this);
//													    	aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
//															{
//													    		@Override
//													    		public void callback(String url, JSONObject object, AjaxStatus status) 
//													    		{
//													    			try 
//													    			{
//													    				Log.d("TEST", "로그인 성공, 즐겨찾기 리스트가 없어서 다시 받아온 결과 == " + object);
//													    				favoriteData.clear();
//													    				JSONObject result = object.getJSONObject("appUserFbmkLists");
//													    				JSONArray jArray = result.getJSONArray("appUserFbmkLists");
//
//																		for(int i = 0; i < jArray.length(); i++)
//																		{
//																			FavoriteData data = new FavoriteData();
//																			data.setFbmkSno(jArray.getJSONObject(i).getInt("fbmkSno"));
//																			data.setProgramID(jArray.getJSONObject(i).getString("courseId"));
//																			data.setTitle(jArray.getJSONObject(i).getString("bandiTitle"));
//																			data.setLinkUrl(jArray.getJSONObject(i).getString("bandiMobUrl"));
//																			favoriteData.add(data);
//																		}
////																		
//																		bandiApp.setFavoriteList(favoriteData);
//																		
//																		for(int i = 0; i < bandiApp.getFavoriteList().size(); i++)
//													                	{
//													                		if(bandiApp.getFavoriteList().get(i).getProgramID().equals(CProgramData.getInstance().getProgramID()))
//													                		{
//													                			String fbmkSno = Integer.toString(bandiApp.getFavoriteList().get(i).getFbmkSno());
//													                			CProgramData.getInstance().setNowFavoriteId(fbmkSno);
//													                			favorite.setSelected(true);
//													                		}
//													                	}
//																		
//																		
//													    			}
//													    			catch(Exception e) {}
//													    		}
//															});
//										                }
//														else
														{
															for(int i = 0; i < bandiApp.getFavoriteList().size(); i++)
										                	{
										                		if(bandiApp.getFavoriteList().get(i).getProgramID().equals(CProgramData.getInstance().getProgramID()))
										                		{
										                			String fbmkSno = Integer.toString(bandiApp.getFavoriteList().get(i).getFbmkSno());
										                			CProgramData.getInstance().setNowFavoriteId(fbmkSno);
										                			favorite.setSelected(true);
										                		}
										                	}
														}
													}
												});
												loginForm.setOnLoginFailureCb(new LoginFormDialog.OnLoginFailureCb() 
												{
													@Override
													public void onLoginFailure() 
													{
														snsIconCheck();
													}
												});
												loginForm.showDialog(true);
											}
											else
											{
												setAodStreaming(repeatDataList.get(index).getLectId(), new APIWork.APICallback()
//												repeatStreamingSetting(new AjaxCallback<JSONObject>()
												{
													@Override
													public void callback(String responseStr)
//													public void callback(String url, JSONObject object, AjaxStatus status)
													{
														JSONObject object = null;
														try {
															object = new JSONObject(responseStr);
														} catch (JSONException e1) {
															Log.d(TAG, "make json object error - " + e1.getMessage());
															e1.printStackTrace();
														}
														
														if(object == null)
														{
															mPlayBtn.get(index).setSelected(false);
														}
														else
														{
															try
															{
																JSONObject result = object.getJSONObject("resultXml");
																Log.d(TAG, "" + result);
																if(result.getString("vodSteamingUrl").equals(""))
																{
																	
																	mPlayBtn.get(index).setSelected(false);
																	String mbUrl = "http://home.ebs.co.kr/bandi/function/bandiMobileUrl2.json";
																	Map<String, Object> params = new HashMap<String, Object>();
																	params.put("programId", CProgramData.getInstance().getProgramID());
																	params.put("appDsCd", "02");
																	
																	AQuery aq = new AQuery(MainActivity.this);
																	aq.ajax(mbUrl, params, Xml.class, new AjaxCallback<Xml>()
																	{
																		
																		@Override
																		public void callback(String url, Xml object, AjaxStatus status)
																		{
																			super.callback(url, object, status);
																		}
																		
																	});
																	
																	Toast.makeText(MainActivity.this, result.getString("resultMsg"), Toast.LENGTH_LONG).show();
																}
																else
																{
																	String streamingUrl = result.getString("vodSteamingUrl");
																	try 
															    	{ 
																		for(int i = 0; i < repeatDataList.size(); i++)
																		{
																			if(i == index)
																			{
																				video.stopPlayback();
																				videoLayout.setVisibility(View.INVISIBLE);
																				videoPlayBtn.setSelected(false);
																				
																				if(playerList.get(index) == null) playerList.set(index, new StreamingMediaPlayer(mPlayBtn.get(index), mStremingBar.get(index), streamingUrl.replace("httpx", "http")));
	//																	    		playerList.get(index).startStreaming(streamingUrl.replace("httpx", "http"), 5208, 216);
																				playerList.get(index).startPlayer();
																	    		replayImg.setVisibility(View.VISIBLE);
																	    		videoPlayBtn.setVisibility(View.INVISIBLE);
																	    		viewRadioCheck.setSelected(false);
																	    		videoFullBtn.setVisibility(View.INVISIBLE);
																	    		controlLayout.setVisibility(View.VISIBLE);
																	    		if(serviceConnection != null && serviceConnection.getAudioService() != null) 
																				{
																					serviceConnection.getAudioService().stopPlayer(playBtn, loading);
																				}
																	    		mStremingBar.get(index).setVisibility(View.VISIBLE);
																	    		mRepeatDate.get(index).setVisibility(View.INVISIBLE);
																			}
																			else
																			{
																				if(playerList.get(i) != null)
																				{
																					playerList.get(i).getMediaPlayer().pause();
																					playerList.get(i).getMediaPlayer().seekTo(0);
																					mPlayBtn.get(i).setSelected(false);
																					mStremingBar.get(i).setVisibility(View.INVISIBLE);
																		    		mRepeatDate.get(i).setVisibility(View.VISIBLE);
																				}
																			}
																		}
															    		
															    	}
																	catch (Exception e) {} 
																}
															}
															catch (JSONException e)
															{
																e.printStackTrace();
															}
														}
													};
												});
											}
										}
										else
										{
											
											// 무료
											setAodStreaming(repeatDataList.get(index).getLectId(), new APIWork.APICallback() 
											
											//repeatStreamingSetting(new AjaxCallback<JSONObject>()
											{
												@Override
												//public void callback(String url, JSONObject object, AjaxStatus status)
												public void callback(String responseStr)
												{
													try
													{
														JSONObject object = new JSONObject(responseStr);
														
														if(serviceConnection != null && serviceConnection.getAudioService() != null) 
														{
															serviceConnection.getAudioService().stopPlayer(playBtn, loading);
														}
														video.stopPlayback();
														JSONObject result = object.getJSONObject("resultXml");
														String streamingUrl = result.getString("vodSteamingUrl");
														try 
												    	{ 
															for(int i = 0; i < repeatDataList.size(); i++)
															{
																if(i == index)
																{
																	
																	video.stopPlayback();
																	videoLayout.setVisibility(View.INVISIBLE);
																	videoPlayBtn.setSelected(false);
																	if(playerList.get(index) == null) playerList.set(index, new StreamingMediaPlayer(mPlayBtn.get(index), mStremingBar.get(index), streamingUrl.replace("httpx", "http")));
																	playerList.get(index).startPlayer();
																	if(serviceConnection != null && serviceConnection.getAudioService() != null) 
																	{
																		serviceConnection.getAudioService().stopPlayer(playBtn, loading);
																	}
														    		replayImg.setVisibility(View.VISIBLE);
														    		videoPlayBtn.setVisibility(View.INVISIBLE);
														    		viewRadioCheck.setSelected(false);
														    		videoFullBtn.setVisibility(View.INVISIBLE);
														    		controlLayout.setVisibility(View.VISIBLE);
														    		mStremingBar.get(index).setVisibility(View.VISIBLE);
														    		mRepeatDate.get(index).setVisibility(View.INVISIBLE);
														    		
																}
																else
																{
																	if(playerList.get(i) != null)
																	{
																		playerList.get(i).getMediaPlayer().pause();
																		playerList.get(i).getMediaPlayer().seekTo(0);
																		mPlayBtn.get(i).setSelected(false);
																		mStremingBar.get(i).setVisibility(View.INVISIBLE);
															    		mRepeatDate.get(i).setVisibility(View.VISIBLE);
//															    		playerList.set(i, null);
																	}
																}
															}
												    	}
														catch (Exception e) {}  
													}
													catch(JSONException e){};
												};
											} /* *** , repeatDataList.get(index).getLectId() *** */ );
										}
									}
								}
							});

							notiContent[0].addView(mView.get(i));
						}
						
						new Handler().post(new Runnable() 
						{
							
							@Override
							public void run() 
							{
								if(notiContent[0].getHeight() <= nowBottomMenuMargin)
								{
									LinearLayout.LayoutParams notiContentParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, nowBottomMenuMargin);
									notiLayout.setLayoutParams(notiContentParams);
								}
								else
								{
									LinearLayout.LayoutParams notiContentParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
									notiLayout.setLayoutParams(notiContentParams);}
								}
							}
						);
					}
					else
					{
						View view = inflater.inflate(R.layout.layout_main_nodata, null);
						FrameLayout.LayoutParams noDataLayoutParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, nowBottomMenuMargin);
						FrameLayout noDataMain = (FrameLayout)view.findViewById(R.id.layout_noDataMain);
						noDataMain.setLayoutParams(noDataLayoutParams);
						TextView noData = (TextView)view.findViewById(R.id.textView_noData);
						noData.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
						noData.setText("다시듣기 내용이 없습니다.");
						notiContent[0].addView(view);
					}
				}
				catch(JSONException e) {}
			}
		});
	}
	
	/**
	 * AOD 의 스트리밍 정보를 가져온 후, ui thread 에서 callback 호출
	 * @param lectId - AOD 개별 아이디
	 * @param apiCb - 스트리밍 정보를 받아온 후 UI에서 처리할 작업
	 */
	private void setAodStreaming(String lectId, APIWork.APICallback apiCb)
	{
		APIWork work = new APIWork(this);
//		work.doThreadJob("http://to302.phps.kr/test/vodSteamInfo.json",null, apiCb);
		work.doThreadJob(Url.AOD_INFO_JSON(bandiApp.getUserId(), CProgramData.getInstance().getProgramID(), lectId), null, apiCb);
	}
	

	/**
	 * @deprecated setAodStreadming 으로 대체
	 * 
	 * @param callback
	 * @param lectId
	 */
	public void repeatStreamingSetting(AjaxCallback<JSONObject> callback, String lectId)
	{
		String url = "http://home.ebs.co.kr/bandi/vodSteamInfo";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", bandiApp.getUserId());
		params.put("programId", CProgramData.getInstance().getProgramID());
		params.put("lectId", lectId);
		params.put("fileType", "json");
		AQuery aq = new AQuery(MainActivity.this);
		aq.ajax(url, params, JSONObject.class, callback);
	}
	
	int bookSelectIndex;
	ArrayList<BookInfoData> bookInfoData = new ArrayList<BookInfoData>();
	public void bookInfoRequest()
	{
//		BandiLog.bookInfoHit(this, CProgramData.getInstance().getProgramID());
		if(bandiApp.getIsFMRadio())
		{
			BandiLog.event(MainActivity.this, "BookInfo");
		}
		else
		{
			BandiLog.event(MainActivity.this, "BookInfoIradio");
		}
		
		String url = "http://home.ebs.co.kr/bandi/bandiBookList";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("programId", CProgramData.getInstance().getProgramID());
		params.put("fileType", "json");
		AQuery aq = new AQuery(this);
		APIWork apiWork = new APIWork(this);
		apiWork.doThreadJob(url, params,
//		aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
		new APIWork.APICallback()
		{
			@Override
//			public void callback(String url, JSONObject object, AjaxStatus status)
			public void callback(String responseStr)
			{
				try
				{
					JSONObject object = new JSONObject(responseStr);
					if(object.getJSONObject("resultXml").getString("resultCd").equals("00"))
					{
						LinearLayout.LayoutParams notiContentParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
						notiLayout.setLayoutParams(notiContentParams);
						notiContent[1].removeAllViews();
						bookInfoData.clear();
						JSONArray jArray = object.getJSONObject("resultXml").getJSONArray("appBookInfos");
						for(int i = 0; i < jArray.length(); i++)
						{
							BookInfoData data = new BookInfoData();
							data.setBookID(jArray.getJSONObject(i).getInt("bookId"));
							data.setBookName(jArray.getJSONObject(i).getString("bookNm"));
							data.setBookDescription(jArray.getJSONObject(i).getString("bookCntn"));
							data.setLinkUrl(jArray.getJSONObject(i).getString("mobLink"));
							data.setStartDay(jArray.getJSONObject(i).getString("startDtm"));
							data.setEndDay(jArray.getJSONObject(i).getString("endDtm"));
							data.setRegisterDay(jArray.getJSONObject(i).getString("crtnDtm"));
							data.setImageUrl(jArray.getJSONObject(i).getString("mobImgUrl"));
							bookInfoData.add(data);
						}
						bookSelectIndex = 0;
						View bookInfoDetailView = inflater.inflate(R.layout.bookinfo_list_detail, null);
						lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, getWidthDP(140));
						LinearLayout bookInfoDetailMain = (LinearLayout)bookInfoDetailView.findViewById(R.id.layout_bookInfoDetailMain);
						bookInfoDetailMain.setLayoutParams(lParams);
						
						lParams = new LinearLayout.LayoutParams(getWidthDP(100), LayoutParams.MATCH_PARENT);
						final ImageView bookInfoDetailImg = (ImageView)bookInfoDetailView.findViewById(R.id.imageView_bookInfoDetailImg);
						bookInfoDetailImg.setLayoutParams(lParams);
						
						lParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, getWidthDP(35));
						FrameLayout bookInfoDetailTitleLayout = (FrameLayout)bookInfoDetailView.findViewById(R.id.layout_bookInfoDetailTitle);
						bookInfoDetailTitleLayout.setLayoutParams(lParams);
						
						FrameLayout bookInfoDetailContentsLayout = (FrameLayout)bookInfoDetailView.findViewById(R.id.layout_bookInfoDetailContents);
						bookInfoDetailContentsLayout.setPadding(getWidthDP(10), getWidthDP(5), getWidthDP(10), getWidthDP(5));
						
						AQuery aq = new AQuery(MainActivity.this);
						aq.id(bookInfoDetailImg).image(bookInfoData.get(0).getImageUrl());
						bookInfoDetailImg.setOnClickListener(new OnClickListener() 
						{
							
							@Override
							public void onClick(View v) 
							{
								
								if(bookInfoData.get(bookSelectIndex).getLinkUrl() == null || bookInfoData.get(bookSelectIndex).getLinkUrl().equals("null") || bookInfoData.get(bookSelectIndex).getLinkUrl().equals(""))
								{
									return;
								}
								BandiLog.bookInfoHit(MainActivity.this, CProgramData.getInstance().getProgramID(), bookInfoData.get(bookSelectIndex).getBookID());
								Uri uri = Uri.parse(bookInfoData.get(bookSelectIndex).getLinkUrl());
								Intent it  = new Intent(Intent.ACTION_VIEW, uri);
								startActivity(it);
								
							}
						});
						
						fParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, Gravity.CENTER_VERTICAL);
						fParams.setMargins(getWidthDP(10), 0, getWidthDP(10), 0);
						final TextView bookInfoDetailTitle = (TextView)bookInfoDetailView.findViewById(R.id.textView_bookInfoDetailTitle);
						bookInfoDetailTitle.setLayoutParams(fParams);
						bookInfoDetailTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(17));
						bookInfoDetailTitle.setText(bookInfoData.get(0).getBookName());
						
						final TextView bookInfoDetailContent = (TextView)bookInfoDetailView.findViewById(R.id.textView_bookInfoDetailContent);
						bookInfoDetailContent.setText(bookInfoData.get(0).getBookDescription());
						bookInfoDetailContent.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(15));
						
						notiContent[1].addView(bookInfoDetailView);
						
						View[] bookInfoList = new View[bookInfoData.size()];
						LinearLayout[] bookInfoListMain = new LinearLayout[bookInfoData.size()];
						FrameLayout[] bookInfoListTitleLayout = new FrameLayout[bookInfoData.size()];
						TextView[] bookInfoListTitle = new TextView[bookInfoData.size()];
						TextView[] bookInfoListDate = new TextView[bookInfoData.size()];
						for(int i = 0; i < bookInfoData.size(); i++)
						{
							bookInfoList[i] = inflater.inflate(R.layout.bookinfo_list, null);
							
							fParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, getWidthDP(45));
							bookInfoListMain[i] = (LinearLayout)bookInfoList[i].findViewById(R.id.layout_bookList);
							bookInfoListMain[i].setLayoutParams(fParams);
							bookInfoListMain[i].setTag(i);
							
							lParams = new LinearLayout.LayoutParams(getWidthDP(253), LayoutParams.MATCH_PARENT);
							bookInfoListTitleLayout[i] = (FrameLayout)bookInfoList[i].findViewById(R.id.layout_bookListTitle);
							bookInfoListTitleLayout[i].setLayoutParams(lParams);
							bookInfoListTitleLayout[i].setPadding(getWidthDP(10), 0, getWidthDP(10), 0);
							
							if(i % 2 == 0) bookInfoListMain[i].setBackgroundColor(Color.parseColor("#fcf8ee"));
							else bookInfoListMain[i].setBackgroundColor(Color.WHITE);
							
							bookInfoListMain[i].setOnClickListener(new OnClickListener()
							{
								
								@Override
								public void onClick(View v)
								{
									int index = Integer.parseInt(v.getTag().toString());
									AQuery aq = new AQuery(MainActivity.this);
									aq.id(bookInfoDetailImg).image(bookInfoData.get(index).getImageUrl());
									bookSelectIndex = index;
									bookInfoDetailTitle.setText(bookInfoData.get(index).getBookName());
									bookInfoDetailContent.setText(bookInfoData.get(index).getBookDescription());
								}
							});
							
							bookInfoListTitle[i] = (TextView)bookInfoList[i].findViewById(R.id.textView_bookListTitle);
							bookInfoListTitle[i].setText(bookInfoData.get(i).getBookName());
							bookInfoListTitle[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
							
							bookInfoListDate[i] = (TextView)bookInfoList[i].findViewById(R.id.textView_bookListDate);
							bookInfoListDate[i].setText(bookInfoData.get(i).getRegisterDay());
							bookInfoListDate[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
							
							notiContent[1].addView(bookInfoList[i]);
						}
					}
					else
					{
						View view = inflater.inflate(R.layout.layout_main_nodata, null);
						FrameLayout.LayoutParams noDataLayoutParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, nowBottomMenuMargin);
						FrameLayout noDataMain = (FrameLayout)view.findViewById(R.id.layout_noDataMain);
						noDataMain.setLayoutParams(noDataLayoutParams);
						TextView noData = (TextView)view.findViewById(R.id.textView_noData);
						noData.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
						noData.setText("도서정보 내용이 없습니다.");
						notiContent[1].addView(view);
					}
					
				} 
				catch (JSONException e)
				{
					e.printStackTrace();
				}
//				super.callback(url, object, status);
			}
			
		});
	}
	
	View[] view;

	public void songListRequest()
	{
		
		if(bandiApp.getIsFMRadio())
		{
			BandiLog.event(MainActivity.this, "SongTable");
		}
		else
		{
			BandiLog.event(MainActivity.this, "SongTableIradio");
		}
		
		LinearLayout.LayoutParams notiContentParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, nowBottomMenuMargin);
		notiLayout.setLayoutParams(notiContentParams);
		
		
//		AQuery aq = new AQuery(MainActivity.this);
		String url = "http://home.ebs.co.kr/bandi/bandiSongList2";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("fileType", "json");
		params.put("progcd", CProgramData.getInstance().getProgramID());
		params.put("regdate", DateUtil.getAdjustedDateString());
		
		APIWork apiWork = new APIWork(MainActivity.this);
		apiWork.doThreadJob(url, params,
//		aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
		new APIWork.APICallback()
		{
			@Override
//			public void callback(String url, JSONObject object, AjaxStatus status) 
			public void callback(String responseStr)
			{
				try
				{	
					JSONObject object = new JSONObject(responseStr);
					
					JSONObject result = object.getJSONObject("bandi");
					if(result.getString("rescd").equals("0000"))
					{
						LinearLayout.LayoutParams notiContentParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
						notiLayout.setLayoutParams(notiContentParams);
						
						notiContent[3].removeAllViews();

						View view = inflater.inflate(R.layout.layout_main_textlist_item, null);
						TextView contents = (TextView)view.findViewById(R.id.textView_textList);
						contents.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
						contents.setText(Html.fromHtml(result.getString("bandi-contents")));
						Log.d(TAG, "song-list : " + result.getString("bandi-contents"));
						
						notiContent[3].addView(view);
						
						new Handler().post(new Runnable() 
						{
							
							@Override
							public void run() 
							{
								if(notiContent[3].getHeight() <= nowBottomMenuMargin)
								{
									LinearLayout.LayoutParams notiContentParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, nowBottomMenuMargin);
									notiLayout.setLayoutParams(notiContentParams);
								}
							}
						});
					}
					else
					{
						notiContent[3].removeAllViews();
						View view = inflater.inflate(R.layout.layout_main_nodata, null);
						FrameLayout.LayoutParams noDataLayoutParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, nowBottomMenuMargin);
						FrameLayout noDataMain = (FrameLayout)view.findViewById(R.id.layout_noDataMain);
						noDataMain.setLayoutParams(noDataLayoutParams);
						TextView noData = (TextView)view.findViewById(R.id.textView_noData);
						noData.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
						noData.setText("선곡표 내용이 없습니다.");
						notiContent[3].addView(view);
					}
				}
				catch (JSONException e) {}
			};
		});
	}

	public void onAirContents()
	{
		
		if(bandiApp.getIsFMRadio())
		{
			BandiLog.event(MainActivity.this, "AirContents");
		}
		else
		{
			BandiLog.event(MainActivity.this, "AirContentsIradio");
		}
		
		LinearLayout.LayoutParams notiContentParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, nowBottomMenuMargin);
		notiLayout.setLayoutParams(notiContentParams);
		
		notiContent[2].removeAllViews();
		String airContents = CProgramData.getInstance().getContents();
		
		if(airContents == null || airContents.equals("null") || airContents.equals(""))
		{
			View view = inflater.inflate(R.layout.layout_main_nodata, null);
			FrameLayout.LayoutParams noDataLayoutParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, nowBottomMenuMargin);
			FrameLayout noDataMain = (FrameLayout)view.findViewById(R.id.layout_noDataMain);
			noDataMain.setLayoutParams(noDataLayoutParams);
			TextView noData = (TextView)view.findViewById(R.id.textView_noData);
			noData.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
			noData.setText("방송내용이 없습니다.");
			notiContent[2].addView(view);
		}
		else
		{
			LinearLayout.LayoutParams notiContentParams1 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			notiLayout.setLayoutParams(notiContentParams1);
			notiContent[2].removeAllViews();
			final View view = inflater.inflate(R.layout.layout_main_textlist_item, null);
			TextView contents = (TextView)view.findViewById(R.id.textView_textList);
			contents.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
//			contents.setText(CProgramData.getInstance().getContents());
			
			String airContent = CProgramData.getInstance().getContents();
			
//			String airContent = "<p>오늘의 방송</p><p><br></p><p>1. 원</p><p>2. 투</p><p>3. 스리</p><p>4. 포</p><p><br></p>";
//			Log.d(TAG, "content - " + airContent);
//			Log.d(TAG, "content2 - " + Html.fromHtml(airContent).toString());
			
			if (airContent != null && airContent.equals(Html.fromHtml(airContent).toString())) {
				contents.setText(airContent);
				Log.d(TAG, "plain text");
			} else {
				contents.setText(Html.fromHtml(airContent));
				Log.d(TAG, "html text");
			}
			
			notiContent[2].addView(view);
			
			new Handler().post(new Runnable()
			{
				
				@Override
				public void run() 
				{
					if(notiContent[2].getHeight() <= nowBottomMenuMargin)
					{
						LinearLayout.LayoutParams notiContentParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, nowBottomMenuMargin);
						notiLayout.setLayoutParams(notiContentParams);
					}
				}
			});
		}
	}

	public interface uiSliderListener
	{
		public void uiSliderClose();
	}
	
	Intent mData;
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode, resultCode, data);
		mData = data;

		Session session = Session.getActiveSession();
		if(session != null) Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
		if(requestCode == LGUP_REQUEST_CODE)
		{
			if(resultCode == RESULT_OK)
			{
				
				if(prefs.getTutorialState() == false)
		        {
					TutorialDialog tDialog = TutorialDialog.getInstance(this);
		        	tDialog.show();
		        }
			}
		}
		
		if(requestCode == 1001)
		{
			Log.d(TAG, "onActivityResult() is called with requestCode 1001.");
			Uri uri = Uri.parse(bandiApp.getStreamUrlAndroidVod()); 
	        video.setVideoURI(uri);
	        video.start();
		}
		
		if(requestCode == 3000)
		{
			if(resultCode == RESULT_OK)
			{
				
				LoginFormDialog dialog = LoginFormDialog.getInstance(MainActivity.this);
				dialog.dismiss();
				
				new Thread(new Runnable() 
				{
					@Override
					public void run()
					{
						try
						{
							String verifier = mData.getExtras().getString(getString(R.string.twitter_oauth_verifier));
							AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
	
							long userID = accessToken.getUserId();
							User user = twitter.showUser(userID);
							bandiApp.setTwitterId(user.getId());
							bandiApp.setTwitterName(user.getName());
							bandiApp.setTwitterAccessToken(accessToken.getToken());
							bandiApp.setTwitterAccessTokenSecret(accessToken.getTokenSecret());
							
							Session session = Session.getActiveSession();
			                if(session != null) session.closeAndClearTokenInformation();
			                bandiApp.setIsFacebookLogin(false);
			                
			                runOnUiThread(new Runnable() 
							{
								@Override
								public void run() 
								{
									if(bandiApp.getCurrentMenu().equals("OnAir"))
									{
										LoginFormDialog dialog = LoginFormDialog.getInstance(MainActivity.this);
										dialog.dismiss();
										snsIconCheck();
									}
									else
									{
										new BandiBoard(MainActivity.this).run();
									}
								}
							});
			                
						}
						catch(TwitterException e)
						{
						}
					}
				}).start();
				
			}
		}
		
		if(requestCode == 2000)
		{
			if(resultCode == RESULT_OK)
			{
				new Thread(new Runnable() 
				{
					@Override
					public void run()
					{
						try
						{
							String verifier = mData.getExtras().getString(getString(R.string.twitter_oauth_verifier));
							AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
	
							long userID = accessToken.getUserId();
							User user = twitter.showUser(userID);
							bandiApp.setTwitterId(user.getId());
							bandiApp.setTwitterName(user.getName());
							bandiApp.setTwitterAccessToken(accessToken.getToken());
							bandiApp.setTwitterAccessTokenSecret(accessToken.getTokenSecret());
							
							Session session = Session.getActiveSession();
			                if(session != null) session.closeAndClearTokenInformation();
			                bandiApp.setIsFacebookLogin(false);
			                
			                
			                
							runOnUiThread(new Runnable() 
							{
								
								@Override
								public void run() 
								{
									snsIconCheck();
									TwitterDialog dialog = new TwitterDialog(MainActivity.this);
									dialog.setListener(new OnTwitterDialogListener()
									{
										
										@Override
										public void OnConfirmClick(String message) 
										{
											updateTwitterStatus update = new updateTwitterStatus();
											update.execute(message);
										}
										
										@Override
										public void OnCancelClick() 
										{
											
										}
									});
									dialog.show();
								}
							});
						}
						catch(TwitterException e)
						{
						}
					}
				}).start();
			}
		}
	}
	
	public void facebookLogin()
	{
		Session.openActiveSession(MainActivity.this, true, new Session.StatusCallback() 
		{
			@Override
			public void call(Session session, SessionState state, Exception exception) 
			{
				if(session.isOpened())
				{
					bandiApp.setIsFacebookLogin(true);
					bandiApp.setTwitterAccessToken("");
					bandiApp.setTwitterAccessTokenSecret("");
					bandiApp.setTwitterName("");
					makeMeRequest(session);
					publishFeedDialog();
					snsIconCheck();
				}
			}
		});
	}
	
	private void makeMeRequest(final Session session) 
	{
	    // Make an API call to get user data and define a 
	    // new callback to handle the response.
	    Request request = Request.newMeRequest(session, new Request.GraphUserCallback() 
	    {
	        @Override
	        public void onCompleted(GraphUser user, Response response) 
	        {
	            // If the response is successful
	            if (session == Session.getActiveSession()) 
	            {
	                if (user != null) 
	                {
	                	bandiApp.setFacebookId(user.getId());
	                	bandiApp.setFacebookName(user.getName());
	                }
	            }
	            if (response.getError() != null)
	            {
	                // Handle errors, will do so later.
	            }
	        }
	    });
	    request.executeAsync();
	} 
	
	public void loginToTwitter(final boolean isBoard)
	{
		final ConfigurationBuilder builder = new ConfigurationBuilder();
		builder.setOAuthConsumerKey(getResources().getString(R.string.twitter_key));
		builder.setOAuthConsumerSecret(getResources().getString(R.string.twitter_secret));

		final Configuration configuration = builder.build();
		final TwitterFactory factory = new TwitterFactory(configuration);
		twitter = factory.getInstance();
		new Thread(new Runnable()
		{
			@Override
			public void run() 
			{
				try 
				{
					requestToken = twitter.getOAuthRequestToken("http://www.ebs.co.kr");
					final Intent intent = new Intent(MainActivity.this, TwitterWebview.class);
					intent.putExtra(TwitterWebview.EXTRA_URL, requestToken.getAuthenticationURL());
					int requestCode = 0;
					if(isBoard == false) requestCode = 2000;
					else requestCode = 3000;
					startActivityForResult(intent, requestCode);
				} 
				catch (TwitterException e)
				{
					e.printStackTrace();
				}
			}
		}).start();
	}
	ProgressDialog pDialog;
	class updateTwitterStatus extends AsyncTask<String, String, Void> 
	{
		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
			
			pDialog = new ProgressDialog(MainActivity.this);
			pDialog.setMessage("Posting to twitter...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected Void doInBackground(String... args) 
		{

			String status = args[0];
			try 
			{
				ConfigurationBuilder builder = new ConfigurationBuilder();
				builder.setOAuthConsumerKey(getResources().getString(R.string.twitter_key));
				builder.setOAuthConsumerSecret(getResources().getString(R.string.twitter_secret));
				
				// Access Token
				String access_token = bandiApp.getTwitterAccessToken();
				// Access Token Secret
				String access_token_secret = bandiApp.getTwitterAccessTokenSecret();

				AccessToken accessToken = new AccessToken(access_token, access_token_secret);
				Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

				// Update status
				StatusUpdate statusUpdate = new StatusUpdate(status);
				twitter4j.Status response = twitter.updateStatus(statusUpdate);
				
			}
			catch (TwitterException e) 
			{
			
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			pDialog.dismiss();
//			Toast.makeText(MainActivity.this, "Posted to Twitter!", Toast.LENGTH_SHORT).show();
			AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		    builder.setMessage("트위터에 등록되었습니다.")
		    .setCancelable(false)
		    .setPositiveButton("확인", new DialogInterface.OnClickListener() 
		    {
		    	public void onClick(DialogInterface dialog, int id) 
		    	{
		    		dialog.dismiss();
		    	}
		    }).show();
		}

	}
	
	int receiveCount = 0;
	boolean isNetwork = false;
	boolean isLTE = false, isWifi = false;
	public class NetworkChangeReceiver extends BroadcastReceiver 
	{
		public NetworkChangeReceiver() {}

		@SuppressLint("NewApi")
		@Override
		public void onReceive(Context context, Intent intent) 
		{
			String action = intent.getAction();
	        
			// 네트웍에 변경이 일어났을때 발생하는 부분
	        if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) 
	        {
	            ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	            NetworkInfo wifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	            NetworkInfo mobile = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
	            Preferences prefs = new Preferences(context);
	            isLTE = false;
	            isWifi = false;
	            if(mobile != null && wifi != null)
	    		{
	    			if (mobile.isConnected())
	    			{
	    				isLTE = true;
	    				isWifi = false;
	    			}
	    			else if(wifi.isConnected())
	    			{
	    				isLTE = false;
	    				isWifi = true;
	    			}
	    			else
	    			{
	    				isLTE = false;
	    				isWifi = false;
	    			}
	    		}
	    		else if(mobile == null && wifi != null)
	    		{
	    			if(wifi.isConnected())
	    			{
	    				isLTE = false;
	    				isWifi = true;
	    			}
	    			else
	    			{
	    				isLTE = false;
	    				isWifi = false;
	    			}
	    		}
	    		else
	    		{
	    			isLTE = false;
    				isWifi = false;
	    		}

	            if(isLTE == false && isWifi == false)
	            {
	            	// 데이터네트워크 사용을 할 수 없을 때..
	            	networkCheckHandler.removeCallbacks(networkCheckRunnable);
	            	audioCheckCount = 0;
	            	checkAudioState();
	            }
	            else 
	            {
	            	if(isLTE == true && isWifi == false)
	            	{
	            		if(prefs.getUsePaidNetwork() == true)
	            		{
	            			if(serviceConnection != null && serviceConnection.getAudioService() != null) 
	    					{
	    	            		// 플레이어 마지막 상태가 플레이중이었다면..
	    	            		// rtsp 때문에 넣음..rtsp 네트워크가 바뀌면 끊어짐 (wifi <-> lte) 
	    	            		
	    	            		if(serviceConnection.getAudioService().wasPlayingPhase() == true)
	    	            		{
	    	            			musicCheckCount = 0;
	    	            			musicHandler.removeCallbacks(musicRunnable);
	    	            			musicCheck();
	    	            		}
	    					}
	    	            	audioCheckHandler.removeCallbacks(audioCheckRunnable);
	    	            	networkCheckHandler.removeCallbacks(networkCheckRunnable);
	            		}
	            	}
	            	else if(isLTE == false && isWifi == true)
	            	{
	            		if(serviceConnection != null && serviceConnection.getAudioService() != null) 
						{
		            		// 플레이어 마지막 상태가 플레이중이었다면..
		            		// rtsp 때문에 넣음..rtsp 네트워크가 바뀌면 끊어짐 (wifi <-> lte) 
		            		
		            		if(serviceConnection.getAudioService().wasPlayingPhase() == true)
		            		{
		            			musicCheckCount = 0;
		            			musicHandler.removeCallbacks(musicRunnable);
		            			musicCheck();
		            		}
						}
		            	audioCheckHandler.removeCallbacks(audioCheckRunnable);
		            	networkCheckHandler.removeCallbacks(networkCheckRunnable);
	            	}
	            }
	        }
		}
	}
	
	int musicCheckCount = 0;
	public void musicCheck()
	{
		musicHandler.postDelayed(musicRunnable, 1000);
	}
	
	Handler musicHandler = new Handler();
	Runnable musicRunnable = new Runnable()
	{
		
		@Override
		public void run()
		{
			if(musicCheckCount < 10)
			{
				AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
	        	if(audioManager.isMusicActive() == false)
	        	{
	        		if(serviceConnection != null && serviceConnection.getAudioService() != null) 
					{
	        			serviceConnection.getAudioService().stopPlayer();
	        			serviceConnection.getAudioService().initPlayer();
	        			musicCheckCount = 10;
		        		musicHandler.removeCallbacks(musicRunnable);
					}
	        	}
	        	musicCheckCount++;
	        	musicCheck();
			}
			else
			{
				musicHandler.removeCallbacks(musicRunnable);
			}
		}
	};
	
	int networkCheckCount;
	Handler networkCheckHandler = new Handler();
	Runnable networkCheckRunnable = new Runnable()
	{
		
		@Override
		public void run()
		{
			ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo wifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			NetworkInfo mobile = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			isLTE = false;
			isWifi = false;
            if(mobile != null && wifi != null)
    		{
    			if (mobile.isConnected())
    			{
    				isLTE = true;
    				isWifi = false;
    			}
    			else if(wifi.isConnected())
    			{
    				isLTE = false;
    				isWifi = true;
    			}
    			else
    			{
    				isLTE = false;
    				isWifi = false;
    			}
    		}
    		else if(mobile == null && wifi != null)
    		{
    			if(wifi.isConnected())
    			{
    				isLTE = false;
    				isWifi = true;
    			}
    			else
    			{
    				isLTE = false;
    				isWifi = false;
    			}
    		}
    		else
    		{
    			isLTE = false;
    			isWifi = false;
    		}

        	// 데이터가 끊겼다..10초 체크
            if(networkCheckCount < 10)
            {
            	networkCheckCount++;
            	checkNetworkState();
            }
            else
            {
            	if(isLTE == false && isWifi == false)
	            {
	            	if(serviceConnection != null && serviceConnection.getAudioService() != null) 
					{
						serviceConnection.getAudioService().stopPlayer(playBtn, loading);
					}

	            	if(networkPopup.isShowing() == false)
	            	{
	            		networkPopup.show();
	            	}
            	}
            	else if(isLTE == true && isWifi == false)
            	{
            		if(prefs.getUsePaidNetwork() == false)
            		{
            			if(serviceConnection != null && serviceConnection.getAudioService() != null) 
    					{
    						serviceConnection.getAudioService().stopPlayer(playBtn, loading);
    					}

    	            	if(networkPopup.isShowing() == false)
    	            	{
    	            		networkPopup.show();
    	            	}
            		}
            	}
            }
		}
	};

	public void checkNetworkState()
	{
		networkCheckHandler.postDelayed(networkCheckRunnable, 1000);
	}
	
	public int audioCheckCount = 0;
	public Handler audioCheckHandler = new Handler();
	public Runnable audioCheckRunnable = new Runnable()
	{
		
		@Override
		public void run()
		{
        	if(audioCheckCount < 10)
    		{
        		checkAudioState();
    			audioCheckCount++;
    		}
        	else
        	{
        		audioCheckHandler.removeCallbacks(audioCheckRunnable);
        		// 10초간 네트워크 연결상태를 확인한다.
        		networkCheckCount = 0;
        		checkNetworkState();
        	}
		}
	};
	
	public void checkAudioState()
	{
		audioCheckHandler.postDelayed(audioCheckRunnable, 1000);
	}
	
	public void callLogin(String viewName, boolean isShowSNS)
	{
		LoginFormDialog loginForm = LoginFormDialog.getInstance(MainActivity.this);
		if(viewName.equals("") == false) bandiApp.setLoginPrevView(viewName);
		loginForm.setOnLoginSuccessCb(new LoginFormDialog.OnLoginSuccessCb() 
		{
			@Override
			public void onLoginSuccess() 
			{
				snsIconCheck();
				
//				if(bandiApp.getFavoriteList() == null || bandiApp.getFavoriteList().size() <= 0)
//                {
//					String url = "http://home.ebs.co.kr/bandi/bandiAppUserFbmkList";
//			    	Map<String, Object> params = new HashMap<String, Object>();
//			    	params.put("userId", bandiApp.getUserId());
//			    	params.put("fileType", "json");
//			    	AQuery aq = new AQuery(MainActivity.this);
//			    	aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
//					{
//			    		@Override
//			    		public void callback(String url, JSONObject object, AjaxStatus status) 
//			    		{
//			    			try 
//			    			{
//			    				favoriteData.clear();
//			    				JSONObject result = object.getJSONObject("appUserFbmkLists");
//			    				JSONArray jArray = result.getJSONArray("appUserFbmkLists");
//
//								for(int i = 0; i < jArray.length(); i++)
//								{
//									FavoriteData data = new FavoriteData();
//									data.setFbmkSno(jArray.getJSONObject(i).getInt("fbmkSno"));
//									data.setProgramID(jArray.getJSONObject(i).getString("courseId"));
//									data.setTitle(jArray.getJSONObject(i).getString("bandiTitle"));
//									data.setLinkUrl(jArray.getJSONObject(i).getString("bandiMobUrl"));
//									favoriteData.add(data);
//								}
////								
//								bandiApp.setFavoriteList(favoriteData);
//								
//								for(int i = 0; i < bandiApp.getFavoriteList().size(); i++)
//			                	{
//			                		if(bandiApp.getFavoriteList().get(i).getProgramID().equals(CProgramData.getInstance().getProgramID()))
//			                		{
//			                			String fbmkSno = Integer.toString(bandiApp.getFavoriteList().get(i).getFbmkSno());
//			                			CProgramData.getInstance().setNowFavoriteId(fbmkSno);
//			                			favorite.setSelected(true);
//			                		}
//			                	}
//			    			}
//			    			catch(Exception e) {}
//			    		}
//					});
//                }
//				else
				{
					for(int i = 0; i < bandiApp.getFavoriteList().size(); i++)
                	{
                		if(bandiApp.getFavoriteList().get(i).getProgramID().equals(CProgramData.getInstance().getProgramID()))
                		{
                			String fbmkSno = Integer.toString(bandiApp.getFavoriteList().get(i).getFbmkSno());
                			CProgramData.getInstance().setNowFavoriteId(fbmkSno);
                			favorite.setSelected(true);
                		}
                	}
				}
			}
		});
		loginForm.setOnLoginFailureCb(new LoginFormDialog.OnLoginFailureCb() 
		{
			@Override
			public void onLoginFailure() 
			{
				snsIconCheck();
			}
		});
		loginForm.showDialog(isShowSNS);
	}
	
	// Setting 메뉴 자동종료기능(TimerService 에서 사용)
	public final BroadcastReceiver quitBandi = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent) 
		{
			finish();
		}
	};
	
	// network 환경 확인 후, 오디오 서비스에 바인딩 
	public void bindAudioService() 
	{
		Log.d(TAG, "오디오 서비스 바인드");
		if (isFreeNetwork()) getApplicationContext().bindService(audioServiceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
		else switchToPauseState();
	}
	
	class NoAscCompare implements Comparator<BandiBoardData>
	{
		//오름차순(ASC)
		@Override
		public int compare(BandiBoardData arg0, BandiBoardData arg1)
		{
			return arg0.getSEQ() < arg1.getSEQ() ? -1 : arg0.getSEQ() > arg1.getSEQ() ? 1:0;
		}
	}
	
	// StatusBar Size 구하는 메서드 ( onCreate에서 실행 불가능 )
    private int getStatusBarSize()
    { 
        Rect rectgle= new Rect();
        Window window= getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectgle);
        int StatusBarHeight= rectgle.top;
        int contentViewTop= window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        int TitleBarHeight = contentViewTop - StatusBarHeight;
        return StatusBarHeight;
    }
	
    boolean isConfigChange = false;
	@Override
	public void onConfigurationChanged(android.content.res.Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		
		dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		dpWidth = (int)(dm.widthPixels / dm.density);
		dpHeight = (int)(dm.heightPixels / dm.density);
		
		if (newConfig.orientation == android.content.res.Configuration.ORIENTATION_LANDSCAPE)
		{
			isConfigChange = true;
			findViewById(R.id.ad_zone).setVisibility(View.GONE);
			findViewById(R.id.title_bar).setVisibility(View.GONE);
			findViewById(R.id.top).setVisibility(View.GONE);
			findViewById(R.id.bottomMenu).setVisibility(View.GONE);
			findViewById(R.id.onAirTools).setVisibility(View.GONE);
			findViewById(R.id.program_image_zone).setVisibility(View.GONE);
			viewRadioHandler.removeCallbacks(viewRadioRunnable);
//			viewRadioHandler.postDelayed(viewRadioRunnable, 5000);
			mainScroll.setScroll(false);
			rParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, dm.heightPixels - getStatusBarSize());
			videoLayout.setLayoutParams(rParams);
		} 
		else if (newConfig.orientation == android.content.res.Configuration.ORIENTATION_PORTRAIT)
		{
			isConfigChange = false;
			findViewById(R.id.ad_zone).setVisibility(View.VISIBLE);
			findViewById(R.id.title_bar).setVisibility(View.VISIBLE);
			findViewById(R.id.top).setVisibility(View.VISIBLE);
			findViewById(R.id.bottomMenu).setVisibility(View.VISIBLE);
			findViewById(R.id.onAirTools).setVisibility(View.VISIBLE);
			findViewById(R.id.program_image_zone).setVisibility(View.VISIBLE);
			findViewById(R.id.control_bar).setVisibility(View.VISIBLE);
			
			viewRadioHandler.removeCallbacks(viewRadioRunnable);
			viewRadioHandler.postDelayed(viewRadioRunnable, 5000);
			
			mainScroll.setScroll(true);
			rParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, videoTempHeight + getWidthDP(50));
			rParams.addRule(RelativeLayout.BELOW, R.id.top);
			videoLayout.setLayoutParams(rParams);
		}
	}

	boolean isOpened = false;
	boolean isClosed = false;
	RelativeLayout.LayoutParams tempParams;
	public void setListenerToRootView()
	{
	    final View activityRootView = getWindow().getDecorView().findViewById(android.R.id.content); 
	    activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() 
	    {
	        @Override
	        public void onGlobalLayout()
	        {
	
	            int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
	            if (heightDiff > 100 ) 
	            { 
	            	// 99% of the time the height diff will be due to a keyboard.
//	                Toast.makeText(getApplicationContext(), "Gotcha!!! softKeyboardup", 0).show();
	            	
	            	if(bandiApp.getCurrentMenu().equals("OnAir"))
	            	{
	            		if(LoginFormDialog.getInstance(MainActivity.this).isShowing())
	            		{
	            		}
	            		else
	            		{
		            		findViewById(R.id.top).setVisibility(View.GONE);
							findViewById(R.id.title_bar).setVisibility(View.GONE);
							findViewById(R.id.control_bar).setVisibility(View.GONE);
							findViewById(R.id.onAirTools).setVisibility(View.GONE);
		            		findViewById(R.id.ad_zone).setVisibility(View.GONE);
		            		findViewById(R.id.layout_bottomBtnGroup).setVisibility(View.GONE);
	//	            		
		            		rParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
							rParams.addRule(RelativeLayout.BELOW, R.id.top);
							rParams.addRule(RelativeLayout.ABOVE, R.id.bottomMenu);
							videoLayout.setLayoutParams(rParams);
	            		}
//	            		RelativeLayout.LayoutParams vParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
//	            		vParams.setMargins(0, 0, 0, getWidthDP(10));
//	            		mainLayout.setPadding(0, 0, 0, getWidthDP(20));
	            		
	            	}
	            	else if(bandiApp.getCurrentMenu().equals("BandiBoard"))
	            	{
	            	
	            		if(LoginFormDialog.getInstance(MainActivity.this).isShowing())
	            		{
	            			
	            		}
	            		else
	            		{
			                findViewById(R.id.ad_zone).setVisibility(View.GONE);
			                findViewById(R.id.layout_board_text).setVisibility(View.GONE);
							findViewById(R.id.top).setVisibility(View.GONE);
							findViewById(R.id.title_bar).setVisibility(View.GONE);
							findViewById(R.id.control_bar).setVisibility(View.GONE);
							findViewById(R.id.bottom_standard).setVisibility(View.GONE);
							findViewById(R.id.bottom_standard1).setVisibility(View.GONE);
	            		}
	            	}
//					RelativeLayout.LayoutParams swapZoneParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, getHeightDP(350));
//		    		swapZoneParams.addRule(RelativeLayout.ABOVE, R.id.bottom_standard);
//		    		swapZoneParams.addRule(RelativeLayout.BELOW, R.id.top);
//		    		swapZone.setLayoutParams(swapZoneParams);
	                if(isOpened == false)
	                {
	                    //Do two things, make the view top visible and the editText smaller
	                }
	                isClosed = false;
	                isOpened = true;
	            }
	            else if(isOpened == true)
	            {
//	                Toast.makeText(getApplicationContext(), "softkeyborad Down!!!", 0).show();  
	            	isClosed = true;
	            	if(bandiApp.getCurrentMenu().equals("OnAir"))
	            	{
	            		if(LoginFormDialog.getInstance(MainActivity.this).isShowing())
	            		{
	            			
	            		}
	            		else
	            		{
		            		findViewById(R.id.top).setVisibility(View.VISIBLE);
							findViewById(R.id.title_bar).setVisibility(View.VISIBLE);
							findViewById(R.id.control_bar).setVisibility(View.VISIBLE);
							findViewById(R.id.onAirTools).setVisibility(View.VISIBLE);
		            		findViewById(R.id.ad_zone).setVisibility(View.VISIBLE);
		            		findViewById(R.id.layout_bottomBtnGroup).setVisibility(View.VISIBLE);
		            		
		            		if(isClosed == true && viewRadioCheck.isSelected())
		            		{
		            			viewRadioHandler.removeCallbacks(viewRadioRunnable);
		            			viewRadioHandler.postDelayed(viewRadioRunnable, 5000);
		            			isClosed = false;
		            		}
		            		
		            		if(tempParams != null) videoLayout.setLayoutParams(tempParams);
	            		}
	            		
	            	}
	            	else if(bandiApp.getCurrentMenu().equals("BandiBoard"))
	            	{
	            		if(LoginFormDialog.getInstance(MainActivity.this).isShowing())
	            		{
	            			
	            		}
	            		else
	            		{
		            		findViewById(R.id.ad_zone).setVisibility(View.VISIBLE);
							findViewById(R.id.top).setVisibility(View.VISIBLE);
							findViewById(R.id.title_bar).setVisibility(View.VISIBLE);
							findViewById(R.id.control_bar).setVisibility(View.VISIBLE);
							findViewById(R.id.bottom_standard).setVisibility(View.VISIBLE);
							findViewById(R.id.bottom_standard1).setVisibility(View.VISIBLE);
	            		}
	            	}
	                isOpened = false;
	            }
	         }
	    });
	}
	
	public Runnable viewRadioRunnable = new Runnable()
	{
		@Override
		public void run()
		{
			FrameLayout onAirTools = (FrameLayout)findViewById(R.id.onAirTools);
			onAirTools.setVisibility(View.INVISIBLE);
			RelativeLayout controlBar = (RelativeLayout)findViewById(R.id.control_bar);
			controlBar.setVisibility(View.INVISIBLE);
		}
	};
	
//	class NetworkStandby extends AsyncTask<Void, Void, Result>
}
