package kr.ebs.bandi;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class AutoRunReceiver extends BroadcastReceiver
{
	static final String TAG = "AutoRunReceiver";
	
	public AutoRunReceiver() {}

	@Override
	public void onReceive(Context context, Intent intent) 
	{
		/*
		 *  1회성 자동실행 예약의 경우 예약을 종료하고 반디를 시작한다.
		 */
		Preferences prefs = new Preferences(context);
		if(prefs.getAutoRun())
		{
//			prefs.putAutoRun(false);
			
			Intent i = new Intent(context, MainActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(i);
		}
		
	}
}
