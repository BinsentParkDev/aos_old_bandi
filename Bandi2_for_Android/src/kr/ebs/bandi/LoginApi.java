package kr.ebs.bandi;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import kr.ebs.bandi.Login.LoginListener;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

/**
 * Login class 대체용 - 새로운 로그인 방식 적용
 * 
 * @since 2017. 4. 26.
 */
public class LoginApi {
	static final String TAG = "LoginApi";
	
	private Context context;
	private Activity activity;
	private BandiApplication bandiApp;
    private Preferences prefs;
    /**
     * 로그인 결과 반환 (code, cookie, body 로 구성)
     */
    HashMap<String, String> responseMsg = new HashMap<String, String>();
    
    private enum HTTPS_METHOD {POST, GET};
	
	public LoginApi(Context context) {
		this.context = context;
		this.activity = (Activity) context;
		this.bandiApp = (BandiApplication) this.activity.getApplication();
		this.prefs =  new Preferences(context);
	}
	
	
	public interface LoginListener
	{
		public void OnLoginSuccess();
		public void OnLoginFailure();
	}
	
	
	
	/**
	 * 로그인 처리후 결과에 따른 후 처리 (listener)진행
	 * @param userid
	 * @param passwd
	 * @param listener
	 */
	public void login(String userid, String passwd, LoginListener listener) {
		if (login(userid, passwd)) listener.OnLoginSuccess();
		else listener.OnLoginFailure(); // 0 은 무의미한 코드로
	}
	
	/**
	 * 로그인 실패 시 각 경우에 대해 alert 을 보여준다.
	 * sso 로그인 처리 후 결과 반환 ( Thread 이용하여 Network 통신함 )
	 * @param userId
	 * @param passwd
	 * @return
	 */
	public boolean login(String userId, String passwd) {
		return login(userId, passwd, true);
	}
	
	/**
	 * sso 로그인 처리 후 결과 반환 ( Thread 이용하여 Network 통신함 )
	 * @param userId
	 * @param passwd
	 * @return
	 */
	public boolean login(String userId, String passwd, boolean showAlert) {
		boolean isLoginSuccess = false; // 로그인 성공여부 반환
		
		bandiApp.setUserPasswd(passwd); // 로그인 성공 시 저장할 수 없으므로 우선 저장하고 시작한다.
		
		final HashMap<String, String> data = new HashMap<String, String>();
		
		data.put("j_username", userId);
		data.put("j_password", passwd);
		data.put("SAMLRequest", Url.SAMLRequest());
		
		Log.d(TAG, "id/pw : " + userId + "/" + passwd);
		
		Thread th = new Thread(new Runnable() {
			@Override
			public void run() {
				 // 로그인 시에는 기존 쿠키 삭제하고 해야함.
//				String cookie = bandiApp.getSsoCookie();
//				cookie = cookie.replaceAll("_idp_session=[^;]*;", "");
//				cookie = cookie.replaceAll("_idp_authn_lc_key=[^;]*;", "");
//				
//				bandiApp.setSsoCookie(cookie);
				
				responseMsg = sendHttpsRequest(Url.LOGIN_HTTPS_POST, HTTPS_METHOD.POST, data );
				if (responseMsg.containsKey("body")) {
//					Log.d(TAG, "body -- " + responseMsg.get("body"));
				}
			}
		});
		th.start();
		
		try {
			th.join();
		} catch (InterruptedException e) {
			Log.e(TAG, "Login Thread join error. ");
			e.printStackTrace();
		}
		
		if (responseMsg.containsKey("code") && responseMsg.get("code") != null) {
			if ("200".equals(responseMsg.get("code"))) {
				// 정상로그인, 쿠키와 회원 기본정보 (id, email, name) 저장.
				HashMap<String, String> hmap = getUserInfoFromXml(responseMsg.get("body"));
				
				if (hmap.get("id") != null && hmap.get("name") != null) {
					BandiApplication app = (BandiApplication) context.getApplicationContext();
		            app.setUserId(hmap.get("id"));
		            app.setUserName(hmap.get("name"));
		            app.setUserEmail(hmap.get("email"));
		            app.setSsoCookie(responseMsg.get("cookie"));
		            
		          	//즐겨찾기 목록을 가져와 저장한다.
		            String jsonStr = Url.getServerText(Url.FAVORITE_LIST_JSON(userId));
		            bandiApp.setFavoriteData(jsonStr);
		            
		            isLoginSuccess = true;
				} else {
					if (showAlert) Dialog.showAlert(context, "로그인 결과(ID, 이름) 정보에 오류가 발생했습니다." );
				}
			} else if ("401".equals(responseMsg.get("code"))) {
				if (showAlert) Dialog.showAlert(context, "ID 또는 패스워드가 잘못되었습니다.");
			} else {
				if (showAlert) Dialog.showAlert(context, "로그인 오류가 발생했습니다. - 오류코드 : " + responseMsg.get("code"));
			}
		} else {
			if (showAlert) Dialog.showAlert(context, "로그인 오류가 발생했습니다." );
		}
		
		return isLoginSuccess;
		
	}
	
	/**
	 * 로그인 후 장시간 접속이 없을 경우 세션이 만료되었는지 확인 <br>
	 * sso 서버 반환값은 100:로그인 유지중, 101:로그인 유지안되어 있음, 102:타계정으로 로그인
	 * @return 세션이 유지되고 있으면 true, 그렇지 않으면 false 반환
	 */
	public boolean isValidSsoSession() {
		boolean result = false;
		if (! bandiApp.isLogin()) return false;
		
		final HashMap<String, String> data = new HashMap<String, String>();
		data.put("userId", bandiApp.getUserId());
		
		Thread th = new Thread(new Runnable() {
			@Override
			public void run() {
				responseMsg = sendHttpsRequest(Url.LOGIN_CHECK_ALIVE_HTTPS_GET, HTTPS_METHOD.GET, data);
			}
		});
		th.start();
		
		try {
			th.join();
		} catch (InterruptedException e) {
			Log.e(TAG, "isValidSsoSession Thread join error. ");
			e.printStackTrace();
		}
		
		Log.d(TAG, responseMsg.get("body"));
		
		if ("100".equals(responseMsg.get("body").trim())) result = true;
		else result = false;
		
		return result;
	}
	
	
	
	/**
	 * https 방식으로 서버 요청 보냄
	 * @param httpsUrl - 서버 URL
	 * @param method - {post, get} 방식 선택
	 * @param data - (key, value) 쌍으로 구성된 query 데이타
	 * @return HashMap, key set {code, cookie, body}
	 */
	private HashMap<String, String> sendHttpsRequest(String httpsUrl, HTTPS_METHOD method, HashMap<String, String> data) {
		HashMap<String, String> responseMsg = new HashMap<String, String>();
		
		try {
			// 연결 수립
			URL url = new URL( httpsUrl );
			HttpsURLConnection httpsConn = (HttpsURLConnection) url.openConnection();
	        httpsConn.setDoOutput(true);
//	        httpsConn.setDoInput(true);
	        httpsConn.setReadTimeout(10000);
	        httpsConn.setConnectTimeout(15000);
	        if (method == HTTPS_METHOD.POST) {
	        	httpsConn.setRequestMethod("POST");
	        } else if (method == HTTPS_METHOD.GET) {
	        	httpsConn.setRequestMethod("GET");
	        }
	        if (! "".equals(bandiApp.getSsoCookie())) {
	        	httpsConn.setRequestProperty("Cookie", bandiApp.getSsoCookie());
	        }	
	        Log.d(TAG, "request cookie : " + bandiApp.getSsoCookie());
	        
	        // data 전송
	        Uri.Builder builder = new Uri.Builder();
	        for (String key : data.keySet()) {
	        	builder.appendQueryParameter(key, data.get(key));
	        	Log.d(TAG, "key/value = "+ key + "/" + data.get(key));
	        }
	        String query = builder.build().getEncodedQuery();
	        
	        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(httpsConn.getOutputStream(), "utf-8"));
			writer.write(query);
			
	        writer.flush();
	        writer.close();  
	        
	        //** get response message
	        //** log **
	        Log.d(TAG, "response code : " + httpsConn.getResponseCode());
	        Map<String, List<String>> map = httpsConn.getHeaderFields();
	        
	        String ssoCookie = "";
	        for (String key : map.keySet()) {
//	        	Log.d(TAG, "response header - " + key + " : " + map.get(key) + " / size : " + map.get(key).size());
	        	
	        	if (key != null && "set-cookie".equals(key.toLowerCase())) {
	        		for (String cookie : map.get(key)) {
	        			Log.d(TAG, "cookie : " + cookie.split(";")[0]);
	        			ssoCookie += cookie.split(";")[0] + "; " ;
	        		}
//	        		Log.d(TAG, "sso cookie : " + ssoCookie);
	        	}
	        }
	        
	        //** log **/
	        
	        BufferedReader bufReader = new BufferedReader(new InputStreamReader(httpsConn.getInputStream(), "utf-8"), 8192);
	        String str = "";
	        StringBuilder strBuilder = new StringBuilder();
	        while((str = bufReader.readLine()) != null) {
	        	strBuilder.append(str);
	        }
	        bufReader.close();
	        
	        httpsConn.disconnect();
	        
	        responseMsg.put("code", String.valueOf(httpsConn.getResponseCode()));
	        responseMsg.put("cookie", ssoCookie);
	        responseMsg.put("body", strBuilder.toString());
	        Log.d(TAG, "response body : " + strBuilder.toString());
	        
	        
		} catch (MalformedURLException e1) {
			Log.e(TAG, "MalformedURLException - ");
			e1.printStackTrace();
		} catch (IOException e) {
			Log.e(TAG, "IOException - ");
			e.printStackTrace();
		} 
		
		return responseMsg;
	}
	
	/**
	 * 로그인이 정상적일 때 반환되는 xml 을 파싱하여 원하는 정보를 반환한다.
	 * @param xmlStr
	 * @return HashMap with key set { id, name, email }
	 */
	private HashMap<String, String> getUserInfoFromXml(String xmlStr) {
		HashMap<String, String> hmap = new HashMap<String, String>();
		//xml element path : //profile/userId,  //profile/userName, //profile/email
		
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
//			InputSource is = new InputSource();
//			is.setCharacterStream(new StringReader(xmlStr));
//			Document doc = dBuilder.parse(is);
			Document doc = dBuilder.parse(new InputSource(new StringReader(xmlStr)));
			NodeList nl;
			nl= doc.getElementsByTagName("userId");
			hmap.put("id", nl.item(0).getTextContent());
//			Log.d(TAG, "id : " + nl.item(0).getTextContent());
			
			nl = doc.getElementsByTagName("userName");
			hmap.put("name", nl.item(0).getTextContent());
//			Log.d(TAG, "name : " + nl.item(0).getTextContent());
			
			nl = doc.getElementsByTagName("email");
			hmap.put("email", nl.item(0).getTextContent());
//			Log.d(TAG, "email : " + nl.item(0).getTextContent());
			
		} catch (ParserConfigurationException e) {
			Log.e(TAG, "getUserInfoFromXml - ParserConfigurationException - " + xmlStr);
			e.printStackTrace();
		} catch (SAXException e) {
			Log.e(TAG, "getUserInfoFromXml - SAXException - " + xmlStr);
			e.printStackTrace();
		} catch (IOException e) {
			Log.e(TAG, "getUserInfoFromXml - IOException - " + xmlStr);
			e.printStackTrace();
		} catch (Exception e) {
			Log.e(TAG, "getUserInfoFromXml - Exception - " + xmlStr);
			e.printStackTrace();
		}
		
		return hmap;
	}
	
	
	
	
	/**
	 * 반디 시작 시, BandiIntro 에서 자동 로그인 처리
	 * @return
	 */
	public boolean doAutoLogin() {
		boolean isLoginSucceed = false;
		
		if ( !"".equals(prefs.getUserId()) && !"".equals(prefs.getPassword()) )
    	{
			isLoginSucceed = login(prefs.getUserId(), prefs.getPassword());
    	}
		
		return isLoginSucceed;
	}
	
	
	/**
	 * 로그인 상태를 확인하고, 로그인이 필요하면 true 반환 이미 로그인 중이면 false 반환 <br>
	 * 로그인이 아닌데 자동로그인 ON 이면 로그인 처리 후 결과 반환 (로그인 성공 시, false) <br>
	 * @return
	 */
	public boolean isNeededToLogin() {
		boolean needToLogin = true;
		
		if (bandiApp.isLogin()) {
			needToLogin = false;
		} else {
			if (prefs.getAutoLogin()) {
				needToLogin = ! doAutoLogin(); //**로그인 성공이면 needToLogin 은 false 값을 가져야함
			}
		}
		
		return needToLogin;
	}
	
}
