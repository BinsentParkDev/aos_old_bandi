package kr.ebs.bandi;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.Character.UnicodeBlock;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import kr.ebs.bandi.GoogleAnalytics;
import kr.ebs.bandi.data.BandiBoardData;
import kr.ebs.bandi.data.CProgramData;
import kr.ebs.bandi.data.EventData;
import kr.ebs.bandi.data.ScheduleCompare;
import kr.ebs.bandi.utils.CScrollViewWithProgress.OnScrollLoadingListener;
import kr.ebs.bandi.utils.CScrollViewWithProgress2;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class BandiBoard implements BaseInterface
{
	static final String TAG = "BandiBoard";

	private Context context;
	private Activity activity;
	MainActivity main;

	DisplayMetrics dm;
	int dpWidth, dpHeight;

	CScrollViewWithProgress2 scroll;
	LayoutInflater inflater;
	LayoutInflater footerInflater;
	View footer;
	ArrayList<BandiBoardData> bandiBoardListData = new ArrayList<BandiBoardData>();
	LinearLayout listLayout;
	ProgressBar p;
	TextView onAirName;
	boolean isLoading = false;
	boolean isLastPage = false;
	TextView noti;
	LinearLayout notiLayout;
	EditText input;
	BandiApplication bandiApp;
	String inputContents;

	FrameLayout topMenuBtn0;
	FrameLayout topMenuBtn1;

	ImageView snsIcon;
	int loginCode = 0;
	TextView noBoardTxt;
	InputMethodManager imm;
	public AudioServiceConnection serviceConnection;

	private FrameLayout.LayoutParams fParams;
	private LinearLayout.LayoutParams lParams;
	private RelativeLayout.LayoutParams rParams;

	Preferences prefs;
	EventData data = null;


	/**
	 * 게시물 불러올 때 페이징 카운트용
	 */
	int pageIndex = 0;


	/**
	 * 공지, 이벤트 게시물 추출용
	 */
	NoticeEventThread nThread;

	/**
	 * 반디 게시물 초기 로딩용
	 */
	BoardListThread blThread;

	/**
	 * 반디 게시물 더보기 게시물 추출용
	 */
	BoardListAddThread blaThread;

	/**
	 * 반디 게시물 작성용
	 */
	PostArticleThread paThread;
	
	


	public BandiBoard(Context context)
	{
		this.context = context;
		this.activity = (Activity) context;

		prefs = new Preferences(context);

		main = (MainActivity)activity;
		bandiApp = (BandiApplication) activity.getApplication();
		imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);

		((BandiApplication) activity.getApplication()).setCurrentMenu(TAG);


		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		footerInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		dpWidth = (int)(dm.widthPixels / dm.density);
		dpHeight = (int)(dm.heightPixels / dm.density);

		/*
		 * 접근 로그 저장
		 */
		BandiLog.event(context, TAG);
		
	}


	/**
	 * 서버에서 비동기적으로 게시물 데이타를 받아와서, <br>
	 * UI 에 표현하기 위한 기능
	 */
	private Handler bHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);

			switch(msg.what)
			{
			case 1: // 반디 게시판 상단 공지, 이벤트
				noti.setText((String) msg.obj);
				if (nThread.isAlive()) nThread.interrupt();
				break;
			case 2: // 반디 게시판 초기 목록
				initBoardList((msg.arg1==1?true:false), (String) msg.obj);
				if (blThread.isAlive()) blThread.interrupt();
				break;
			case 3: // 반디 게시판 게시물 더보기용
				addBoardList((String) msg.obj);
				if (blaThread.isAlive()) blaThread.interrupt();
				break;
			case 4: // 반디 게시물 등록 후 서버 텍스트
				
				try {
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					Log.d(TAG, "msg.obj" + (String) msg.obj);
					InputStream istream = new ByteArrayInputStream(((String) msg.obj).getBytes("utf-8"));
					Document doc = builder.parse(istream);
					Element result = doc.getDocumentElement();
					NodeList nodes = result.getElementsByTagName("code");
					Node node = nodes.item(0);
					String code = node.getTextContent().trim();
					
					if ("0".equals(code)) {
						input.setText(""); // 작성 칸 비우기
						initListSetting(false, ScheduleCompare.getInstance().getNowOnAirId());
					} else if ("9".equals(code)) {
						nodes = result.getElementsByTagName("message");
						node = nodes.item(0);
						String message = node.getTextContent();
						
						Dialog.showAlert(BandiBoard.this.context, message);
					} else {
						Toast.makeText(context, "글을 올리는 중 에러가 발생하였습니다.", Toast.LENGTH_LONG).show();
					}
					
					Log.d(TAG, "code: " + code + " / nodes length : " + nodes.getLength() );
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Toast.makeText(context, "error-글을 올리는 중 에러가 발생하였습니다.", Toast.LENGTH_LONG).show();
				}
				
				
//				if ("0".equals(((String) msg.obj).trim())) {
//					initListSetting(false, ScheduleCompare.getInstance().getNowOnAirId());
//				} else {
//					Toast.makeText(context, "글을 올리는 중 에러가 발생하였습니다.", Toast.LENGTH_LONG).show();
//				}
				
				if (paThread.isAlive()) paThread.interrupt();
				break;
			}
		}
	};


	/**
	 * 상단 공지, 이벤트 게시물 받아오기
	 */
	class NoticeEventThread extends Thread
	{
		@Override
		public void run()
		{
			// 반디 게시판 메뉴 - 상단 공지글
			String title = getEventNoticeList();
			Message msg = bHandler.obtainMessage(1, title);
			bHandler.sendMessage(msg);
		}
	}

	/**
	 * 반디게시판 공지글 받아오기
	 */
	private String getEventNoticeList() {
		String title = "공지사항이 없습니다.";

		try {
			JSONObject jObject = new JSONObject(Url.getServerText(Url.BANDIBOARD_NOTICE_JSON));

			JSONArray result = jObject.getJSONArray("bandiEventLists");
			for(int i = 0; i < result.length(); i++)
			{
				if(result.getJSONObject(i).getString("evtClsCd").equals("001"))
				{
					data = new EventData();
					data.setEventID(result.getJSONObject(i).getLong("evtId"));
					data.setLinkUrl(result.getJSONObject(i).getString("linkUrl"));
					data.setEventTitle(result.getJSONObject(i).getString("evtTitle"));
					data.setEventCode(result.getJSONObject(i).getString("evtClsCd"));
					data.setEventStartDay(result.getJSONObject(i).getString("evtStartDt"));
					data.setEventEndDay(result.getJSONObject(i).getString("evtEndDt"));
					data.setEventMessage(result.getJSONObject(i).getString("evtCntn"));
					data.setEventUrl(result.getJSONObject(i).getString("mobLinkUrl"));
					data.setEventDeviceCode(result.getJSONObject(i).getString("shwSiteDsCd"));
					data.setImagePathUrl(result.getJSONObject(i).getString("mobThmnlFilePathNm"));
					data.setImagePhsUrl(result.getJSONObject(i).getString("mobThmnlFilePhscNm"));

					title = result.getJSONObject(i).getString("evtTitle");

					break;
				}
			}

		} catch (JSONException e) {
			Log.e(TAG, "공지, 이벤트 목록 추출 실패 - " + e.getMessage());
			e.printStackTrace();
		}

		return title;
	}

	/**
	 * 게시물 목록 받아오기 Thread <br>
	 * called handleMessage with msg.what = 2; <br>
	 */
	class BoardListThread extends Thread
	{
		String programId = null;
		boolean isPulltoRefresh;
		public BoardListThread(boolean isPulltoRefresh, String programId)
		{
			this.isPulltoRefresh = isPulltoRefresh;
			this.programId = programId;
		}

		@Override
		public void run()
		{
			String urlStr = Url.BANDIBOARD_LIST_JSON(programId);
			String jsonStr = Url.getServerText(urlStr);
			Message msg = bHandler.obtainMessage(2);
			msg.arg1 = (isPulltoRefresh ? 1 : 0);
			msg.obj = jsonStr;
			bHandler.sendMessage(msg);
		}
	}

	/**
	 * 사용자 액션으로 게시물 추가로 불러올 때 사용되는 Thread <br>
	 * called handleMessage with msg.what = 3 <br>
	 */
	class BoardListAddThread extends Thread
	{
		String programId;
		long seq;

		public BoardListAddThread(String programId, long seq)
		{
			this.programId = programId;
			this.seq = seq;
		}

		@Override
		public void run()
		{
			String jsonStr = Url.getServerText(Url.BANDIBOARD_LIST_ADD_JSON(programId, seq));
			Message msg = bHandler.obtainMessage(3, jsonStr);
			bHandler.sendMessage(msg);
		}
	}

	/**
	 * 반디 게시물 작성용
	 */
	class PostArticleThread extends Thread
	{
		Map<String, Object> params;

		public PostArticleThread(Map<String, Object> params)
		{
			this.params = params;
		}

		@Override
		public void run()
		{
			String responseStr = Url.postBandiBoardArticle(this.params);
			Message msg = bHandler.obtainMessage(4, responseStr);
			bHandler.sendMessage(msg);
		}
	}

	@SuppressLint({ "SetJavaScriptEnabled", "NewApi", "ClickableViewAccessibility" })
	@Override
	public void run()
	{
		BaseActivity.swapTitle(context, R.string.bandi_board);
		BaseActivity.swapLayout(context, R.layout.bandiboard);

		footer = footerInflater.inflate(R.layout.board_list_footer, null);

		main.repeatListViewInit();

		ImageView icon = (ImageView)activity.findViewById(R.id.imageView_mainIcon);
		icon.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				activity.findViewById(R.id.layout_menu1).performClick();
			}
		});
		FrameLayout controlBg = (FrameLayout)activity.findViewById(R.id.control_bar_bg);
		controlBg.setBackgroundColor(Color.parseColor("#3f3833"));

		rParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, getWidthDP(50));
		FrameLayout titleLayout = (FrameLayout)activity.findViewById(R.id.layout_bandiBoard_title);
		titleLayout.setLayoutParams(rParams);

		lParams = new LinearLayout.LayoutParams(getHeightDP(48), getHeightDP(21));
		ImageView notiIcon = (ImageView)activity.findViewById(R.id.imageView_boardNotiIcon);
		notiIcon.setLayoutParams(lParams);

		noBoardTxt = (TextView)activity.findViewById(R.id.textView_noBoard);
		noBoardTxt.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));

		noti = (TextView)activity.findViewById(R.id.textView_bandiBoardNoti);
		noti.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));

		notiLayout = (LinearLayout)activity.findViewById(R.id.layout_bandiboard_boardBtn);
		notiLayout.setPadding(getWidthDP(5), getWidthDP(5), getWidthDP(5), getWidthDP(5));
		notiLayout.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				if(data == null)
				{
					AlertDialog.Builder builder = new AlertDialog.Builder(context);
				    builder.setMessage("공지사항이 없습니다.")
				    .setCancelable(true)
				    .setPositiveButton("확인", new DialogInterface.OnClickListener()
				    {
				    	public void onClick(DialogInterface dialog, int id)
				    	{
				    		dialog.dismiss();
				    	}
				    }).show();
				}
				else
				{
					EventDialog d = new EventDialog(context);
					d.setData(data);
					d.show();
				}
			}
		});
		rParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, getHeightDP(295));
		scroll = (CScrollViewWithProgress2)activity.findViewById(R.id.scrollView_bandiboard);
		scroll.setLayoutParams(rParams);
		scroll.setOnTouchListener(new OnTouchListener()
		{
		    @Override
		    public boolean onTouch(View v, MotionEvent event)
		    {
		        if (event.getAction() == MotionEvent.ACTION_UP)
		            main.mainScroll.requestDisallowInterceptTouchEvent(false);
		        else
		        	main.mainScroll.requestDisallowInterceptTouchEvent(true);

		        return false;
		    }
		});

		scroll.setHeight(getHeightDP(295));
		scroll.setListener(new OnScrollLoadingListener()
		{

			@Override
			public void onScrollLastPosition()
			{
				if(isLoading == false)
				{
					isLoading = true;
					if(isLastPage == false)
					{
						listLayout.addView(footer);
						new Handler().postDelayed(new Runnable()
						{

							@Override
							public void run()
							{
								scroll.fullScroll(ScrollView.FOCUS_DOWN);
								listAdd();
							}
						}, 50);
					}

				}
			}

			@Override
			public void onLoadingStart()
			{
				// TODO Auto-generated method stub
				p.setVisibility(View.VISIBLE);
				initListSetting(true, CProgramData.getInstance().getProgramID());
			}

			@Override
			public void onLoadingFinish()
			{
				// TODO Auto-generated method stub

			}
		});

		RelativeLayout writeGroupLayout = (RelativeLayout)activity.findViewById(R.id.layout_bandiboard_write);
		writeGroupLayout.setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(3), getWidthDP(10));


		nThread = new NoticeEventThread();
		nThread.start();

		// 이벤트 공지 글 받아오기.
//		String url = "http://home.ebs.co.kr/bandi/bandiEventNoticeList";
//		AQuery aq = new AQuery(context);
//		Map<String, Object> params = new HashMap<String, Object>();
//		params.put("appDsCd", "02");
//		params.put("fileType", "json");
//		aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
//		{
//
//			@Override
//			public void callback(String url, JSONObject object, AjaxStatus status)
//			{
//				try
//				{
//					JSONArray result = object.getJSONArray("bandiEventLists");
//					for(int i = 0; i < result.length(); i++)
//					{
//						if(result.getJSONObject(i).getString("evtClsCd").equals("001"))
//						{
//							data = new EventData();
//							data.setEventID(result.getJSONObject(i).getLong("evtId"));
//							data.setLinkUrl(result.getJSONObject(i).getString("linkUrl"));
//							data.setEventTitle(result.getJSONObject(i).getString("evtTitle"));
//							data.setEventCode(result.getJSONObject(i).getString("evtClsCd"));
//							data.setEventStartDay(result.getJSONObject(i).getString("evtStartDt"));
//							data.setEventEndDay(result.getJSONObject(i).getString("evtEndDt"));
//							data.setEventMessage(result.getJSONObject(i).getString("evtCntn"));
//							data.setEventUrl(result.getJSONObject(i).getString("mobLinkUrl"));
//							data.setEventDeviceCode(result.getJSONObject(i).getString("shwSiteDsCd"));
//							data.setImagePathUrl(result.getJSONObject(i).getString("mobThmnlFilePathNm"));
//							data.setImagePhsUrl(result.getJSONObject(i).getString("mobThmnlFilePhscNm"));
//
//							noti.setText(result.getJSONObject(i).getString("evtTitle"));
//
//							break;
//						}
//					}
//
//					if(noti.getText().toString().equals(""))
//					{
//						noti.setText("공지사항이 없습니다.");
//					}
//				}
//				catch(JSONException e) {}
//			}
//
//		});

		if(CProgramData.getInstance().getAppUseBoardYn() != null)
		{
			if(CProgramData.getInstance().getAppUseBoardYn().equals("Y"))
			{
				scroll.setVisibility(View.VISIBLE);
				noBoardTxt.setVisibility(View.INVISIBLE);

				initListSetting(false, CProgramData.getInstance().getProgramID());
			}
			else
			{
				scroll.setVisibility(View.INVISIBLE);
				noBoardTxt.setVisibility(View.VISIBLE);
			}
		}



		onAirName = (TextView)activity.findViewById(R.id.textView_onAirName);
		onAirName.setText(bandiApp.getTitleForBoard());
		onAirName.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(20));

		p = (ProgressBar)activity.findViewById(R.id.progressBar1);
		p.setVisibility(View.INVISIBLE);



		rParams = new RelativeLayout.LayoutParams(getWidthDP(350), getHeightDP(31));
		rParams.addRule(RelativeLayout.CENTER_VERTICAL);
		rParams.addRule(RelativeLayout.LEFT_OF, R.id.imageButton_write);
		rParams.addRule(RelativeLayout.RIGHT_OF, R.id.imageView_badiboard_snsIcon);
		rParams.setMargins(0, 0, getWidthDP(5), 0);
		input = (EditText)activity.findViewById(R.id.editText_input);
		input.setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
		input.setLayoutParams(rParams);
		input.setPadding(getWidthDP(15), 0, getWidthDP(15), 0);
		input.setFocusable(false);
		input.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
//				if(CProgramData.getInstance().getAppUseBoardYn() == null)
//				{
//					new Handler().postDelayed(new Runnable()
//				    {
//
//						@Override
//						public void run()
//						{
//							imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
//						}
//
//				    }, 0);
//					return;
//				}
				if(CProgramData.getInstance().getAppUseBoardYn().equals("Y"))
				{

					if(bandiApp.isLogin() == false && bandiApp.isTwitterLogin() == false && bandiApp.isFacebookLogin() == false)
					{
						new Handler().post(new Runnable()
					    {

							@Override
							public void run()
							{
								imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
							}

					    });
						LoginFormDialog loginForm = LoginFormDialog.getInstance(context);
						bandiApp.setLoginPrevView("bandiBoard");
						final EditText et = (EditText) v;
						loginForm.setOnLoginSuccessCb(new LoginFormDialog.OnLoginSuccessCb()
						{
							@Override
							public void onLoginSuccess()
							{
								snsIconCheck();
								if (bandiApp.isBadMember(BandiBoard.this.context)) {
									et.clearFocus();
//				                    InputMethodManager imm = (InputMethodManager) et.getContext().getSystemService(Context.INPUT_METHOD_SERVICE); 
//				                    imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
								}
							}
						});
						loginForm.setOnLoginFailureCb(new LoginFormDialog.OnLoginFailureCb()
						{
							@Override
							public void onLoginFailure()
							{
								snsIconCheck();
							}
						});

						loginForm.showDialog(false);
					}
					else 
					{
						// 로그인 되었을 경우, 각 로그인 상황에 맞게 불량회원 정보 검색
						if (bandiApp.isBadMember(BandiBoard.this.context)) {
							v.clearFocus();
						}
					}


				}
				else
				{
					new Handler().post(new Runnable()
				    {

						@Override
						public void run()
						{
							imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
						}

				    });

					AlertDialog.Builder builder = new AlertDialog.Builder(context);
				    builder.setMessage("해당 프로그램은 반디게시판이 운영되지 않습니다.")
				    .setCancelable(false)
				    .setPositiveButton("확인", new DialogInterface.OnClickListener()
				    {
				    	public void onClick(DialogInterface dialog, int id)
				    	{
				    		dialog.dismiss();
				    	}
				    }).show();
				}
			}
		});



		listLayout = (LinearLayout)activity.findViewById(R.id.layout_bandiboardList);

		rParams = new RelativeLayout.LayoutParams(getHeightDP(34), getHeightDP(34));
		rParams.setMargins(0, 0, getWidthDP(5), 0);
		rParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		ImageButton writeBtn = (ImageButton)activity.findViewById(R.id.imageButton_write);
		writeBtn.setLayoutParams(rParams);
		writeBtn.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if(CProgramData.getInstance().getAppUseBoardYn() == null) return;
				if(CProgramData.getInstance().getAppUseBoardYn().equals("Y"))
				{

					if(bandiApp.isLogin() == false && bandiApp.isTwitterLogin() == false && bandiApp.isFacebookLogin() == false)
					{
						LoginFormDialog loginForm = LoginFormDialog.getInstance(context);
						bandiApp.setLoginPrevView("bandiBoard");
						loginForm.setOnLoginSuccessCb(new LoginFormDialog.OnLoginSuccessCb()
						{
							@Override
							public void onLoginSuccess()
							{
								snsIconCheck();
								if (bandiApp.isBadMember(BandiBoard.this.context)) return;
							}
						});
						loginForm.setOnLoginFailureCb(new LoginFormDialog.OnLoginFailureCb()
						{
							@Override
							public void onLoginFailure()
							{
								snsIconCheck();
							}
						});

						loginForm.showDialog(false);
					}
					else
					{
						if (bandiApp.isBadMember(BandiBoard.this.context)) return;
						
						inputContents = input.getText().toString();
//						input.setText("");

//						String url = "http://home.ebs.co.kr/bandi/bandiAppPost2";
						Map<String, Object> params = new HashMap<String, Object>();
						params.put("progcd", ScheduleCompare.getInstance().getNowOnAirId());
						params.put("contents", inputContents);
						params.put("appDsCd", "02");

						if(bandiApp.getIsFMRadio())	params.put("broadType", "radio");
						else params.put("broadType", "iradio");

						if(loginCode == 0)
						{
							params.put("userid", bandiApp.getUserId());
							params.put("writer", bandiApp.getUserName());
						}
						else if(loginCode == 1)
						{
							params.put("snsDsCd", "001");
							params.put("userid", "");
							params.put("writer", "");
							params.put("snsUserId", bandiApp.getFacebookId());
							params.put("snsUserNm", bandiApp.getFacebookName());
							params.put("snsUseYn", "Y");
						}
						else if(loginCode == 2)
						{
							params.put("snsDsCd", "002");
							params.put("userid", "");
							params.put("writer", "");
							params.put("snsUserId", bandiApp.getTwitterId());
							params.put("snsUserNm", bandiApp.getTwitterName());
							params.put("snsUseYn", "Y");
						}

						paThread = new PostArticleThread(params);
						paThread.start();

//						AQuery aq = new AQuery(context);
//						aq.ajax(url, params, String.class, new AjaxCallback<String>()
//						{
//							@Override
//							public void callback(String url, String object, AjaxStatus status)
//							{
//								if(object.equals("0"))
//								{
//									initListSetting(false, ScheduleCompare.getInstance().getNowOnAirId());
//								}
//								else
//								{
//									Toast.makeText(context, "글을 올리는 중 에러가 발생하였습니다.", Toast.LENGTH_LONG).show();
//								}
//							};
//
//						});
					}
				}
				else
				{
					AlertDialog.Builder builder = new AlertDialog.Builder(context);
				    builder.setMessage("해당 프로그램은 반디게시판이 운영되지 않습니다.")
				    .setCancelable(false)
				    .setPositiveButton("확인", new DialogInterface.OnClickListener()
				    {
				    	public void onClick(DialogInterface dialog, int id)
				    	{
				    		dialog.dismiss();
				    	}
				    }).show();

				}
			}
		});


		serviceConnection = AudioServiceConnection.getInstance();

		topMenuBtn0 = (FrameLayout)activity.findViewById(R.id.topMenuBtn0);
        topMenuBtn1 = (FrameLayout)activity.findViewById(R.id.topMenuBtn1);
		topMenuBtn0.setOnClickListener(topBtnListener);
		topMenuBtn1.setOnClickListener(topBtnListener);

		rParams = new RelativeLayout.LayoutParams(getHeightDP(33), getHeightDP(33));
		rParams.addRule(RelativeLayout.CENTER_VERTICAL);
		rParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		snsIcon = (ImageView) activity.findViewById(R.id.imageView_badiboard_snsIcon);
		snsIcon.setLayoutParams(rParams);
		snsIconCheck();
		
		//** Google Analytics **
		if (bandiApp.getIsFMRadio())
		{
			new GoogleAnalytics(this.activity).sendScreenView(TAG, GoogleAnalytics.CHANNEL.FM);
		}
		else 
		{
			new GoogleAnalytics(this.activity).sendScreenView(TAG, GoogleAnalytics.CHANNEL.IRADIO);
		}
		
    	
	}

	public void snsIconCheck()
	{
		if(isNeededToLogin() == false)
		{
			snsIcon.setBackgroundResource(R.drawable.bandi_sns_ico_1);
			snsIcon.setVisibility(View.VISIBLE);
			loginCode = 0;
		}
		else if(bandiApp.isFacebookLogin())
		{
			snsIcon.setBackgroundResource(R.drawable.bandi_sns_ico_3);
			snsIcon.setVisibility(View.VISIBLE);
			loginCode = 1;
		}
		else if(bandiApp.isTwitterLogin())
		{
			snsIcon.setBackgroundResource(R.drawable.bandi_sns_ico_2);
			snsIcon.setVisibility(View.VISIBLE);
			loginCode = 2;
		}
		else
		{
			snsIcon.setVisibility(View.GONE);
			loginCode = 0;
		}
	}

	OnClickListener topBtnListener = new OnClickListener()
	{

		@Override
		public void onClick(View v)
		{
			if(v.getId() == R.id.topMenuBtn0)
			{
				if(v.isSelected()) return;
				ScheduleCompare.getInstance().setData(bandiApp.getRadioData());
				v.setSelected(true);
				topMenuBtn1.setSelected(false);
				bandiApp.setIsFMRadio(true);

				if(MainActivity.isWifi(context) == false && prefs.getUsePaidNetwork() == false)
				{
					String confirmMsg = context.getResources().getString(R.string.use_paid_network_alert_msg);
					Dialog.confirm(context, confirmMsg, new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which)
						{
//							boardTextLayout.setVisibility(View.GONE);
							new Handler().postDelayed(new Runnable()
							{
								@Override
								public void run()
								{
									LinearLayout setting = (LinearLayout) activity.findViewById(R.id.layout_menu7);
									setting.performClick();
								}
							}, 300);
						}
					}, "설정으로 이동");
					return;
				}

				if(serviceConnection.getAudioService() != null)
				{
					serviceConnection.getAudioService().stopPlayer(main.playBtn, main.loading);
//					serviceConnection.getAudioService().switchUrl(bandiApp.getStreamUrlAndroid(), bandiApp.getStreamUrlAndroid2());
					serviceConnection.getAudioService().switchUrl(AudioService.Channel.FM, 
							bandiApp.getStreamUrlAndroid(), bandiApp.getStreamUrlAndroid2());
					serviceConnection.getAudioService().initPlayer(main.playBtn, main.loading);
				}
				else
				{
					try { main.bindAudioService(); } catch (Exception e) { Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show(); }
				}

				new Handler().postDelayed(new Runnable()
				{
					@Override
					public void run()
					{
						initListSetting(false, ScheduleCompare.getInstance().getNowOnAirId());
					}
				}, 1000);

			}
			else if(v.getId() == R.id.topMenuBtn1)
			{
				if(v.isSelected()) return;
				ScheduleCompare.getInstance().setData(bandiApp.getIRadioData());
				v.setSelected(true);
				topMenuBtn0.setSelected(false);
				bandiApp.setIsFMRadio(false);

				if(MainActivity.isWifi(context) == false && prefs.getUsePaidNetwork() == false)
				{
					String confirmMsg = context.getResources().getString(R.string.use_paid_network_alert_msg);
					Dialog.confirm(context, confirmMsg, new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which)
						{
//							boardTextLayout.setVisibility(View.GONE);
							new Handler().postDelayed(new Runnable()
							{
								@Override
								public void run()
								{
									LinearLayout setting = (LinearLayout) activity.findViewById(R.id.layout_menu7);
									setting.performClick();
								}
							}, 300);
						}
					}, "설정으로 이동");
					return;
				}

				if(serviceConnection.getAudioService() != null)
				{
					serviceConnection.getAudioService().stopPlayer(main.playBtn, main.loading);
//					serviceConnection.getAudioService().switchUrl(bandiApp.getStreamUrlIradioAndroid(), bandiApp.getStreamUrlIradioAndroid2());
					serviceConnection.getAudioService().switchUrl(AudioService.Channel.IRADIO,
							bandiApp.getStreamUrlIradioAndroid(), bandiApp.getStreamUrlIradioAndroid2());
					serviceConnection.getAudioService().initPlayer(main.playBtn, main.loading);
				}
				else
				{
					try { main.bindAudioService(); } catch (Exception e) { Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show(); }
				}

				new Handler().postDelayed(new Runnable()
				{
					@Override
					public void run()
					{
						initListSetting(false, ScheduleCompare.getInstance().getNowOnAirId());
					}
				}, 1000);

			}
		}
	};

	public boolean isNeededToLogin()
	{
		boolean needToLogin = true;

		if ("".equals( bandiApp.getUserId() ))
		{
			Preferences prefs = new Preferences(context);
            if (prefs.getAutoLogin())
            {
                if (!("".equals(prefs.getUserId()) || "".equals(prefs.getPassword())))
                {
//                    Login lg = new Login(context);
                	LoginApi lg = new LoginApi(context);
                    if (lg.login(prefs.getUserId(), prefs.getPassword()) )
                    {
                        needToLogin = false;
                    }
                }
            }
        }
		else
		{
            needToLogin = false;
        }

		return needToLogin;
	}


	/**
	 * 초기 반디 게시물 설정 <br>
	 * BoardHandler 에서 호출됨. <br>
	 * @param jsonStr
	 */
	public void initBoardList(boolean isPulltoRefresh, String jsonStr)
	{
		if (jsonStr == null || "".equals(jsonStr))
		{
			Toast.makeText(this.context, "게시판 목록을 불러오지 못했습니다.", Toast.LENGTH_SHORT).show();
			return;
		}

//		Log.d(TAG, "jsonStr : " + jsonStr);

		try {
			JSONObject jObject = new JSONObject(jsonStr);

			pageIndex = 0;
			bandiBoardListData.clear();
			listLayout.removeAllViews();

			JSONArray jArray = jObject.getJSONArray("bandiPosts");
			for(int i = 0; i < jArray.length(); i++)
			{
				BandiBoardData data = new BandiBoardData();
				data.setWriter(jArray.getJSONObject(i).getString("writer"));
				data.setRegDate(jArray.getJSONObject(i).getString("regdate"));
				
				String contents = jArray.getJSONObject(i).getString("contents");
				Log.d(TAG, "contents : " + contents);
				data.setContents(TextUtil.HTMLDecode(contents));
				
				data.setAdminYN(jArray.getJSONObject(i).getString("adminYn"));
				data.setSEQ(jArray.getJSONObject(i).getLong("seq"));
				data.setSNSUserName(jArray.getJSONObject(i).getString("snsUserNm"));
				data.setSNSUseYN(jArray.getJSONObject(i).getString("snsUseYn"));

				bandiBoardListData.add(data);
			}

			View[] view = new View[jArray.length()];
			LinearLayout[] main = new LinearLayout[jArray.length()];
			TextView[] name = new TextView[jArray.length()];
			TextView[] date = new TextView[jArray.length()];
			TextView[] contents = new TextView[jArray.length()];

			for(int i = 0; i < jArray.length(); i++)
			{

				if(bandiBoardListData.get(i).getAdminYN().equals("Y"))
				{
					view[i] = inflater.inflate(R.layout.bandi_board_manager_list_item, null);
					contents[i] = (TextView)view[i].findViewById(R.id.textView_managerMsg);
//					String leftReplace = bandiBoardListData.get(i).getContents().replace("&lt;", "<");
//					String rightReplce = leftReplace.replace("&gt;", ">");
//					contents[i].setText(rightReplce);
					contents[i].setText(bandiBoardListData.get(i).getContents());
					
				}
				else
				{
					view[i] = inflater.inflate(R.layout.bandiboard_list_item, null);

					main[i] = (LinearLayout)view[i].findViewById(R.id.layout_bandiListMain);
					main[i].setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));
					if(i % 2 == 0)
					{
						main[i].setBackgroundColor(Color.parseColor("#fcf8ee"));
					}
					else
					{
						main[i].setBackgroundColor(Color.parseColor("#ffffff"));
					}

					name[i] = (TextView)view[i].findViewById(R.id.textView_listName);
					name[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
					if(bandiBoardListData.get(i).getSNSUseYn().equals("N"))
					{
						name[i].setText(doNameMasking(bandiBoardListData.get(i).getWriter())); // 작성자명 마스킹 처리
					}
					else if(bandiBoardListData.get(i).getSNSUseYn().equals("Y"))
					{
						name[i].setText(doNameMasking(bandiBoardListData.get(i).getSNSUserName())); // 작성자명 마스킹 처리
					}

					lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					lParams.setMargins(getWidthDP(5), 0, 0, 0);
					date[i] = (TextView)view[i].findViewById(R.id.textView_listDate);
					date[i].setLayoutParams(lParams);
					date[i].setText(bandiBoardListData.get(i).getRegDate());
					date[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));

					lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					lParams.setMargins(0, getWidthDP(5), 0, 0);
					contents[i] = (TextView)view[i].findViewById(R.id.textView_listContents);
					contents[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
					if(bandiBoardListData.get(i).getContents().equals("null") || bandiBoardListData.get(i).getContents() == null)
					{
						contents[i].setText("");
					}
					else
					{
//						String leftReplace = bandiBoardListData.get(i).getContents().replace("&lt;", "<");
//						String rightReplce = leftReplace.replace("&gt;", ">");
//						contents[i].setText(rightReplce);
						contents[i].setText(bandiBoardListData.get(i).getContents());
					}

				}

				listLayout.addView(view[i]);
			}

			if(jArray.length() == 10)
			{
				isLastPage = false;
//				listLayout.addView(footer);
			}
			else
			{
				isLastPage = true;
			}

			if(isPulltoRefresh)
			{
				isLoading = false;
				p.setVisibility(View.INVISIBLE);
				scroll.loadingFinish();
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 반디 게시물 초기 로딩 (thread 사용)
	 *
	 * @param isPulltoRefresh
	 * @param id
	 */
	public void initListSetting(final boolean isPulltoRefresh, String id)
	{
		blThread = new BoardListThread(isPulltoRefresh, id);
		blThread.start();
	}

	/**
	 * 초기 반디 게시물을 불러 오는 기능 <br>
	 * initListSetting 에서 initListSetting2 로 <b>이름 변경</b> <br>
	 * initListSetting 새로 작성 <br>
	 * @param isPulltoRefresh
	 * @param id
	 * @deprecated
	 */
	public void initListSetting2(final boolean isPulltoRefresh, String id)
	{
		String url = "http://home.ebs.co.kr/bandi/bandiBoardList";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("fileType", "json");
		params.put("programId", id);
		params.put("listType", "L");
		params.put("pageSize", "10");
		AQuery aq = new AQuery(context);

		aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
		{

			@Override
			public void callback(String url, JSONObject object, AjaxStatus status)
			{
				pageIndex = 0;
				bandiBoardListData.clear();
				listLayout.removeAllViews();

				JSONArray jArray;
				try
				{
					jArray = object.getJSONArray("bandiPosts");
					for(int i = 0; i < jArray.length(); i++)
					{
						BandiBoardData data = new BandiBoardData();
						data.setWriter(jArray.getJSONObject(i).getString("writer"));
						data.setRegDate(jArray.getJSONObject(i).getString("regdate"));
						data.setContents(jArray.getJSONObject(i).getString("contents"));
						data.setAdminYN(jArray.getJSONObject(i).getString("adminYn"));
						data.setSEQ(jArray.getJSONObject(i).getLong("seq"));
						data.setSNSUserName(jArray.getJSONObject(i).getString("snsUserNm"));
						data.setSNSUseYN(jArray.getJSONObject(i).getString("snsUseYn"));
						bandiBoardListData.add(data);
					}


					View[] view = new View[jArray.length()];
					LinearLayout[] main = new LinearLayout[jArray.length()];
					TextView[] name = new TextView[jArray.length()];
					TextView[] date = new TextView[jArray.length()];
					TextView[] contents = new TextView[jArray.length()];
					for(int i = 0; i < jArray.length(); i++)
					{

						if(bandiBoardListData.get(i).getAdminYN().equals("Y"))
						{
							view[i] = inflater.inflate(R.layout.bandi_board_manager_list_item, null);
							contents[i] = (TextView)view[i].findViewById(R.id.textView_managerMsg);
//							String leftReplace = bandiBoardListData.get(i).getContents().replace("&lt;", "<");
//							String rightReplce = leftReplace.replace("&gt;", ">");
//							contents[i].setText(rightReplce);
							contents[i].setText(bandiBoardListData.get(i).getContents());
						}
						else
						{
							view[i] = inflater.inflate(R.layout.bandiboard_list_item, null);

							main[i] = (LinearLayout)view[i].findViewById(R.id.layout_bandiListMain);
							main[i].setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));
							if(i % 2 == 0)
							{
								main[i].setBackgroundColor(Color.parseColor("#fcf8ee"));
							}
							else
							{
								main[i].setBackgroundColor(Color.parseColor("#ffffff"));
							}

							name[i] = (TextView)view[i].findViewById(R.id.textView_listName);
							name[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
							if(bandiBoardListData.get(i).getSNSUseYn().equals("N"))
							{
								name[i].setText(bandiBoardListData.get(i).getWriter());
							}
							else if(bandiBoardListData.get(i).getSNSUseYn().equals("Y"))
							{
								name[i].setText(bandiBoardListData.get(i).getSNSUserName());
							}

							lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
							lParams.setMargins(getWidthDP(5), 0, 0, 0);
							date[i] = (TextView)view[i].findViewById(R.id.textView_listDate);
							date[i].setLayoutParams(lParams);
							date[i].setText(bandiBoardListData.get(i).getRegDate());
							date[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));

							lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
							lParams.setMargins(0, getWidthDP(5), 0, 0);
							contents[i] = (TextView)view[i].findViewById(R.id.textView_listContents);
							contents[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
							if(bandiBoardListData.get(i).getContents().equals("null") || bandiBoardListData.get(i).getContents() == null)
							{
								contents[i].setText("");
							}
							else
							{
//								String leftReplace = bandiBoardListData.get(i).getContents().replace("&lt;", "<");
//								String rightReplce = leftReplace.replace("&gt;", ">");
//								contents[i].setText(rightReplce);
								contents[i].setText(bandiBoardListData.get(i).getContents());
							}

						}

						listLayout.addView(view[i]);
					}


					if(jArray.length() == 10)
					{
						isLastPage = false;
//						listLayout.addView(footer);
					}
					else
					{
						isLastPage = true;
					}

					if(isPulltoRefresh)
					{
						isLoading = false;
						p.setVisibility(View.INVISIBLE);
						scroll.loadingFinish();
					}

				} catch (JSONException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				super.callback(url, object, status);
			}

		});
	}

	/**
	 * BoardHandler 에서 호출됨 <br>
	 * 반디 게시물을 추가로 불러와 노출할 때 사용됨
	 */
	public void addBoardList(String jsonStr)
	{
		pageIndex++;
		JSONObject jObject;

		try {
			jObject = new JSONObject(jsonStr);
			JSONArray jArray = jObject.getJSONArray("bandiPosts");

			for(int i = 0; i < jArray.length(); i++)
			{
				BandiBoardData data = new BandiBoardData();
				data.setWriter(jArray.getJSONObject(i).getString("writer"));
				data.setRegDate(jArray.getJSONObject(i).getString("regdate"));
				
				String contents = jArray.getJSONObject(i).getString("contents");
				Log.d(TAG, "contents : " + contents);
				data.setContents(TextUtil.HTMLDecode(contents));
				
				data.setAdminYN(jArray.getJSONObject(i).getString("adminYn"));
				data.setSEQ(jArray.getJSONObject(i).getLong("seq"));
				bandiBoardListData.add(data);
			}

			View[] view = new View[jArray.length()];
			LinearLayout[] main = new LinearLayout[jArray.length()];
			TextView[] name = new TextView[jArray.length()];
			TextView[] date = new TextView[jArray.length()];
			TextView[] contents = new TextView[jArray.length()];

			for(int i = 0; i < jArray.length(); i++)
			{

				if(bandiBoardListData.get(i + (pageIndex * 10)).getAdminYN().equals("Y"))
				{
					view[i] = inflater.inflate(R.layout.bandi_board_manager_list_item, null);
					contents[i] = (TextView)view[i].findViewById(R.id.textView_managerMsg);
//					String leftReplace = bandiBoardListData.get(i + (pageIndex * 10)).getContents().replace("&lt;", "<");
//					String rightReplce = leftReplace.replace("&gt;", ">");
//					contents[i].setText(rightReplce);
					contents[i].setText(bandiBoardListData.get(i + (pageIndex * 10)).getContents());
				}
				else
				{

					view[i] = inflater.inflate(R.layout.bandiboard_list_item, null);

					main[i] = (LinearLayout)view[i].findViewById(R.id.layout_bandiListMain);
					main[i].setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));
					if(i % 2 == 0)
					{
						main[i].setBackgroundColor(Color.parseColor("#fcf8ee"));
					}
					else
					{
						main[i].setBackgroundColor(Color.parseColor("#ffffff"));
					}

					name[i] = (TextView)view[i].findViewById(R.id.textView_listName);
					name[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
					name[i].setText(doNameMasking(bandiBoardListData.get(i + (pageIndex * 10)).getWriter()));

					lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					lParams.setMargins(getWidthDP(5), 0, 0, 0);
					date[i] = (TextView)view[i].findViewById(R.id.textView_listDate);
					date[i].setLayoutParams(lParams);
					date[i].setText(bandiBoardListData.get(i + (pageIndex * 10)).getRegDate());
					date[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));

					lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					lParams.setMargins(0, getWidthDP(5), 0, 0);
					contents[i] = (TextView)view[i].findViewById(R.id.textView_listContents);
					contents[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
					if(bandiBoardListData.get(i + (pageIndex * 10)).getContents().equals("null") || bandiBoardListData.get(i + (pageIndex * 10)).getContents() == null)
					{
						contents[i].setText("");
					}
					else
					{
//						String leftReplace = bandiBoardListData.get(i + (pageIndex * 10)).getContents().replace("&lt;", "<");
//						String rightReplce = leftReplace.replace("&gt;", ">");
//						contents[i].setText(rightReplce);
						contents[i].setText(bandiBoardListData.get(i + (pageIndex * 10)).getContents());
					}


				}
				listLayout.addView(view[i]);
			}

			if(jArray.length() == 10)
			{
//				listLayout.addView(footer);
				isLastPage = false;
			}
			else
			{
//				listLayout.removeView(footer);
				isLastPage = true;
			}
			listLayout.removeView(footer);



		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		isLoading = false;
	}

	public void listAdd()
	{
		ArrayList<BandiBoardData> copy = new ArrayList<BandiBoardData>(bandiBoardListData);
		Collections.sort(copy, new NoAscCompare());
		long seq = copy.get(0).getSEQ();
		String programId = CProgramData.getInstance().getProgramID();

		this.blaThread = new BoardListAddThread(programId, seq);
		this.blaThread.start();
	}

	/**
	 * 원래 이름은 listAdd 였으나, listAdd2 로 변경 <br>
	 * listAdd 는 재작성됨. <br>
	 * 네트워크작업은 BoardListAddThread 로  <br>
	 * 데이타 처리 작업은 addBoardList 로 대체
	 *
	 * @deprecated
	 */
	public void listAdd2()
	{

		//http://s-home.ebs.co.kr/bandi/bandiBoardList?fileType=json&programId=10009938&listType=N&pageSize=5&seq=86
		String url = "http://home.ebs.co.kr/bandi/bandiBoardList";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("fileType", "json");
		params.put("programId", CProgramData.getInstance().getProgramID());
		params.put("listType", "N");
		params.put("pageSize", "10");

		ArrayList<BandiBoardData> copy = new ArrayList<BandiBoardData>(bandiBoardListData);
		Collections.sort(copy, new NoAscCompare());
		params.put("seq", copy.get(0).getSEQ());
		AQuery aq = new AQuery(context);

		aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
		{

			@Override
			public void callback(String url, JSONObject object, AjaxStatus status)
			{

				pageIndex++;
				JSONArray jArray;
				try
				{
					jArray = object.getJSONArray("bandiPosts");
					for(int i = 0; i < jArray.length(); i++)
					{
						BandiBoardData data = new BandiBoardData();
						data.setWriter(jArray.getJSONObject(i).getString("writer"));
						data.setRegDate(jArray.getJSONObject(i).getString("regdate"));
						data.setContents(jArray.getJSONObject(i).getString("contents"));
						data.setAdminYN(jArray.getJSONObject(i).getString("adminYn"));
						data.setSEQ(jArray.getJSONObject(i).getLong("seq"));
						bandiBoardListData.add(data);
					}

//					listLayout.removeView(footer);

					View[] view = new View[jArray.length()];
					LinearLayout[] main = new LinearLayout[jArray.length()];
					TextView[] name = new TextView[jArray.length()];
					TextView[] date = new TextView[jArray.length()];
					TextView[] contents = new TextView[jArray.length()];
					for(int i = 0; i < jArray.length(); i++)
					{

						if(bandiBoardListData.get(i + (pageIndex * 10)).getAdminYN().equals("Y"))
						{
							view[i] = inflater.inflate(R.layout.bandi_board_manager_list_item, null);
							contents[i] = (TextView)view[i].findViewById(R.id.textView_managerMsg);
//							String leftReplace = bandiBoardListData.get(i + (pageIndex * 10)).getContents().replace("&lt;", "<");
//							String rightReplce = leftReplace.replace("&gt;", ">");
//							contents[i].setText(rightReplce);
							contents[i].setText(bandiBoardListData.get(i + (pageIndex * 10)).getContents());
						}
						else
						{

							view[i] = inflater.inflate(R.layout.bandiboard_list_item, null);

							main[i] = (LinearLayout)view[i].findViewById(R.id.layout_bandiListMain);
							main[i].setPadding(getWidthDP(10), getWidthDP(10), getWidthDP(10), getWidthDP(10));
							if(i % 2 == 0)
							{
								main[i].setBackgroundColor(Color.parseColor("#fcf8ee"));
							}
							else
							{
								main[i].setBackgroundColor(Color.parseColor("#ffffff"));
							}

							name[i] = (TextView)view[i].findViewById(R.id.textView_listName);
							name[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));
							name[i].setText(bandiBoardListData.get(i + (pageIndex * 10)).getWriter());

							lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
							lParams.setMargins(getWidthDP(5), 0, 0, 0);
							date[i] = (TextView)view[i].findViewById(R.id.textView_listDate);
							date[i].setLayoutParams(lParams);
							date[i].setText(bandiBoardListData.get(i + (pageIndex * 10)).getRegDate());
							date[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(14));

							lParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
							lParams.setMargins(0, getWidthDP(5), 0, 0);
							contents[i] = (TextView)view[i].findViewById(R.id.textView_listContents);
							contents[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, getCalSP(16));
							if(bandiBoardListData.get(i + (pageIndex * 10)).getContents().equals("null") || bandiBoardListData.get(i + (pageIndex * 10)).getContents() == null)
							{
								contents[i].setText("");
							}
							else
							{
//								String leftReplace = bandiBoardListData.get(i + (pageIndex * 10)).getContents().replace("&lt;", "<");
//								String rightReplce = leftReplace.replace("&gt;", ">");
//								contents[i].setText(rightReplce);
								contents[i].setText(bandiBoardListData.get(i + (pageIndex * 10)).getContents());
							}


						}
						listLayout.addView(view[i]);
					}

					if(jArray.length() == 10)
					{
//						listLayout.addView(footer);
						isLastPage = false;
					}
					else
					{
//						listLayout.removeView(footer);
						isLastPage = true;
					}
					listLayout.removeView(footer);
				}
				catch (JSONException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				isLoading = false;
				super.callback(url, object, status);
			}

		});
	}

	class NoAscCompare implements Comparator<BandiBoardData> {

		/**
		 * 오름차순(ASC)
		 */
		@Override
		public int compare(BandiBoardData arg0, BandiBoardData arg1) {
			// TODO Auto-generated method stub
			return arg0.getSEQ() < arg1.getSEQ() ? -1 : arg0.getSEQ() > arg1.getSEQ() ? 1:0;
		}

	}

	public int getWidthDP(int dpValue)
	{

		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpWidth / 360, context.getResources().getDisplayMetrics());
	}

	public int getHeightDP(int dpValue)
	{
		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue * dpHeight / 640, context.getResources().getDisplayMetrics());
	}

	public int getCalSP(int spValue)
	{
		return spValue * dpHeight / 640;

//		return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, spValue * dpHeight / 640, getResources().getDisplayMetrics());
	}

	/**
	 * 마스킹 규칙에 따라 이름 마스킹.(*) 처리. <br/>
	 * 한글명 : 첫글자, 마지막 글자 노출 나머지는 * 처리, 빈칸은 그대로 노출 <br/>
	 * 영문명 : 첫 3글자 노출, 빈칸은 그대로 노출 <br/>
	 * @param name
	 * @return 마스킹 처리된 이름
	 */
	public static String doNameMasking(String name) {
		StringBuilder maskedName = new StringBuilder();
		boolean containsHangul = false;
		for (int i=0; i<name.length(); i++) {
			char c = name.charAt(i);
			if (Character.getType(c) == Character.OTHER_LETTER) {
				containsHangul = true;
				break;
			}
		}

		if (containsHangul) {
			//Log.d(TAG, "name length (ko) : " + name.length() + " / " + name.substring(0,1)+ " / " + name.substring(1,2));

			for (int i=0; i<name.length(); i++) {
				if (i==0 || i==name.length()-1 || " ".equals(name.substring(i, i+1))) {
					maskedName.append(name.substring(i, i+1));
				} else {
					maskedName.append("*");
				}
			}
		} else {
			for (int i=0; i<name.length(); i++) {
				if (i < 3 || " ".equals(name.substring(i, i+1))) {
					maskedName.append(name.substring(i, i+1));
				} else {
					maskedName.append("*");
				}
			}
		}

		return maskedName.toString();
	}
}
